## Work Wole Overview

The work role overview page provides a top-level view of the progression **between work roles**. It is the first page that appears upon entering the roadmap section of the 90cos training and evaluation website.


### The Roadmap is Interactive!

- Hover over a work role for more information
- If a work role has a defined [roadmap](./roadmap.md), clicking on a work role will take you its roadmap.

### Data Definitions

The data for the work role overview nodes are provided in the [work-role json files](../files/work-roles.md). Data is provided as an array of nodes and is compiled using the roadmap script (./mttl/scripts/build/roadmap_data.py). To define a new node, you will use this data structure:

```json
"roadmap_nodes": [
        {
            "id": "90COS",
            "group": "parent", //optional
            "subgroup": "subgroup", //optional
            "upgrades": ["CCD", "JSRE", "Sysadmin", "CST", "PO", "SM"]
        }
    ]
```

`id`: The unique id of the node   
`group`: The group that the node will be placed in. This allows us to group like nodes.   
`subgroup`: A subgroup for the node - nodes placed in the same subgroup should be in the same group. Nodes in a subgroup will always appear adjacent to each other.   
`upgrades`: an array of unique ids for **upgrades** for this work-role. The nodes specified must not have a higher y-coordinate than its parent.    
