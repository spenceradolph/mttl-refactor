# How to validate data
## JSON
There are times when requirements change; when this happens, there needs to be a way to ensure entered data conforms to the necessary structure and format. This explains the process used for validation of new or modified requirements; i.e. the Knowledge, Skills, Abilities and Tasks (KSATs) as well as Training Task Lists (TTLs). An explanation of these files can be found [here - for eval rellinks](../files/eval_rellinks.md), [here - for training rellinks](../files/training_rellinks.md), [here - for KSATs](../files/requirements-jsons.md), and [here - for work roles](../files/work-roles.md).    


### Validation Tools

*  One tool used to validate Java Script Object Notation (JSON) files is  ***JSON Schema***; for detailed information on this technology see the [JSON Schema](https://json-schema.org/understanding-json-schema/index.html) website.
*  Other tools include CYT developed Python scripts.

### JSON Schema

The JSON Schema is a definition of how a JSON file should be structured. It defines any required keys, the types of data stored in a particular key, and other value checks. The information below explains each implemented schema.

#### Definitions

*  `$ref`: This is a link to a predefined definition.
*  `additionalProperties`: This determines whether or not additional properties, other than those specified, are allowed. If the value is true then properties not defined will not be allowed.
*  `definitions`: This allows the definition of certain objects expected to appear more than once and is a way to practice code reuse. Each item in this key => value type object defines a particular object that can be referenced anywhere in the schema, including other definitions.
*  `items`: This defines the type of objects allowed in an array object.
*  `maxItems`: This is the maximum length allowed in an array object.
*  `minItems`: This is the minimum length allowed in an array object.
*  `maxLength`: This is the maximum length allowed in a string object.
*  `minLength`: This is the minimum length allowed in a string object.
*  `minProperties`: This defines the minimum amount of properties allowed in the object; useful for `patternProperties`.
*  `pattern`: A regular expression defining the allowed content of string objects.
*  `patternProperties`: Like `properties`, this defines the allowed properties but uses regular expression values to define key names.
*  `properties`: This defines the key => value pairs allowed in a particular object using specific key names.
*  `required`: This determines the minimum allowed property keys, other keys are ignored but those specified in this list must be present.
*  `title`: Simply a designation for the schema.
*  `type`: This defines the type of object being declared; some options are object, array, string, integer, number, etc.

#### KSAT Schema Template

This schema defines the structure and content of a requirement JSON file; i.e. KNOWLEDGE.json, SKILLS.json, ABILITIES.json and TASKS.json. 

- Found in [mttl/scripts/json_templates.py](https://gitlab.com/90cos/mttl/-/blob/master/mttl/scripts/json_templates.py) in the `ksat_item_template` function.

The first thing you notice in the schema above is the amount of extra stuff; don't worry, it'll be explained soon. The extra data shown is necessary for the comparison process during validation. The information below explains the extra data. One minor difference in this schema and the one shown in the link above is the one additional item in the list of the `training` and `eval` properties, which is added under the hood.

The `definitions` define one object that can be reused.
*  The `trn_evl` key defines an object that is an array of arrays and can be referred to using its name.
   *  The inner array is defined such that it must have 5 elements, whose data type is a string, and each string must have at least one character.
   *  The outer array definition states that each element must be another array and zero elements are allowed.

Everything after `definitions` define the actual structure of the requirement JSON file.
*  Each property in this structure has another object as its value and each key name has to match the defined regular expression; no other property names are allowed.
*  The inner object must have the keys defined in `required` and other key names are ignored. Adding `additionalProperties: False`, after `required`, would cause other key names to be invalid.
*  The following properties must have a string as the value: `description`, `topic`, `requirement_owner` and `comments`.
*  The following properties must have a list of strings or an empty list as the value: `parent`, `children` and `requirement_src`.
*  The following properties must have a list of 5 element lists of strings or an empty list: `training` and `eval`.

#### Training Rel-Link Schema Template

- Found in [mttl/scripts/json_templates.py](https://gitlab.com/90cos/mttl/-/blob/master/mttl/scripts/json_templates.py) in the `trn_rel_link_item_template` function.

#### Eval Rel-Link Schema Template

- Found in [mttl/scripts/json_templates.py](https://gitlab.com/90cos/mttl/-/blob/master/mttl/scripts/json_templates.py) in the `evl_rel_link_item_template` function.

# How to test scripts
## Python
***It must be noted that the Python scripts run the JSON Schema validation***. *However*, more specific value validation is also handled by these scripts. This allows checks against the most current data available in the MTTL.
