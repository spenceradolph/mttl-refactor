# Database Schema

### Entity Relationships
```mermaid
erDiagram
    requirements }|--o{ work-Roles: ksat-id
    requirements }o--o{ relLinks : ksat-id
```

### Class Diagram
```mermaid
classDiagram
      requirements <|-- work-Roles
      requirements <|-- evalRelLinks
      requirements <|-- trainingRelLinks
      requirements : +Object _id<Primary Key>
      requirements : +String ksat_id<Primary Key>
      requirements : +Array<String> parent
      requirements : +String description
      requirements : +String topic
      requirements : +Array<String> requirement_src
      requirements : +String requirement_owner
      requirements : +String comments
      requirements : +String updated_on
      requirements : +String created_on
      class work-Roles {
          +String work-role
          +String ksat_id<Primary Key>
          +String proficiency
      }
      class evalRelLinks {
          +Object _id<Primary Key>
          +String topic
          +Array<Object> KSATs
          +String test_id
          +String test_type
          +String question_id
          +String network
          +String question_name
          +String question_proficiency
          +String estimated_time_to_complete
          +String created_on
          +String updated_on
          +Array<String> work-roles
          +String map_for
      }
      class trainingRelLinks {
          +Object _id<Primary Key>
          +Array<Object> KSATs
          +String course
          +String module
          +String subject
          +String topic
          +String module_id
          +Array<String> references
          +int lecture_time
          +int perf_demo_time
          +Array<Object> lesson_objectives
          +Array<Object> performance_objectives
          +String network
          +String created_on
          +String updated_on
          +Array<String> work-roles
          +String map_for
      }
```
