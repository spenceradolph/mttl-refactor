# How to find, insert, modify, delete KSAT or Rel-link information
These tools are part of the ```mttl.py``` script.

#### Typical Usage
It will be easier to find and update the JSON files directly only if you want to change single attributes in the rel-link/ksat JSON objects. Fields that you should not edit manually are the following:
1. _id
2. updated_on
3. created_on
4. reviewed_on

**If you are adding or removing KSATs/rel-links, adding or removing rel-link mappings you need to use these scripts to do so.**

### Script Flow

```mermaid
sequenceDiagram
    participant U as User
    participant S as MTTL Script
    participant M as MongoDB
    participant R as GitLab

    U->>S: Run script.
    Note over U,S: ./mttl.py
    rect rgb(204, 255, 255)
       S-->>M: Import Repo JSON into Collections.
       Note over S,M: mongoimport
       S->>M: Modify Mongo Documents.
       S-->>M: Export Collections into Repo JSON.
       Note over S,M: mongoexport
    end
    U->>R: Add, Commit, and Push Changes.
```

## Find
The find tool is used to find KSATs or rel-link documents saved in the MTTL datasets. This tool will return the documents found in stdout.
### Examples
```sh
// Find KSATs
./mttl.py find ksat '{"ksat_id": "K0001"}'
```
For this example, think 'find all ksat documents WHERE _id == K0001'
```sh
// Find Rel-Links
./mttl.py find rel-link '{"$and":[{"course":"IDF"}, {"module": "cpp_overload"}]}'
```
For this example, think 'find all rel-link documents WHERE course == IDF AND module == cpp_overload'
```sh
// Find ksat and only return _id and description fields
./mttl.py find ksat '{"ksat_id": "K0001"}' -p _id description
```
### Help Menu
```sh
usage: find.py [-h] [-p PROJECTION [PROJECTION ...]] {ksat,rel-link} find_query

find script to print MTTL data to stdout

positional arguments:
  {ksat,rel-link}       select the type of item you want to find
  find_query            mongodb query to find document to output '{...}' (wrap KEYs and VALs in double quotes)

optional arguments:
  -h, --help            show this help message and exit
  -p PROJECTION [PROJECTION ...], --projection PROJECTION [PROJECTION ...]
                        only return these fields to stdout
```
The find script requires two arguments, the ksat or rel-link flags and the find query itself. The ksat or rel-link flags are used to indicate what type of document you are attempting to find. The find query is a string dictionary indicative of the [mongodb find](https://docs.mongodb.com/manual/reference/method/db.collection.find/) query parameter. 

## Insert
The insert tool is used to insert new ksats and rel-links into the MTTL datasets.
### Examples
```sh
// Using default ```batch.json```
./mttl.py insert ksat
  or
./mttl.py insert rel-link
```
```sh
// Using user defined file path
./mttl.py insert ksat -f path/to/my/file
```
### Help Menu
```sh
usage: insert.py [-h] [-f FILE] {ksat,rel-link}

insert script to add ksat/rel-link data to datasets

positional arguments:
  {ksat,rel-link}       select what type of items you are inserting

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  use import file to add ksat/rel-links in batch (default=batch.json). If rel-links have mappings to 
                        nonexistent KSATs the rel-link will not be inserted and will be added to the 'rel_links_w_error.json' file
```
The insert tool requires a single argument, ksat or rel-link, which is used to specify what type of document you want to insert. The tool will use the import file to iteratively import new data into the MTTL datasets. The ```-f``` flag can be used to specify your own file path. Note: the ```type``` field is very important becuase it is used to autogenerate the _id for each JSON in the import file.
### Import file syntax
The import file will need to be a list of json objects. The objects will be validated from the [KSAT Schema Template](#ksat-schema-template), [Eval Rel-Link Schema Template](#eval-rel-link-schema-template), [Training Rel-Link Schema Template](#training-rel-link-schema-template). Pay attention to required attributes.
Note: KSAT objects will no longer need a specific _id field (this will be auto generated). They will need the 'ksat_type' field, that will be 'knowledge', 'skills', 'abilities', or 'tasks'.

#### Import KSAT file example
```json
[
   {
	"ksat_type": "skills",
	"description": "Skill in conducting vulnerability scans and recognizing vulnerabilities in security systems.",
	"parent": [],
	"topic": "",
	"requirement_src": [
		"NCWF"
	],
	"requirement_src_id": "S0001",
	"requirement_owner": "USCYBERCOM/J7",
	"comments": "SP-DEV-001",
        "references": []
   },
   ...
]
```

#### Import Rel-Link file example
```json
[
    {
        "KSATs": [
            {
                "ksat_id": {
		    "$oid": "5f457f1e1ea90ba9adb32af6"
		},
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/public/training/material/see-basics/-/tree/master/ojt.rel-link.json"
            },
            {
                "ksat_id": {
		    "$oid": "5f457f1e1ea90ba9adb32a39"
		},
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/public/training/material/see-basics/-/tree/master/ojt.rel-link.json"
            }
        ],
        "test_id": "SEE",
        "test_type": "knowledge",
        "question_id": "SEE_OJT",
        "question_name": "ojt",
        "question_proficiency": "3",
        "network": "NIPR",
        "topic": "",
        "language": "",
        "complexity": 0,
        "estimated_time_to_complete": 0,
        "work-roles": [
            "Basic-SEE"
        ],,
        "references": []
        "map_for": "eval"
    },
    ...
]
```

## Modify
The modify tool is used to change any ksat or rel-link document that's currently in the MTTL datasets
### Examples
```sh
// Modify KSAT string attribute
./mttl.py modify ksat --update-query '{"_id": "K0001"}' '{"$set":{"description": "My New Description"}}'
```
Think of this like 'WHERE _id == K0001 update the description to "My New Description"'

```sh
// Modify Rel-Link with new mapping
./mttl.py modify rel-link --insert-mapping 5ee9250a761cf87371cf310c K0001 B https://my.mdbook.com
```
For this example think 'WHERE _id == 5ee9250a761cf87371cf310c add mapping K0001 at proficiency B with the url https://my.mdbook.com

```sh
// Modify KSAT by changing the _id (the old _id must exist)
./mttl.py modify ksat --modify-ksat-id K0001:skills K0043:skills
```
The modify-ksat-id flag will change the ksat_id of the KSAT to a new ksat_id that's found automatically.

```sh
// Modify Rel-Link by delete an existing mapping
./mttl.py rel-link --delete-mapping '{"course": "IDF", "module": "cpp_oop"}' K0001 K0043
```
For this example think 'WHERE course == IDF AND module == cpp_oop delete mapping to K0001 and K0043. Note that this will also remove the _id link in the K0001 and K0043 ksat documents, as well.

### Help Menu
```sh
usage: modify.py [-h]
                 (-u FIND_QUERY UPDATE_QUERY | -k OLD_ID:{knowledge, skill, ability, task} [OLD_ID:{knowledge, skill, ability, task} ...] | -i REL_LINK_ID KSAT_ID PROFICIENCY URL | -d FIND_QUERY [KSAT_ID ...])
                 {ksat,rel-link}

modify script to update ksat/rel-link objects in datasets

positional arguments:
  {ksat,rel-link}       select the type of item you want to modify

optional arguments:
  -h, --help            show this help message and exit
  -u FIND_QUERY UPDATE_QUERY, --update-query FIND_QUERY UPDATE_QUERY
                        mongodb query to update specified documents found by find-query '{...}' (wrap KEYs and VALs in double quotes)
  -k OLD_ID:{knowledge, skill, ability, task} [OLD_ID:{knowledge, skill, ability, task} ...], --modify-ksat-id OLD_ID:{knowledge, skill, ability, task} [OLD_ID:{knowledge, skill, ability, task} ...]
                        list (space delimited) of KEY:VAL pairs of OLD_ID:{knowledge, skills, abilities, tasks} to update mappings of documents found by find-query
  -i REL_LINK_ID KSAT_ID PROFICIENCY URL, --insert-mapping REL_LINK_ID KSAT_ID PROFICIENCY URL
                        insert a new mapping into a rel-link. takes four values; the REL_LINK_ID, the ksat_id (5 chars), item_proficiency, and the url
  -d FIND_QUERY [KSAT_ID ...], --delete-mapping FIND_QUERY [KSAT_ID ...]
                        deletes a mapping from rel-links. takes two or more values; the FIND_QUERY string '{...}' to find the rel-links to update, and the ksat_id of the
                        KSAT (5 chars) to delete
```
The modify tool will update all documents found with the --find-query flag so make sure to use the find tool to verify you will update the correct documents. Using the _id of the rel-link in the find will guarantee you update only one rel-link document.

## Delete
The delete tool is used to remove ksat and rel-link documents from the MTTL dataset
### Examples
```sh
// Remove KSAT
./mttl.py delete ksat 5f457f1e1ea90ba9adb32a9a 5f457f1e1ea90ba9adb32a9d 5f457f1e1ea90ba9adb32aa2
```
For this example, think 'delete ksat documents WHERE _id == 5f457f1e1ea90ba9adb32a9aOR _id == 5f457f1e1ea90ba9adb32a9d OR _id == 5f457f1e1ea90ba9adb32aa2
```sh
// Remove Rel-Link
./mttl.py delete rel-link 5ee59b2f9f0e466920062028 5ee59b2f9f0e466920062029 5ee59b2f9f0e46692006202a
```
For this example, think 'delete rel-links documents WHERE _id == 5ee59b2f9f0e466920062028 OR _id == 5ee59b2f9f0e466920062029 OR _id == 5ee59b2f9f0e46692006202a
Note: The rel-link _id is the 12 byte hexadecimal values at the top of the document.
### Help Menu
```sh
usage: delete.py [-h] {ksat,rel-link} _id [_id ...]

delete script to remove ksat/rel-link objects from datasets

positional arguments:
  {ksat,rel-link}  select what type of item you want to remove
  _id              list of either ksat or rel-link _id's

optional arguments:
  -h, --help       show this help message and exit
```
The delete tool will take in as many _id values that you choose to pass in. These values must be space delimited. 

## Revert changes before commit
If you find you made change you do not want to stage and commit to your branch you can easily do a ```git status```, find the files that your queries changed, and revert these files back with ```git checkout [PATH TO FILE]```.

If you're using VSCode, you can easily navigate to the 'Source Control' tab on the left and revert the individual files that were changed.