## 1. What Elements Call MongoDB

Right now, mongodb is only called during the CI/CD pipeline. 

There are several pipeline jobs that load the jsons in the `mttl/database` directory into MongoDB (https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/blob/master/mttl/scripts/before_script.sh). These jobs do one of three things:

1. Unit tests (see unit-test and validate stages of the CI pipeline [here](https://90cos-mttl.90cos.com/documentation/pipeline/index.html))

2. Data modification (see the pre-build stage, in particular `Fetch Rel-Links & Children`) 

3. Compiling data for use in Angular (See the [Create MTTL Dataset](https://90cos-mttl.90cos.com/documentation/pipeline/data.html)), stored as an artifact as `mttl/src/data/mttl.min.json`. You can see the api module loads the mttl.min.json file from the [source code here](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/blob/master/mttl/src/app/api.service.ts) (line 7, lines 8-46)

## 2. How will we maintain interoperability with CYV

Because the Mongo instances last around 30 seconds and on a gitlab runner, nobody would be able to access these temp databases anyways.
They would still have access to the jsons, as I don't believe that we are phasing them out anytime soon
And if we do we would have an overlap period where we can inform that we are switching to the database/have a fleshed-out CRUD API

## 3. How long until Mongo is not used - or will it ever be completely deprecated?

There are options here - we could replace all of our pipeline scripts and use the postgres server instead of temporary Mongo instances and  

a. Just export the same compiled json (this is not a solution that I think would be great other than something temporary, but even then I would rather do option b)
b. Utilize the existing express js API to pull the data. For example `GET /api/ksats` called to the Express server could return a json dict of ksats like the mttl.min.json, `GET /api/ksat/A0001` might get the data for one ksat in particular. Here is an (ugly) diagram that illustrates the problem that we are trying to solve with the CRUD API:

![Untitled_Diagram](../uploads/Untitled%20Diagram.png)

## CRUD API mapped to SQL operations

Our API calls should follow this

| API Operation | SQL Operation | HTTP Operation |
|---------------|---------------|----------------|
| Create        | Insert        | POST/PUT       |
| Read          | Select        | GET            |
| Update        | Update        | POST/PUT       |
| Delete        | Delete        | DELETE         |

Until we have our security measures sorted out, we will only have Read requests

## Proposed API Endpoints

At this time, I am assuming nothing about the structure of the database, and I am not worrying too much about technicalities. These are just potential functionalities that we/users might need in order to use the MTTL and its API

| API Endpoint      | Description                                                             |
|-------------------|-------------------------------------------------------------------------|
| /api/ksats        | Returns a list of KSATS, maybe just returns mttl.min.json data for now. |
| /api/ksat         | Returns information on an individual KSAT                               |
| /api/workroles    | Returns a list of work roles                                             |
| /api/workrole/CCD | Returns a list of KSATs for the CCD work role                            |
| /api/modules      | Returns a list of modules                                               |

Obviously these are pretty simple queries, but we can have more advanced queries as we think of the need for them. These are only examples needed for basic MTTL functionality
