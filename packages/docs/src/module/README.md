# MTTL 'module' datatype 

The Apprentice program, and, more specifically, the Virtual Training Records (**VTR**) program is being ***modularized*** to improve workflow for apprentices and mentors as well as improving transparency for flight commanders and other supervisors.  

The current/previous work flow for the apprentice program basically involves the flight commander or other supervisor requesting a VTR for a specific work role for an apprentice. A GitLab repository is generated for that apprentice with all the KSAT issues for that particular work role.  

Mentors and apprentices then decide together how the apprentice will complete requirements, whereby each KSAT is an issue ticket. This approach is helpful in that it allows mentors and apprentices absolute autonomy.  
However, this unbridled ***autonomy*** can be burdensome for mentors and apprentices. The shadow warrior mentors are very busy defending democracy. Having to design a learning trajectory, construct a syllabus, and , in essence, piece together detailed lesson plans by reading a myriad KSATs is a bit cumbersome for the mentor. 

Further, looking at large number of un-grouped KSATs can be daunting for the mentor and the apprentice. Moreover, itemized bits of learning do not help the apprentice understand the overarching goals or most significant aspects of their learning trajectory.  In essence, they lose the forest for the trees.  

#  Current Development
A few steps have been and are being undertaken to modularize the training workflow. 
VTRs have been organized into overarching milestones, which conceptually groups topics at the highest level.  

For the training work flow and its concomitant visualization in the Roadmap, training is being organized into pathways (referred to as 'sequences') that are comprised of instructional 'units', that are defined by constituent 'modules' i.e. 

```mermaid
graph TD;
    Sequences-->Units;
    Units-->Modules; 
```



# Current Status
The underlying premise for organizing upgrade requirements and training are the KSATs, described [here](https://90cos-mttl.90cos.com/documentation/mttl/ksat-definitions.html).  

The KSATs come from definitions for Knowledge, Skill, and Ability according to National Initiative for Cybersecurity Education (NICE) Cybersecurity Workforce Framework (NCWF), cf. [here](https://90cos-mttl.90cos.com/documentation/mttl/ksat-definitions.html)

- Knowledge informs performance
- Skill is competence to manipulate tools 
- Ability is competence in observable behaviors or behaviors that result in observable products 
- Tasks are "***a Succession of work activities used to produce a distinguishable and discernable output that can be independently used or consumed***"

Consequently, KSATs can be envisioned as:  
- knowledge informing skills e.g. knowing how to use a power tool,  
- skills that constitute an ability, e.g. power tool use and knowledge of electrical circuits to install a light switch,  
- and abilities that are used to create a task, e.g. plumbing, carpentry, and electrical abilities to create a house.



```mermaid
graph LR;
    K0700-->S0397;
    K0308-->S0397;
    K0060-->S0397;
    K0061-->S0397;
    S0397-->A0612;
    S0397-->A0612;
    A0612-->T0022;
    A0611-->T0022; 
```


The KSAT framework typically describes relationships as parent-child where the parent determines the children.  
With tasks as the top level, there are certain allowed parent child relationships.  

| Item | Allowable Parent(s) |
| ------ | ------ |
| Knowledge | Skill, Ability, Task |
| Skill | Ability, Task | 
| Ability | Task | 
| Task | None | 

or conversely:   

| Item      | Allowable Children        |   
| ------    | ------                    |   
| Task      | Ability, Skill, Knowledge |   
| Ability   | Skill, Knowledge          |   
| Skill     | Knowledge                 |    
| Knowledge | None                      |    
  
    


        

```mermaid
graph TD;
    T-->A;
    T-->S; 
    T-->K;
    A-->S;
    A-->K; 
    S-->K;
```

Considering this structure, one might simply split a work role into its individual tasks.  
However, many of the tasks are not as actionable as one might suspect at first blush.  
For example, of the 44 as of yet identified Tasks, the Senior CCD Linux work role requires 27 i.e. T0001,T0002,T0003,T0004,T0005,T0006,T0007,T0008,T0009,T0010,T0011,T0012,T0013,T0014,T0015,T0016,T0017,T0018,T0019,T0020,T0021,T0022,T0023,T0024,T0025,T0026,T0027,T0028,T0029,T0030,T0031,T0032,T0033,T0034,T0035,T0036,T0037.  
Whereby, overarching tasks might require much contemplation for a mentor to devise an appropriate worked example and determine child KSAs, cf. :  
- T0001 - (U) Modify cyber capabilities to detect and disrupt nation-state cyber threat actors across the threat chain (kill chain) of adversary execution
    - T0001 - has no *correctly* identified children. [It has 32 Task KSATs identified as children]
- T0037 - (U) Document and communicate tradecraft, best practices, TTPs, training, briefings, presentations, papers, studies, lessons learned, etc. to both technical and non-technical audiences
    - T0037 - currently has no identified children

## Purpose 
In order to assist mentors, apprentices, and supervisors, it will be important to split work role (and specialization) training into actionable, understandable components. 
The purpose of these overarching divisions should be intuitive while reinforcing and supporting the use of VTRs for *work role* training.  Consequently, the primary motivation behind each division or ***module*** will be understandable from a work role requirements' perspective and a career / learning trajectory pathway perspective. 


## Benefits 
For the apprentice - the apprentice will be able to be conceptualize what is required of them. It will also support apprentices in understanding how the ***module*** and its expected product(s), skills, abilities, and knowledge fit within the context of performing future work role requirements. In short, it helps the apprentice understand the role that they are training for and better understand the expectations of that work role in concrete, actionable terms.

For the mentor - the ***module*** approach will provide an intuitive i.e. understandable, less frustrating, more manageable and consequently timesaving, approach to assigning objectives to apprentices.

For the supervisor - the ***module*** approach provides a faster understanding of the apprentice's work role progression and flight readiness. 

## Requirements 
Initially, the ***module*** will be an abstract used to facilitate the organization of training requirements for apprentices and concomitantly will be used to assist in visualize those training requirements on the training roadmap. 
The ***module*** will be realized by provide actionable groupings i.e. 'milestones' in the VTR, with an understandable rationale for grouping (e.g. project titles).   


## Template 
         "properties": {
            "uniq_id":  {"type": "string"},
            "module_name":  {"type": "string"},
            "description":  {"type": "string"},
            "required_tasks": {
                "type": "array",
                "items":{"type": "dictionary"} //{ksat_id: string, proficiency: string}
                 "minItems": 1
                }, //does this close or contribute to any tasks?
            "required_skills": {
                "type": "array",
                "items":{"type": "dictionary"} //{ksat_id: string, proficiency: string}
                 "minItems": 1
                }, //does this close or contribute to any tasks?
            "required_abilities": {
                "type": "array",
                "items":{"type": "dictionary"} //{ksat_id: string, proficiency: string}
                 "minItems": 1
                }, //does this close or contribute to any tasks?
             "required_knowledge": {
                "type": "array",
                "items":{"type": "dictionary"} //{ksat_id: string, proficiency: string}
                 "minItems": 1
                }, //does this close or contribute to any tasks?
            "objective": "string" //overarching objective - should be a specific task / project i.e. create a remote access trojan,
            "content": {
                "type": "array",
                "items":{"type": "dictionary"}, // list of training resources, e.g. website, youtube, videos, books, that are used for study - can be specified as required or recommended
                 "minItems": 0
            } 
            "performance": {
                "type": "array",
                "items":{"type": "dictionary"}, // list of labs and projects that are used for the training phase - - can be specified as required or recommended
                 "minItems": 0
            } 
            "assessment": {
                "type": "array",
                "items":{"type": "dictionary"}, // list of labs and projects that are used for the justifying completion of module - - can be specified as required or recommended - at least 1 required assessment lab or project must be specified; projects must have clear acceptance criteria / rubrics 
                 "minItems": 1
            } 
            "comments": {"type": "string", "minLength": 0} // explanation of special requirements 
        }
    }


## Worked Example 

Currently, the SCCD-L evaluation is divided into 6 overarching milestones: 
- Organizational,  
- General (including Linux System Architecture, Build Environments),  
- User Mode (including Processes, Threads, Synchronization, memory Management, Asynchronous IO, Static and Dynamic Libraries, C APIs, Network Programming [BSD sockets and netfilter]),  
- Kernel Mode,   
- Reverse Engineering, 
- Vulnerability Research 
- CNO 

The overarching milestones may then be further divided into more tractable, manageable sequences, modules. 

```mermaid
graph TD;
    Kernel-Driver ==> KD-Module1  
    Kernel-Driver ==> KD-Module2  
    Kernel-Driver ==> KD-Module3  
    Kernel-Driver ==> KD-Module4  
    KD-Module1 --> Skill1  
    KD-Module1 --> Skill2  
    KD-Module1 --> Skill3  
    KD-Module1 --> Skill4  
    Skill1 -.-> Knowledge1  
    Skill1 -.-> Knowledge2  
    Skill1 -.-> Knowledge3  
    Skill1 -.-> Knowledge4  
```
For example, the top level **Kernel-level Driver Module** could be split into 4 more tractable modules. Each of those modules would cover approximately 1/4 of the KSATs required by the top level Kernel-level Driver module. [*1/4 is an estimate* - when the modules are mapped, it may be determined that 2 is sufficient or 6 are required.] Overlap is intentional and necessary. 

As an example the overarching Kernel Driver module might require the four underlying modules:  
- KD-Unit1 - "Hello World" - install dependencies, craft a kernel module that prints "Hello World" from kernel space - using initialization and exit functions 
- KD-Unit2 - "make some noise" - allow user programs to send and receive data from the kernel module [creating a character device]
- KD-Unit3 - "I want to be a lumberjack" - create a keyboard logger using a kernel module 
- KD-Unit4 - "sniff sniff" - log data from a specific source IP 



```mermaid
graph TD;
    KD-Unit1 -- Hello World --> A0543  
    KD-Unit1 -- Hello World --> S0333  
    KD-Unit1 -- Hello World --> S0321  
    KD-Unit1 -- Hello World --> Skill1  
    KD-Unit1 --> Skill2  
    KD-Unit1 --> Skill3  
    KD-Unit1 --> Skill4  
    A0543 -.-> K0501  
    A0543 -.-> K0142 
    A0543 -.-> K0509  
    A0543 -.-> Knowledge4  
```

Given this, a "Hello World" module belonging to the overarching **Kernel-level Driver Unit** will be:
```JSON
  "kernel-module-1":{
    "uniq_id":  "KD-Module1",
    "module_name":  "hello_world",
    "description": "install dependencies, craft a kernel module that prints Hello World from kernel space using initialization and exit functions ",
    "objective": "This is the first step in creating kernel modules. To fulfill the requirements of this module, the kernel module will provide two separate messages upon being removed and inserted ",
    "content": [
        {"ref": "https://www.thegeekstuff.com/2013/07/write-linux-kernel-module/"}
     ],
    "performance": [
        {"perf": "link to lab with unit test"},
        {"perf": "another lab with unit test"},
        {"perf": "yet another lab with unit test"}
    ],
    "assessment": [
         {"eval": "link to lab with unit test"}
    ], 
    "required_tasks": [ 
        {"ksat_id": "T0005", "proficiency": "2"}, 
        {"ksat_id": "T0034", "proficiency": "1"},
        {"ksat_id":"T0008", "proficiency": "2"}
    ], 
    "required_abilities": [
        {"ksat_id": "A0543", "proficiency": "3"},
        {"ksat_id": "A0001", "proficiency": "2"}
    ], 
    "required_skills": [
        {"ksat_id": "S0005", "proficiency": "2"}, 
        {"ksat_id": "S0034", "proficiency": "3"},
        {"ksat_id":"S0008", "proficiency": "1"}
    ],
    "required_knowledge": [
        {"ksat_id": "K0005", "proficiency": "A"}, 
        {"ksat_id": "K0034", "proficiency": "B"},
        {"ksat_id":"K0008", "proficiency": "C"}, 
        {"ksat_id": "K0007", "proficiency": "D"}, 
        {"ksat_id": "K0234", "proficiency": "C"},
        {"ksat_id":"K8080", "proficiency": "B"}
    ], 
    "comments": ""
}
```

