
# Training Roadmap Layout
* The training [roadmap](https://90cos-mttl.90cos.com/roadmap) provides a modified hierarchical tree view of work role progression within the 90th COS.

<img src="../uploads/mttl_roadmap_example.png" alt="Work Role Roadmap" width="360"/>  

The roadmap starts with entry to the 90th COS and progresses to the most senior work roles at bottom (e.g. CCD -> SCCD-> MCCD).  

Hovering over a work role will provide a brief description of that work role.   

* The work roles on the level below **90 COS** do not have another work role as a prerequisite.
* The lines connecting work roles represent the available paths to transition from one work role to another.
* Any work role that does not have lines connecting to the under side does not lead to a more advanced work role.

Each work role will have its own specific road map that can be reached by clicking on that work role. 
For example, clicking on the **Product Owner** work role would lead to the following ***work role roadmap***:  
<img src="../uploads/roadmap_pathway_PO_example.png" alt="Work Role Roadmap for Product Owner" width="360"/>  



# Individualized Roadmaps per Work Role
* Clicking the name of a work role will go to the specific training roadmap for that work role (if one has been created).  
* The work role road map provides a link to request training, links for recommended training pathways (as appropriate), a link with lists of preparation materials, and a link to request an exam.


***Individuals can manage their current work role training expectations through their***  *V*irtual *T*raining *R*ecord.  **Click [here](https://90cos.gitlab.io/cyt/training-systems/vtr-system/) to learn more about VTRs.**