# Work Role KSAT Proficiency Requirements

The following are proficiency codes defined by the JCT&CS. Each work role requirement of a KSAT has a required proficiency defined. 

| Knowledge Levels | Definition |
|-----|--------|
| A | An 'A' indicates the individual must be able to identify basic facts and terms about the subject.|
| B | A 'B' indicates the individual must be able to identify relationships of basic facts and state general principles about the subject.|
| C | A 'C' indicates the individual must be able to analyze facts and principles and draw conclusions about the subject.|
| D | A 'D' indicates the individual must be able to evaluate conditions and make proper decisions about the subject.|

| Skill/Ability Levels | Definition |
|-----|--------|
| 1 | A '1' indicates the individual must be familiar with this competency and be generally capable of independently handling simple tasks or assignments.|
| 2 | A '2' indicates the individual must be capable of independently handling some complex tasks or assignments related to this competency, but may need direction and guidance on others.|
| 3 | A '3' indicates the individual must be capable of independently handling a wide variety of complex and or high profile tasks or assignments related to this competency. Must be an authority in this area and or often sought out by others for advice or to teach/mentor others on highly complex or challenging tasks or assignments related to this competency.|
