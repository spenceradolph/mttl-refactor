# Contributing to the MTTL

### Submitting KSATs

You can submit KSATs to the MTTL by pressing the "Add KSAT" button on the top right corner of the MTTL on the search and filter bar.   

<img src="../uploads/mttl_add_ksat.png" alt="Add KSAT" height="100"/>  
  


**Notice**: in order to add a new KSAT, users must be signed in with GitLab's oauth.
To login, click on the GitLab icon in the top right corner.  

<img src="../uploads/mttl_oauth_login_button.png" alt="Add KSAT" height="100"/>  


    
For a KSAT  to be fully considered, please provide:  
  - "***ksat_type***" i.e. "knowledge, skill, or ability"
  - "***description***" - be specific but not too specific - i.e. do not require a specific tool, command, or language unless that is the requirement
  - "***topic***" - please scan the list of existing topics first 
  - "***requirement_src***" e.g. "NCWF". If you do not know the exact requirement source, explain the basis for your recommendation  
  - "comments/notes" - include the ***work role*** or explain why the work-role/specialization was not included   
    
	  



![add_ksat](../uploads/mttl_add_ksat.png)


  ---  

### Submitting Training and Eval Links
Similarly, eval and training links may be submitted using the [MTTL page](https://90cos-mttl.90cos.com/mttl).  
In order to submit an eval or training link, press the `+` button on the right side of either training or eval to add a link.

![add_link](../uploads/add_link.png)
--- 

### Contributing to the MTTL by editing the JSON Files
Anybody with a 90COS gitlab account can modify or add to the MTTL. Any changes or additions to the MTTL will be reviewed and approved or rejected by a designated Subject Matter Expert (SME) for the associated work role that the changes made apply to.

The MTTL consists of four distinct .json files described in the [MTTL Data Dictionary](../dev/file_overview.md#data-file-directory-structure).   
In depth explanations for individual file types are available for:  
   - [Eval *rel-links.json](../files/eval_rellinks.md)  
   - [Training *rel-links.json](../files/training_rellinks.md)  
   - [Requirements i.e. KSAT jsons](../files/requirements-jsons.md)  
   - [Work roles jsons](../files/work-roles.md)  
   

An example of a single KSAT definition inside this file is shown and described below. Adding new KSATs to these JSON files will require using the MTTL development environment or sending in a new KSAT request and to follow the [Change Management Workflow](../dev/file_overview.md#change-management-workflow) documented on the MTTL Home page. 

| Field | Required? | Description |
|-------|-----------|---------|
| _id | required (generated) | The KSAT object `_id`. This will be auto-generated when inserting using the `./mttl.py insert` script |
| ksat_id | required | The KSAT id number. When adding a new KSAT, add to the bottom of the page and use the next available number sequence |
| description | required | Reference [Bloom's Taxonomy](https://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy/) to help write meaningful KSAT descriptions. This taxonomy was created specifically for teaching, learning, and assessment. The taxonomy focuses on using verbs and gerunds to label categories and subcategories. *These “action words” describe the cognitive processes by which thinkers encounter and work with knowledge.* The cognitive process is represented by 6 categories.|
| parent | optional | List all parent `_id` related to this KSAT.  A parent KSAT is typically a broader KSAT that this KSAT supports. This KSAT represents an essential building-block for its parent(s) KSAT. Reference the [KSAT parent-child](ksat-parent-child-rels.md) for more information on this relationship. |
| topic | required | Provide a topic this KSAT supports. |
| requirement_src | required | List one or more requirement documents that originated this KSAT. These are for KSATs originated external to the 90COS |
| requirement_owner | required |Provide the office of primary responsibility (OPR) for this KSAT.  These are for KSATs originated external to the 90COS |
| comments | optional | Provide any additional supporting comments. |
| updated_on | optional | The date this KSAT was last modified.  This is automatically updated by the pipeline |

```json
{
	"_id": {
		"$oid": "5f457f1e1ea90ba9adb32aa2"
	},
	"ksat_id": "K0003",
	"parent": [
		{
			"$oid": "5f457f1e1ea90ba9adb32c9c"
		}
	],
	"description": "Understand the purpose of breaking down requirements to atomic functions.",
	"topic": "Software Engineering fundamentals",
	"requirement_src": [
		"ACC CCD CDD TTL"
	],
	"requirement_owner": "ACC A3/2/6KO",
	"comments": "Making sure that you don't have one massive function that does everything, but breaking down the functions by discrete tasks"
}
```
Current rel-link.json definitions are listed [here](../files/eval_rellinks.md)

