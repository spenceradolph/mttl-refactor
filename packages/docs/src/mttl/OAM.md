**Plan on How to Categorize VTRs into Milestones**

There is a [docx](../uploads/Plans_for_VTRs.docx) and a [PDF](../uploads/Plans_for_VTRs.pdf) available for this plan. These formats contain the old tables by work role that were removed from this document, as the [Misc Tables](../../misc-tables) page shows the OAM list in the same format, and is automatically updated.

**Plan on How to Categorize VTRs into Milestones**

At the highest level, VTRs will need to be organized into top level, i.e. structural or overarching, milestones. As a first step, the approximate number of KSATs would be identified. Given the relative average complexity and time expected to complete KSATs, an approximate number of KSATs appropriate to a milestone will be determined. This is not an exact number but rather an approximation.

There are many reasons that the number of KSATs per milestone should not be exact including: given the nature of learning and human behavior, the number would be arbitrary and inexact anyway; KSAT complexity and required proficiency will vary; similarly, time and effort needed to display appropriate proficiency will necessarily vary.

Consequently, the realization of KSATs into milestones will differ across work roles. For example, the Scrum Master role may have many tasks of similar complexity and time requirement and may have similarly sized milestones. By contrast, the Cyber Capabilities Developer (CCD) may have more issues of a lower complexity at a lower proficiency in initial milestones, whereas later milestones may have KSATs of greater complexity at a higher proficiency and consequently fewer such KSATs in the milestone.

Also, milestones will necessarily be conceptually and thematically organized. It would make little sense to group in unrelated KSATs. Networking KSATs would most commonly be grouped with other networking KSATs. Of course, there is overlap and no readily binary or pigeonhole categorization of KSATs required by a work role.

Additionally, milestone design should remain cognizant that where modules have been predefined, they can be used to create milestones. There will need to be a way to disaggregate module created milestones and more structural i.e. overarching milestones.

The exact milestones can and should be refined over time especially given changing requirements (including the addition, removal, and change of KSATs and expected proficiencies) as well as in response to iterative feedback from mentors and apprentices.

**The Relationship between Modules and Milestones**

The overarching milestones will be generated first when a VTR is created. These milestones are the overarching organizational topics for a work role they do not impeded nor are they impeded by the allocation of work role issues (i.e. KSATs) to a pathway module. [Modules may or may not be mapped to milestones in future - regardless there is no effect on overarching milestones. Though there is a small caveat that overarching milestones must be generated first.]

**Work Roles and Milestones**

Looking at the work role / KSAT Proficiency table, marked differences in work role mappings are readily apparent. CCD has the most mapped KSATs with 645 currently mapped KSATs; whereas, SEE has the fewest with 28 mapped KSATs. Further, many work roles only have one or no currently mapped KSATs.

| **Work Role** | **Proficiencies / KSATs** |
| --- | --- |
| CCD | Cyber Capability Developer645 &quot;work-role&quot;: &quot;CCD&quot;,328 &quot;proficiency&quot;: &quot;B&quot;120 &quot;proficiency&quot;: &quot;2&quot;75 &quot;proficiency&quot;: &quot;A&quot;69 &quot;proficiency&quot;: &quot;3&quot;34 &quot;proficiency&quot;: &quot;1&quot;19 &quot;proficiency&quot;: &quot;C&quot; |
| SCCD-L | 114 &quot;work-role&quot;: &quot;SCCD-L&quot;,45 &quot;proficiency&quot;: &quot;2&quot;28 &quot;proficiency&quot;: &quot;B&quot;20 &quot;proficiency&quot;: &quot;C&quot;9 &quot;proficiency&quot;: &quot;&quot;8 &quot;proficiency&quot;: &quot;3&quot;2 &quot;proficiency&quot;: &quot;A&quot;2 &quot;proficiency&quot;: &quot;1&quot; |
| SCCD-W | 158 &quot;work-role&quot;: &quot;SCCD-W&quot;,49 &quot;proficiency&quot;: &quot;2&quot;44 &quot;proficiency&quot;: &quot;&quot;26 &quot;proficiency&quot;: &quot;B&quot;21 &quot;proficiency&quot;: &quot;C&quot;10 &quot;proficiency&quot;: &quot;3&quot; 6 &quot;proficiency&quot;: &quot;A&quot;2 &quot;proficiency&quot;: &quot;1&quot; |
| MCCD | 30 &quot;work-role&quot;: &quot;MCCD&quot;,11 &quot;proficiency&quot;: &quot;C&quot;11 &quot;proficiency&quot;: &quot;3&quot;5 &quot;proficiency&quot;: &quot;D&quot;2 &quot;proficiency&quot;: &quot;2&quot;1 &quot;proficiency&quot;: &quot;B&quot; |
| Scrum Master | 24 &quot;work-role&quot;: &quot;SM&quot;,14 &quot;proficiency&quot;: &quot;B&quot;8 &quot;proficiency&quot;: &quot;C&quot;1 &quot;proficiency&quot;: &quot;A&quot;1 &quot;proficiency&quot;: &quot;2&quot; |
| Product Owner | 136 &quot;work-role&quot;: &quot;PO&quot;,52 &quot;proficiency&quot;: &quot;B&quot;33 &quot;proficiency&quot;: &quot;2&quot;21 &quot;proficiency&quot;: &quot;&quot;13 &quot;proficiency&quot;: &quot;A&quot;11 &quot;proficiency&quot;: &quot;C&quot;5 &quot;proficiency&quot;: &quot;3&quot;1 &quot;proficiency&quot;: &quot;1&quot; |
| TAE | 83 &quot;work-role&quot;: &quot;TAE&quot;,58 &quot;proficiency&quot;: &quot;C&quot;23 &quot;proficiency&quot;: &quot;3&quot;2 &quot;proficiency&quot;: &quot;2&quot; |
| SEE | 28 &quot;work-role&quot;: &quot;SEE&quot;,28 &quot;proficiency&quot;: &quot;3&quot; |

**Work Roles with Existing Overarching Milestone Categories**

The below documentation is for work roles that have at least one overarching milestone marked as TBD and will remain here for planning purposes. Please check the [Misc Tables](../../misc-tables) page for an up-to-date list of overarching milestones in a similar format.

_ **Scrum Master Overarching Milestones** _

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile Basics | K0366, K0367, K0395, K0453, K0668 |
| Agile Proficiency | K0261 |
| Agile for Scrum Masters | K0624 |
| Scrum Specifics | K0941, K0942, K0943, K0944, K0262, K0452, K0454, K0455, K0456, K0621, K0623 |
| Project Management Tools (especially Jira) | A0650 |
| 90 COS | TBD |
| Policy and Compliance | TBD |

_ **Senior Scrum Master Overarching Milestones** _

The Senior Scrum Master does not yet have KSATs mapped to it. Based on descriptions of the expected proficiencies of a Senior Scrum Master and the Senior PO role the milestones could be divided into:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile 1 (Processes) | TBD |
| Agile 2 (Leadership/Mentoring) | TBD |
| Agile 3 (Managing many Teams) | TBD |
| Organization Overview | TBD |
| Policy and Compliance | TBD |

_ **Agile Coach Overarching Milestones** _

The Agile Coach does not yet have KSATs mapped to it. Based on descriptions of the expected proficiencies of a Senior Scrum Master and the Senior PO role the milestones could be divided into:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile 1 (Processes) | TBD |
| Agile 2 (Leadership/Mentoring) | TBD |
| Agile 3 (Managing many Teams) | TBD |
| Organization Overview | TBD |
| Policy and Compliance | TBD |
| Affecting Organizational Change | TBD |

_**Client Systems Technician (CST) Milestones**_

The CST does not yet have KSATs mapped to it. Based on descriptions of the expected proficiencies of a CST, the overarching milestones could be divided into:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Basic Hardware | TBD |
| Basic System Administration | TBD |
| Error Identification and Troubleshooting | TBD |
| Policy and Compliance | TBD |
| Basic System Hardening | TBD |
| Basic Networking | TBD |
| OPSEC | TBD |
| 90 COS | TBD |

_ **Junior Site Reliability Engineer Overarching Milestones** _

Currently, only 12 KSATs are mapped to the JSRE work role.

Many additional KSATs may be mapped to the JSRE work role. Given, the current mapped KSATs and the work role description the overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Cloud Services | K0896, K0897, K0898, K0899, K0900, A0743, A0744 |
| Securing Cloud Services | K0901, K0902 |
| Day 2 Operations | K0895 |
| Software Architecture | K0903, K0904 |
| Networking Fundamentals | TBD |
| Advanced Networking / Network Security | TBD |

_**Senior Site Reliability Engineer (SSRE) Overarching Milestones**_

Currently, no KSATs have been mapped to the SSRE work role. Based on the JSRE work role and work role expectations, the overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Cloud Services | TBD |
| Securing Cloud Services | TBD |
| Day 2 Operations | TBD |
| Software Architecture | TBD |
| Networking Fundamentals | TBD |
| Advanced Networking / Network Security | TBD |
| Mentoring | TBD |

_**Special Area Developer (SAD) Overarching Milestones**_

Currently, only 2 KSATs are mapped to SAD.

Given the description of the SAD work role and the 2 mapped KSATs, possible overarching milestones could include:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Vulnerability Research | K0766, S0204 |
| Remote Exploits | TBD |
| Mission | TBD |

_ **Sysadmin Overarching Milestones** _

Currently no KSATs are mapped to the sysadmin work role.

Based on work role expectations, overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Infrastructure | TBD |
| Platforms | TBD |
| Networking | TBD |
| Services | TBD |

_ **Instructor Overarching Milestones** _

Currently no KSATs are mapped to the instructor work role.

Based on work role expectations, overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Pedagogy (e.g. planned pacing, scaffolding, higher level questions, etc.) | TBD |
| Learning Environment (e.g. Preparation of Materials) | TBD |
| Delivery (e.g. actual pacing, scaffolding, chunking, wait time, higher level questions, responsiveness to learners, etc.) | TBD |
| Curriculum (e.g. Syllabi and Units) | TBD |

![](RackMultipart20210616-4-ry6yve_html_237499165a11f2b9.gif)