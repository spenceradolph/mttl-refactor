# Table of Contents

- [Table of Contents](#table-of-contents)
- [Coordination with Customers](#coordination-with-customers)
  - [New Requirement Process](#new-requirement-process)
  - [New Work-role Process](#new-work-role-process)

# Coordination with Customers

When a customer submits a request for new requirements, work-roles, etc., certain points about the request need to be discussed with 90 COS/CYT to ensure the customer's intent is accurately portrayed. While not all inclusive to every possible scenario, this section explains the basic process needed for coordination.

\[ [TOP](#table-of-contents) \]

## New Requirement Process

```mermaid
graph TD
   pre_conn>Pre-Contact with CYT] --> new_reqs_issue[Customer: Fill and submit a new REQs issue on the MTTL repo]
   new_reqs_issue --> post_conn>Post-Contact with CYT]

   post_conn --> collab[Customer, CYT: Collaborate to convert REQs to KSATs]
   collab --> change_management[CYT: Change Management GitLab Process]
   change_management --> approved{Gov Approved}
   approved --> |Yes| merged[CYT: Merge MR into MTTL]
   approved --> |Rejected| close[CYT: Close MR]
   approved --> |No| collab

   style pre_conn fill:darkseagreen,stroke:#333
   style post_conn fill:darkseagreen,stroke:#333
   style approved fill:lightsalmon,stroke:#333
```

When a new requirement is necessary, the customer should follow the following steps:  
1. Using local processes, determine the new requirement(s) and submit a new requirement issue in the MTTL with this information.
   - Consider the work-role in which this new requirement belongs.
   - Write a statement that explains what a person needs to know, do, or have in order to be successful in the work-role.
   - Using the [proficiency code definitions](https://90cos-mttl.90cos.com/documentation/mttl/ksat-proficiency-reqs.html) chart, determine the proficiency code that best identifies the level of knowledge or performance needed for the new requirement(s).
     - This should be in relation to the requirement's work-role.
     - In general, a basic level work-role will have a lower proficiency than its senior level equivalent.
2. When contacted by 90 COS/CYT, continue conversations about the requirement until it is either merged into the MTTL or canceled.
   - Any communication may be used but entering/replying to comments into the GitLab issue is also requested.
   - Ensure that CYT understands the proper verbiage for the requirement(s).
   - Ensure that CYT understands the proper proficiency code necessary for the identified requirement.
   - In cooperation with a CYT government person, approve the final requirement details.
3. One of the following will occur:
   - When approved and the pipeline passes, the changes will be merged by a CYT government person.
   - When changes are necessary, step 2 continues until approved and the pipeline passes or it's rejected.
   - If rejected, the issue is closed and the requirement is not merged.

\[ [TOP](#table-of-contents) \]

## New Work-role Process

```mermaid
graph TD
   pre_conn>Pre-Contact with CYT] --> new_wr_issue[Customer: Fill and submit a new WR issue on the MTTL repo]
   new_wr_issue --> post_conn>Post-Contact with CYT]

   post_conn --> collab[Customer, CYT: Collaborate to add WR]
   collab --> change_management[CYT: Change Management GitLab Process]
   change_management --> approved{CC/DO Approved}
   approved --> |Yes| vol[Gov: Add WR to SQ Operation Instructions]
   approved --> |Rejected| close[CYT: Close MR]
   approved --> |No| collab

   vol --> merged[CYT: Merge MR into MTTL]

   style pre_conn fill:darkseagreen,stroke:#333
   style post_conn fill:darkseagreen,stroke:#333
   style approved fill:lightsalmon,stroke:#333
```

The coordination process for a new work-role is similar to the one used for new requirements except details must be clarified about the work-role name and any pre/post work-roles that may be attached. For example, a new work-role may be required before obtaining a more advanced version, or vice versa. Below is the process.

1. Using local processes, determine the new work-role(s) and submit a new work-role issue in the MTTL with this information.
   - Consider a name that best reflects the job performed.
   - Consider the level of the work-role. 
     - Is it a basic, senior or master level work-role?
     - Is there another work-role that comes before and/or after this one?
   - Ensure any necessary documentation is completed, such as a unit VOL update.
2. When contacted by 90 COS/CYT, continue conversations about the work-role until it is either merged into the MTTL or canceled.
   - Any communication may be used but entering/replying to comments into the GitLab issue is also requested.
   - Ensure CYT understands the work-role's level, including other work-roles that come before and/or after this one.
   - Provide any required documentation, such as unit VOLs, for the work-role.
   - In cooperation with a CYT government person, approve the final work-role details.
3. One of the following will occur:
   - When approved and the pipeline passes, the changes will be merged by a CYT government person.
   - When changes are necessary, step 2 continues until approved and the pipeline passes or it's rejected.
   - If rejected, the issue is closed and the requirement is not merged.

\[ [TOP](#table-of-contents) \]
