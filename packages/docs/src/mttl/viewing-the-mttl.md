# Viewing the MTTL

The 90COS MTTL landing page can be can be viewed [here](https://90cos-mttl.90cos.com/). Below is an example of the MTTL web page.

The **Master Training Task List (MTTL)** frontend is composed of three pages.

* The Home page - The page that you land on when you first navigate to the MTTL

* The MTTL page - This page shows a coverage report for a work role, a chart showing all Knowledge, Skill, Ability, Task (KSAT) IDs aligned to each work role and specialization, and a list of KSAT IDs that are not aligned to any work role.

* Metrics pages - These pages provide summaries of coverage for training, evaluation, as well as by work role.

* The Training Roadmap page - This page provides an overview of work role progression at the 90th. Additionally, diagrams are provided by work role* showing the required courses for each work role, *as they are defined*.
---
### Main Page
This page states the vision and mission of the 90th COS. It provides information on contacting the CYT as well as providing a link to the external links disclaimer policy. 
---
### MTTL Page
The first Item on the Main page is a color coded coverage report displaying all KSATs along with links to any available training or evaluation links for that item. Coverage signifies content that either provides instruction on that KSAT (training) or tests someone that the meet the requirement specified by the KSAT (evaluation). Items that have both training and evaluation coverage will be colored green. Items with only training coverage will be blue. Items with only evaluation coverage will be yellow. Any items that have neither training nor evaluation coverage will be grey. Items that do not have full coverage are assumed to be met via on the job training.

Each entry shows a list of information that includes a KSAT ID; a description; related KSATs; topic; the requirement owner**; the requirement source**;  a list of work roles that require that KSAT; children (KSATs that are sub components), parents (KSATs to which that KSAT belongs); links to the available training and evaluation material.  
The ***Filter by*** option in the middle of the search bar allows filtering the KSATs by work role. The search bar will return results containing the text in the search. The controls below the Table provide pagination and setting the number of items to display at once.
---
### Metrics Pages

**View All**
This page shows overall coverage reports. The percentage shown is the percentage of items with either both training and evaluation, training only, or evaluation only coverage. A coverage of 100% means all KSATs of that work role have the correlating coverage.

**Metrics Lookup**
This page allows you to look up coverage reports for custom searches. This can be by source, owner, topic, or work role. 

**Miscellaneous Tables**  
This provides different tables highlighting particular areas of particular relevance to KSATs: 
- **IDF KSAT Coverage** provides a searchable table of KSATs included in and mapped to the IDF course for CCD training as offered by the [39th IOS](https://gitlab.com/39iosdev/ccd-iqt/idf).  
- **The Work Role / Specialization** provides a tabular overview of KSAT mappings to work roles and specializations.
-  **KSATs without Work Roles/Specializations** provides a list of KSATs not currently mapped to work roles or specializations

---
### Training Roadmap
* The training [roadmap](https://90cos-mttl.90cos.com/roadmap) provides a modified hierarchical tree view of work role progression within the 90th COS. It provides brief descriptions of each work role as well as the ability to click on each work role to learn more about expectations and training  as well as scheduling training and evaluation.

--- 

###  MTTL Search and Filter Bar 
![images_mttl](../uploads/mttl_search_and_filter.png)

- The top of the MTTL is a [search and filter bar](mttl/searching-the-mttl.md) that allows for customized searches and a plethora of filtering options. 

- The bar with the percentage indicates the percentage of KSATs being covered by both training and evaluations. 



### MTTL Field Description:

| Field | Explanation |
| ------------| ------------|
| **ID** | The KSAT identified.  KSAT IDs begin with an alpha character to indicate its category: <br><br> T - Task, A - Ability, S - Skill, K - Knowledge |
| **Description** | A description of the task, ability, skill, or knowledge needed to satisfy this KSAT |
| **Topic** | The topic or subject related to the KSAT such as Python, Networking, Assembly, etc. |
| **Owner** | The organization that originated the KSAT |
| **Source** | The source document or requirement for a KSAT (if available) |
| **Work Role/Specialization** | This is the 90COS defined work role or specialization. These are defined in the [Work Role and Specification table](#work-roles-and-specializations) above. |
| **Children** | This is the reciprocal of the Parent(s) field and shows all the children that support this KSAT |
| **Parent(s)** | Parents are KSATs that this KSAT supports. This KSAT is a building block for the parent(s) KSATs |
| **Training** | A carousel of links of training that cover this topic. Clicking a listed item will redirect you to the training material |
| **Evaluation** | A carousel of links of evaluations that cover this topic. Clicking a listed item will redirect you to the evaluation material |
