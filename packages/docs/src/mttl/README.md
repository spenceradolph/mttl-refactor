# How to use the MTTL
Questions to be answered:  
1. What is a KSAT?
1. How do I search through the MTTL to find ones that might exist before I submit a new one?
1. How do I write a good KSAT using Bloom's taxonomy?
1. How do I know which proficiency code to use?
1. What is a parent? Source? and why is this important?
1. How do I specify a source correctly?

### Master Training Task List

The 90COS Master Training Task List (MTTL) is a repository of knowledge, skills, abilities, and tasks (KSATs) determined as qualification requirements for each work role or specialization in the organization. One or more work roles or specializations are assigned to each KSAT.

### Work Roles and Specializations 

| Work Role / Specialization | Description |
|------------|------------|
| CCD | Cyber Capability Developer |
| SAD | Specialty Area Developer |
| SCCD-L | Senior Linux Cyber Capability Developer |
| SCCD-W | Senior Windows Cyber Capability Developer |
| TAE | Test Automation Engineer | 
|Instructor |  |  
|SEE | Standard/Evaluation Examination|
|MCCD | Master Cyber Capabilities Developer | 
|CST | Client Systems Technician | 
| Sysadmin | | 
|JSRE | Junior Site Reliability Engineer | 
| SSRE | Senior Site reliability Engineer | 
| PO | Product Owner |
| SM | Scrum Master | 
| SPO | Senior Product Owner |
| SSM | Senior Scrum Master |
| AC | Agile Coach | 

<!-- 
### Specializations

| Specialization | Description |
|------------|------------|  -->
