"Coverage %" Shall measure the mapping of training and/or evaluation to an item directly.  It shall not take into account relationships to determine satisfaction.

![KSAT_Relationships.svg](uploads/84b2c5841a0c343a9f77dd8c0ca33913/KSAT_Relationships.svg)

The process for metrics automation is highly dependent on the files discussed in the [MTTL Automation Design]() page.

TO BE CONTINUED...

For a parent to be satisfied all children must be satisfied.
- Measure children accomplishment (highlighting) as an individual cell vs row highlighting.
-- Green for all sat, yellow for some sat, red for no sat?  Maybe highlight each item Green, red? or ???

For training coverage, KSA Parents are NOT satisfied automatically by satisfaction of the children.  A value must be populated in the training column manually.  Do not autopopulate that value.

For eval coverage, Children tests may or may not satisfy the parent task.
- If children do satisfy parent then the eval column should autopopulate children evals. (coverage depth is is sum of tests)
- if NOT then new tests should populate the eval column.
Note: In either case a file designates the answer... the default blank indicates a "decision has not! been made and must be addressed"
--- 

For TASKS, Parents May or May NOT be satisfied by children however a deliberate decision must be made to evaluate whether or not the children fully satisfiy the requirement.

[Screenshot_from_2020-03-26_12-56-36](uploads/81c20933efd1a7d8e48d93fd301b12b9/Screenshot_from_2020-03-26_12-56-36.png)