**This page describes the process to be followed when suggesting or documenting a new work role and/or specialization.**

Work roles and Specializations are documented in accordance with ACCI Cybercrew Instructions Volumes [1](https://static.e-publishing.af.mil/production/1/acc/publication/acci17-202v1/acci17-202v1.pdf), [2](https://static.e-publishing.af.mil/production/1/af_a2_6/publication/afi17-202v2/afi10-1703v2.pdf), and [3](https://static.e-publishing.af.mil/production/1/af_a2_6/publication/afi17-202v3/afi10-1703v3.pdf).  Per these regulaitons units with cybercrews are supposed to define their own ACC Manuals (or ACCMANS) which are commonly referred to as a units "Vols".  This process may be followed either before updates to Vols (as a planning and coordination tool) or after (to develop training and evals for new work roles and/or specializations.

[[_TOC_]]

## Initiate the Issue (Who: Anyone)

Create a new issue in the MTTL repository using the Requirements template.
* Give the issue the REQS and the TODO label
* Use the comments in the issue to discuss with other stakeholders.

**NOTE: There is a risk of analysis paralysis at this point.  If necessary create the item without any KSAT's and use individual MR's to debate each item**
* *If possible,* Identify existing and new KSATs and the proficiency codes for each required
  * Format for this identification does not matter (e.g. attach as a spreadsheet)

## Approve Creation of MR (Who: CYT, DO, or CC)

Reasoning:  Up till this point no resources have been affected and anyone can propose ideas but going forward CYT resources will be impacted.  Because of this CYT, the DO, or CC must approve next steps.
Approval to create the MR does NOT approve the creation of the work role.  The work role is only approved when the updated MDS Vol's are approved (and why it's a blocking issue).

## Create Merge Request (Who: CYT Dev)

After the details for the new work role are finalized the work role and initial KSATs are incorporated into the MTTL by the dev team. 
* Create new entry in the WORK-ROLES.json or SPECIALIZATIONS.json files. More information can be found [here](https://gitlab.com/90cos/public/mttl/-/wikis/Home#work-roles-and-specializations-json-files)
* Add entry to the (new/old) knowledge, skills, abilities, or tasks JSON 'work-roles' or 'specializations' fields with the proficiency that the work-role/specialization requires. More information can be found [here](https://gitlab.com/90cos/public/mttl/-/wikis/Home#knowledge-skill-ability-and-task-json-files)
* Attach updated MDS Volumes or create a blocking issue to update the squadron MDS Vol's 1, 2, & 3 in accordance with ACCI17-202V1/2/3 respectively. **(Do not remove WIP until resolved)**
* Create a new Label within the MTTL project so future changes can be associated with it
* Ensure the merge request closes the issue

## TODO STUB - Add instructions (if necessary) for any frontend/display requirements to gitlab pages

## After Issue/Merge Actions (aka What we DON'T do in this process)
- Training developed and/or mapped
- Evals developed and/or mapped
