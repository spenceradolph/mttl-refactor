## Metrics Generation
1.  In previous iterations of the MTTL, Metrics graphs were generated in the CI pipeline. This version use [Angular Pipes](https://angular.io/guide/pipes) to filter our MTTL dataset and create metrics. This allows us to generate metrics over any number of criteria. 
2.  Metrics uses [chart.js](https://www.chartjs.org/) to generate its stacked bar and pie graphs.

### Variables that affect pipes
These variables are roughly the same across all pages that filter KSATs (right now, the MTTL page and the Metrics pages)

* `ksats` The list of dicts that contain data from the KSATs imported into the MTTL from MTTL.min.json by the API service
* `wr_obj` An array of work roles to check. Right now, it uses `and` logic and can only be set to multiple work roles in the code. 
* `filter_obj` A key and value to filter KSATs by
* `coverage_calculator` This value maps coverage in the MTTL into four catagories:
  * `t_and_e` This designation means that this ksat is covered by at least one training link and at least one eval link
  * `e_o` This designation refers to KSATs that are covered by eval only; there are no training links for this KSAT
  * `t_o` This designation refers to KSATs that are covered by training only; there are no eval links for this KSAT
  * `none` This designation refers to KSATs that are covered by neither training nor eval 


### Pipe that uses all filter options
`ksats | filterMTTL:filter_obj | filterByArray: 'wr_spec':wr_obj | coverageCalculator: 't_and_e'`
