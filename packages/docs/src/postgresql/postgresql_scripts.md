
## Postgreql related scripts

### The **server/scripts/old/postgres/** directory   
| File                           | Purpose |
|--------------------------------|---------|  
| postgres/append_oams_to_autocomplete.py| This script can be used to append oams to the autocomplete.min.json file |
| postgres/backup_postgres.sh| Creates a plain text backup of the mttl database |
| postgres/check_module_ksat_validity.py| verifies that modules only contain existing KSATs   |
| postgres/check_postgres_status.sh| one liner to see if PostgreSQL is up and accessible |
| postgres/CONNECT_TO_POSTGRES_LOCALLY| a plain text file with helpful commands and reminders for using the MTTL's Postgresql |
| postgres/format_mttl_for_postgres.sh| removes formatting irregularities from mttl.min.json for import to the mttl database |
| postgres/import_all_modules_to_postgres.py| iteratively imports all module data to corresponding tables |
| postgres/import_all_requirements_to_postgres.py| iteratively imports all KSAT requirement files to corresponding tables  |
| postgres/import_all_sequences_to_postgres.py| iteratively imports all module sequence data  |
| postgres/import_all_units_to_postgres.py| iteratively imports all module unit data  |
| postgres/import_all_work_role_jsons_to_postgres.py| iteratively imports all work role .json data  |
| postgres/import_autocomplete_to_local_postgres.py| imports autocomplete.min.json  |
| postgres/import_module_to_postgres.py| imports data from an individual module json |
| postgres/import_mttl_to_local_postgres.sh| deprecated script to call import_mttl_to_postgres.py |
| postgres/import_mttl_to_postgres.py| imports mttl.min.json to PostgreSQL |
| postgres/import_mttl_to_production_postgres.sh| script to format and then import mttl  |
| postgres/import_postgres_on_pod.sh| `psql` one liner to import mttl from backup |
| postgres/import_postgres_on_production.sh| imports mttl from back up on pods |
| postgres/import_requirements_to_postgres.py| imports individual requirements file to PostgreSQL |
| postgres/import_sequence_to_postgres.py| imports an individual module sequence  |
| postgres/import_sql_back_to_postgres.sh| another one liner for importing the mttl database from backup |
| postgres/import_unit_to_postgres.py| imports an individual module unit  |
| postgres/import_workrole_json_to_postgres.py| imports an individual work role json file |
| postgres/mttl_postgres_backup.sql| a copy of the backup file  |
| postgres/prepare_for_postgres.sh| ensure that build dependencies are available for Postgresql-client - used in the *postgres_prep* job of `gitlab-ci.yml` ***check if  deprecated***  |
| postgres/scour_mttl_postgres_on_production.sh| clears all mttl database tables on pods  |
| postgres/scour_postgres_locally.sh| clears all mttl database tables in the dev environment database |
| postgres/scour_postgres_on_pod.sh| may be redundant now that functionality is the same as `scour_mttl_postgres_on_production.sh` |
| postgres/script_import_autocomplete_to_local_postgres.sh| a shell script to call the Python script to import Autocomplete to Postgresql - can most likely be removed with basic tweaking to `import_autocomplete_to_local_postgres.py` |  



### PostgreSQL files in  **server/scripts/oauth/** directory   
| File                           | Purpose |
|--------------------------------|---------|   
|scripts/oauth/add_endpoint_check_on_prod.sh| uses `add_endpoint.sql` to create entries necessary for endpoint check of the ***gitlab*** database |
|scripts/oauth/add_endpoint_check.sh| was used directly on PostgreSQL pods - now deprecated |
|scripts/oauth/add_endpoint_check_to_gl.py| a Python script to create necessary information for the gitlab endpoint check |
|scripts/oauth/add_endpoint.sql| a plain text sql command to add the necessary information for the gitlab endpoint check |
|scripts/oauth/create_gitlab_db_on_prod.sh| uses the `create_gitlab_db.sql` file to create the GitLab database on application pods |
|scripts/oauth/create_gitlab_db.py| A python script to create the Gitlab DB - also contains other oauth related functions |
|scripts/oauth/create_gitlab_db.sh| uses the `create_gitlab_db.sql` file to create the GitLab database on PostgreSQL pods |
|scripts/oauth/create_gitlab_db.sql| `CREATE database gitlab;`  |


### PostgreSQL files in  **server/scripts/old/utils/** directory   
| File                           | Purpose |
|--------------------------------|---------|   
|scripts/utils/check_endpoints.py| relevant to Postgresql as it lists all the PostgreSQL endpoints to test  |
|scripts/utils/endpoint_templates.py| contains templates for expected return data from endpoint checks - when making any changes to SQL output, it may be necessary to revise this file |
