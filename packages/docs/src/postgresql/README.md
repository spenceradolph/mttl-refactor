## PostgreSQL on the MTTL

PostgreSQL is slowly being introduced to the MTTL in order to leverage the affordances of relational databases.  

### PostgreSQL in the local dev environment

PostgreSQL tables are created in the `build_frontend.sh` script located in the root directory.  
This script calls:
```bash
bash server/scripts/old/postgres/import_mttl_to_local_postgres.sh
python3 server/scripts/old/postgres/import_all_modules_to_postgres.py
python3 server/scripts/old/postgres/import_all_units_to_postgres.py
python3 server/scripts/old/postgres/import_all_sequences_to_postgres.py
python3 server/scripts/old/postgres/import_all_work_role_jsons_to_postgres.py
python3 server/scripts/old/postgres/import_all_requirements_to_postgres.py
python3 /server/scripts/old/postgres/import_autocomplete_to_local_postgres.py 
```  
each step generating corresponding tables.  
The majority of non-oauth related PostgreSQL scripts are located in `server/scripts/old/postgres/`. These pertain mainly to the ***mttl** database. 
A few helpful tips and useful reminders can be found in `server/scripts/old/postgres/CONNECT_TO_POSTGRES_LOCALLY`  
***Oauth** related scripts are found primarily in `server/scripts/oauth/`. These pertain mainly to the ***gitlab*** database.  

Connecting locally is accomplished via: `psql -h postgres -d mttl -U mttl_user`  

### PostgreSQL on the review and production pods  
Access to Postgresql is slightly different. It relies heavily on the $PGCONN variable. Please see gitlab variables for more information. Also, a password, available from other devs, is required.  

### PostgreSQL in `gitlab-ci.yml`  
The *postgres_prep* job of the project-build stage includes:  
```yaml
    - bash ./server/scripts/old/postgres/prepare_for_postgres.sh
    - bash ./server/scripts/old/postgres/import_mttl_to_local_postgres.sh
    - python3 ./server/scripts/old/postgres/import_all_work_role_jsons_to_postgres.py
    - python3 ./server/scripts/old/postgres/import_all_modules_to_postgres.py
    - python3 ./server/scripts/old/postgres/import_all_units_to_postgres.py
    - python3 ./server/scripts/old/postgres/import_all_sequences_to_postgres.py
    - python3 ./server/scripts/old/postgres/import_all_requirements_to_postgres.py
    - python3 ./server/scripts/old/postgres/script_import_autocomplete_to_local_postgres.py
    - python3 ./server/scripts/old/postgres/check_module_ksat_validity.py
    - export DATABASE_URL=postgres://mttl_user@postgres:5432/mttl
    - python3 ./server/scripts/oauth/create_gitlab_db.py
    - python3 ./server/scripts/oauth/add_endpoint_check_to_gl.py
    - bash ./server/scripts/old/postgres/backup_postgres.sh
    - cp mttl_postgres_backup.sql ./server/scripts/old/postgres/
```

which, in essence, creates and validates all PostgreSQL tables on a PostgreSQL pod and then stores the back up information (a plain text SQL file) as an artifact to be used later when creating the review PostreSQL database and updating the production *mttl* database.   



### Deploying PostgreSQL to review and production apps  
With the relatively small amount of non-volatile data in the `mttl` database, it is easier and less time consuming to simply add tables from backup `mttl_postgres_backup.sql` than making comparative updates.  
Consequently, the review and production apps have their existing *mttl* databases purged with `server/scripts/old/postgres/scour_mttl_postgres_on_production.sh` which is called by the `check_endpoints_on_pods.sh` script in the review and production stages of the GitLab CI/CD pipeline.  

This relies on `kubectl` commands run from the runner e.g.:
``` bash
export PODNAME=$(kubectl get pods --namespace $KUBE_NAMESPACE |grep -v "NAME\|RESTARTS\|erminating\|postgresql-0"|grep -o "^[^ ]*")
echo "PODNAME is $PODNAME"
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/old/postgres/scour_mttl_postgres_on_production.sh
echo "scoured, now import postgres"
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/old/postgres/import_postgres_on_production.sh
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/oauth/create_gitlab_db_on_prod.sh
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/oauth/add_endpoint_check_on_prod.sh
echo "finishing check_endpoints_on_pods.sh"
```  

In essence this uses  `/app/server/scripts/old/postgres/scour_mttl_postgres_on_production.sh` to cleanse existing data. Then the *mttl* database is recreated using `/app/server/scripts/old/postgres/import_postgres_on_production.sh` - basically using a `psql` command to recreate the database from the `mttl_postgres_backup.sql` backup.  

All output from this script `check_endpoints_on_pods.sh > endpoint_log.txt 2>&1 ` is saved as an artifact `endpoint_log.txt` that is then accessible from the corresponding review or production stage of the Gitlab CI/CD pipeline.  

This is in preparation for `./server/scripts/old/utils/check_endpoints.py ` which verifies the validity of the output of the endpoints defined in `server.js` - including the output of many PostgreSQL queries.  


## PostgreSQL and `server.js`  

There are many endpoints defined in `server.js` that simply provide the output of SQL queries to (primarily) the *mttl* database.  

A typical endpoint query in `server.js` has the form of:  

```javascript 
app.get('/work-roles', (req, res) => makeQuery(req, res, mttlQueries.getWorkRoles, pgPool));  
```

These queries rely on two main components: 
- pgpool  
- mttlQueries.*   

`pgPool` specifies the `server.js` defined pool of PostgreSQL queries to use for the connection.  
`mttlQueries` refers to a list of common queries stored in `server/database/pg_old/mttlQueries.json`. 
The ***server/database/pg_old/*** directory also includes `userQueries.json`, which is a list of authentication related queries.  
Additionally, more complex queries are saved as *.sql* files and imported to `server.js` individually e.g. `getModule.sql`.  





