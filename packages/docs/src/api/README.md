# The MTTL API

### An increasing number of API endpoints are being added to the MTTL. 

The following endpoints are simply appended to `https://90cos-mttl.90cos.com/` 


**Most**, if not all, of the results are provided in typical SQL fashion. Consequently, many of the results are better suited for data processing than human readable output e.g. `long` format as opposed to `wide` format in some instances cf. [here](https://www.theanalysisfactor.com/wide-and-long-data/)  

If you would like any additional query results, including in a format not currently available, simply use the `contact us` or `bug report` functions. 



- `/units/overview`
- `/units/sequences`
- `/unit/:unitid` e.g. `/unit/asm_0001`
- `/work-roles/sequences/overview`
- `/work-roles/sequences/details`
- `/work-role/:workrole/sequences` i.e. `/work-role/CCD/sequences`  

## Work Role End Points 
 - `/work-roles`  provides a list of all work roles for 90 COS   

 - `/work-role/:wr` e.g. `/work-role/CCD` provides a list of all expected KSATs for a specific work role and the minimum proficiency level at which those expectations should be met  



## Module Related End Points 

The module related endpoints provide information about sequences, units, modules, and various relations among them.    
*Sequences* are learning pathways comprised of larger instructional *units*.  *Units* are composed of more detailed *modules*, which essentially represent lessons.   

### Sequence details 


- `/work-roles/sequences/overview`  provides a brief overview of  all available sequences   

- `/work-roles/sequences/details` - provides a list of sequences and their constituent units   

- `/work-role/:workrole/sequences` shows the sequences available for a particular work role such as CCD.   
   


   
### Unit details
- `/units/overview` provides an overview of all available units including the number of modules available for each. 

- `/units/sequences` provides fewer details, primarily for listing sequences of instructional units  

- `/unit/:unitid` e.g. `/unit/unit0001` displays thorough information about a particular unit.   


### Module details 

- `/modules`  lists all module ids, their KSATs, and the proficiency of those KSATs supported by the module.   

- `/module/:moduleid`  Provides a compiled module JSON for the module viewer

- `/module/:moduleid/overview`  provides descriptive, top level details about a particular module.     

- `/module/:moduleid/content`  lists the instructional content, e.g. course material, for a particular module.    

- `/module/:moduleid/ksats`  lists the KSATs, the type of KSAT, and the proficiency at which KSATs are covered for a particular module.    

- `/module/:moduleid/performance`  lists labs or other performance requirements for a particular module.     

- `/module/:moduleid/resources`  lists recommended resources for a particular module.   

- `/module/:moduleid/assessments` lists the assessments for a particular module.   

- `/module/:moduleid/tests`    (This points to assessments endpoint)

### KSAT Queries

`/ksats` Lists all ksats arranged by Knowledge, Skill, Ability, and Task like this:

```json
{
    "knowledge": { "K0001", ... },
    "skills": { "S0001", ... },
    "abilities": { "A0001", ... },
    "tasks": { "T0001", ... },
}
```

### Invalid Queries

- `/invalid-query/` Tests error handling for invalid SQL query
- `/nonexistent-query/` Tests error handling for a SQL query that does not exist
