## Filters.json

```json
[
    {
        "filter_type":"mttl",
        "filter_text":"mttl"
    },

    ( . . . )

    {
        "filter_type":"requirement_src",
        "filter_text":"JCT&CS"
    }
]
```

The filters.json file, located at `mttl/src/app/all-metrics/filters.json` determines the list of metrics that is displayed on the all metrics page. 

### To add an entry:

Simply append a dict with the following data
```json
{
    "filter_type":"data column",
    "filter_text":"filter text"
}
```
Where the filter type is a key in the MTTL data structure.

