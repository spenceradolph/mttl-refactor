
## Check endpoints 

Checking endpoints is an important part of quality control. Checking the endpoints listed in `server.js` *should* ensure that the app its constituent components (including database connections) are working within expected parameters.  

There are two key elements to this check in the review and production jobs of gitlab-ci.  

- `check_endpoints_on_pods.sh`
- `server/scripts/old/utils/check_endpoints.py https://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN`  


### check_endpoints_on_pods.sh 
`check_endpoints_on_pods.sh` is a bash script that is executed on/from *the GitLab runner*. Consequently, it relies on `kubectl` commands to execute its constituent components.   

``` bash 
export PODNAME=$(kubectl get pods --namespace $KUBE_NAMESPACE |grep -v "NAME\|RESTARTS\|erminating\|postgresql-0"|grep -o "^[^ ]*")
echo "PODNAME is $PODNAME"
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/old/postgres/scour_mttl_postgres_on_production.sh
echo "scoured, now import postgres"
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/old/postgres/import_postgres_on_production.sh
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/oauth/create_gitlab_db_on_prod.sh
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- bash /app/server/scripts/oauth/add_endpoint_check_on_prod.sh
echo "finishing check_endpoints_on_pods.sh"
exit 0
```

Because the review apps and production pods having differing naming schema and processes, it is important to obtain the `$PODNAME`.  
This information is then used to clear existing tables with `scour_mttl_postgres_on_production.sh` - this helps if table changes have been pushed to review apps or for production.  

Given the small size of the tables, it is easiest to drop and readd the new tables using `import_postgres_on_production.sh`. 

***However*** the GitLab table is not dropped - it is what is used for oauth. ***The careful reader will note that there is a script `create_gitlab_db` to ensure that the GitLab table exists.*** If the GitLab table exists, this script exits quietly.  

`add_endpoint_check_on_prod.sh` is a simple PostgreSQL script to ensure that the necessary data exist in the GitLab database for the endpoint check.  





