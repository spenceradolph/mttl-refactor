## Deploy scripts in Review and Production

### CI Variables used:
```yaml
    POSTGRES_ENABLED: "true"
    POSTGRES_DB: mttl
    POSTGRES_USER: mttl_user
    POSTGRES_PASSWORD: secret_password
    AUTO_DEVOPS_DEPLOY_DEBUG: "true"
    AUTO_DEVOPS_ATOMIC_RELEASE: "true"
```

Please see the Customizing Auto Devops page for more information:
https://docs.gitlab.com/ee/topics/autodevops/customize.html#database

**NOTE:** All PostgreSQL variables POSTGRES_.... must be defined in gitlab-ci.yml or using gitlab -> settings -> ci/cd or the review and production phases of the pipeline will fail.

### Script

```yaml
    - export K8S_SECRET_GLCONN="postgres://mttl_user:${K8S_SECRET_POSTGRES_PASSWD}@$CI_ENVIRONMENT_SLUG-postgresql:5432/gitlab"
    - export K8S_SECRET_PGCONN="postgres://mttl_user:${K8S_SECRET_POSTGRES_PASSWD}@$CI_ENVIRONMENT_SLUG-postgresql:5432/mttl"
    - cp ./mttl/scripts/deploy/autodeploy.sh /usr/local/bin/auto-deploy
    - chmod +x /usr/local/bin/auto-deploy
    - cp ./server/scripts/old/utils/check_endpoints_on_pods.sh /usr/local/bin/check_endpoints_on_pods.sh
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - echo "auto-deploy deploy completed"
    - auto-deploy persist_environment_url
    - check_endpoints_on_pods.sh > endpoint_log.txt 2>&1 
    - python3 ./server/scripts/old/utils/check_endpoints.py https://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
```

We use a customizable version of the auto-deploy scripts from autodevops for our deployment of production and review apps. The file can be located at mttl/scripts/deploy/autodeploy.sh.

Each pipeline creates a **Review App** to test the functionality of the website before being merged to master.  
Merge requests create a button that says 'View App'.   
<img src="../uploads/mttl_view_app.png" alt="MTTL View App Button" />    
  

Non merge request pipelines also create review apps - however, the URL will need to be located in the job logs (output screen) from the review stage. 

In the Review Stage of the MTTL pipeline:  
<img src="../uploads/mttl_review_stage.png" alt="MTTL Review stage " />      

  
In order to see the review app URL, locate text containing "*Application  should be accessible at*":  
<img src="../uploads/mttl_reviewapp_URL.png" alt="MTTL Review App URL " />      
  

### Scripts of special note: 

The `check_endpoints_on_pods.sh` script does much more than check_endpoints - it uses multiple `kubectl` commands to prepare the review pods and the production pod with updates to Postgresql. It generates an artifact that contains the output from all included scripts.

The `./server/scripts/old/utils/check_endpoints.py` runs a script verifying the expected output of all endpoints defined in `server.js`  



---   


### Changes to autodeploy.sh

Below are the changes that we have made to customize gitlab's auto-deploy scripts

1. The AUTO_DEVOPS_ATOMIC_RELEASE variable now affects the PostgreSQL pod.


---
***For reference, the *helm* scripts used by autodeploy are located [here](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/tree/master/assets/auto-deploy-app)***
