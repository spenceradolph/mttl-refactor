## Project Build Stage

### Angular Build Job

**What this stage does**: This stage installs npm dependencies and builds the Angular project using `ng build --prod` (or `ng build --configuration=oauth-review`), 

**What makes this stage fail**: Something might be wrong with the Angular code. The error message is helpful here.

NOTE: Sometimes this job fails randomly. If the error message is not about Angular, try running it a second time.

### Build Docs

**What this job does**: This job builds this MDBook.

**What makes this job fail**: This job fails if there is an error building the MDBook. Generally, the error message will be helpful in explaining why. The most common issue is one with the [SUMMARY.md formatting](https://rust-lang.github.io/mdBook/format/summary.html).

**Scripts Used**: The scripts that preform these changes are located located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/build/build_docs.sh).

### Generate Kumu

**What this job does**: This job builds the data that is called by kumu.io for the visualization page

**What makes this job fail**: This job fails if there is an error parsing data for kumu i.e. from Mongo to a JSON. This job does not normally fail.

**Scripts Used**: The script that performs these changes is located located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/build/create_kumu_dataset.py).