## Quality Stage

### Pep8

**What this job does**: This job checks the python code in this project (Except for the code in node_modules, which isn't our code) for any violations of the [Pep8 standards](https://www.python.org/dev/peps/pep-0008/) using the [flake8 library](https://pypi.org/project/flake8/).

**What makes this job fail**: This job will fail if the Python code in your branch does not meet [pep8 standards](https://www.python.org/dev/peps/pep-0008/). 

**Note**: You can attempt to autofix the python code if you include --fix in your commit message. This will enable the pep8-and-autofix job.

**Note 2**: If you read the source code for this job, you will see that it ignores rule [E402](https://www.flake8rules.com/rules/E402.html). We don't follow that rule.

### Pep8-and-autofix

**What this job does**: This job checks the Python code in this project (Except for the code in node_modules, which is not CYT created code) for any violations of the [Pep8 standards](https://www.python.org/dev/peps/pep-0008/) using the [flake8 library](https://pypi.org/project/flake8/). It then automatically fixes any errors, if possible, using the [autopep8 library](https://pypi.org/project/autopep8/).

**What makes this job fail**: This job will fail if the Python code in your branch does not meet pep8 standards. 

**Note**: This job will only run if you include --fix in your commit message. This will enable the pep8-and-autofix job.

**Note 2**: If you read the source code for this job, you will see that it ignores rule E402. We don't follow that rule.

### Maintainability

**What this job does**: This job checks the python code in this project (Except for the code in node_modules, which isn't our code) to ensure that the (cyclomatic complexity)[https://docs.codeclimate.com/docs/cyclomatic-complexity] remains low, while the (maintainability index)[https://docs.codeclimate.com/docs/maintainability] remains high. This job uses the [radon library](https://pypi.org/project/radon/).

### Angular Linting

**What this job does**: This job uses Angular's built-in eslint plugin to ensure that our TypeScript code is following best practices.

**What makes this job fail**: This job fails if the linter finds any issues with the code.

### Server.js Linting

**What this job does**: This job uses eslint to check the express server and ensure that our Javascript code is following best practices.

**What makes this job fail**: This job fails if the linter finds any issues with the code.

### Validate KSATS

**What this job does**: These steps validate that the current requirements documents in the database (located in mttl/database/requirements/) are valid JSON files and that they match our JSON Schemas located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/json_templates.py). 

**What makes this job fail**: These jobs will fail if something there are problematic KSAT definitions.

**Scripts Used**: The script that makes these checks is located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/validate/validate_requirements.py).

### Validate Requirement Owner

**What this job does**: This job validates that every KSAT that comes from a requirement source also has a requirement owner

**What makes this job fail**: A requirement-sourced KSAT that does not have a requirement owner. This job **must** be allowed to fail as there are multiple reasons for a KSAT to not have a requirement owner. 

**Scripts Used**: The script that makes these checks is located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/scripts/validate/validate_req_owners.py).

### Validate Rel-Link URLs

**What this job does**: This CI step validates that all URLs linked to by training and evaluations are valid. It will skip any links to ***confluence pages***, so manual checking is encouraged before submitting anything from ***confluence*** onto the MTTL.

**What makes this job fail**: This job fails if any URL included in a rel-link does not return **status code 200**.

**Scripts Used**: The script that makes these checks is located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/validate/validate_rel_link_urls.py).

### Validate Rel-Links

**What this job does**: These jobs check the Training and Eval Rel-Links to make sure that they match the schemas located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/validate/validate_requirements.py).

**What makes this job fail**: These jobs fail if the training or eval rel-links do not follow the schema or are not valid JSONs

**Scripts Used**: The script that makes these checks is located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/scripts/validate/validate_rel_links.py).

### Validate Work Roles

**What this job does**: This CI job validates that:
1. All work roles in the database are mapped in roadmap.json
1. All work roles match the schema

**What makes this job fail**: This job fails if a work role is omitted from the roadmap or if a work role json does not patch the schema

**Scripts Used**: The script that makes these checks is located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/validate/validate_work_roles.py).
