## test i.e. Security Scans

**What this job does**: This is an ***optional job*** that performs multiple security scans.  It is based primarily on GitLab provided templates. 
The scans are divided in multiple stages:   
- [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) scans the source code for known vulnerabilities  
- [DAST](https://docs.gitlab.com/ee/user/application_security/dast/) checks for vulnerabilities in deployed environments  
- [Dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) checks for security vulnerabilities in dependencies
- [Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html) uses [sitespeed.io](https://www.sitespeed.io/) for performance tests. This test is primarily intended to compare proposed changes. It is not invoke by `--scan` but is included with `DOISCAN`.  The performance tests can be invoked without other scans by adding `--performance` to the commit message  
- [License Scanning](https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html) checks for license compliance e.g. [MIT](https://opensource.org/licenses/MIT),  [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html), and others 
- [Secret Detection](https://github.com/zricethezav/gitleaks) scans for leaks in git e.g. hardcoded secrets.  This scans the most current commit as well as all commits after a specified date.  It is invoked with `--scan` but can also be activated without other scans by adding `--leaks` to the commit message.  
- [Container Scanning](https://github.com/aquasecurity/trivy) leverages the `trivy` vulnerability scanner to identify problems with the container and artifacts.   It is invoked with `--scan` but can also be activated without other scans by adding `--container-check` to the commit message. 
 


**How to invoke this job**: The security scan functionality is activated by adding `--scan` to the commit message or by setting the CI variable `DOISCAN` to `true`.  Scans identified above may be run separately as well.  

**What makes this job fail**: These jobs are allowed to fail. ***However**, issues should be created for any severity **medium** or higher. 

**Scripts Used**: This job executes many different scripts across many different stages.  More explanation is provided in the links to the individual jobs.  

**Artifacts Produced**:  This job produces multiple `.json` files that begin with `gl-` in the file name. 