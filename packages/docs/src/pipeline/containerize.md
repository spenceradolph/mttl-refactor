## Containerize Stage

**What this job does**: This job builds the docker container located at ./Dockerfile to prepare it for the deploy/review stage.

**What makes this job fail**: This job fails if there was a problem with the `docker build` command that builds the dockerfile. Any error message given will generally regard the dockerfile specifically

**Scripts Used**: This job relies upon GitLab's Auto Devops build CI script, which is located [here](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml)