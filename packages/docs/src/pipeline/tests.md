## Unit-Test Stage

### Test Identical Desc

**What this job does**: This CI job validates that no two KSATs have the same description

**What makes this job fail**: This job will fail if there are two KSATs that have the exact same description

**Scripts Used**: The script that checks for identical descriptions is located [here](https://gitlab.com/90cos/mttl/-/tree/master/server/scripts/old/tests/test_identical_desc.py).

### Test MTTL (Insert, Modify, Delete)

**What this job does**: These tests validate the mttl.py script's functionality for inserting, modifying, and deleting from the mongo database and the MTTL.

**What makes this job fail**: This job will fail if `./mttl.py` does not work for either inserting, modifying, or deleting

**Scripts Used**: The scripts that check that `.mttl.py` is functioning properly are located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/mttl_cmds/).

### Test Mapping Nonexistent KSATs

**What this job does**: This job validates that all rel-links are mapped to KSATs that actually exist in the requirements directory.

**What makes this job fail**: This job will fail if a rel-link points to a KSAT does not exist.

**Scripts Used**: The scripts that check that KSATs exist are located [here](https://gitlab.com/90cos/mttl/-/tree/master/server/scripts/old/tests/test_mapping_nonexistent_KSATs.py).

### Test Parent KSAs

**What this job does**: This job validates that all KSATs mapped as parents actually exist in the requirements directory.

**What makes this job fail**: This job will fail if a KSAT has a parent that does not exist.

**Scripts Used**: The scripts that checks for KSAT parents are located [here](https://gitlab.com/90cos/mttl/-/tree/master/server/scripts/old/tests/test_parent_exist.py).