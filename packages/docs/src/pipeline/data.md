## Generate Data Stage

This stage builds all of the datasets that will be needed for the MTTL

### Create MTTL Dataset

**What this job does**: This job reads from the requirements JSONs, which now contain children and rel-link data, and combines it into a file called MTTL.min.json, which is what the angular project uses for its data.

**What makes this job fail**: This job fails if there is an error in the python script. Check the error message for more information.

**Scripts Used**: The scripts that preform these changes are located located [here](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/tree/master/mttl/scripts/build/create_mttl_dataset.py).

### Create TTL Dataset

**What this job does**: This job reads from the work role jsons to create a list of work roles.

**What makes this job fail**: This job fails if there is an error in the python script. Check the error message for more information.

**Scripts Used**: The scripts that preform these changes are located located [here](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/tree/master/mttl/scripts/build/create_ttl_dataset.py).

### Create Roadmap

**What this job does**: This job reads from roadmap directory and creates roadmap.min.json

**What makes this job fail**: This job fails if there is an error in the python script. Check the error message for more information.

**Scripts Used**: The scripts that preform these changes are located located [here](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/tree/master/mttl/scripts/build/create_roadmap.py).

### Create Extra Datasets

**What this job does**: This job reads from the jsons and creates a file (currently unused) that tracks two things:
1. A list of KSATs that do not belong to any work role
1. A list of KSATs covered in the IDF course.

**What makes this job fail**: This job fails if there is an error in the python script. Check the error message for more information.

**Scripts Used**: The scripts that preform these changes are located located [here](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/tree/master/mttl/scripts/build/create_extra_datasets.py).