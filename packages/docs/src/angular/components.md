## Angular Components

These are the components that we created and use for the MTTL

| Component Name    | Component Purpose                                | Comments |
| --- | --- | --- |
| AppComponent      | Provides wrapper and navbar for angular website  | AppComponent and MTTLComponent have IntroJS tutorials |
| HomepageComponent | Provides a basic landing page for this project   | Currently just an emblem and blurb - needs to be fleshed out |
| MTTLComponent | This component defines the main MTTL page | |
| KsatLinkPromptComponent | This component defines the popup for adding a link to an existing KSAT | |
| NewKsatPopupComponent | This component defines the popup for creating a new KSAT | |
| MetricsComponent | This component defines the Metrics Lookup page | |
| AllMetricsComponent | This component imports and iterates through the filters defined in the filters.json from its directory, which creates the All Metrics page | | 
| RoadmapComponent | This component defines the roadmap page | |
| PageNotFoundComponent | This component defines the 404 page | |
| ContactUsComponent | This component provides a popup to contact CYT through gitlab | |  
| BugReportComponent | This component provides a popup to report a bug through a gitlab ticket | |  
| DisclaimerComponent | This component defines the legal disclaimer for external links | |  
| FAQComponent | This component defines the FAQ page | |  
| IdfKsatsComponenet | This component defines the IDF ksats table on the misc. tables page | |  
| MiscTablesComponent | Defines the Misc. Tables page. Contains IdfKsats, NoWrMetrics, and WrMetrics Components | |
| ModifyKsatComponent | This component provides a popup to report a bug through a gitlab ticket | |  
| ModularRoadmapComponent | This component provides a view for modules | WIP |
| ModuleMakerComponent | This component provides a way to create json files for modules | WIP |
| NoWrMetricsComponent | This is part of the Misc. Tables page | |
| RoadmapComponent | This component defines the container for the roadmapOverviewComponent and the WorkroleRoadmapComponent | |
| RoadmapOverviewComponent | This component defines an overview of work role progression in the 90th | |
| WorkroleRoadmapComponent | This component defines a learning sequence for obtaining the knowledge for earning a work role | WIP |
| WrMetricsComponent | Part of the misc tables page, this component defines a table that lists the KSATs required for each work role | |
