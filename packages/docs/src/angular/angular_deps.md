## Dependencies imported from Angular

### Imports 
```javascript
/* ANGULAR IMPORTS */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

/* ANGULAR MATERIAL */
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button'; 
import {MatInputModule} from '@angular/material/input'; 
import {MatCardModule} from '@angular/material/card';

/* ANGULAR FORMS */
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
```

### Angular Imports

* **[Browser Module](https://angular.io/api/platform-browser/BrowserModule)**: Exports required infrastructure for all Angular apps. This module is included by default in all Angular apps by default

* **[NgModule](https://angular.io/api/core/NgModule)**: Another default Angular import. Allows you to import created components.

* **[HTTPClientModule](https://angular.io/api/common/http/HttpClientModule)**: Allows http requests to be made. In our case, the requests are made to the express server.

* **[FlexLayoutModule](https://github.com/angular/flex-layout)** helps with flex layout for elements generated using an ngFor.

### Angular Material

We use [Angular Material](https://material.angular.io/), which is a UI component library in various parts of the project, for example: we use it in the popup windows for the adding/modifying KSATs dialogs. We also plan on using it in the revised training roadmap. These imports allow us to use material elements in our project

### Angular Forms

We use [Angular Forms](https://angular.io/guide/forms), which is an Angular Forms library, for things like our adding/modifying KSATs dialogs.
