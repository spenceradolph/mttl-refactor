## Third Party Modules

| Module                    | purpose                                                                                               | related commands  (if applicable)                      | recommended resources                                                                  |
|-------------------------|-------------------------------------------------------------------------------------------------------|--------------------------------------------------------|----------------------------------------------------------------------------------------|
| NGX Pagination            | Provides pagination for the MTTL Page                                                                 |                                | [npm page](https://www.npmjs.com/package/ngx-pagination)                   |
| Cookie Service            | Stores cookies to remember halfmoon settings and introjs tutorial status                              |                                | [npm page](https://www.npmjs.com/package/ngx-cookie-service)               |
| FontAwesomeModule | Provides fontawesome icons | [npm page](https://www.npmjs.com/package/@fortawesome/angular-fontawesome) |
| CarouselModule | Provides bootstrap carousel for the expanded panes | [documentation](https://valor-software.com/ngx-bootstrap/#/carousel) |
