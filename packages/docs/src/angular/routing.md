## Routing Module

`./mttl/src/app/app-routing.module.ts`

### Routing Logic

This module will route everything that is not defined in server.js. Here are the current valid routes:

```javascript
// NAMING CONVENTION
// Ensure that routing names are all kebab-case, never camelCase or snake_case
// Route parameters ex. the workRole part of mttl/:workRole are camelCase
const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'mttl', component: MttlComponent},
  {path: 'mttl/:workRole', component: MttlComponent},
  {path: 'misc-tables', component: MiscTablesComponent },
  {path: 'metrics', component: MetricsComponent},
  {path: 'wr-metrics', component: WrMetricsComponent},
  {path: 'all-metrics', component: AllMetricsComponent},
  {path: 'roadmap', component: RoadmapComponent },
  {path: 'modular-roadmap', component: ModularRoadmapComponent },
  {path: 'module-maker', component: ModuleMakerComponent },
  {path: 'faq', component: FAQComponent},
  {path: 'bug-report', component: BugReportComponent},
  {path: 'disclaimer', component: DisclaimerComponent},
  {path: '**', component: PageNotFoundComponent}
  // Variables can be passed to components through the url structure
  // The variable name is what you will end up importing in the component
  // {path: 'URL/:IMPORTVARIABLE', component: ComponentName}
];
```

Any page that is not explicitly listed in the above array will be routed to the 404 page.

### Adding a New Route

If you want to add a new page to the MTTL, first create an [Angular Component](https://angular.io/cli/generate), then add it anywhere above the 404 page.

For example, if I created a knock knock joke page:
```javascript
// NAMING CONVENTION
// Ensure that routing names are all kebab-case, never camelCase or snake_case
// Route parameters ex. the workRole part of mttl/:workRole are camelCase
const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'mttl', component: MttlComponent},
  {path: 'mttl/:workRole', component: MttlComponent},
  {path: 'misc-tables', component: MiscTablesComponent },
  {path: 'metrics', component: MetricsComponent},
  {path: 'wr-metrics', component: WrMetricsComponent},
  {path: 'all-metrics', component: AllMetricsComponent},
  {path: 'roadmap', component: RoadmapComponent },
  {path: 'modular-roadmap', component: ModularRoadmapComponent },
  {path: 'module-maker', component: ModuleMakerComponent },
  {path: 'faq', component: FAQComponent},
  {path: 'bug-report', component: BugReportComponent},
  {path: 'disclaimer', component: DisclaimerComponent},
  {path: 'knock-knock-jokes', component: KnockKnockComponent }, // <---- The new component goes here
  {path: '**', component: PageNotFoundComponent}
  // Variables can be passed to components through the url structure
  // The variable name is what you will end up importing in the component
  // {path: 'URL/:PARAMETER', component: ComponentName}
];
```

### Naming Convention
Ensure that routing names are all `kebab-case`, never `camelCase` or `snake_case`

Route parameters ex. the workRole part of mttl/:workRole are `camelCase`

For example: 

```javascript
const routes: Routes = [
  {path: 'bad_name/:bad_param', component: HomepageComponent},  // INCORRECT
  {path: 'badName/:bad-param', component: MttlComponent},       // INCORRECT
  {path: 'good-name/:goodParam', component: MttlComponent},     // CORRECT
];
```
