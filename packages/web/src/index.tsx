import { onDOMContentLoaded } from 'halfmoon';
import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components';

// Styling
require('halfmoon/css/halfmoon-variables.min.css');
require('intro.js/introjs.css');

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root'),
    onDOMContentLoaded
);
