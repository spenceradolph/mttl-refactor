import * as d3 from 'd3';
import { useEffect } from 'react';
import './Roadmap.scss';

export const WorkroleRoadmap = ({ currentEntry }: any) => {
    let canvas: any;
    let data: Record<string, any>;
    let arrowSize = 500;
    let yTransform = 50;
    let xTransform = 50;
    let triangle = d3.symbol().type(d3.symbolTriangle).size(arrowSize);
    let innerRadius = 45;
    let outerRadius = 55;
    let roadmapArc = d3
        .arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius)
        .startAngle(0)
        .endAngle(180 * (Math.PI / 180));
    let diamOffset = 2 * ((innerRadius + outerRadius) / 2);
    let roadOffset = yTransform - 19;
    let viewWidth = 750;
    let viewHeight: number;
    let points = '50,5   100,5  125,30  125,80 100,105 50,105  25,80  25, 30';

    const makeSVG = () => {
        const svg = d3
            .select('.roadmap')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100%')
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .attr('viewBox', '0 0 ' + viewWidth + ' ' + viewHeight)
            .style('background-color', '#eee');
        const g = svg.append('g').attr('class', 'g');
        //.attr("transform", "translate(" + this.width/2 + "," + this.margin.top + ")");

        return g;
    };

    const drawRoadmap = (g: any) => {
        const startArrow = g.append('g').attr('class', 'startArrow');

        startArrow.append('rect').attr('height', 10).attr('width', 100).attr('fill', '#707070').attr('y', roadOffset).attr('x', xTransform);
        startArrow
            .append('path')
            .attr('d', triangle)
            .attr('stroke', '#2D5C79')
            .attr('stroke-width', '2')
            .attr('fill', '#F0F7FD')
            .attr('transform', 'rotate(-30) translate(15 ' + yTransform + ')');

        let xPos = 100;
        let yPos = 0;
        let direction = 1;
        data.forEach((element: any, index: any) => {
            createBox(xPos, yPos, element, g);
            xPos += 250 * direction;
            if (index % 2 === 1 && index !== data.length - 1) {
                if (direction > 0) {
                    fillerRect(xPos + xTransform - 5, yPos + roadOffset, g);
                    g.append('path')
                        .attr('d', roadmapArc)
                        .attr('transform', 'translate(' + (xPos + xTransform + 20) + ',' + (yPos + +roadOffset + outerRadius) + ')')
                        .attr('fill', '#707070');
                    fillerRect(xPos + xTransform - 5, yPos + roadOffset + diamOffset, g);
                    xPos -= 250; // shift to accommodate change in direction
                } else if (index !== data.length - 1) {
                    xPos += 250; // shift to accommodate change in direction
                    fillerRect(xPos + xTransform - 21, yPos + roadOffset, g);
                    g.append('path')
                        .attr('d', roadmapArc)
                        .attr('transform', 'translate(' + (xPos + xTransform - 20) + ',' + (yPos + roadOffset + outerRadius) + ') rotate(180)')
                        .attr('fill', '#707070');
                    fillerRect(xPos + xTransform - 21, yPos + roadOffset + diamOffset, g);
                }
                direction = direction * -1;
                yPos += diamOffset;
            }
        });

        if (direction > 0) {
            // Line connecting Roadmap to stopsign
            g.append('rect')
                .attr('height', 10)
                .attr('width', 75)
                .attr('fill', '#707070')
                .attr('transform', 'translate(' + (xPos + xTransform - 1) + ',' + (yPos + roadOffset) + ')');
            // Stopsign
            g.append('polygon')
                .attr('points', points)
                .attr('stroke', '#fff')
                .attr('stroke-width', 3)
                .attr('fill', '#cf142b')
                .attr('transform', 'translate(' + (xPos + xTransform) + ',' + (yPos + yTransform - 70) + ')');
            // STOP text
            g.append('text')
                .text('STOP')
                .attr('fill', '#FFF')
                .style('font-size', '20pt')
                .style('font-weight', 'bolder')
                .attr('pointer-events', 'none')
                .style('text-anchor', 'middle')
                .attr('x', xPos + xTransform + 75)
                .attr('y', yPos + 43);
        } else if (direction < 0) {
            fillerRect(xPos + xTransform + 225, yPos + roadOffset, g);
            g.append('polygon')
                .attr('points', points)
                .attr('stroke', '#fff')
                .attr('stroke-width', 3)
                .attr('fill', '#cf142b')
                .attr('transform', 'translate(' + (xPos + xTransform + 100) + ',' + (yPos + yTransform - 70) + ')');
            // STOP text
            g.append('text')
                .text('STOP')
                .attr('fill', '#FFF')
                .style('font-size', '20pt')
                .style('font-weight', 'bolder')
                .attr('pointer-events', 'none')
                .style('text-anchor', 'middle')
                .attr('x', xPos + xTransform + 175)
                .attr('y', yPos + 43);
        }
    };

    function createBox(x: any, y: any, element: any, canvas: any) {
        // const hoverEvent = hoveredStep;
        const xOffset = 25;
        canvas
            .append('rect')
            .attr('height', 10)
            .attr('width', 255)
            .attr('fill', '#707070')
            .attr('x', x + xTransform - 5)
            .attr('y', y + roadOffset);
        const stepGroup = canvas.append('g');
        stepGroup
            .append('rect')
            .attr('id', element.name.replace(/\s/g, ''))
            .attr('height', 30)
            .attr('width', 200)
            .attr('x', x + xTransform + xOffset)
            .attr('y', y + yTransform - 30)
            .attr('stroke', '#2D5C79')
            .attr('stroke-width', '2')
            .attr('fill', '#F0F7FD')
            // @ts-ignore
            .on('mouseover', function (d, i) {
                // @ts-ignore
                d3.select(this).transition().duration(500).style('stroke', '#000').attr('height', 90);
                // return hoverEvent.emit(element);
            })
            // @ts-ignore
            .on('mouseout', function (d, i) {
                // @ts-ignore
                d3.select(this).transition().duration(500).style('stroke', '#2D5C79').attr('height', 30);
            })
            .on('click', (d: any) => {
                if (element.link.length > 0 && element.link !== '#') {
                    window.open(element.link, '_blank');
                }
            });

        // Clip path
        stepGroup
            .append('clipPath')
            .attr('id', 'text-clip-' + element.name.replace(/\s/g, ''))
            .append('use')
            .attr('xlink:href', '#' + element.name.replace(/\s/g, ''));
        // Title
        stepGroup
            .append('text')
            .text(element.name)
            .attr('x', x + xTransform + 125)
            .attr('y', y + yTransform - 10)
            .attr('pointer-events', 'none')
            .style('font-family', 'sans-serif')
            .style('text-anchor', 'middle')
            .style('fill', '#2D5C79')
            .style('font-size', 'small')
            .style('font-weight', 'bold');

        // Description text is hidden until rect expands
        stepGroup
            .append('text')
            .attr('pointer-events', 'none')
            .style('font-size', 'x-small')
            .style('font-family', 'sans-serif')
            .attr('x', x + xTransform + xOffset + 10)
            .attr('y', y + yTransform + 16)
            .attr('clip-path', 'url(#text-clip-' + element.name.replace(/\s/g, '') + ')')
            .text(element.description)
            .call(wordWrap, 180, element.name.replace(/\s/g, ''));
    }

    function fillerRect(x: any, y: any, g: any) {
        g.append('rect').attr('height', 10).attr('width', 26).attr('x', x).attr('y', y).attr('fill', '#707070');
    }

    function wordWrap(text: any, width: any, clip: any) {
        text.each(function () {
            // @ts-ignore
            const currentText = d3.select(this);
            const words = currentText.text().split(/\s+/).reverse();
            let word;
            let line: any = [];
            let lineNumber = 0;
            const lineHeight = 1; // ems
            const x = currentText.attr('x');
            const y = currentText.attr('y');
            const dy = 0;
            let tspan = currentText
                .text(null)
                .append('tspan')
                .attr('x', x)
                .attr('y', y)
                .attr('dy', dy + 'em');
            word = words.pop();
            while (word) {
                line.push(word);
                tspan.text(line.join(' '));
                // @ts-ignore
                if (tspan.node().getComputedTextLength() > width) {
                    // TODO: potentially null?
                    line.pop();
                    tspan.text(line.join(' '));
                    line = [word];
                    tspan = currentText
                        .append('tspan')
                        .attr('x', x)
                        .attr('y', y)
                        .attr('dy', ++lineNumber * lineHeight + dy + 'em')
                        .attr('clip-path', 'url(#text-clip-' + clip + ')')
                        .text(word);
                }
                word = words.pop();
            }
        });
    }

    useEffect(() => {
        const inputData = currentEntry.current; // TODO: revert to getting from parent? (8th index is PO)
        data = inputData.paths[0].path;

        viewHeight = diamOffset * (data.length / 2) + 50;
        const instructions: any = {};
        instructions.link = '';
        instructions.name = inputData.name + ' Roadmap';
        instructions.description = 'Hover over any step to see more information. Most steps are clickable and will guide you to relevant links';
        // hoveredStep.emit(instructions);
        canvas = makeSVG();
        drawRoadmap(canvas);
    }, []);

    return <div className="roadmap"></div>;
};
