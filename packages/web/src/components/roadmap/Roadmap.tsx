import { faMapSigns, faReply } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import halfmoon from 'halfmoon';
import { useCallback, useEffect, useRef, useState } from 'react';
import nodeInfo from './roadmap.json';
import './Roadmap.scss';
import { RoadmapOverview } from './RoadmapOverview';
import { WorkroleRoadmap } from './WorkroleRoadmap';

// TODO: BUG: need to disable scrolling / zooming in and dragging around the roadmap overview (doesn't appear enabled on the production version)

export const Roadmap = () => {
    const [loaded, setLoaded] = useState(false);
    const [enteredWRRoadmap, setEnteredWRRoadmap] = useState(false);
    const [workroleInstructionsShown, setWorkroleInstructionsShown] = useState(false);
    const currentEntry = useRef(nodeInfo[0]);
    const [renderTrigger, reRenderParent] = useState(nodeInfo[0]); // TODO: could potentially refactor this by changing how the parent component re renders (put re-rendering part into its own child?)

    useEffect(() => {
        if (!loaded) {
            halfmoon.initStickyAlert({
                content: '<ul><li>Hover over work roles for more information.</li><li>Click to view training paths for that work role</li></ul>',
                title: "I'm Interactive!",
                fillType: 'filled',
                alertType: 'alert-primary',
                timeShown: 3000,
            });
            setLoaded(true);
        }
    }, [renderTrigger]);

    const loadRoadmap = useCallback(() => {
        if (currentEntry.current.paths.length > 0) {
            if (!workroleInstructionsShown) {
                setWorkroleInstructionsShown(true);
                halfmoon.initStickyAlert({
                    content: `<ul><li>Hover over road map steps for more information.</li>
                                <li>Click to view relevant links for that work role step</li></ul>`,
                    title: 'Work Role Roadmaps',
                    fillType: 'filled',
                    alertType: 'alert-primary',
                    timeShown: 3000,
                });
            }
            setEnteredWRRoadmap(true);
        } else {
            halfmoon.initStickyAlert({
                content: 'Training paths for ' + currentEntry.current.full_name + ' are not yet available.',
                title: 'Currently Unavailable',
                alertType: 'alert-danger',
                fillType: 'filled',
            });
        }
    }, [workroleInstructionsShown]);

    if (!enteredWRRoadmap) {
        return (
            <div id="roadmap_component">
                <div className="d3-box mx-auto">
                    <RoadmapOverview
                        setCurrentEntry={(newValue: any) => (currentEntry.current = newValue)}
                        loadRoadmap={() => {
                            loadRoadmap();
                        }}
                        reRenderParent={reRenderParent}
                    />
                </div>
                <div className="wr-info mx-auto">
                    <h2>{currentEntry.current.name}</h2>
                    <p className="wr-name">
                        {currentEntry.current.full_name} <span style={{ display: currentEntry.current.has_ttl ? 'none' : '' }}>(NO REQUIREMENTS CURRENTLY MAPPED)</span>
                    </p>
                    <hr className="info-hr" />
                    <p>{currentEntry.current.description}</p>
                </div>
            </div>
        );
    }

    return (
        <div id="roadmap_component">
            <div className="wr-roadmap-titlebar">
                <h2 className="title">
                    Product Owner Roadmap <FontAwesomeIcon icon={faMapSigns} />
                </h2>
                <button className="btn back-button" onClick={() => setEnteredWRRoadmap(!enteredWRRoadmap)}>
                    <FontAwesomeIcon icon={faReply} /> Return to Overview
                </button>
            </div>

            <div className="d3-roadmap-box mx-auto">
                <WorkroleRoadmap currentEntry={currentEntry} />
            </div>
        </div>
    );
};
