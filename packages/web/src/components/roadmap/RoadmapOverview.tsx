import cytoscape from 'cytoscape';
import React, { useEffect } from 'react';
import nodeInfo from './roadmap.json';
import './Roadmap.scss';
const dagre = require('cytoscape-dagre');

const addRoadmapNodes = (cy: cytoscape.Core) => {
    const roadmapData = nodeInfo.sort((a, b) => (a.id > b.id ? 1 : -1)).reverse();
    const uniqueGroups = [...new Set(roadmapData.map((item) => item.group))].sort();
    const subGroups: any = [];

    uniqueGroups.forEach((element) => {
        if (element) {
            cy.add({
                data: {
                    id: element,
                    name: '',
                },
                grabbable: false,
            });
        }
    });

    roadmapData.forEach((element) => {
        if (element.subgroup) {
            if (!subGroups.includes(element.subgroup)) {
                cy.add({
                    data: {
                        id: element.subgroup,
                        name: '',
                        parent: element.group,
                    },
                    grabbable: false,
                });
                subGroups.push(element.subgroup);
            }
            cy.add({
                data: {
                    id: element.id,
                    name: element.name,
                    parent: element.subgroup,
                    has_ttl: element.has_ttl,
                },
                grabbable: false,
            });
        } else {
            cy.add({
                data: {
                    id: element.id,
                    name: element.name,
                    parent: element.group,
                    has_ttl: element.has_ttl,
                },
                grabbable: false,
            });
        }
    });
};

const addRoadmapEdges = (cy: cytoscape.Core) => {
    const roadmapData = nodeInfo.sort();

    roadmapData.forEach((parentNode) => {
        parentNode.upgrades.forEach((childNode) => {
            cy.add({
                data: {
                    source: parentNode.id,
                    target: childNode,
                },
            });
        });
    });
};

type OverviewProps = {
    // TODO: actual typings for some of these
    setCurrentEntry: any;
    loadRoadmap: any;
    reRenderParent: any;
};

export const RoadmapOverview = React.memo(
    (Props: OverviewProps) => {
        const { setCurrentEntry, loadRoadmap, reRenderParent } = Props;

        let cy: cytoscape.Core;

        useEffect(() => {
            cytoscape.use(dagre);
            cy = cytoscape({
                container: document.getElementById('cy'),
                style: [
                    {
                        selector: 'node',
                        style: {
                            label: (ele: any) => ele.data().name,
                            'text-wrap': 'wrap',
                            'text-halign': 'center',
                            'text-valign': 'center',
                            backgroundColor: (node) => (node.data().has_ttl ? '#fff' : '#D0D0D0'),
                            'border-width': (node) => (node.data().has_ttl ? '5' : '8'),
                            'border-style': (node) => (node.data().has_ttl ? 'solid' : 'double'),
                            'border-color': (node) => (node.data().has_ttl ? '#00308F' : '#808080'),
                            shape: 'round-rectangle',
                            width: '300',
                            height: '150',
                            'font-size': '60pt',
                            'font-family': 'sans-serif',
                        },
                    },
                    {
                        selector: ':parent',
                        css: {
                            'text-valign': 'top',
                            'text-halign': 'center',
                            'background-color': '#eee',
                            'border-width': '0',
                            // @ts-ignore
                            padding: '0px',
                            events: 'no',
                        },
                    },
                    {
                        selector: ':selected',
                        css: {
                            'border-color': 'black',
                            'border-width': 5,
                        },
                    },
                    {
                        selector: 'edge',
                        style: {
                            width: 6,
                            'curve-style': 'taxi',
                            'taxi-direction': 'downward',
                            'target-arrow-shape': 'triangle',
                            'target-arrow-color': '#808080',
                            'line-color': '#808080',
                            events: 'no',
                        },
                    },
                    {
                        selector: 'core',
                        // @ts-ignore
                        css: {
                            'active-bg-size': 0,
                        },
                    },
                ],
            });
            addRoadmapNodes(cy);
            addRoadmapEdges(cy);
            cy.selectionType('single');
            cy.boxSelectionEnabled(false);

            // @ts-ignore
            cy.layout({ name: 'dagre', rankSep: 100, nodeSep: 75, ranker: 'network-simplex' }).run();

            cy.bind('click', 'node', (evt) => {
                loadRoadmap();
            });

            cy.bind('mouseover', 'node', (evt) => {
                const nodeId = evt.target.id();
                evt.target.select();
                // console.log( 'hovered ' + nodeId );

                const result = nodeInfo.find(({ id }) => id === nodeId);
                // return hoverEvent.emit(result);
                setCurrentEntry(result);
                reRenderParent(result);
            });

            cy.bind('mouseout', 'node', (evt) => {
                evt.target.unselect();
                // console.log( 'unhovered ' + evt.target.id() );
            });

            cy.userPanningEnabled(false);
            cy.userZoomingEnabled(false);

            // TODO: these are usually on an update angular (ngDoCheck?) method
            // Potentially because the data inside might change?
            // @ts-ignore // also this is un-typed? (is this method from the dagre library?)
            cy.fit(cy, 10);
            cy.center();
        }, [loadRoadmap]);

        return <div id="cy"></div>;
    },
    () => true
);
