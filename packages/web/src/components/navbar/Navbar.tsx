import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faAngleDown, faAngleRight, faMoon } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import halfmoon from 'halfmoon';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { AppState } from '../../state';

type NavbarProps = {
    state: AppState;
    setContactUsOpen: any;
};

export const Navbar = (Props: NavbarProps) => {
    const { state, setContactUsOpen } = Props;
    const { userData } = state;

    const [hoverState, setHoverState] = useState(false);

    // TODO: fix alignment in styling (something in halfmoon might be overriding it...but paddingRight isn't getting set right...)
    const loggedInDivs = (
        <li className={`nav-item dropdown toggle-on-hover`}>
            <a className="nav-link" id="nav-link-dropdown-toggle" style={{ paddingRight: '21 !important' }}>
                {userData?.displayName} <span> &nbsp;</span>
                <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
            </a>
            <div className="dropdown-menu w-200" aria-labelledby="nav-link-dropdown-toggle">
                <a href={`//${window.location.hostname}/auth/logout`} className="dropdown-item" style={{ textAlign: 'left' }}>
                    {/* TODO: they do other frontend stuff when logging out (probably in profile.service.ts)..will likely also need to do that stuff...(reducer?) (or hard refresh for now...) */}
                    Logout{' '}
                </a>
            </div>
        </li>
    );

    const loggedOutDivs = (
        <div>
            {/* TODO: STYLE: make this change color when you hover over it */}
            <a href={`//${window.location.hostname}/auth/gitlab`} style={{ color: 'inherit' }}>
                <li className="nav-item" style={{ textAlign: 'center', paddingTop: '4px' }}>
                    <FontAwesomeIcon icon={faGitlab} />
                </li>
            </a>
        </div>
    );

    return (
        <nav
            className="navbar"
            style={{
                justifyContent: 'space-between',
                fontFamily: 't26-carbon, monospace',
            }}
        >
            <div className="float-left main-title">
                <Link to="/home" className="navbar-brand float-left">
                    <div id="step1">
                        <img src={`http://${window.location.host}/shadow_warrior.svg`} className="float-left mt-5" id="step2" />
                        90 COS Training & Evals &nbsp;
                    </div>
                </Link>
                <FontAwesomeIcon icon={faMoon} size="xs" className="nav-link px-0 darkness-icon" onClick={() => halfmoon.toggleDarkMode()} />
            </div>

            <ul className="navbar-nav float-right d-none d-md-flex">
                <li className="nav-item">
                    <Link id="step3" to="/mttl" className="nav-link">
                        MTTL
                    </Link>
                </li>

                <li className={`nav-item dropdown toggle-on-hover`}>
                    <a id="step4" className="nav-link">
                        Docs &nbsp;
                        <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
                    </a>
                    <div className="dropdown-menu p-0" aria-labelledby="nav-link-dropdown-toggle">
                        <a className="dropdown-item" target="_blank" href="/documentation">
                            MTTL
                        </a>
                        <a className="dropdown-item" target="_blank" href="https://90cos.gitlab.io/cyv/admin/index.html">
                            Eval
                        </a>
                        <div style={{ textAlign: 'center' }} className="dropdown-submenu dropdown-item" onMouseEnter={() => setHoverState(true)} onMouseLeave={() => setHoverState(false)}>
                            Study
                            <FontAwesomeIcon icon={faAngleRight} className="float-right" />
                            <div
                                className="sub-menu card p-0 w-100"
                                style={{
                                    visibility: hoverState ? 'visible' : 'hidden',
                                    display: hoverState ? 'block' : '',
                                }}
                            >
                                <a className="dropdown-item" target="_blank" href="https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html">
                                    CCD MQF
                                </a>
                                <a className="dropdown-item" target="_blank" href="https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html">
                                    PO MQF
                                </a>
                            </div>
                        </div>
                    </div>
                </li>

                <li id="step5" className={`nav-item dropdown toggle-on-hover`}>
                    <a id="nav-link-dropdown-toggle" className="nav-link" target="_blank">
                        Visualizations <span> &nbsp;</span>
                        <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
                    </a>
                    <div className="dropdown-menu w-200" aria-labelledby="nav-link-dropdown-toggle">
                        <a target="_blank" href="https://embed.kumu.io/8b3f4b0521168c52778467b3e692e6c2" className="dropdown-item">
                            Source to KSAT
                        </a>
                        <a target="_blank" href="https://embed.kumu.io/16150a45739ac2f74274b77fa2cad910" className="dropdown-item">
                            Owner to KSAT
                        </a>
                        <a target="_blank" href="https://embed.kumu.io/546a25cd04adf28a1f5c62df8b4eea5d" className="dropdown-item">
                            Work-Role to KSAT
                        </a>
                    </div>
                </li>

                <li id="step8" className={`nav-item dropdown toggle-on-hover`}>
                    <a className="nav-link" id="nav-link-dropdown-toggle">
                        Training <span> &nbsp;</span>
                        <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
                    </a>
                    <div className="dropdown-menu w-200" aria-labelledby="nav-link-dropdown-toggle">
                        <a target="_blank" href="https://gitlab.com/90cos/cyt/training/modules" className="dropdown-item">
                            90COS Training
                        </a>
                        <a target="_blank" href="https://gitlab.com/39iosdev/ccd-iqt/idf" className="dropdown-item">
                            39IOS IDF
                        </a>
                    </div>
                </li>

                <li id="step9" className="nav-item">
                    <Link id="step3" to="/roadmap" className="nav-link">
                        Roadmap
                    </Link>
                </li>

                <li id="step10" className={`nav-item dropdown toggle-on-hover`}>
                    <a className="nav-link" id="nav-link-dropdown-toggle">
                        Metrics <span> &nbsp;</span>
                        <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
                    </a>
                    <div className="dropdown-menu w-200" aria-labelledby="nav-link-dropdown-toggle">
                        <Link to="/all-metrics" className="dropdown-item">
                            View All
                        </Link>
                        <Link to="/metrics" className="dropdown-item">
                            Metrics Lookup
                        </Link>
                        <Link to="/misc-tables" className="dropdown-item">
                            Miscellaneous Tables
                        </Link>
                    </div>
                </li>

                <li id="step11" className={`nav-item dropdown toggle-on-hover`}>
                    <a className="nav-link" id="nav-link-dropdown-toggle">
                        VTR<span> &nbsp;</span>
                        <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
                    </a>
                    <div className="dropdown-menu dropdown-menu-right w-200" aria-labelledby="nav-link-dropdown-toggle">
                        <a target="_blank" href="https://90cos.gitlab.io/cyt/training-systems/vtr-system/" className="dropdown-item">
                            VTR Docs
                        </a>
                        <a target="_blank" href="https://www.youtube.com/playlist?list=PL79OPuC-kzYx_-GpeKYDtP8XPYh7dYGxm" className="dropdown-item">
                            VTR Videos
                        </a>
                    </div>
                </li>

                <li id="step12" className={`nav-item dropdown toggle-on-hover`}>
                    <a className="nav-link" id="nav-link-dropdown-toggle">
                        Support <span> &nbsp;</span>
                        <FontAwesomeIcon icon={faAngleDown} className="mt-4" />
                    </a>
                    <div className="dropdown-menu dropdown-menu-right w-200" aria-labelledby="nav-link-dropdown-toggle">
                        <span
                            id="contact-us-cursor"
                            className="dropdown-item"
                            style={{ textAlign: 'center' }}
                            onClick={() => {
                                if (!userData) {
                                    alert('must be logged in to contact...');
                                    return;
                                }
                                setContactUsOpen(true);
                            }}
                        >
                            Contact Us
                        </span>
                        <Link to="/faq" className="dropdown-item">
                            FAQ
                        </Link>
                        <a target="_blank" href="https://www.gitlab.com/spenceradolph/mttl-refactor" className="dropdown-item">
                            View Code Repo
                        </a>
                    </div>
                </li>

                {userData ? loggedInDivs : loggedOutDivs}
            </ul>
        </nav>
    );
};
