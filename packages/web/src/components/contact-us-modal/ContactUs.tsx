import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import { api } from '../../state';

type ContactUsProps = {
    contactUsOpen: any;
    setContactUsOpen: any;
};

export const ContactUs = (Props: ContactUsProps) => {
    const { contactUsOpen, setContactUsOpen } = Props;

    const [reason, setReason] = useState('');
    const [explanation, setExplanation] = useState('');
    const [notes, setNotes] = useState('');

    const submitContactUs = async () => {
        const results = await api.submitContactUs(reason, explanation, notes);
        if (results.error) return; // TODO: error handling (not authenticated and other error: '')
        alert('sent the message!');
        setContactUsOpen(false);
    };

    // TODO: setting 'required' but not doing the actual checks? (could be a potential bug...also on the other modals with forms...)

    return (
        <Dialog open={contactUsOpen} onClose={() => setContactUsOpen(false)}>
            <DialogTitle>Contact Us</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="reason_text"
                    label="Reason for contacting us"
                    fullWidth
                    variant="standard"
                    required
                    value={reason}
                    onChange={(e) => {
                        setReason(e.target.value);
                    }}
                />
                <TextField
                    margin="dense"
                    id="explanation_text"
                    label="Explanation"
                    fullWidth
                    required
                    variant="standard"
                    value={explanation}
                    onChange={(e) => {
                        setExplanation(e.target.value);
                    }}
                />
                <TextField
                    margin="dense"
                    id="notes_text"
                    label="Notes, Contact Info, etc. (Optional)"
                    fullWidth
                    variant="standard"
                    value={notes}
                    onChange={(e) => {
                        setNotes(e.target.value);
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setContactUsOpen(false)}>Close</Button>
                <Button onClick={() => submitContactUs()}>Submit</Button>
            </DialogActions>
        </Dialog>
    );
};
