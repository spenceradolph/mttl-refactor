import { Chart } from 'chart.js'; // TODO: update these packages (need old version for easy port from angular to react...)
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { useEffect } from 'react';
import { AppState, Dispatch, api } from '../../state';
import './AllMetrics.scss';
import filters from './filters.json';

type AllMetricsProps = {
    state: AppState;
    dispatch: Dispatch;
};

export const AllMetrics = (Props: AllMetricsProps) => {
    const { state, dispatch } = Props;
    const { mttl } = state;

    const fetchMetrics = async () => {
        const mttlData = await api.fetchMttl();
        dispatch({ type: 'update-mttl', mttlData });
    };

    useEffect(() => {
        if (!mttl) fetchMetrics();
        if (mttl) generateCharts();
    }, [mttl]);

    function coverage_transform(items: any[], field: string): number[] {
        let itemList;

        // eval_links, training_links

        switch (field) {
            case 't_and_e':
                itemList = items.filter((el) => {
                    if (el.eval_links.length > 0 && el.training_links.length > 0) {
                        return el;
                    }
                });
                break;
            case 't_o':
                itemList = items.filter((el) => {
                    if (el.eval_links.length === 0 && el.training_links.length > 0) {
                        return el;
                    }
                });
                break;
            case 'e_o':
                itemList = items.filter((el) => {
                    if (el.eval_links.length > 0 && el.training_links.length === 0) {
                        return el;
                    }
                });
                break;
            case 'none':
                itemList = items.filter((el) => {
                    if (el.eval_links.length === 0 && el.training_links.length === 0) {
                        return el;
                    }
                });
                break;
        }

        // @ts-ignore
        return itemList;
    }

    function filter_transform(items: any[], filterObj: any[]): any[] {
        if (!filterObj) {
            return items;
        }
        if (filterObj.length > 0) {
            // filter obj column : search , search string
            // items are all the KSATs
            let itemsFiltered = items;
            filterObj.forEach((elem) => {
                // console.log("elem that we are iterating", elem );
                // elem = column search and filterText
                if (elem.column === 'search') {
                    // @ts-ignore
                    const tempArray = [];
                    itemsFiltered.forEach((element) => {
                        //   for (var key in element) {
                        //     if (element.hasOwnProperty(key)) {
                        //        if (key === 'eval_links'){
                        //         //  console.log("eval_links", element.eval_links)
                        //          for (var link in element.eval_links){
                        //            console.log("link is ", link, " type ", typeof(link))
                        //          }
                        //        }
                        //       //  else  if (key === 'training_links'){
                        //       //   for (var link in element.eval_links){
                        //       //     console.log("link is ", link)
                        //       //   }
                        //       // }
                        //     }
                        // }
                        if (element.description.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
                            tempArray.push(element);
                            // @ts-ignore
                        } else if (element.topic.map((value) => value.toLowerCase()).indexOf(elem.filterText.toLowerCase()) !== -1) {
                            tempArray.push(element);
                        } else if (element.comments.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
                            tempArray.push(element);
                        } else if (element.ksat_id.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
                            tempArray.push(element);
                        }
                        // else if (element._id.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
                        //   tempArray.push(element);
                        // }
                        else {
                            if (element.hasOwnProperty('eval_links') && element.eval_links.length > 0) {
                                // @ts-ignore
                                element.eval_links.forEach((el) => {
                                    if (el.title.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
                                        tempArray.push(element);
                                    }
                                });
                            }

                            if (element.hasOwnProperty('training_links') && element.training_links.length > 0) {
                                // @ts-ignore
                                element.training_links.forEach((el) => {
                                    if (el.title.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
                                        tempArray.push(element);
                                    }
                                });
                            }
                        }
                    });
                    // @ts-ignore
                    itemsFiltered = [...new Set(tempArray)];
                } else if (elem.column === 'work_roles') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        if (elem.filterText === 'No Work-Role') {
                            if (it.work_roles === undefined || it.work_roles.length === 0) {
                                return true;
                            }
                        } else if (it.work_roles.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'courses') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        // console.log(it['work_roles'])
                        if (it.courses.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'parents') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        // console.log(it['work_roles'])
                        if (it.parents.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'ksats_id') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        // console.log(it['work_roles'])
                        if (it.ksat_id.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'description') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        // console.log(it['work_roles'])
                        if (it.description.toLowerCase().indexOf(elem.filterText.toLowerCase()) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'coverage') {
                    let coverage = '';
                    switch (elem.filterText) {
                        case 'Training and Eval': {
                            coverage = 't_and_e';
                            break;
                        }
                        case 'Eval Only': {
                            coverage = 'e_o';
                            break;
                        }
                        case 'Training Only': {
                            coverage = 't_o';
                            break;
                        }
                        default: {
                            coverage = 'none';
                            break;
                        }
                    }
                    itemsFiltered = coverage_transform(itemsFiltered, coverage);
                } else if (elem.column === 'requirement_owner') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        if (it.requirement_owner.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'requirement_src') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        if (it.requirement_src.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'topic') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        // @ts-ignore
                        const lowerCaseNames = it.topic.map((value) => value.toLowerCase());
                        if (lowerCaseNames.indexOf(elem.filterText.toLowerCase()) !== -1) {
                            return true;
                        }
                    });
                } else if (elem.column === 'oam') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        if (it.hasOwnProperty('oam') && it.oam !== '' && it.oam !== undefined && it.oam !== null) {
                            // @ts-ignore
                            const lowerCaseNames = it.oam.map((value) => (value !== null && value !== '' ? value.toLowerCase() : value));
                            if (lowerCaseNames.indexOf(elem.filterText.toLowerCase()) !== -1) {
                                return true;
                            } else if (lowerCaseNames.join().includes(elem.filterText.toLowerCase())) {
                                return true;
                            }
                            // some, find, match =>  no luck;  ES6 will have includes for Arrays
                        }
                    });
                } else if (elem.column === 'ksat_type') {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        if (it.ksat_type.indexOf(elem.filterText) !== -1) {
                            return true;
                        }
                    });
                } else {
                    itemsFiltered = itemsFiltered.filter((it) => {
                        if (it[elem.column].toLowerCase() === elem.filterText.toLowerCase()) {
                            return true;
                        }
                    });
                }
            });
            return itemsFiltered; // i.e. by Topic
        } else {
            return items;
        }
    }

    function coverageText(filter: any): string {
        let output;
        if (filter.filter_type === 'mttl') {
            output = 'Total MTTL Coverage';
        } else if (filter.filter_type === 'work_roles') {
            output = filter.filter_text + ' Work Role Coverage';
        } else {
            output = filter.filter_text + ' Coverage';
        }
        return output;
    }

    function getData(filter: any): any {
        const filterType = filter.filter_type;
        const filterText = filter.filter_text;

        // @ts-ignore
        let filterObj = [];
        const output: { [key: string]: any } = {
            total: 0,
            covered: 0,
            evalCovered: 0,
            trnCovered: 0,
            noCoverage: 0,
        };
        if (filterType !== 'mttl') {
            filterObj = [{ column: filterType, filterText: filterText }];
        }
        if (mttl) {
            // @ts-ignore
            const filteredKsats = filter_transform(mttl, filterObj);
            output.total = filteredKsats.length;
            output.covered = coverage_transform(filteredKsats, 't_and_e').length;
            output.evalCovered = coverage_transform(filteredKsats, 'e_o').length;
            output.trnCovered = coverage_transform(filteredKsats, 't_o').length;
            output.noCoverage = coverage_transform(filteredKsats, 'none').length;
        }

        return output;
    }

    function generateCharts(): void {
        filters.forEach((filter) => {
            const data = getData(filter);
            const ctx = document.getElementById(filter.filter_text);
            calculateBarChart(data.covered, data.evalCovered, data.trnCovered, data.noCoverage, ctx);
        });
    }

    function calculateBarChart(covered: number, evalCovered: number, trnCovered: number, noCoverage: number, ctx: any): void {
        ctx.height = 45;
        const total = covered + noCoverage + trnCovered + evalCovered;
        const chart = new Chart(ctx, {
            // @ts-ignore
            type: 'horizontalBar',
            plugins: [ChartDataLabels],
            data: {
                labels: ['KSATs Covered'],
                datasets: [
                    {
                        label: 'Training and Eval (' + covered + '/' + total + ')',
                        data: [covered],
                        backgroundColor: '#6EC664',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                    {
                        label: 'Eval (' + (evalCovered + covered) + '/' + total + ')',
                        data: [evalCovered],
                        backgroundColor: '#F0AB00',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                    {
                        label: 'Training (' + (trnCovered + covered) + '/' + total + ')',
                        data: [trnCovered],
                        backgroundColor: '#0066CC',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                    {
                        label: 'No Coverage (' + noCoverage + '/' + total + ')',
                        data: [noCoverage],
                        backgroundColor: '#707070',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                ],
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false,
                },
                scales: {
                    // @ts-ignore
                    xAxes: [
                        {
                            stacked: true,
                            ticks: { max: total },
                            display: false,
                            gridLines: {
                                display: false,
                            },
                        },
                    ],
                    // @ts-ignore
                    yAxes: [{ display: false, stacked: true }],
                },
                legend: {
                    display: false,
                    labels: {
                        fontSize: 20,
                    },
                },
                plugins: {
                    // Change options for ALL labels of THIS CHART
                    datalabels: {
                        color: '#ffffff',
                        formatter: (value, contxt) => {
                            const percentage = Math.round((value / total) * 100);
                            if (percentage) {
                                return percentage + '%';
                            }
                            return '';
                        },
                        font: {
                            size: 20,
                        },
                    },
                },
            },
        });
    }

    const coverageContainers = filters.map((filter, index) => {
        return (
            <div className="coverage_container" key={index}>
                <div className="coverage_info">
                    <h3>{coverageText(filter)}</h3>
                </div>
                <br />
                <div className="key">
                    <div className="coverage_key">
                        <div className="box CoveredBox"></div>
                        <span className="keyText">
                            Training and Eval
                            <br />
                            {getData(filter).covered}/{getData(filter).total}
                        </span>
                    </div>
                    <div className="coverage_key">
                        <div className="box TrnBox"></div>
                        <span className="keyText">
                            Training
                            <br />
                            {getData(filter).covered + getData(filter).trnCovered}/{getData(filter).total}
                        </span>
                    </div>
                    <div className="coverage_key">
                        <div className="box EvalBox"></div>
                        <span className="keyText">
                            Eval
                            <br />
                            {getData(filter).covered + getData(filter).evalCovered}/{getData(filter).total}
                        </span>
                    </div>
                    <div className="coverage_key">
                        <div className="box NoCoverageBox"></div>
                        <span className="keyText">
                            No Coverage
                            <br />
                            {getData(filter).noCoverage}/{getData(filter).total}
                        </span>
                    </div>
                </div>
                <canvas id={filter.filter_text} className="data_canvas"></canvas>
            </div>
        );
    });

    return <div className="all-metrics-component-style">{coverageContainers}</div>;
};
