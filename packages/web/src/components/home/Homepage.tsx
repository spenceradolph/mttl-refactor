import { Tooltip } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { AppState } from '../../state';
import './Homepage.scss';

import { Steps, Step } from 'intro.js-react';

const steps: Step[] = [
    {
        intro: "Welcome to 90th Cyber Operations Squadron's \nMaster Training Task List",
    },
    {
        element: '#step2',
        intro: 'Clicking here returns to the landing page',
        position: 'bottom',
    },
    {
        element: '#step3',
        intro: 'MTTL - is the interactive database listing work role requirements by Knowledge, Skills, Abilities, and Tasks (KSATs)',
        position: 'bottom',
    },
    {
        element: '#step4',
        intro:
            "Links to this website's official documentation and evaluation related resources, including recommended study materials" +
            'for 90th work roles, evaluation scheduling, and information about work role requirements.',
        position: 'bottom',
    },
    {
        element: '#step5',
        intro: 'This is a Kumu network graph of the relationships among KSATs',
        position: 'bottom',
    },
    {
        intro: 'CYT can recommend a variety of training resources. CYV can help with any evaluation needs.',
    },
    {
        element: '#step8',
        intro: 'Training resources provided by 90 COS, 39 IOS, and others.',
        position: 'bottom',
    },
    {
        element: '#step9',
        intro: 'A training road map illustrating the training pathways by work role.',
        position: 'bottom',
    },
    {
        element: '#step10',
        intro: 'Analysis of training and evaluation coverage organized by KSAT and work role.',
        position: 'bottom',
    },
    {
        element: '#step11',
        intro: 'Learn about the Virtual Training Records here.',
        position: 'bottom',
    },
    {
        element: '#step12',
        intro: 'Please contact CYT with any questions.',
        position: 'bottom',
    },
];

type HomepageProps = {
    userData: AppState['userData'];
    setContactUsOpen: any;
};

export const Homepage = (Props: HomepageProps) => {
    const { userData, setContactUsOpen } = Props;

    const navigate = useNavigate();

    return (
        <div className="homepage-component-style">
            <div id="homepage">
                <h1>90th Cyberspace Operations Squadron</h1>
                <Tooltip title="Click here to learn more about the 90th">
                    <a href="https://www.afhra.af.mil/About-Us/Fact-Sheets/Display/Article/862054/90-cyberspace-operations-squadron-afspc/">
                        <img src="shadow_warrior.svg" width="390" />
                    </a>
                </Tooltip>
                <h3>
                    <b>Mission</b>: Accelerate Global Vigilance, Reach, and Power by rapidly developing joint cyber capabilities
                </h3>
                <h3>
                    <b>Vision</b>: A ready Cyber Development Force enabling tomorrow's operations with capabilities delivered today
                </h3>
            </div>
            <div className="text-align-left float-left d-inline-block" id="info">
                E-mail questions, comments, or concerns to <br />
                <a href="mailto:90IOS.DOT.INBOX@us.af.mil" style={{ color: 'inherit' }}>
                    90IOS.DOT.INBOX@us.af.mil{' '}
                </a>
                or{' '}
                <span
                    id="contactUs"
                    onClick={() => {
                        if (!userData) {
                            alert('must be logged in to contact');
                            return;
                        }
                        setContactUsOpen(true);
                    }}
                >
                    {' '}
                    Contact Us
                </span>
                <br />
                <Link to="/disclaimer" id="externalLinks">
                    <em>External Links Disclaimer</em>
                </Link>
                <br />
            </div>
            <Steps
                enabled={localStorage.getItem('mttl-introJsPartOne') === null}
                steps={steps}
                initialStep={0}
                onExit={() => {
                    localStorage.setItem('mttl-introJsPartOne', 'Watched!');
                    // navigate('/mttl'); // TODO: fix bug where unable to go to /misc-tables from homepage, get sent to /mttl instead because this is triggered?
                }}
            />
        </div>
    );
};
