import { AppState } from '../../state';
import './MiscTables.scss';

type NoWrMetricsProps = {
    ksats_no_wr_body: AppState['ksats_no_work_role'];
};

export const NoWrMetrics = (Props: NoWrMetricsProps) => {
    const { ksats_no_wr_body } = Props;

    const tasksArray: any = [];
    const abilitiesArray: any = [];
    const skillsArray: any = [];
    const knowledgeArray: any = [];

    (ksats_no_wr_body ?? []).forEach((element: any) => {
        if (element.ksat_id.charAt(0) === 'T') {
            tasksArray.push(element.ksat_id);
        } else if (element.ksat_id.charAt(0) === 'A') {
            abilitiesArray.push(element.ksat_id);
        } else if (element.ksat_id.charAt(0) === 'S') {
            skillsArray.push(element.ksat_id);
        } else if (element.ksat_id.charAt(0) === 'K') {
            knowledgeArray.push(element.ksat_id);
        }
    });

    const tasks = tasksArray.join(', ');
    const abilities = abilitiesArray.join(', ');
    const skills = skillsArray.join(', ');
    const knowledge = knowledgeArray.join(', ');

    return (
        <>
            <h2>KSATs without Work Roles</h2>
            <div id="no-wr-metrics-component">
                <div className="mttl-table">
                    <table className="table table-striped">
                        <thead className="mttl-head">
                            <tr>
                                <th></th>
                                <th className="header">Tasks</th>
                                <th></th>
                                <th className="header">Abilities</th>
                                <th></th>
                                <th className="header">Skills</th>
                                <th></th>
                                <th className="header">Knowledge</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody className="mttl-content">
                            <tr></tr>
                            <tr>
                                <td width="50"></td>
                                <td className="label-expander">{tasks}</td>
                                <td width="50"></td>
                                <td className="label-expander">{abilities}</td>
                                <td width="50"></td>
                                <td className="label-expander">{skills}</td>
                                <td width="50"></td>
                                <td className="label-expander">{knowledge}</td>
                                <td width="50"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
};
