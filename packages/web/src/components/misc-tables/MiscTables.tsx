import './MiscTables.scss';
import { NoWrMetrics } from './NoWrMetrics';
import { WrMetrics } from './WrMetrics';
import { OamMetrics } from './OamMetrics';
import { useEffect } from 'react';
import { api, AppState, Dispatch } from '../../state';

type MiscTablesProps = {
    state: AppState;
    dispatch: Dispatch;
};

export const MiscTables = (Props: MiscTablesProps) => {
    const { state, dispatch } = Props;
    const { milestones, ksats_by_work_role, ksats_no_work_role, autocomplete } = state;

    const fetchMilestones = async () => {
        const milestoneData = await api.fetchMilestones();
        dispatch({ type: 'update-milestones', milestoneData });
    };

    const fetchKsatsByWorkRole = async () => {
        const ksatData = await api.fetchKsatsByWorkRole();
        dispatch({ type: 'update-ksats-by-work-role', ksatData });
    };

    const fetchKsatsNoWorkRole = async () => {
        const ksatData = await api.fetchKsatsNoWorkRole();
        dispatch({ type: 'update-ksats-no-work-role', ksatData });
    };

    const fetchAutocomplete = async () => {
        const autocompleteData = await api.fetchAutocomplete();
        dispatch({ type: 'update-autocomplete', autocompleteData });
    };

    useEffect(() => {
        if (!milestones) fetchMilestones();
        if (!ksats_by_work_role) fetchKsatsByWorkRole();
        if (!ksats_no_work_role) fetchKsatsNoWorkRole();
        if (!autocomplete) fetchAutocomplete();
    }, []);

    return (
        <div className="misc-tables-component-style">
            <div className="card">
                <WrMetrics ksats_by_work_role_json={ksats_by_work_role} autocomplete={autocomplete} />
            </div>
            <div className="card">
                <OamMetrics milestones_json={milestones} autocomplete={autocomplete} />
            </div>
            <div className="card">
                <NoWrMetrics ksats_no_wr_body={ksats_no_work_role} />
            </div>
        </div>
    );
};
