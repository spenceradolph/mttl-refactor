import { faSortDown, faSortUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Autocomplete, Divider, FormControl, FormLabel, MenuItem, Select, TextField } from '@mui/material';
import { ChangeEvent, useEffect, useState } from 'react';
import { AppState } from '../../state';
import './WrMetrics.scss';
import { FilterType } from '../mttl/ksatFilter';
import ReactPaginate from 'react-paginate';

type WrMetricsProps = {
    ksats_by_work_role_json: AppState['ksats_by_work_role'];
    autocomplete: AppState['autocomplete'];
};

export const WrMetrics = (Props: WrMetricsProps) => {
    const { ksats_by_work_role_json, autocomplete } = Props;

    const [filters, setFilters] = useState<FilterType[]>([]);

    const [selectedDropdown, setSelectedDropdown] = useState('');
    const [wrSort, setWrSort] = useState(true);

    useEffect(() => {}, [selectedDropdown]);

    const filterOptions = ['Work Role', 'Tasks', 'Abilities', 'Skills', 'Knowledge'];

    const filtered_ksats = [...(ksats_by_work_role_json ?? [])].filter((ksat) => {
        for (const filter of filters) {
            if (filter.column === 'work_roles') {
                if (ksat.work_role !== filter.value) return false;
            }
            if (filter.column === 'tasks') {
                if (!ksat.tasks.includes(filter.value)) return false;
            }
            if (filter.column === 'abilities') {
                if (!ksat.abilities.includes(filter.value)) return false;
            }
            if (filter.column === 'skills') {
                if (!ksat.skills.includes(filter.value)) return false;
            }
            if (filter.column === 'knowledge') {
                if (!ksat.knowledge.includes(filter.value)) return false;
            }
        }

        return true;
    });

    // TODO: likely don't need a copy like this, and can append to previous filter
    const sorted_ksats = [...filtered_ksats].sort((a: any, b: any) => {
        const compareA = wrSort ? a.work_role.toUpperCase() : b.work_role.toUpperCase();
        const compareB = wrSort ? b.work_role.toUpperCase() : a.work_role.toUpperCase();
        if (compareA < compareB) return -1;
        if (compareB < compareA) return 1;
        return 0;
    });

    let wrData: any[] = [];
    for (let item of sorted_ksats ?? []) {
        if (!item.hasOwnProperty('skills') || item.skills === null || item.skills === undefined) {
            item.skills = [];
        }
        if (!item.hasOwnProperty('abilities') || item.abilities === null || item.abilities === undefined) {
            item.abilities = [];
        }
        if (!item.hasOwnProperty('tasks') || item.tasks === null || item.tasks === undefined) {
            item.tasks = [];
        }
        if (!item.hasOwnProperty('knowledge') || item.knowledge === null || item.knowledge === undefined) {
            item.knowledge = [];
        }
        if (item.knowledge.length > 0 || item.abilities.length > 0 || item.tasks.length > 0 || item.skills.length > 0) {
            wrData.push({
                'work-role': item.work_role,
                work_role: item.work_role,
                knowledge: item.knowledge,
                skills: item.skills,
                abilities: item.abilities,
                tasks: item.tasks,
            });
        }
    }

    const getRealFilter = (filter: string) => {
        switch (filter) {
            case 'Work Role':
                return 'work_roles';
            case 'Milestones':
                return 'oam';
            case 'Tasks':
                return 'tasks';
            case 'Abilities':
                return 'abilities';
            case 'Skills':
                return 'skills';
            case 'Knowledge':
                return 'knowledge';
            default:
                return '';
        }
    };

    const [currentItems, setCurrentItems] = useState<any[]>([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(5);

    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(wrData?.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(wrData?.length / itemsPerPage));
        // TODO: BUG: filters gets updated before chance to calculate the new currentItems? bug when sometimes it doesn't appear
    }, [itemOffset, itemsPerPage, wrSort, ksats_by_work_role_json, filters]);

    const handlePageClick = (event: any) => {
        const newOffset = (event.selected * itemsPerPage) % wrData?.length;
        setItemOffset(newOffset);
    };

    const selectNumPerPage = (event: ChangeEvent<HTMLSelectElement>) => {
        event.preventDefault();
        setItemsPerPage(parseInt(event.currentTarget.value));
        event.stopPropagation();
    };

    return (
        <>
            <h2>Work Roles</h2>
            <div id="wr-metrics-component">
                <div className="navbar second-level">
                    <div className="form-control w-auto mr-5">
                        <FormControl className="w-150" id="search-input">
                            <FormLabel>Filter By</FormLabel>
                            <Select label="Filter" value={selectedDropdown} onChange={(e) => setSelectedDropdown(e.target.value)}>
                                {filterOptions.map((filter, index) => {
                                    return (
                                        <MenuItem value={filter} key={index}>
                                            {filter}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </div>
                    <div className="form-control filter-field mr-5">
                        <div id="search-input">
                            <div className="ml-5 search-boxes my-auto custom-mat">
                                <Autocomplete
                                    options={
                                        autocomplete === null || selectedDropdown === ''
                                            ? []
                                            : selectedDropdown === 'Work Role'
                                            ? autocomplete[`${'work_roles'}`]
                                            : selectedDropdown === 'Tasks'
                                            ? autocomplete[`${'ksats_id'}`].filter((ksat_id: string) => ksat_id.charAt(0) === 'T')
                                            : selectedDropdown === 'Abilities'
                                            ? autocomplete[`${'ksats_id'}`].filter((ksat_id: string) => ksat_id.charAt(0) === 'A')
                                            : selectedDropdown === 'Skills'
                                            ? autocomplete[`${'ksats_id'}`].filter((ksat_id: string) => ksat_id.charAt(0) === 'S')
                                            : selectedDropdown === 'Knowledge'
                                            ? autocomplete[`${'ksats_id'}`].filter((ksat_id: string) => ksat_id.charAt(0) === 'K')
                                            : ''
                                    }
                                    renderInput={(params) => <TextField {...params} label="Filter" style={{ width: '50%', float: 'left' }} />}
                                    onChange={(e) => {
                                        // @ts-ignore
                                        if (e.target.textContent === '') return;
                                        // if (!filters.includes())
                                        // console.log(e);
                                        // @ts-ignore // TODO: better setup for values and onChange
                                        setFilters([...filters, { column: getRealFilter(selectedDropdown), value: e.target.textContent }]);
                                    }}
                                    isOptionEqualToValue={(option: any, value: any) => option.value === value.value} // used to disable warning, happens when previous selection still in TextField and isn't actually an option
                                />
                                {filters.map((filter, index) => {
                                    return (
                                        <div
                                            key={index}
                                            onClick={() => {
                                                // TODO: put this function outside of this?
                                                setFilters(filters.filter((thisFilter) => thisFilter !== filter));
                                            }}
                                        >
                                            {filter.value}
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                    <Divider orientation="vertical" className="vert-divider bg-light-dm ml-10 mr-10" />
                    <div id="clear_filter">
                        <button
                            id="clear_filter"
                            className="btn"
                            onClick={() => {
                                setFilters([]);
                            }}
                        >
                            Clear All
                        </button>
                    </div>
                </div>
                <div className="mttl-table">
                    <h1></h1>
                    <table className="table table-striped">
                        <thead className="mttl-head">
                            <tr>
                                <th></th>
                                <th className="header-selectable" onClick={() => setWrSort(!wrSort)}>
                                    Work Role
                                    {wrSort ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
                                </th>
                                <th></th>
                                <th className="header">Tasks</th>
                                <th></th>
                                <th className="header">Abilities</th>
                                <th></th>
                                <th className="header">Skills</th>
                                <th></th>
                                <th className="header">Knowledge</th>
                            </tr>
                        </thead>
                        <tbody className="mttl-content">
                            {currentItems.map((wr, index) => {
                                return (
                                    <tr key={index}>
                                        <td width="50"></td>
                                        <td style={{ overflowY: 'auto' }}>{wr['work_role']}</td>
                                        <td width="10"></td>
                                        <td style={{ overflowY: 'auto' }}>{wr.tasks.join(', ')}</td>
                                        <td width="10"></td>
                                        <td style={{ overflowY: 'auto' }}>{wr.abilities.join(', ')}</td>
                                        <td width="10"></td>
                                        <td style={{ overflowY: 'auto' }}>{wr.skills.join(', ')}</td>
                                        <td width="10"></td>
                                        <td style={{ overflowY: 'auto' }}>{wr.knowledge.join(', ')}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>

                <div className="footer" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <div className="pagination-size" style={{ width: '20%', display: 'float' }}>
                        <div>Work Roles Per Page:</div>
                        <select className="form-control size-controller" onChange={(e) => selectNumPerPage(e)} value={itemsPerPage}>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={5}>5</option>
                            <option value={10}>10</option>
                        </select>
                    </div>
                    <div id="paginate2id" style={{ margin: '0 auto', left: '50%' }}>
                        <ReactPaginate
                            nextLabel="next >"
                            onPageChange={handlePageClick}
                            pageRangeDisplayed={3}
                            marginPagesDisplayed={1}
                            pageCount={pageCount}
                            previousLabel="< previous"
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            previousLinkClassName="page-link"
                            nextClassName="page-item"
                            nextLinkClassName="page-link"
                            breakLabel="..."
                            breakClassName="page-item"
                            breakLinkClassName="page-link"
                            containerClassName="pagination2"
                            activeClassName="active"
                        />
                    </div>
                    <span className="results-numbers" style={{ float: 'right', display: 'float' }}>
                        Work Role {itemOffset + 1}-{Math.min(wrData.length, itemOffset + itemsPerPage)} of {wrData.length}
                        &nbsp; &nbsp;{' '}
                    </span>
                </div>
            </div>
        </>
    );
};
