import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormLabel } from '@mui/material';
import FAQData from './faq.json';
import './Faq.scss';

type FaqProps = {
    setContactUsOpen: any;
    userData: any;
};

export const Faq = (Props: FaqProps) => {
    const { setContactUsOpen, userData } = Props;

    return (
        <div className="faq-component-style" id="faq">
            <div className="form-control h-auto pt-0 pb-0 d-inline-block">
                <h3>
                    <FontAwesomeIcon icon={faSearch} className="float-left mr-10 mt-5" />
                </h3>
                <div className="my-auto">
                    {/* <Input className="search-box float-left mt-0 mb-0 pt-0 pb-0"> */}
                    {/* <FormLabel>How can we help you?</FormLabel> */}
                    {/* <input /> */}
                    {/* </Input> */}
                    <div>This search bar is useless and styled with angular specific components :( - TODO: implement functionality / styling</div>
                </div>
            </div>
            <div className="faq-wrapper table-overflow-y">
                <div
                    style={{
                        flexFlow: 'row wrap',
                        boxSizing: 'border-box',
                        display: 'flex',
                        placeContent: 'stretch space-around',
                        alignItems: 'stretch',
                    }}
                >
                    {FAQData.map((FAQ, index) => {
                        return (
                            <div
                                key={index}
                                className="card p-0 faq-card p-auto"
                                style={{
                                    maxHeight: '20vh',
                                    width: '40rem',
                                    margin: '2rem 0',
                                    height: '20vh',

                                    borderRadius: '10px',
                                    textAlign: 'center',
                                    overflowY: 'auto',
                                }}
                            >
                                <div className="content">
                                    <h2 className="content-title faq-question">{FAQ.question}</h2>
                                    <p className="text-muted faq-answer" dangerouslySetInnerHTML={{ __html: FAQ.answer }}></p>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
            <p style={{ fontSize: 'larger', textAlign: 'center' }}>
                Please
                <span
                    className="btn btn-link px-0"
                    onClick={() => {
                        if (!userData) {
                            alert('must be logged in to contact...');
                            return;
                        }
                        setContactUsOpen(true);
                    }}
                >
                    {' '}
                    Contact Us
                </span>{' '}
                with any additional questions
            </p>
        </div>
    );
};
