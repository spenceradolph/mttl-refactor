import { useEffect, useReducer, useState } from 'react';
import { BrowserRouter, Navigate, Route, Routes, useNavigate } from 'react-router-dom';
import { api, initialState, reducer } from '../state';
import { AllMetrics } from './all-metrics';
import './App.scss';
import { Disclaimer } from './disclaimer';
import { Faq } from './faq';
import { Homepage } from './home';
import { Metrics } from './metrics';
import { MiscTables } from './misc-tables';
import { Mttl } from './mttl';
import { Navbar } from './navbar';
import { PageNotFound } from './page-not-found';
import { Roadmap } from './roadmap';
import { ContactUs } from './contact-us-modal';

export const App = () => {
    /**
     * Global State Management starts here and passed to components...
     */
    const [state, dispatch] = useReducer(reducer, initialState);
    const { userData } = state;

    const fetchUserData = async () => {
        const user_json = await api.fetchUserData(); // TODO: don't display failed / bad response code in the console? (or don't send it in the first place...)
        if (user_json.error) return; // TODO: better check, do some standard response if error...
        dispatch({ type: 'update-user-data', userData: user_json });
    };

    useEffect(() => {
        if (!userData) fetchUserData();
    }, []);

    const [contactUsOpen, setContactUsOpen] = useState(false);

    return (
        <div className="app-component-style">
            <div className="page-wrapper with-navbar">
                <div className="sticky-alerts"></div>
                <BrowserRouter>
                    <Navbar state={state} setContactUsOpen={setContactUsOpen} />
                    <div className="content-wrapper">
                        <Routes>
                            <Route path="/" element={<Navigate replace to="/home" />} />
                            <Route path="/home" element={<Homepage userData={userData} setContactUsOpen={setContactUsOpen} />} />
                            <Route path="/mttl" element={<Mttl state={state} dispatch={dispatch} />} />
                            <Route path="/roadmap" element={<Roadmap />} />
                            <Route path="/all-metrics" element={<AllMetrics state={state} dispatch={dispatch} />} />
                            <Route path="/metrics" element={<Metrics state={state} dispatch={dispatch} />} />
                            <Route path="/misc-tables" element={<MiscTables state={state} dispatch={dispatch} />} />
                            <Route path="/faq" element={<Faq setContactUsOpen={setContactUsOpen} userData={userData} />} />
                            <Route path="/disclaimer" element={<Disclaimer />} />
                            <Route path="*" element={<PageNotFound />} />
                        </Routes>
                    </div>
                </BrowserRouter>
                <ContactUs contactUsOpen={contactUsOpen} setContactUsOpen={setContactUsOpen} />
            </div>
        </div>
    );
};
