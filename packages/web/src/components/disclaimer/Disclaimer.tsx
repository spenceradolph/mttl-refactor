import './Disclaimer.scss';

export const Disclaimer = () => {
    return (
        <div className="disclaimer-component">
            <h1>Links Disclaimer</h1>
            <h3>
                <b>External Links Disclaimer</b>
            </h3>
            <p>
                The appearance of external links on this site does not constitute official endorsement on behalf of the 90th Cyberspace Operations Squadron, the U.S. Department of the Air Force, or
                the Department of Defense.
                <br />
                The Department of the Air Force does not endorse any non-federal entities, products, or services.
                <br />
                The Department of the Air Force does not exercise any responsibility or oversight of the content at external link destinations. <br />
            </p>
            <h3>
                <b>Overview</b>
            </h3>
            <p>
                This website is provided on behalf of the 90th Cyberspace Operations Squadron. <br />
                Any links are provided for educational and informative purposes only.
            </p>
            <h3>
                <b>Privacy Act Statement </b>
            </h3>
            <p>
                If you choose to provide us with personal information - such as filling out a Contact Us form with an e-mail address or providing your GitLab username - we only use that information to
                respond to your message or request. We will only share the information you give us with another government agency if your inquiry relates to that agency, or as otherwise required by
                law. We never create individual profiles or give it to any private organizations. We never collect information for commercial marketing. Though you must provide an e-mail address or
                GitLab username for a response, we recommend that you NOT include any other personal information.
            </p>
            <h3>
                <b>Cookie Use and Monitoring</b>
            </h3>
            <p>
                The site uses cookies for user customization. Specifically, cookies are used to maintain user settings. Certain functionality is not available without cookies. Any functionality
                provided by cookies may be achieved by contacting us directly using the provided e-mail address. We do not control the editorial or user policies of external links on this site. Users
                should read privacy and user agreements when visiting sites external links.
                <br />
                <br />
                <br />
                <em>
                    Unauthorized attempts to deny service, upload information, change information, or to attempt to access a non-public site from this service are strictly prohibited and may be
                    punishable under Title 18 of the U.S. Code to include the Computer Fraud and Abuse Act of 1986 and the National Information Infrastructure Protection Act.
                </em>
                <br />
                <br />
                <br />
                If you have any questions or comments about the information presented here, please e-mail us at <a href="mailto:90IOS.DOT.INBOX@us.af.mil">90IOS.DOT.INBOX@us.af.mil</a> or{' '}
                <span id="contactUs"> contact us </span>.
            </p>
        </div>
    );
};
