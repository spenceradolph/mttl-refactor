import { faChartPie, faRulerHorizontal } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MTTL_Type } from '@mttl/common';
import { Chart } from 'chart.js'; // TODO: update these packages (need old version for easy port from angular to react...)
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { useEffect, useRef, useState } from 'react';
import { api, AppState, Dispatch } from '../../state';
import './Metrics.scss';
import { ksatFilter } from '../mttl/ksatFilter'; // TODO: get this from higher up?

type MetricsProps = {
    state: AppState;
    dispatch: Dispatch;
};

export const Metrics = (Props: MetricsProps) => {
    const { state, dispatch } = Props;
    const { mttl, autocomplete, milestones } = state;

    const [isInitial, setIsInitial] = useState(true);
    const [chartType, setChartType] = useState<'bar' | 'pie'>('bar');
    const [firstDropDown, setFirstDropDown] = useState('mttl');
    const [secondDropDown, setSecondDropDown] = useState('');

    const filteredMttl = [...(mttl ?? [])].filter((ksat, index) => ksatFilter(ksat, [{ column: firstDropDown, value: secondDropDown }]));

    const covered = [...filteredMttl.filter((mttl) => parseInt(mttl.training_count) * parseInt(mttl.eval_count) > 0)].length;
    const evalCovered = [...filteredMttl.filter((mttl) => parseInt(mttl.training_count) === 0 && parseInt(mttl.eval_count) > 0)].length;
    const trnCovered = [...filteredMttl.filter((mttl) => parseInt(mttl.training_count) > 0 && parseInt(mttl.eval_count) === 0)].length;
    const noCoverage = [...filteredMttl.filter((mttl) => parseInt(mttl.training_count) === 0 && parseInt(mttl.eval_count) === 0)].length;
    const total = filteredMttl.length;

    const chart = useRef();
    const ctx = useRef(document.getElementById('chart'));

    const fetchMttl = async () => {
        const mttlData = await api.fetchMttl();
        // TODO: could do error handling here? (look around for a nice try/catch spot in the frontend? or let things fail for now???)
        dispatch({ type: 'update-mttl', mttlData });
    };

    const fetchAutocomplete = async () => {
        const autocompleteData = await api.fetchAutocomplete();
        dispatch({ type: 'update-autocomplete', autocompleteData });
    };

    const fetchMilestones = async () => {
        const milestoneData = await api.fetchMilestones();
        dispatch({ type: 'update-milestones', milestoneData });
    };

    useEffect(() => {
        if (!mttl) fetchMttl();
        if (!autocomplete) fetchAutocomplete();
        if (!milestones) fetchMilestones();
    }, []);

    useEffect(() => {
        if (mttl && autocomplete && milestones && isInitial) {
            // const [trainingAndEvalPercent, evalPercent, trainingPercent, noCoveragePercent] = getPercentages(mttl);
            setIsInitial(false);
            calculateChart(chartType);
        }
    }, [mttl, autocomplete, milestones]);

    function calculateBarChart(covered: number, evalCovered: number, trnCovered: number, noCoverage: number): void {
        const total = covered + noCoverage + trnCovered + evalCovered;
        // @ts-ignore
        chart.current = new Chart(ctx.current, {
            type: 'horizontalBar',
            // @ts-ignore
            plugins: [ChartDataLabels],
            data: {
                labels: ['KSATs Covered'],
                datasets: [
                    {
                        label: 'Training and Eval (' + covered + '/' + total + ')',
                        data: [covered],
                        backgroundColor: '#6EC664',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                    {
                        label: 'Eval (' + (evalCovered + covered) + '/' + total + ')',
                        data: [evalCovered],
                        backgroundColor: '#F0AB00',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                    {
                        label: 'Training (' + (trnCovered + covered) + '/' + total + ')',
                        data: [trnCovered],
                        backgroundColor: '#0066CC',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                    {
                        label: 'No Coverage (' + noCoverage + '/' + total + ')',
                        data: [noCoverage],
                        backgroundColor: '#707070',
                        borderWidth: 3,
                        // @ts-ignore
                        borderSkipped: null,
                    },
                ],
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false,
                },
                scales: {
                    xAxes: [
                        {
                            stacked: true,
                            ticks: { max: total },
                            display: false,
                            gridLines: {
                                display: false,
                            },
                        },
                    ],
                    yAxes: [{ display: false, stacked: true }],
                },
                legend: {
                    display: false,
                    labels: {
                        fontSize: 20,
                    },
                },
                plugins: {
                    // Change options for ALL labels of THIS CHART
                    datalabels: {
                        color: '#ffffff',
                        // @ts-ignore
                        formatter: (value, contxt) => {
                            const percentage = Math.round((value / total) * 100);
                            if (percentage) {
                                return percentage + '%';
                            }
                            return '';
                        },
                        font: {
                            size: 20,
                        },
                    },
                },
            },
        });
    }

    function calculatePieChart(covered: number, evalCovered: number, trnCovered: number, noCoverage: number): void {
        const total = covered + noCoverage + trnCovered + evalCovered;
        // @ts-ignore
        chart.current = new Chart(ctx.current, {
            type: 'doughnut',
            // @ts-ignore
            plugins: [ChartDataLabels],
            data: {
                labels: [
                    'Training and Eval (' + Math.trunc((covered / total) * 100) + '%)',
                    'Eval (' + Math.trunc((evalCovered / total) * 100) + '%)',
                    'Training(' + Math.trunc((trnCovered / total) * 100) + '%)',
                    'No Coverage (' + Math.trunc((noCoverage / total) * 100) + '%)',
                ],
                datasets: [
                    {
                        data: [covered, evalCovered, trnCovered, noCoverage],
                        backgroundColor: ['#6EC664', '#F0AB00', '#0066CC', '#707070'],
                        borderWidth: 1,
                    },
                ],
            },
            options: {
                responsive: true,
                scales: {
                    xAxes: [
                        {
                            ticks: { max: total },
                            display: false,
                            gridLines: {
                                display: false,
                            },
                        },
                    ],
                    yAxes: [{ display: false }],
                },
                legend: {
                    display: false,
                    labels: {
                        fontSize: 20,
                    },
                },
                plugins: {
                    // Change options for ALL labels of THIS CHART
                    datalabels: {
                        // @ts-ignore
                        formatter: (value, contxt) => {
                            const percentage = Math.round((value / total) * 100);
                            if (percentage) {
                                return percentage + '%';
                            }
                            return '';
                        },
                        color: '#ffffff',
                        font: {
                            size: 20,
                        },
                    },
                },
            },
        });
    }

    // TODO: BUG: chart size is different when toggled between pie and bar charts (bar specifically is larger on the 2nd rendering)
    // This bug is also present in the angular version! (dang)
    // Also when the bottom selector is used to switch between mttl and work role and etc....
    function calculateChart(chartType: string): void {
        // console.log('calculate chart');
        ctx.current = document.getElementById('chart');
        if (chartType === 'bar') {
            // @ts-ignore
            ctx.current.height = 45;
            calculateBarChart(covered, evalCovered, trnCovered, noCoverage);
        } else {
            // @ts-ignore
            ctx.current.height = 65;
            calculatePieChart(covered, evalCovered, trnCovered, noCoverage);
        }
    }

    function toggleChartType() {
        if (chartType === 'bar') {
            setChartType('pie');
        } else {
            setChartType('bar');
        }
    }

    useEffect(() => {
        if (!isInitial) {
            // @ts-ignore
            chart.current.destroy();
            calculateChart(chartType);
        }
    }, [chartType]);

    useEffect(() => {
        if (firstDropDown === 'mttl') return setSecondDropDown('');
        setSecondDropDown(autocomplete[`${firstDropDown}`][0]);
    }, [firstDropDown]); // re-render if these change

    useEffect(() => {
        if (!isInitial) {
            // @ts-ignore
            chart.current.destroy();
            calculateChart(chartType);
        }
    }, [secondDropDown]);

    const getTitleString = () => {
        if (firstDropDown === 'mttl') return 'MTTL Coverage';
        if (firstDropDown === 'work_roles') return `Work Role: ${secondDropDown} Coverage`;
        if (firstDropDown === 'requirement_owner') return `Owner: ${secondDropDown} Coverage`;
        if (firstDropDown === 'requirement_src') return `Source: ${secondDropDown} Coverage`;
        if (firstDropDown === 'topic') return `Topic: ${secondDropDown} Coverage`;
        if (firstDropDown === 'oam') return `Milestone: ${secondDropDown} Coverage`;
        return 'N/A';
    };

    const secondSelectOptions =
        firstDropDown === 'mttl'
            ? null
            : autocomplete[`${firstDropDown}`].map((thing: any, index: number) => {
                  return (
                      <option key={index} value={thing}>
                          {thing}
                      </option>
                  );
              });

    return (
        <div className="metrics-component-style">
            <div className="coverage_container">
                <div className="coverage_info">
                    <h3>{getTitleString()}</h3>
                    <div style={{ padding: '1.5rem 1rem 0 0' }}>
                        {chartType === 'bar' ? (
                            <FontAwesomeIcon id="fa-icon" icon={faChartPie} onClick={() => toggleChartType()} />
                        ) : (
                            <FontAwesomeIcon id="fa-icon" icon={faRulerHorizontal} onClick={() => toggleChartType()} />
                        )}
                    </div>

                    <br />
                    <div className="key">
                        <div className="coverage_key">
                            <div className="box CoveredBox"></div>
                            Training and Eval
                            <br />
                            {covered}/{total}
                        </div>
                        <div className="coverage_key">
                            <div className="box TrnBox"></div>
                            Training
                            <br />
                            {covered + trnCovered}/{total}
                        </div>
                        <div className="coverage_key">
                            <div className="box EvalBox"></div>
                            Eval
                            <br />
                            {covered + evalCovered}/{total}
                        </div>
                        <div className="coverage_key">
                            <div className="box NoCoverageBox"></div>
                            No Coverage
                            <br />
                            {noCoverage}/{total}
                        </div>
                    </div>
                </div>
                <canvas id="chart" className="data_canvas"></canvas>
                <br />
                <br />
                <div className="coverage_selector">
                    <select value={firstDropDown} className="form-control" onChange={(e) => setFirstDropDown(e.target.value)}>
                        <option value="mttl">MTTL</option>
                        <option value="work_roles">Work Role</option>
                        <option value="requirement_owner">Owner</option>
                        <option value="requirement_src">Source</option>
                        <option value="topic">Topic</option>
                        <option value="oam">Overarching Milestone</option>
                    </select>
                    <select value={secondDropDown} onChange={(e) => setSecondDropDown(e.target.value)} hidden={firstDropDown === 'mttl'} className="form-control" placeholder="Filter" id="filterInput">
                        {secondSelectOptions}
                    </select>
                </div>
            </div>
        </div>
    );
};
