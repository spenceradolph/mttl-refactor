import { faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormGroup, Autocomplete, Tooltip } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import { api } from '../../state';

type NewKsatProps = {
    newKsatOpen: any;
    setNewKsatOpen: any;
    autocomplete: any;
};

export const NewKsat = (Props: NewKsatProps) => {
    const { newKsatOpen, setNewKsatOpen, autocomplete } = Props;

    const [topic, setTopic] = useState('');
    const [desc, setDesc] = useState('');
    const [source, setSource] = useState('');
    const [owner, setOwner] = useState('');
    const [parentKsats, setParentKsats] = useState('');
    const [childKsats, setChildKsats] = useState('');
    const [trainingLinks, setTrainingLinks] = useState('');
    const [trainingRefs, setTrainingRefs] = useState('');
    const [evalLinks, setEvalLinks] = useState('');
    const [workroleData, setWorkroleData] = useState([{ workRole: '', proficiency: '', milestone: '' }]);

    const submitNewKsat = async () => {
        const ksatData = {
            topic,
            desc,
            source,
            owner,
            parentKsats,
            childKsats,
            trainingLinks,
            trainingRefs,
            evalLinks,
            workroleData,
        };
        const results = await api.submitNewKsat(JSON.stringify(ksatData));
        if (results.error) return; // TODO: error handling (not authenticated and other error: '')
        alert('submitted the new ksat!'); // TODO: 90th uses the sticky alerts again (I think from halfmoon)
        setNewKsatOpen(false);
    };

    // TODO: refactor this please, it's awful
    let betterMilestones: any = {};
    if (autocomplete !== null) {
        for (const row of autocomplete['milestones_by_work_roles']) {
            for (const key in row) {
                if (row.hasOwnProperty(key)) {
                    betterMilestones[key] = row[key].milestones ?? [];
                }
            }
        }
    }

    return (
        <Dialog open={newKsatOpen} onClose={() => setNewKsatOpen(false)}>
            <DialogTitle>Add KSAT</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Topic`}
                    fullWidth
                    variant="standard"
                    value={topic}
                    required
                    onChange={(e) => {
                        setTopic(e.target.value);
                    }}
                />

                {workroleData.map((data, index) => {
                    return (
                        <FormGroup row key={index}>
                            <Autocomplete
                                onChange={(e) => {
                                    const tempData = [...workroleData];
                                    // @ts-ignore
                                    tempData[index].workRole = e.target.textContent;
                                    setWorkroleData(tempData);
                                }}
                                disableClearable
                                style={{ width: '35%' }}
                                options={autocomplete !== null ? autocomplete['work_roles'] : []}
                                renderInput={(params) => <TextField {...params} label="Work Role" />}
                                isOptionEqualToValue={(option: any, value: any) => option.value === value.value} // used to disable warning, happens when previous selection still in TextField and isn't actually an option
                            />
                            <Autocomplete
                                onChange={(e) => {
                                    const tempData = [...workroleData];
                                    // @ts-ignore
                                    tempData[index].proficiency = e.target.textContent;
                                    setWorkroleData(tempData);
                                }}
                                value={data.proficiency}
                                style={{ width: '20%' }}
                                options={['A', 'B', 'C', 'D', '1', '2', '3']}
                                renderInput={(params) => <TextField {...params} label="Proficiency" />}
                                isOptionEqualToValue={(option: any, value: any) => option.value === value.value} // used to disable warning, happens when previous selection still in TextField and isn't actually an option
                            />
                            <Autocomplete
                                disabled={data.workRole === ''}
                                onChange={(e) => {
                                    const tempData = [...workroleData];
                                    // @ts-ignore
                                    tempData[index].milestone = e.target.textContent;
                                    setWorkroleData(tempData);
                                }}
                                disableClearable
                                value={data.milestone}
                                style={{ width: '30%' }}
                                options={autocomplete !== null && data.workRole !== '' ? betterMilestones[data.workRole] : []}
                                renderInput={(params) => <TextField {...params} label="Milestone" />}
                                isOptionEqualToValue={(option: any, value: any) => option.value === value.value} // used to disable warning, happens when previous selection still in TextField and isn't actually an option
                            />
                            {workroleData.length > 1 ? (
                                <Tooltip title="Remove workrole">
                                    <div>
                                        <FontAwesomeIcon
                                            className="clickable-item"
                                            icon={faTrash}
                                            onClick={() => {
                                                // setKsatData([...ksatData, { ksat_id: '', proficiency: '' }]);
                                                const tempData = [...workroleData].filter((ksat, index1) => index1 !== index); // TODO: again, don't need all the array copies and spread...not efficient anyway but safer for dummies
                                                setWorkroleData([...tempData]);
                                            }}
                                        />
                                    </div>
                                </Tooltip>
                            ) : null}
                            {workroleData.length === index + 1 && data.workRole !== '' && data.proficiency !== '' && data.milestone !== '' ? (
                                <Tooltip title="Add workrole">
                                    <div>
                                        <FontAwesomeIcon
                                            className="clickable-item"
                                            icon={faPlus}
                                            onClick={() => {
                                                setWorkroleData([...workroleData, { workRole: '', proficiency: '', milestone: '' }]);
                                            }}
                                        />
                                    </div>
                                </Tooltip>
                            ) : null}
                        </FormGroup>
                    );
                })}

                <TextField
                    autoFocus
                    margin="dense"
                    label={`Description`}
                    fullWidth
                    variant="standard"
                    value={desc}
                    required
                    onChange={(e) => {
                        setDesc(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Source`}
                    style={{ width: '50%' }}
                    variant="standard"
                    value={source}
                    helperText="A document, publication, or other source"
                    required
                    onChange={(e) => {
                        setSource(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Owner`}
                    style={{ width: '50%' }}
                    variant="standard"
                    helperText="Organization that determines requirement"
                    value={owner}
                    required
                    onChange={(e) => {
                        setOwner(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Parent KSATs`}
                    style={{ width: '50%' }}
                    variant="standard"
                    value={parentKsats}
                    required
                    onChange={(e) => {
                        setParentKsats(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Child KSATs`}
                    style={{ width: '50%' }}
                    variant="standard"
                    value={childKsats}
                    required
                    onChange={(e) => {
                        setChildKsats(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Training Links`}
                    helperText="URLs that provide relevant training"
                    fullWidth
                    variant="standard"
                    value={trainingLinks}
                    required
                    onChange={(e) => {
                        setTrainingLinks(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Training References`}
                    helperText="Offline training resources, sources of training links, etc."
                    fullWidth
                    variant="standard"
                    value={trainingRefs}
                    required
                    onChange={(e) => {
                        setTrainingRefs(e.target.value);
                    }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    label={`Eval Links`}
                    helperText="Resources that test proficiency of this KSAT item"
                    fullWidth
                    variant="standard"
                    value={evalLinks}
                    required
                    onChange={(e) => {
                        setEvalLinks(e.target.value);
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setNewKsatOpen(false)}>Close</Button>
                <Button onClick={() => submitNewKsat()}>Submit</Button>
            </DialogActions>
        </Dialog>
    );
};
