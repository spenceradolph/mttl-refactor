import { MTTL_Type } from '@mttl/common';
import { FilterType } from './ksatFilter';
import { MttlRow } from './MttlRow';
import { MttlTableHead } from './MttlTableHead';
import { ModifyKsat } from './ModifyKsat';
import { NewLink } from './NewLink';
import { useEffect, useState } from 'react';
import { AppState } from '../../state';

type MttlRowsProps = {
    ksats: MTTL_Type[];
    currentItems: MTTL_Type[];
    selectedCols: string[];
    setSort: any;
    sort: any;
    filters: FilterType[];
    setFilters: any;
    userData: AppState['userData'];
    autocomplete: AppState['autocomplete'];
};

export const MttlRows = (Props: MttlRowsProps) => {
    const { currentItems, selectedCols, setSort, sort, filters, setFilters, userData, ksats, autocomplete } = Props;

    const [modifyKsatOpen, setModifyKsatOpen] = useState(false);
    const [ksatToModify, setKsatToModify] = useState('');

    const [newLinkOpen, setNewLinkOpen] = useState(false);
    const [linkType, setLinkType] = useState<'Training' | 'Eval'>('Training');

    const editKsat = (ksat_id: string) => {
        if (!userData) {
            alert('must be logged in to suggest modification');
            return;
        }
        setKsatToModify(ksat_id);
        setModifyKsatOpen(true);
    };

    const createLink = (ksat_id: string, link_type: 'Training' | 'Eval') => {
        if (!userData) {
            alert('must be logged in to add a link');
            return;
        }
        setKsatToModify(ksat_id);
        setLinkType(link_type);
        setNewLinkOpen(true);
    };

    const [globalTFilter, setGlobalTFilter] = useState({ active: false, profLevel: 'any', resourceType: 'any', showCommercial: true }); // global training
    const [globalEFilter, setGlobalEFilter] = useState({ active: false, profLevel: 'any', resourceType: 'any', showCommercial: true }); // global eval

    useEffect(() => {}, []);

    return (
        <div className="mttl-table">
            <table className="mat-table cdk-table alt-bg">
                <MttlTableHead selectedCols={selectedCols} setSort={setSort} sort={sort} />
                <tbody>
                    {currentItems.map((item, index) => (
                        <MttlRow
                            key={item.ksat_id}
                            KSAT={item}
                            selectedCols={selectedCols}
                            filters={filters}
                            setFilters={setFilters}
                            editKsat={editKsat}
                            createLink={createLink}
                            ksats={ksats}
                            globalTFilter={globalTFilter}
                            globalEFilter={globalEFilter}
                            setGlobalTFilter={setGlobalTFilter}
                            setGlobalEFilter={setGlobalEFilter}
                        />
                    ))}
                </tbody>
            </table>
            <ModifyKsat modifyKsatOpen={modifyKsatOpen} setModifyKsatOpen={setModifyKsatOpen} ksat_id={ksatToModify} />
            <NewLink newLinkOpen={newLinkOpen} setNewLinkOpen={setNewLinkOpen} link_type={linkType} ksat_id={ksatToModify} autocomplete={autocomplete} />
        </div>
    );
};
