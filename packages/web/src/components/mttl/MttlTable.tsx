import { MTTL_Type } from '@mttl/common';
import { useEffect, useState } from 'react';
import { MttlFooter } from './MttlFooter';
import { MttlRows } from './MttlRows';
import { ksatColSort } from './ksatColSort';
import { FilterType } from './ksatFilter';
import { AppState } from '../../state';

type MttlTableProps = {
    ksats: MTTL_Type[];
    selectedCols: string[];
    filters: FilterType[];
    setFilters: any;
    userData: AppState['userData'];
    autocomplete: AppState['autocomplete'];
};

type SortState = {
    col: string;
    direction: 'desc' | 'asc';
};

export const MttlTable = (Props: MttlTableProps) => {
    const { ksats, selectedCols, filters, setFilters, userData, autocomplete } = Props;

    const [sort, setSort] = useState<SortState>({ col: 'ksat_id', direction: 'asc' });
    const sortedItems = [...ksats].sort((ksat_a, ksat_b) => ksatColSort(ksat_a, ksat_b, sort));

    const defaultItemsPerPage = parseInt(localStorage.getItem('mttl-pagination') ?? '50');

    const [currentItems, setCurrentItems] = useState<MTTL_Type[]>([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(defaultItemsPerPage);

    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(sortedItems?.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(sortedItems?.length / itemsPerPage));
    }, [itemOffset, itemsPerPage, sort, ksats]);

    const handlePageClick = (event: any) => {
        const newOffset = (event.selected * itemsPerPage) % sortedItems?.length;
        setItemOffset(newOffset);
    };

    useEffect(() => {}, [currentItems]);

    return (
        <>
            <MttlRows
                autocomplete={autocomplete}
                currentItems={currentItems}
                selectedCols={selectedCols}
                setSort={setSort}
                sort={sort}
                filters={filters}
                setFilters={setFilters}
                userData={userData}
                ksats={ksats}
            />
            <MttlFooter
                handlePageClick={handlePageClick}
                pageCount={pageCount}
                setItemsPerPage={setItemsPerPage}
                itemsPerPage={itemsPerPage}
                itemOffset={itemOffset}
                numElements={sortedItems.length}
            />
        </>
    );
};
