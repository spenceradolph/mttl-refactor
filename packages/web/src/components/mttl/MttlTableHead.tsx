import { faSort, faSortDown, faSortUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type MttlTableHeadProps = {
    selectedCols: string[];
    setSort: any;
    sort: any;
};

export const MttlTableHead = (Props: MttlTableHeadProps) => {
    const { selectedCols, setSort, sort } = Props;

    const setThisSort = (column: string) => {
        if (column === sort.col) {
            setSort({ ...sort, direction: sort.direction === 'desc' ? 'asc' : 'desc' });
        } else {
            setSort({ col: column, direction: 'asc' });
        }
    };

    const ksatIdCol = (
        <th className="">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('ksat_id');
                }}
            >
                KSAT ID
                {sort.col !== 'ksat_id' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const descriptionCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('description');
                }}
            >
                Description
                {sort.col !== 'description' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const topicCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('topic');
                }}
            >
                Topic
                {sort.col !== 'topic' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const ownerCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('requirement_owner');
                }}
            >
                Owner
                {sort.col !== 'requirement_owner' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const sourceCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('requirement_src');
                }}
            >
                Source
                {sort.col !== 'requirement_src' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const workRoleCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('work_roles');
                }}
            >
                Work Role
                {sort.col !== 'work_roles' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const childrenCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('child_count');
                }}
            >
                Children
                {sort.col !== 'child_count' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const parentCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('parent_count');
                }}
            >
                Parents
                {sort.col !== 'parent_count' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const trainingCol = (
        <>
            <th className="header-selectable">
                <span
                    className="clickable-element"
                    onClick={() => {
                        setThisSort('training_count');
                    }}
                >
                    Training
                    {sort.col !== 'training_count' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
                </span>
            </th>
            <th></th>
        </>
    );

    const evalCol = (
        <>
            <th className="header-selectable">
                <span
                    className="clickable-element"
                    onClick={() => {
                        setThisSort('eval_count');
                    }}
                >
                    Evaluation
                    {sort.col !== 'eval_count' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
                </span>
            </th>
            <th></th>
        </>
    );

    const courseCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('courses');
                }}
            >
                Course
                {sort.col !== 'courses' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    const milestoneCol = (
        <th className="header-selectable">
            <span
                className="clickable-element"
                onClick={() => {
                    setThisSort('oam');
                }}
            >
                Milestones
                {sort.col !== 'oam' ? <FontAwesomeIcon icon={faSort} /> : sort.direction === 'asc' ? <FontAwesomeIcon icon={faSortDown} /> : <FontAwesomeIcon icon={faSortUp} />}
            </span>
        </th>
    );

    // Can't sort by comment on 90th's
    const commentsCol = (
        <th className="header-selectable">
            <span className="clickable-element">
                Comments
                <FontAwesomeIcon icon={faSort} />
            </span>
        </th>
    );

    // TODO: coverage column is always there? (doesn't appear to be un-selectable from the modal on 90th's version)
    return (
        <thead>
            <tr id="header_row" className="mat-header-row cdk-header-row ng-tns-c130-5 alt-th ng-star-inserted" style={{ height: '5px' }}>
                <th className=""></th>
                {selectedCols.includes('ksat_id') ? ksatIdCol : null}
                {selectedCols.includes('description') ? descriptionCol : null}
                {selectedCols.includes('topic') ? topicCol : null}
                {selectedCols.includes('requirement_owner') ? ownerCol : null}
                {selectedCols.includes('requirement_src') ? sourceCol : null}
                {selectedCols.includes('work_roles') ? workRoleCol : null}
                {selectedCols.includes('child_count') ? childrenCol : null}
                {selectedCols.includes('parent_count') ? parentCol : null}
                {selectedCols.includes('training_count') ? trainingCol : null}
                {selectedCols.includes('eval_count') ? evalCol : null}
                {selectedCols.includes('courses') ? courseCol : null}
                {selectedCols.includes('oam') ? milestoneCol : null}
                {selectedCols.includes('comments') ? commentsCol : null}
            </tr>
        </thead>
    );
};
