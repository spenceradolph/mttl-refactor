import { MTTL_Type } from '@mttl/common';

export type FilterType = {
    column: string;
    value: string;
};

// TODO: figure out if case should matter (it did at least for comparison purposes, but maybe they're messing up the data (K0005 vs K0309))
export const ksatFilter = (ksat: MTTL_Type, filters: FilterType[]): boolean => {
    for (const filter of filters) {
        if (filter.column === 'work_roles' && !ksat.work_roles.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'topic' && !ksat.topic.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'requirement_owner' && !ksat.requirement_owner.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'requirement_src' && !ksat.requirement_src.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'courses' && !ksat.courses.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'parents' && !ksat.parents.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'ksat_id' && ksat.ksat_id !== filter.value) return false;
        if (filter.column === 'ksat_type' && ksat.ksat_type !== filter.value) return false;
        if (filter.column === 'oam' && !ksat.oam.some((value) => value.toUpperCase() === filter.value.toUpperCase())) return false;
        if (filter.column === 'coverage') {
            if (filter.value === 'training-and-eval' && !(parseInt(ksat.training_count) > 0 && parseInt(ksat.eval_count) > 0)) return false;
            if (filter.value === 'training-no-eval' && !(parseInt(ksat.training_count) > 0 && parseInt(ksat.eval_count) === 0)) return false;
            if (filter.value === 'eval-no-training' && !(parseInt(ksat.training_count) === 0 && parseInt(ksat.eval_count) > 0)) return false;
            if (filter.value === 'no-coverage' && !(parseInt(ksat.training_count) === 0 && parseInt(ksat.eval_count) === 0)) return false;
        }
    }

    return true;
};

export const ksatSearch = (ksat: MTTL_Type, searches: string[]): boolean => {
    if (searches.length === 0) return true;

    for (const search of searches) {
        if (ksat.ksat_id === search) continue; // TODO: should consider partial matches on ksat_id (ex: 'A00' instead of 'A0004')

        // TODO: other search functionality here

        // didn't get a 'hit'
        return false;
    }

    // All searches had some sort of 'hit' on this ksat
    return true;
};
