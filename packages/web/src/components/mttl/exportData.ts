import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

export const downloadFile = (fileType: any, data: any): void => {
    // This is all actually pass by reference
    const fixedData = undecorateTrainingAndEval(data);
    const expandedData = expandProficiencyData(fixedData);
    // const csvData = this.convertToCSV(expandedData, ['ksat_id']);
    const csvData = convertToCSV(expandedData, [
        'ksat_id',
        'parents',
        'description',
        'topic',
        'requirement_src',
        'requirement_owner',
        'children',
        'work_roles',
        'child_count',
        'parent_count',
        'training_count',
        'eval_count',
        'proficiency_data',
        'comments',
        'updated_on',
        'oam',
        'courses',
        'training_links',
        'eval_links',
    ]);
    if (fileType === 'csv') {
        const blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        const dwldLink = document.createElement('a');
        const url = URL.createObjectURL(blob);
        const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
        if (isSafariBrowser) {
            // if Safari open in new window to save file with random filename.
            dwldLink.setAttribute('target', '_blank');
        }
        dwldLink.setAttribute('href', url);
        dwldLink.setAttribute('download', getFilename() + '.csv');
        dwldLink.style.visibility = 'hidden';
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);
    } else if (fileType === 'xlsx') {
        // @ts-ignore
        const betterParents = betterParents(data);
        // although JS is pass by reference
        // @ts-ignore
        const betterUpdate = betterUpdate(betterParents);
        // @ts-ignore
        const betterWorkRoles = betterWorkRoles(betterUpdate);
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(betterWorkRoles);
        const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        saveAsExcelFile(excelBuffer, getFilename());
    }
};

const saveAsExcelFile = (buffer: any, fileName: string): void => {
    const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
};

const getFilename = (): any => {
    const d = new Date();
    const date = d.getDate();
    const month = d.getMonth() + 1;
    const year = d.getFullYear().toString().substr(-2);
    const hour = d.getHours();
    const minutes = d.getMinutes();
    const dateStr = month + '_' + date + '_' + year + '_' + hour + '_' + minutes;
    const fileNameWithDate = 'MTTL' + '_' + dateStr;
    return fileNameWithDate;
};

const removeKey = (obj: any) => {
    delete obj.Comments;
    return obj;
    // once the wrong KSAT is fixed and / or a pipeline check is added this is superfluous
};

const betterParents = (jsonData: any[]): any[] => {
    jsonData.forEach((element) => {
        const quotes = /"/gi;
        const bl = /\[/gi;
        const br = /\]/gi;
        let newParents = JSON.stringify(element.parent);
        newParents = newParents.replace(quotes, '');
        newParents = newParents.replace(br, '');
        newParents = newParents.replace(bl, '');
        element.parent = newParents;
    });
    return jsonData;
};

const betterUpdate = (jsonData: any[]): any[] => {
    jsonData.forEach((element) => {
        const zero = /null/gi;
        const dq = /"/gi;
        const sq = /'/gi;
        let newUpdate = JSON.stringify(element.updated_on);
        newUpdate = newUpdate.replace(zero, '');
        newUpdate = newUpdate.replace(dq, '');
        newUpdate = newUpdate.replace(sq, '');
        element.updated_on = newUpdate;
    });
    return jsonData;
};

const betterWorkRoles = (jsonData: any[]): any[] => {
    jsonData.forEach((element) => {
        const quotes = /"/gi;
        const bl = /\[/gi;
        const br = /\]/gi;
        if (element.work_roles !== undefined) {
            let newWrs = JSON.stringify(element.work_roles);
            newWrs = newWrs.replace(quotes, '');
            newWrs = newWrs.replace(br, '');
            newWrs = newWrs.replace(bl, '');
            element.work_roles = newWrs;
        }

        if (element.work_roles !== undefined) {
            let newWrSpec = JSON.stringify(element.work_roles);
            newWrSpec = newWrSpec.replace(quotes, '');
            newWrSpec = newWrSpec.replace(br, '');
            newWrSpec = newWrSpec.replace(bl, '');
            element.work_roles = newWrSpec;
        }
    });
    return jsonData;
};

const expandProficiencyData = (jsonData: any[]): any[] => {
    jsonData.forEach((element) => {
        if (JSON.stringify(element).includes('proficiency_data')) {
            const newProficiencyData = JSON.stringify(element.proficiency_data);
        }
        let newProficiency = JSON.stringify(element.proficiency_data);
        const wr = /"work-role":/gi;
        const prof = /"proficiency":/gi;
        const emptyQuotes = /""/gi;
        const quotes = /"/gi;
        const br = /},{/gi;
        const start = /\[\{/gi;
        const end = /\}\]/gi;
        newProficiency = newProficiency.replace(wr, '');
        newProficiency = newProficiency.replace(prof, '');
        newProficiency = newProficiency.replace(emptyQuotes, 'undefined');
        newProficiency = newProficiency.replace(quotes, '');
        newProficiency = newProficiency.replace(br, ';');
        newProficiency = newProficiency.replace(start, '');
        newProficiency = newProficiency.replace(end, '');
        newProficiency = newProficiency.replace(',', ':');
        newProficiency = newProficiency.replace('[]', '');
        element.proficiency_data = newProficiency;
    });
    return jsonData;
};

const undecorateTrainingAndEval = (jsonData: any[]): any[] => {
    jsonData.forEach((element) => {
        if (JSON.stringify(element).includes('Comment')) {
            // Note - 'Comment' not 'comment' - there was one 'Comment', it was empty and broke things
            element = removeKey(element);
        }
        let newcomments = element.comments;
        newcomments = newcomments.replace('"', "'");
        newcomments = newcomments.replace('\\', '');
        newcomments = newcomments.replace(/"  "/g, ' ');
        newcomments = newcomments.replace(/"  +"/g, ' ');
        newcomments = newcomments.replace(/^\s+|\s+$/g, '');
        element.comments = newcomments;

        let newtopic = element.topic;
        newtopic = newtopic.toString();
        newtopic = newtopic.replace('"', "'");
        newtopic = newtopic.replace('\\', '');
        newtopic = newtopic.replace(/"  "/g, ' ');
        newtopic = newtopic.replace(/"  +"/g, ' ');
        newtopic = newtopic.replace(/^\s+|\s+$/g, '');
        element.topic = newtopic;

        if (element.children !== undefined) {
            let newchildren = element.children;
            newchildren = newchildren.toString();
            newchildren = newchildren.replace(/\"/g, "'");
            newchildren = newchildren.replace(/\\/g, '');
            newchildren = newchildren.replace(/"  "/g, ' ');
            newchildren = newchildren.replace(/"  +"/g, ' ');
            newchildren = newchildren.replace(/","/g, ';');
            newchildren = newchildren.replace(/" "/g, '');
            newchildren = newchildren.replace(/^\s+|\s+$/g, '');
            element.children = newchildren;
        }
        //these are arrays

        if (element['work-roles'] !== undefined) {
            let newwork_roles = JSON.stringify(element['work-roles']);
            newwork_roles = newwork_roles.replace(/\"/g, "'");
            newwork_roles = newwork_roles.replace(/\\/g, '');
            newwork_roles = newwork_roles.replace(/"  "/g, ' ');
            newwork_roles = newwork_roles.replace(/"  +"/g, ' ');
            newwork_roles = newwork_roles.replace(/","/g, ';');
            newwork_roles = newwork_roles.replace(/" "/g, '');
            newwork_roles = newwork_roles.replace(/^\s+|\s+$/g, '');
            element.work_roles = newwork_roles;
        }

        if (element.updated_on !== undefined) {
            let newupdated_on = JSON.stringify(element.updated_on);
            newupdated_on = newupdated_on.replace(/\'/g, '');
            newupdated_on = newupdated_on.replace(/\"/g, "'");
            newupdated_on = newupdated_on.replace(/\\/g, '');
            newupdated_on = newupdated_on.replace(/"  "/g, ' ');
            newupdated_on = newupdated_on.replace(/"  +"/g, ' ');
            newupdated_on = newupdated_on.replace(/^\s+|\s+$/g, '');
            element.updated_on = newupdated_on;
        }

        let description = element.description;
        description = description.replace(/\"/g, "'");
        description = description.replace(/\\/g, '');
        description = description.replace(/"  +"/g, ' ');
        description = description.replace(/^\s+|\s+$/g, '');
        element.description = description;

        let requirement_owner = element.requirement_owner;
        requirement_owner = requirement_owner.toString();
        requirement_owner = requirement_owner.replace(/\"/g, "'");
        requirement_owner = requirement_owner.replace(/"  +"/g, ' ');
        requirement_owner = requirement_owner.replace(/^\s+|\s+$/g, '');
        element.requirement_owner = requirement_owner;

        let requirement_src = element.requirement_src;
        requirement_src = requirement_src.toString();
        requirement_src = requirement_src.replace(/\"/g, "'");
        requirement_src = requirement_src.replace(/"  +"/g, ' ');
        requirement_src = requirement_src.replace(/^\s+|\s+$/g, '');
        element.requirement_src = requirement_src;

        if (element.requirement_src_id !== undefined) {
            let requirement_src_id = element.requirement_src_id;
            requirement_src_id = requirement_src_id.replace(/\'/g, '');
            requirement_src_id = requirement_src_id.replace(/\"/g, '');
            requirement_src_id = requirement_src_id.replace(/"  +"/g, ' ');
            requirement_src_id = requirement_src_id.replace(/^\s+|\s+$/g, '');
            element.requirement_src_id = requirement_src_id;
        }

        let newcourses = element.courses;
        const arrC = [];
        for (const o in newcourses) {
            if (newcourses.hasOwnProperty(o)) {
                arrC.push(JSON.stringify(newcourses[o]) + ' ');
            }
        }
        newcourses = arrC.join('; ');
        if (arrC.length > 0) {
            element.courses = newcourses;
        }

        let neweval = element.eval_links;
        const arrE = [];
        for (const o in neweval) {
            if (neweval.hasOwnProperty(o)) {
                arrE.push(JSON.stringify(neweval[o].url) + ' ');
            }
        }
        neweval = arrE.join('; ');

        if (arrE.length > 0) {
            element.eval_links = neweval;
        }

        let newtraining = element.training_links;
        const arrT = [];
        for (const o in newtraining) {
            if (newtraining.hasOwnProperty(o)) {
                arrT.push(JSON.stringify(newtraining[o].url) + ' ');
            }
        }
        newtraining = arrT.join('; ');

        if (arrT.length > 0) {
            element.training_links = newtraining;
        }
    });
    return jsonData;
};

const exportExcel = (jsonData: any[]): void => {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData);
    delete ws.Comments;
    // eslint-disable-next-line
    const wb: XLSX.WorkBook = { Sheets: { data: ws }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    saveExcelFile(excelBuffer);
};

const saveExcelFile = (buffer: any): void => {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';
    const data: Blob = new Blob([buffer], { type: fileType });
    FileSaver.saveAs(data, getFilename() + fileExtension);
};

const convertToCSV = (objArray: any, headerList: any): string => {
    const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'index,';
    headerList.forEach((element: any) => {
        row += element + ',';
    });
    row = row.slice(0, -1);
    str += row + '\r\n'; // header row
    array.forEach((element: any, idx: any) => {
        let line = idx + 1 + '';
        headerList.forEach((index: any) => {
            const head = index;
            let nextfield = ' ';
            if (array[idx][head] !== undefined && array[idx][head] !== '') {
                nextfield = '"' + array[idx][head] + '"';
            }
            if (typeof nextfield !== 'string') {
                console.log('next field is : ', typeof nextfield);
            }
            line += ',' + nextfield;
        });
        str += line + '\r\n';
    });
    return str;
};
