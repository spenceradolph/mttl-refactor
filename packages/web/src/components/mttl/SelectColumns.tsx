import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';

type SelectColumnsProps = {
    selectedCols: string[];
    setSelectedCols: any;
    columnSelectOpen: any;
    setColumnSelectOpen: any;
};

export const SelectColumns = (Props: SelectColumnsProps) => {
    const { selectedCols, setSelectedCols, columnSelectOpen, setColumnSelectOpen } = Props;

    const selectAll = () => {
        setSelectedCols([
            'ksat_id',
            'description',
            'topic',
            'requirement_owner',
            'requirement_src',
            'work_roles',
            'child_count',
            'parent_count',
            'training_count',
            'eval_count',
            'courses',
            'oam',
            'comments',
        ]);
    };

    const deselectAll = () => {
        setSelectedCols([]);
    };

    const save = () => {
        // TODO: no difference between saving and cancel, production version doesn't 'commit' the changes until save is hit....
        // TODO: save the selection between sessions (via localstorage or cookie or something...)
        setColumnSelectOpen(false);
    };

    const handleCheck = (newColumn: string) => {
        if (!selectedCols.includes(newColumn)) {
            setSelectedCols([...selectedCols, newColumn]);
        } else {
            setSelectedCols(selectedCols.filter((column) => column !== newColumn));
        }
    };

    // TODO: can easily refactor checkboxes to have a cleaner config for vertical alignment probably, one-line per checkbox would be ideal...could create custom component at the bottom...
    return (
        <Dialog open={columnSelectOpen} onClose={() => setColumnSelectOpen(false)}>
            <DialogTitle>Select Columns</DialogTitle>
            <DialogContent>
                <FormControl component="fieldset">
                    <FormGroup aria-label="position">
                        <FormControlLabel
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('ksat_id')} onClick={() => handleCheck('ksat_id')} />
                                </div>
                            }
                            label="KSAT ID"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="description"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('description')} onClick={() => handleCheck('description')} />
                                </div>
                            }
                            label="Description"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="topic"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('topic')} onClick={() => handleCheck('topic')} />
                                </div>
                            }
                            label="Topic"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="requirement_owner"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('requirement_owner')} onClick={() => handleCheck('requirement_owner')} />
                                </div>
                            }
                            label="Owner"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="requirement_src"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('requirement_src')} onClick={() => handleCheck('requirement_src')} />
                                </div>
                            }
                            label="Source"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="workrole"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('work_roles')} onClick={() => handleCheck('work_roles')} />
                                    {selectedCols.includes('work_roles') ? (
                                        <div>
                                            Proficiency
                                            <Checkbox checked={selectedCols.includes('wr_proficiency')} onClick={() => handleCheck('wr_proficiency')} />
                                        </div>
                                    ) : null}
                                </div>
                            }
                            label="Work Role"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="children"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('child_count')} onClick={() => handleCheck('child_count')} />
                                </div>
                            }
                            label="Children"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="parents"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('parent_count')} onClick={() => handleCheck('parent_count')} />
                                </div>
                            }
                            label="Parents"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="training_links"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('training_count')} onClick={() => handleCheck('training_count')} />
                                </div>
                            }
                            label="Training"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="eval_links"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('eval_count')} onClick={() => handleCheck('eval_count')} />
                                </div>
                            }
                            label="Evaluation"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="courses"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('courses')} onClick={() => handleCheck('courses')} />
                                </div>
                            }
                            label="Courses"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="oam"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('oam')} onClick={() => handleCheck('oam')} />
                                </div>
                            }
                            label="Milestones"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                        <FormControlLabel
                            value="comments"
                            control={
                                <div style={{ display: 'table-cell' }}>
                                    <Checkbox checked={selectedCols.includes('comments')} onClick={() => handleCheck('comments')} />
                                </div>
                            }
                            label="Comments"
                            labelPlacement="end"
                            style={{ display: 'table' }}
                        />
                    </FormGroup>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => selectAll()}>Select All</Button>
                <Button onClick={() => deselectAll()}>Deselect All</Button>
                <Button onClick={() => setColumnSelectOpen(false)}>Cancel</Button>
                <Button onClick={() => save()}>Save</Button>
            </DialogActions>
        </Dialog>
    );
};
