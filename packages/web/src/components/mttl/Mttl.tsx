import { CircularProgress } from '@mui/material';
import { useEffect, useState } from 'react';
import { api, AppState, Dispatch } from '../../state';
import './Mttl.scss';
import { MttlTable } from './MttlTable';
import { MttlTopBar } from './MttlTopBar';
import { SelectColumns } from './SelectColumns';
import { ksatFilter, FilterType, ksatSearch } from './ksatFilter';
import { Step, Steps } from 'intro.js-react';
import { useNavigate } from 'react-router-dom';

const steps: Step[] = [
    {
        intro: "Welcome to the 90th Cyber Operations Squadron's <b>Master Training Task List (MTTL)</b> database",
    },
    {
        element: '#m2',
        intro: 'You can search the MTTL here...',
        position: 'bottom',
    },
    {
        element: '#m3',
        intro: '...or filter the MTTL here.',
        position: 'bottom',
    },
    {
        element: '#coverage-legend',
        intro: 'This bar shows the percentage of KSAT mapping completion',
        position: 'left',
    },
    {
        element: '#m4a',
        intro: 'Green - Mapped to training and Evaluation',
        position: 'bottom',
    },
    {
        element: '#m4b',
        intro: 'Orange - only evaluations mapped',
        position: 'bottom',
    },
    {
        element: '#m4c',
        intro: 'Blue - only training mapped',
        position: 'bottom',
    },
    {
        element: '#m4d',
        intro: 'None - Training and Evaluation yet to be mapped',
        position: 'bottom',
    },
    {
        element: '#m6',
        intro: 'Click here to recommend a KSAT',
        position: 'bottom',
    },
    {
        element: '#m7',
        intro: 'Click here to export the MTTL to CSV or XLSX',
        position: 'bottom',
    },
    {
        element: '#m8',
        intro: 'The abbreviated designation for the specific Knowledge, Skill, Ability, or Task [KSAT]',
        position: 'bottom',
    },
    {
        element: '#m9',
        intro: 'An explanation of what is expected for this KSAT',
        position: 'bottom',
    },
    {
        element: '#m10',
        intro: 'The more general subject to which this KSAT belongs',
        position: 'bottom',
    },
    {
        element: '#m11',
        intro: 'The authority requiring this KSAT',
        position: 'bottom',
    },
    {
        element: '#m12',
        intro: 'The work roles that are mapped to this KSAT',
        position: 'bottom',
    },
    {
        element: '#m13',
        intro: 'KSATs necessitated as a consequence of this KSAT',
        position: 'bottom',
    },
    {
        element: '#m14',
        intro: 'KSATS to which this KSAT is a part of',
        position: 'bottom',
    },
    {
        element: '#m15',
        intro: 'Training resources mapped to this KSAT',
        position: 'bottom',
    },
    {
        element: '#m16',
        intro: 'Evaluation resources mapped to this KSAT',
        position: 'bottom',
    },
    {
        element: '#m17',
        intro: 'Most displayed elements of the database are <b>clickable</b>.<br/><i>Click on elements to find or suggest training and evaluation resources.</i>',
    },
];

type MttlProps = {
    state: AppState;
    dispatch: Dispatch;
};

export const Mttl = (Props: MttlProps) => {
    const { state, dispatch } = Props;
    const { mttl, userData, autocomplete } = state;

    // Default columns selected
    // TODO: create a typescript enum and use instead of hard-coded strings (and use in child components)
    // TODO: should refactor to a simple JS Object with properties as the columns and boolean as the state, forget about using strings...
    const [selectedCols, setSelectedCols] = useState<string[]>([
        'ksat_id',
        'description',
        'topic',
        'requirement_owner',
        'requirement_src',
        'work_roles',
        'child_count',
        'parent_count',
        'training_count',
        'eval_count',
        // 'wr_proficiency'
        // 'courses',
        // 'oam',
        // 'comments',
    ]);
    const [columnSelectOpen, setColumnSelectOpen] = useState(false);

    // TODO: typescript types with enum / union so
    const [filters, setFilters] = useState<FilterType[]>([]);
    const [searches, setSearches] = useState<string[]>([]);

    const fetchMttl = async () => {
        const mttlData = await api.fetchMttl();
        dispatch({ type: 'update-mttl', mttlData });
    };

    const fetchAutocomplete = async () => {
        const autocompleteData = await api.fetchAutocomplete();
        dispatch({ type: 'update-autocomplete', autocompleteData });
    };

    useEffect(() => {
        if (!mttl) fetchMttl();
        if (!autocomplete) fetchAutocomplete();
    }, [filters]);

    const filteredMttl = mttl === null ? [] : mttl.filter((ksat) => ksatFilter(ksat, filters)).filter((ksat) => ksatSearch(ksat, searches));

    return (
        <div id="mttl-component">
            <MttlTopBar
                state={state}
                dispatch={dispatch}
                filters={filters}
                setFilters={setFilters}
                filteredMttl={filteredMttl}
                setColumnSelectOpen={setColumnSelectOpen}
                searches={searches}
                setSearches={setSearches}
            />
            {mttl ? (
                <MttlTable autocomplete={autocomplete} ksats={filteredMttl} selectedCols={selectedCols} filters={filters} setFilters={setFilters} userData={userData} />
            ) : (
                <div style={{ position: 'absolute', left: '50%', top: '50%' }}>
                    <CircularProgress />
                </div>
            )}
            <SelectColumns selectedCols={selectedCols} setSelectedCols={setSelectedCols} columnSelectOpen={columnSelectOpen} setColumnSelectOpen={setColumnSelectOpen} />
            <Steps
                enabled={localStorage.getItem('mttl-introJsPartTwo') === null}
                steps={steps}
                initialStep={0}
                onExit={() => {
                    localStorage.setItem('mttl-introJsPartTwo', 'Watched!');
                }}
            />
        </div>
    );
};
