import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { KsatCarouselCard } from './ksat-carousel-card';

import './KsatCarousel.scss';
import { useEffect, useState } from 'react';

export interface KSATCardData {
    ksatId: string;
    description: string;
}

type KsatCarouselProps = {
    visible: boolean;
    content: any[];
    setFilters: any;
    filters: any;
};

export const KsatCarousel = (Props: KsatCarouselProps) => {
    const { visible, content, setFilters, filters } = Props;

    if (!visible) return null;

    const numVisibleCards = 5;
    const [visibleCards, setVisibleCards] = useState<KSATCardData[]>([]);
    const [currIndex, setCurrIndex] = useState(0);

    const indexOfCard = (card: KSATCardData) => {
        for (let i = 0; i < content.length; i++) {
            if (content[i].ksatId === card.ksatId) {
                return i;
            }
        }
        return -1;
    };

    const shiftLeft = () => {
        if (currIndex === 0) {
            setCurrIndex(content.length - (((content.length - 1) % numVisibleCards) + 1));
        } else {
            const tempIndex = currIndex - numVisibleCards;
            setCurrIndex(tempIndex < 0 ? 0 : tempIndex);
        }
    };

    const shiftRight = () => {
        if (currIndex >= content.length - numVisibleCards) {
            setCurrIndex(0);
        } else {
            setCurrIndex(currIndex + numVisibleCards);
        }
    };

    useEffect(() => {
        const tempArray: KSATCardData[] = [];
        for (let i = 0; i < numVisibleCards; i++) {
            if (i + currIndex >= content.length) {
                break;
            }
            tempArray.push(content[i + currIndex]);
        }
        setVisibleCards(tempArray);
    }, [visible, currIndex]);

    return (
        <div className="training-carousel d-flex align-items-center m-10 h-100" style={{ float: 'left', position: 'relative' }}>
            <FontAwesomeIcon icon={faChevronLeft} className="icon-button" size="3x" onClick={() => shiftLeft()} />
            <div className="flex-fill d-flex justify-content-around h-full">
                {visibleCards.map((card, index) => {
                    const { ksatId, description } = card;
                    return <KsatCarouselCard key={index} ksatId={ksatId} description={description} index={indexOfCard(card) + 1} setFilters={setFilters} filters={filters} />;
                })}
            </div>
            <FontAwesomeIcon icon={faChevronRight} className="icon-button" size="3x" onClick={() => shiftRight()} />
        </div>
    );
};
