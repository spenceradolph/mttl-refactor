import { faBook, faHammer, faToolbox, faHome } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FilterType } from '../../../ksatFilter';
import './KsatCarouselCard.scss';

type KsatCarouselCardProps = {
    ksatId: string;
    description: string;
    index: any;
    setFilters: (filters: FilterType[]) => void;
    filters: any;
};

export const KsatCarouselCard = (Props: KsatCarouselCardProps) => {
    const { ksatId, description, index, setFilters, filters } = Props;

    const getIcon = () => {
        let icon = faBook;
        switch (ksatId.charAt(0)) {
            case 'K': {
                icon = faBook;
                break;
            }
            case 'S': {
                icon = faHammer;
                break;
            }
            case 'A': {
                icon = faToolbox;
                break;
            }
            case 'T': {
                icon = faHome;
                break;
            }
            default: {
                break;
            }
        }
        return icon;
    };

    const getIconColor = () => {
        let color = '#000000';
        switch (ksatId.charAt(0)) {
            case 'K': {
                color = '#00abd4';
                break;
            }
            case 'S': {
                color = '#b43000';
                break;
            }
            case 'A': {
                color = '#b48a00';
                break;
            }
            case 'T': {
                color = '#5d00b4';
                break;
            }
            default: {
                break;
            }
        }
        return color;
    };

    return (
        <a className="card bg-light-lm d-flex flex-column align-items-start h-full w-250 p-5 m-0" onClick={() => setFilters([...filters, { column: 'ksat_id', value: ksatId }])}>
            <h2 className="card-title font-size-14 font-weight-bold align-self-center my-10">{ksatId}</h2>
            <p className="card-description m-5">{description}</p>
            <div className="text-white rounded position-absolute top-0 left-0 p-5 m-10" style={{ backgroundColor: getIconColor() }}>
                <FontAwesomeIcon icon={getIcon()} size="lg" />
            </div>
            <p className="text-muted position-absolute m-5 top-0 right-0">{index}</p>
        </a>
    );
};
