import './ResourceCarousel.scss';
import { ResourceCarouselCard } from './resource-carousel-card';
import { ResourceCarouselOptions } from './resource-carousel-options';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight, faCog, faFilter, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { useEffect, useState } from 'react';
import { Tooltip } from '@mui/material';
import { AppState } from '../../../../state';
import { MTTL_Type } from '@mttl/common';

interface CarouselFilterData {
    proficiency: string;
    type: string;
    showComm: boolean;
}

interface CarouselCardData {
    description: string;
    proficiency: string;
    title: string;
    type: string;
    url: string;
}

type ResourceCarouselProps = {
    visible: boolean;
    content: MTTL_Type['training_links'] | MTTL_Type['eval_links'];
    filter: any;
    carouselType: string;
    globalFilter: { active: boolean; profLevel: string; resourceType: string; showCommercial: boolean }; // TODO: don't manually type these, import or declare in 1 spot
    setGlobalFilter: any;
    localFilter: { active: boolean; profLevel: string; resourceType: string; showCommercial: boolean };
    setLocalFilter: any;
};

export const ResourceCarousel = (Props: ResourceCarouselProps) => {
    const { visible, content, filter, carouselType, globalFilter, setGlobalFilter, localFilter, setLocalFilter } = Props;

    if (!visible) return null;

    const [optionsOpen, setOptionsOpen] = useState(false);

    const numVisibleCards = 4;
    const [visibleCards, setVisibleCards] = useState<CarouselCardData[]>([]);
    const [currIndex, setCurrIndex] = useState(0);

    const filteredContent = [...content].filter((thing) => {
        if (localFilter.active) {
            if (localFilter.profLevel !== 'any' && localFilter.profLevel !== thing.proficiency) return false;
            if (localFilter.resourceType !== 'any' && localFilter.resourceType !== thing.type) return false;
            if (!localFilter.showCommercial && thing.type === 'commercial') return false;
        }

        if (globalFilter.active) {
            if (globalFilter.profLevel !== 'any' && globalFilter.profLevel !== thing.proficiency) return false;
            if (globalFilter.resourceType !== 'any' && globalFilter.resourceType !== thing.type) return false;
            if (!globalFilter.showCommercial && thing.type === 'commercial') return false;
        }
        return true;
    });

    const indexOfCard = (card: CarouselCardData) => {
        for (let i = 0; i < filteredContent.length; i++) {
            if (content[i].url === card.url) {
                return i;
            }
        }
        return -1;
    };

    const shiftLeft = () => {
        if (currIndex === 0) {
            // TODO: don't go by content, go by filteredContent eventually (same on shiftRight)
            setCurrIndex(filteredContent.length - (((filteredContent.length - 1) % numVisibleCards) + 1));
        } else {
            const tempIndex = currIndex - numVisibleCards;
            setCurrIndex(tempIndex < 0 ? 0 : tempIndex);
        }
    };

    const shiftRight = () => {
        if (currIndex >= filteredContent.length - numVisibleCards) {
            setCurrIndex(0);
        } else {
            setCurrIndex(currIndex + numVisibleCards);
        }
    };

    useEffect(() => {
        const tempArray: CarouselCardData[] = [];
        for (let i = 0; i < numVisibleCards; i++) {
            if (i + currIndex >= filteredContent.length) {
                break;
            }
            tempArray.push(filteredContent[i + currIndex]);
        }
        setVisibleCards(tempArray);
    }, [visible, currIndex, localFilter, globalFilter]);

    return (
        <div>
            <div className="training-carousel d-flex justify-content-start align-items-center m-10 h-200">
                <div className="align-self-stretch d-flex flex-column justify-content-between w-50">
                    <Tooltip title="Select carousel options">
                        <div>
                            <FontAwesomeIcon icon={faCog} size="3x" className="icon-button" onClick={() => setOptionsOpen(!optionsOpen)} />
                        </div>
                    </Tooltip>
                    <div>
                        {/* TODO: better organization, likely take this outside the return and create it higher in the function */}
                        {localFilter.active ? (
                            <div className="card filter-size bg-light-lm position-relative w-full mx-0 my-5 px-0">
                                <Tooltip title="toolTipFilter">
                                    <span className="clickable-element" onClick={() => {}}>
                                        <FontAwesomeIcon className="position-absolute top-0 left-0 m-5" icon={faFilter} size="lg" />
                                    </span>
                                </Tooltip>
                                <Tooltip title="Clear Local Carousel Filter">
                                    <span className="clickable-element" onClick={() => setLocalFilter({ active: false })}>
                                        <FontAwesomeIcon className="remove-filter position-absolute top-0 right-0 m-5" icon={faTimesCircle} size="lg" />
                                    </span>
                                </Tooltip>
                                <p className="text-muted text-center position-absolute bottom-0 left-0 right-0 m-5">Local</p>
                            </div>
                        ) : null}
                        {globalFilter.active ? (
                            <div className="card filter-size bg-light-lm position-relative w-full mx-0 my-5 px-0">
                                <Tooltip title="toolTipFilter">
                                    <span className="clickable-element" onClick={() => {}}>
                                        <FontAwesomeIcon className="position-absolute top-0 left-0 m-5" icon={faFilter} size="lg" />
                                    </span>
                                </Tooltip>
                                <Tooltip title="Clear Global Carousel Filter">
                                    <span className="clickable-element" onClick={() => setGlobalFilter({ active: false })}>
                                        <FontAwesomeIcon className="remove-filter position-absolute top-0 right-0 m-5" icon={faTimesCircle} size="lg" />
                                    </span>
                                </Tooltip>
                                <p className="text-muted text-center position-absolute bottom-0 left-0 right-0 m-5">Global</p>
                            </div>
                        ) : null}
                    </div>
                </div>

                <FontAwesomeIcon icon={faChevronLeft} size="3x" className="icon-button" onClick={() => shiftLeft()} />

                <div className="flex-fill d-flex justify-content-around h-full">
                    {visibleCards.map((card, index) => {
                        return (
                            <ResourceCarouselCard
                                key={index}
                                index={indexOfCard(card) + 1}
                                title={card.title}
                                description={card.description}
                                type={card.type}
                                proficiency={card.proficiency}
                                url={card.url}
                            />
                        );
                    })}
                </div>

                {filteredContent.length === 0 ? <p className="text-center text-muted flex-fill">No Resources Currently Available for this KSAT</p> : null}

                <FontAwesomeIcon icon={faChevronRight} size="3x" className="icon-button" onClick={() => shiftRight()} />
            </div>
            <ResourceCarouselOptions open={optionsOpen} setOpen={setOptionsOpen} data={{ carType: carouselType }} setLocalFilter={setLocalFilter} setGlobalFilter={setGlobalFilter} />
        </div>
    );
};
