import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControlLabel, FormGroup, FormLabel, MenuItem, Select, Switch } from '@mui/material';
import { useEffect, useState } from 'react';
import './ResourceCarouselOptions.scss';

type ResourceCarouselOptionsProps = {
    data: any;
    open: any;
    setOpen: any;
    setLocalFilter: any;
    setGlobalFilter: any;
};

export const ResourceCarouselOptions = (Props: ResourceCarouselOptionsProps) => {
    const { data, open, setOpen, setLocalFilter, setGlobalFilter } = Props;

    const [applyGlobal, setApplyGlobal] = useState(false);
    const [showCommercial, setShowCommercial] = useState(true);
    const [profLevel, setProfLevel] = useState('any');
    const [resourceType, setResourceType] = useState('any');

    const submitOptions = () => {
        // don't set default options
        if (profLevel === 'any' && showCommercial && resourceType === 'any') {
            setOpen(false);
            return;
        }

        if (applyGlobal) {
            setGlobalFilter({ active: true, profLevel, resourceType, showCommercial });
        } else {
            setLocalFilter({ active: true, profLevel, resourceType, showCommercial });
        }
        setOpen(false);
    };

    useEffect(() => {
        // setApplyGlobal(false);
    }, [open]);

    return (
        <Dialog open={open} onClose={() => setOpen(false)}>
            <DialogTitle>Filter {data.carType} Carousel</DialogTitle>
            <DialogContent style={{ overflow: 'unset' }}>
                <FormGroup>
                    <FormLabel>Proficiency Level</FormLabel>
                    <Select
                        value={profLevel}
                        onChange={(e) => {
                            setProfLevel(e.target.value);
                        }}
                    >
                        <MenuItem value="any">any</MenuItem>
                        <MenuItem value="1">1</MenuItem>
                        <MenuItem value="2">2</MenuItem>
                        <MenuItem value="3">3</MenuItem>
                        <MenuItem value="A">A</MenuItem>
                        <MenuItem value="B">B</MenuItem>
                        <MenuItem value="C">C</MenuItem>
                        <MenuItem value="D">D</MenuItem>
                    </Select>
                </FormGroup>
                <FormGroup>
                    <FormLabel>Resource Type</FormLabel>
                    <Select
                        value={resourceType}
                        onChange={(e) => {
                            setResourceType(e.target.value);
                        }}
                    >
                        <MenuItem value="any">any</MenuItem>
                        {data.carType === 'Training' ? <MenuItem value="IQT">IQT</MenuItem> : null}
                        {data.carType === 'Training' ? <MenuItem value="self-study">Self Study</MenuItem> : null}
                        {data.carType === 'Training' ? <MenuItem value="commercial">Commercial</MenuItem> : null}
                        {data.carType === 'Eval' ? <MenuItem value="ccv-performance">CCV Performance Question</MenuItem> : null}
                        {data.carType === 'Eval' ? <MenuItem value="ccv-knowledge">CCV Knowledge Question</MenuItem> : null}
                    </Select>
                </FormGroup>
                <FormGroup>
                    <FormControlLabel
                        control={<Switch defaultChecked disabled={data.carType === 'Eval'} value={showCommercial} onChange={(e) => setShowCommercial(!showCommercial)} />}
                        label="Show Commercial Material"
                    />
                    <FormControlLabel control={<Switch value={applyGlobal} checked={applyGlobal} onChange={(e) => setApplyGlobal(!applyGlobal)} />} label="Apply Filter Globally" />
                </FormGroup>
            </DialogContent>
            <div className="actions">
                <DialogActions>
                    <Button onClick={() => setOpen(false)}>Close</Button>
                    <Button onClick={() => submitOptions()}>Submit</Button>
                </DialogActions>
            </div>
        </Dialog>
    );
};
