import './ResourceCarouselCard.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBook, faCreditCard, faFlagUsa } from '@fortawesome/free-solid-svg-icons';

type ResourceCarouselCardProps = {
    index: number;
    title: string;
    description: string;
    type: string;
    proficiency: string; // TODO: this was mistyped on the 90th's as number
    url: string;
};

export const ResourceCarouselCard = (Props: ResourceCarouselCardProps) => {
    const { index, title, description, type, proficiency, url } = Props;

    // TODO: possibly export this into a helper file, may be used in ksat carousel as well (and other similar functions)
    const getIconColor = () => {
        let color = '#808080';
        switch (type.charAt(0)) {
            case 'c': {
                color = '#b43000';
                break;
            }
            case 's': {
                color = '#b48a00';
                break;
            }
            case 'I': {
                color = '#00abd4';
                break;
            }
            default: {
                break;
            }
        }
        return color;
    };

    const getIcon = () => {
        let icon = faBook;
        switch (type.charAt(0)) {
            case 'c': {
                //rel-link
                icon = faCreditCard;
                break;
            }
            case 's': {
                //supp-link
                icon = faBook;
                break;
            }
            case 'I': {
                //module
                icon = faFlagUsa;
                break;
            }
            default: {
                break;
            }
        }
        return icon;
    };

    const formatTitleText = (text: string) => {
        return text.replace(/[_-]/g, ' ');
    };

    return (
        <a className="card bg-light-lm d-flex flex-column justify-content-start h-full w-200 p-5 m-0" href={url} target="_blank">
            <div className="align-self-center text-white rounded p-10 m-5" style={{ backgroundColor: getIconColor() }}>
                <FontAwesomeIcon icon={getIcon()} size="3x" />
            </div>
            <h2 className="card-title font-size-14 font-weight-bold my-5">{formatTitleText(title)}</h2>
            <p className="card-description mb-20">{description}</p>
            <p className="text-muted position-absolute m-5 top-0 left-0">{index}</p>
            <p className="text-muted position-absolute m-5 bottom-0 left-0">proficiency: {proficiency}</p>
            <p className="text-muted position-absolute m-5 bottom-0 right-0">{type}</p>
        </a>
    );
};
