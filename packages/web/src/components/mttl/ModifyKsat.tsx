import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import { api } from '../../state';

type ModifyKsatProps = {
    modifyKsatOpen: any;
    setModifyKsatOpen: any;
    ksat_id: string;
};

export const ModifyKsat = (Props: ModifyKsatProps) => {
    const { modifyKsatOpen, setModifyKsatOpen, ksat_id } = Props;

    const [desc, setDesc] = useState('');

    const submitModifyKsat = async () => {
        const results = await api.submitModifyKsat(desc, ksat_id);
        if (results.error) return; // TODO: error handling (not authenticated and other error: '')
        alert('submitted the suggestion!'); // TODO: 90th uses the sticky alerts again (I think from halfmoon)
        setModifyKsatOpen(false);
    };

    return (
        <Dialog open={modifyKsatOpen} onClose={() => setModifyKsatOpen(false)}>
            <DialogTitle>Modify KSAT: {ksat_id}</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="ksat_modification"
                    label="Suggested Modification"
                    fullWidth
                    variant="standard"
                    required
                    value={desc}
                    onChange={(e) => {
                        setDesc(e.target.value);
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setModifyKsatOpen(false)}>Close</Button>
                <Button onClick={() => submitModifyKsat()}>Submit</Button>
            </DialogActions>
        </Dialog>
    );
};
