import { faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Autocomplete, FormGroup, FormLabel, Tooltip } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import { useEffect, useState } from 'react';
import { api, AppState } from '../../state';

type NewLinkProps = {
    newLinkOpen: any;
    setNewLinkOpen: any;
    ksat_id: string;
    link_type: 'Training' | 'Eval';
    autocomplete: AppState['autocomplete'];
};

export const NewLink = (Props: NewLinkProps) => {
    const { newLinkOpen, setNewLinkOpen, ksat_id, link_type, autocomplete } = Props;

    const [desc, setDesc] = useState('');
    const [category, setCategory] = useState('');
    const [links, setLinks] = useState('');
    const [references, setReferences] = useState('');

    const [resourceType, setResourceType] = useState('');
    const [otherVisible, setOtherVisible] = useState(false);
    const [other, setOther] = useState('');
    const [ksatData, setKsatData] = useState([{ ksat_id: '', proficiency: '' }]);

    const submitNewLink = async () => {
        const linkData = {
            ksat_id,
            link_type,
            desc,
            resourceType,
            other,
            category,
            links,
            ksatData,
        };
        const results = await api.submitNewLink(JSON.stringify(linkData));
        if (results.error) return; // TODO: error handling (not authenticated and other error: '')
        alert('submitted the new link!'); // TODO: 90th uses the sticky alerts again (I think from halfmoon)
        setNewLinkOpen(false);
    };

    useEffect(() => {
        setKsatData([{ ksat_id, proficiency: '' }]);
    }, [newLinkOpen]);

    return (
        <Dialog open={newLinkOpen} onClose={() => setNewLinkOpen(false)}>
            <DialogTitle>Add {link_type} Link</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    label={`${link_type} Description`}
                    fullWidth
                    variant="standard"
                    value={desc}
                    onChange={(e) => {
                        setDesc(e.target.value);
                    }}
                />

                {ksatData.map((data, index) => {
                    return (
                        <FormGroup row key={index}>
                            <Autocomplete
                                onChange={(e) => {
                                    const tempData = [...ksatData];
                                    // @ts-ignore
                                    tempData[index].ksat_id = e.target.textContent;
                                    setKsatData(tempData);
                                }}
                                disableClearable
                                value={data.ksat_id}
                                style={{ width: '45%' }}
                                options={data.ksat_id === '' ? autocomplete['ksats_id'] : [data.ksat_id]}
                                renderInput={(params) => <TextField {...params} label="KSAT" />}
                                isOptionEqualToValue={(option: any, value: any) => option.value === value.value} // used to disable warning, happens when previous selection still in TextField and isn't actually an option
                            />
                            <Autocomplete
                                onChange={(e) => {
                                    const tempData = [...ksatData];
                                    // @ts-ignore
                                    tempData[index].proficiency = e.target.textContent;
                                    setKsatData(tempData);
                                }}
                                disableClearable
                                value={data.proficiency}
                                style={{ width: '45%' }}
                                options={data.ksat_id === '' ? [] : data.ksat_id.charAt(0) === 'K' ? ['A', 'B', 'C', 'D'] : ['1', '2', '3']}
                                renderInput={(params) => <TextField {...params} label="Proficiency" />}
                                isOptionEqualToValue={(option: any, value: any) => option.value === value.value} // used to disable warning, happens when previous selection still in TextField and isn't actually an option
                            />
                            {ksatData.length > 1 ? (
                                <Tooltip title="Remove Ksat">
                                    <div>
                                        <FontAwesomeIcon
                                            className="clickable-item"
                                            icon={faTrash}
                                            onClick={() => {
                                                // setKsatData([...ksatData, { ksat_id: '', proficiency: '' }]);
                                                const tempData = [...ksatData].filter((ksat, index1) => index1 !== index); // TODO: again, don't need all the array copies and spread...not efficient anyway but safer for dummies
                                                setKsatData([...tempData]);
                                            }}
                                        />
                                    </div>
                                </Tooltip>
                            ) : null}
                            {ksatData.length === index + 1 && data.ksat_id !== '' && data.proficiency !== '' ? (
                                <Tooltip title="Add Ksat">
                                    <div>
                                        <FontAwesomeIcon
                                            className="clickable-item"
                                            icon={faPlus}
                                            onClick={() => {
                                                setKsatData([...ksatData, { ksat_id: '', proficiency: '' }]);
                                            }}
                                        />
                                    </div>
                                </Tooltip>
                            ) : null}
                        </FormGroup>
                    );
                })}

                <TextField
                    margin="dense"
                    label={`${link_type} Category`}
                    helperText="Recommend one word category"
                    fullWidth
                    variant="standard"
                    value={category}
                    onChange={(e) => {
                        setCategory(e.target.value);
                    }}
                />
                <TextField
                    margin="dense"
                    label={`${link_type} Links`}
                    helperText="URLs that provide relevant training"
                    fullWidth
                    variant="standard"
                    value={links}
                    onChange={(e) => {
                        setLinks(e.target.value);
                    }}
                />
                <TextField
                    margin="dense"
                    label={`${link_type} References`}
                    helperText="Offline training resources, sources of training links, etc."
                    fullWidth
                    variant="standard"
                    value={references}
                    onChange={(e) => {
                        setReferences(e.target.value);
                    }}
                />
                <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Resource Type</InputLabel>
                    <Select
                        value={resourceType}
                        labelId="demo-simple-select-label"
                        label="Resource Type"
                        onChange={(e) => {
                            setResourceType(e.target.value);
                            if (e.target.value === 'other') {
                                setOtherVisible(true);
                            } else {
                                setOtherVisible(false);
                            }
                        }}
                    >
                        <MenuItem value={'inperson-course'}>In Person Course</MenuItem>
                        <MenuItem value={'online-course'}>Web Based Course</MenuItem>
                        <MenuItem value={'web-text'}>Web-text: walk through, reference, etc.</MenuItem>
                        <MenuItem value={'video'}>Web-video: e.g. YouTube</MenuItem>
                        <MenuItem value={'text'}>Text based reference</MenuItem>
                        <MenuItem value={'other'}>Other, please specify</MenuItem>
                    </Select>
                </FormControl>
                <TextField
                    margin="dense"
                    label={`Other - Please describe`}
                    fullWidth
                    style={{ display: otherVisible ? '' : 'none' }}
                    variant="standard"
                    value={other}
                    onChange={(e) => {
                        setOther(e.target.value);
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setNewLinkOpen(false)}>Close</Button>
                <Button onClick={() => submitNewLink()}>Submit</Button>
            </DialogActions>
        </Dialog>
    );
};
