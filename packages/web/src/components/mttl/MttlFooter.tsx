import { ChangeEvent } from 'react';
import ReactPaginate from 'react-paginate';

type MttlFooterProps = {
    handlePageClick: any;
    pageCount: number;
    setItemsPerPage: any;
    itemsPerPage: any;
    itemOffset: any;
    numElements: number;
};

export const MttlFooter = (Props: MttlFooterProps) => {
    const { handlePageClick, pageCount, setItemsPerPage, itemsPerPage, itemOffset, numElements } = Props;

    const selectNumPerPage = (event: ChangeEvent<HTMLSelectElement>) => {
        event.preventDefault();
        localStorage.setItem('mttl-pagination', event.currentTarget.value); // TODO: should have constants for these hard coded strings (since they are used in different files)
        setItemsPerPage(event.currentTarget.value);
        event.stopPropagation();
    };

    return (
        <div className="footer" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <div className="pagination-size" style={{ width: '20%', display: 'float' }}>
                <div>KSATs Per Page:</div>
                <select className="form-control size-controller" onChange={(e) => selectNumPerPage(e)} value={itemsPerPage}>
                    <option value={25}>25</option>
                    <option value={50}>50</option>
                    <option value={100}>100</option>
                    <option value={1000}>1000</option>
                </select>
            </div>
            <div id="paginate2id" style={{ margin: '0 auto', left: '50%' }}>
                <ReactPaginate
                    nextLabel="next >"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={3}
                    marginPagesDisplayed={1}
                    pageCount={pageCount}
                    previousLabel="< previous"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination2"
                    activeClassName="active"
                />
            </div>
            <span className="results-numbers" style={{ float: 'right', display: 'float' }}>
                KSAT {itemOffset + 1}-{Math.min(numElements, itemOffset + itemsPerPage)} of {numElements}
                &nbsp; &nbsp;{' '}
            </span>
        </div>
    );
};
