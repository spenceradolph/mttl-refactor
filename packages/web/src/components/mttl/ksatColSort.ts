import { MTTL_Type } from '@mttl/common';

// TODO: this is typed 2 times
type SortState = {
    col: string;
    direction: 'desc' | 'asc';
};

export const ksatColSort = (ksat_a: MTTL_Type, ksat_b: MTTL_Type, sort: SortState): number => {
    const compareA = sort.direction === 'asc' ? ksat_a : ksat_b;
    const compareB = sort.direction === 'asc' ? ksat_b : ksat_a;

    if (sort.col === 'ksat_id') {
        if (compareA.ksat_id.toUpperCase() < compareB.ksat_id.toUpperCase()) return -1;
        if (compareA.ksat_id.toUpperCase() > compareB.ksat_id.toUpperCase()) return 1;
    }

    if (sort.col === 'description') {
        if (compareA.description.toUpperCase() < compareB.description.toUpperCase()) return -1;
        if (compareA.description.toUpperCase() > compareB.description.toUpperCase()) return 1;
    }

    if (sort.col === 'topic') {
        // TODO: noteworthy that it's given to us as a string[] but all of them only have 1 element...
        // TODO: figure out how 90th sorts with multiple items in these arrays (but for now probably low on priority)
        // Likely they just use the first item in the list, but could be a combination of each item in the list?
        const compareItemA = compareA.topic[0]?.toUpperCase() ?? '';
        const compareItemB = compareB.topic[0]?.toUpperCase() ?? '';
        if (compareItemA < compareItemB) return -1;
        if (compareItemA > compareItemB) return 1;
    }

    if (sort.col === 'requirement_owner') {
        const compareItemA = compareA.requirement_owner[0]?.toUpperCase() ?? '';
        const compareItemB = compareB.requirement_owner[0]?.toUpperCase() ?? '';
        if (compareItemA < compareItemB) return -1;
        if (compareItemA > compareItemB) return 1;
    }

    if (sort.col === 'requirement_src') {
        const compareItemA = compareA.requirement_src[0]?.toUpperCase() ?? '';
        const compareItemB = compareB.requirement_src[0]?.toUpperCase() ?? '';
        if (compareItemA < compareItemB) return -1;
        if (compareItemA > compareItemB) return 1;
    }

    if (sort.col === 'work_roles') {
        const compareItemA = compareA.work_roles[0]?.toUpperCase() ?? '';
        const compareItemB = compareB.work_roles[0]?.toUpperCase() ?? '';
        if (compareItemA < compareItemB) return -1;
        if (compareItemA > compareItemB) return 1;
    }

    if (sort.col === 'child_count') {
        if (parseInt(compareA.child_count) < parseInt(compareB.child_count)) return -1;
        if (parseInt(compareA.child_count) > parseInt(compareB.child_count)) return 1;
    }

    if (sort.col === 'parent_count') {
        if (parseInt(compareA.parent_count) < parseInt(compareB.parent_count)) return -1;
        if (parseInt(compareA.parent_count) > parseInt(compareB.parent_count)) return 1;
    }

    if (sort.col === 'training_count') {
        if (parseInt(compareA.training_count) < parseInt(compareB.training_count)) return -1;
        if (parseInt(compareA.training_count) > parseInt(compareB.training_count)) return 1;
    }

    if (sort.col === 'eval_count') {
        if (parseInt(compareA.eval_count) < parseInt(compareB.eval_count)) return -1;
        if (parseInt(compareA.eval_count) > parseInt(compareB.eval_count)) return 1;
    }

    if (sort.col === 'courses') {
        const compareItemA = compareA.courses[0]?.toUpperCase() ?? '';
        const compareItemB = compareB.courses[0]?.toUpperCase() ?? '';
        if (compareItemA < compareItemB) return -1;
        if (compareItemA > compareItemB) return 1;
    }

    if (sort.col === 'oam') {
        const compareItemA = compareA.oam[0]?.toUpperCase() ?? '';
        const compareItemB = compareB.oam[0]?.toUpperCase() ?? '';
        if (compareItemA < compareItemB) return -1;
        if (compareItemA > compareItemB) return 1;
    }

    // TODO: 90th's sort seems different, they may keep the previous sort and work from that (probably not too hard to implement that way...)
    // If equal, sorting always goes by ksat_id at the end, ascending
    if (ksat_a.ksat_id.toUpperCase() < ksat_b.ksat_id.toUpperCase()) return -1;
    if (ksat_a.ksat_id.toUpperCase() > ksat_b.ksat_id.toUpperCase()) return 1;
    return 0; // Likely never hit because no 2 ksats have the same ID
};
