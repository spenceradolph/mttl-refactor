import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import { api } from '../../state';

type BugReportProps = {
    bugReportOpen: any;
    setBugReportOpen: any;
};

export const BugReport = (Props: BugReportProps) => {
    const { bugReportOpen, setBugReportOpen } = Props;

    const [bugShort, setBugShort] = useState('');
    const [bugDesc, setBugDesc] = useState('');
    const [bugUrl, setBugUrl] = useState('');

    const submitBugReport = async () => {
        setBugUrl('Waiting for issue url...');
        const results = await api.submitBugReport(bugShort, bugDesc);
        if (results.error) return; // TODO: error handling (not authenticated and other error: '')
        const { url } = results;
        // TODO: better displaying of loading, url, and overall making it similar to 90th production
        // Also disable submit and stuff after submitting....so we don't do it twice
        setBugUrl(url);
    };

    return (
        <Dialog open={bugReportOpen} onClose={() => setBugReportOpen(false)}>
            <DialogTitle>Report a Bug</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="bug_description"
                    label="Problem Description"
                    fullWidth
                    variant="standard"
                    required
                    value={bugShort}
                    onChange={(e) => {
                        setBugShort(e.target.value);
                    }}
                />
                <TextField
                    margin="dense"
                    id="steps_to_recreate"
                    label="Steps to Recreate"
                    fullWidth
                    variant="standard"
                    value={bugDesc}
                    onChange={(e) => {
                        setBugDesc(e.target.value);
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setBugReportOpen(false)}>Close</Button>
                <Button onClick={() => submitBugReport()}>Submit</Button>
            </DialogActions>
            <div style={{ display: bugUrl === '' ? 'none' : '' }}>{bugUrl}</div>
        </Dialog>
    );
};
