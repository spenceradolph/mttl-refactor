import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faBug, faCode, faFileExport, faList, faPlusSquare, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MTTL_Type } from '@mttl/common';
import { Autocomplete, Divider, FormControl, FormLabel, MenuItem, Select, TextField, Tooltip } from '@mui/material';
import { useState } from 'react';
import { AppState, Dispatch } from '../../state';
import { BugReport } from './BugReport';
import { NewKsat } from './NewKsat';
import { downloadFile } from './exportData';
import { FilterType } from './ksatFilter';

type MttlTopBarProps = {
    state: AppState;
    dispatch: Dispatch;
    filters: FilterType[];
    setFilters: (newFilters: FilterType[]) => void;
    filteredMttl: MTTL_Type[];
    setColumnSelectOpen: any;
    searches: string[];
    setSearches: (newSearches: string[]) => void;
};

export const MttlTopBar = (Props: MttlTopBarProps) => {
    const { state, dispatch, filters, setFilters, filteredMttl, setColumnSelectOpen, searches, setSearches } = Props;
    const { mttl, userData, autocomplete } = state;

    const [bugReportOpen, setBugReportOpen] = useState(false);
    const [newKsatOpen, setNewKsatOpen] = useState(false);
    const [exportDropdown, setExportDropdown] = useState(false);

    const trainingAndEvalPercent =
        filteredMttl.length === 0 ? 0 : Math.round(([...filteredMttl.filter((mttl) => parseInt(mttl.training_count) * parseInt(mttl.eval_count) > 0)].length / filteredMttl.length) * 100);
    const evalPercent =
        filteredMttl.length === 0 ? 0 : Math.round(([...filteredMttl.filter((mttl) => parseInt(mttl.training_count) === 0 && parseInt(mttl.eval_count) > 0)].length / filteredMttl.length) * 100);
    const trainingPercent =
        filteredMttl.length === 0 ? 0 : Math.round(([...filteredMttl.filter((mttl) => parseInt(mttl.training_count) > 0 && parseInt(mttl.eval_count) === 0)].length / filteredMttl.length) * 100);
    const noCoveragePercent =
        filteredMttl.length === 0 ? 0 : Math.round(([...filteredMttl.filter((mttl) => parseInt(mttl.training_count) === 0 && parseInt(mttl.eval_count) === 0)].length / filteredMttl.length) * 100);

    const filterOptions = ['Work Role', 'Topic', 'Owner', 'Source', 'Course', 'Parent', 'KSAT ID', 'KSAT Type', 'Milestone'];
    const getRealFilter = (filter: string) => {
        switch (filter) {
            case 'Work Role':
                return 'work_roles';
            case 'Topic':
                return 'topic';
            case 'Owner':
                return 'requirement_owner';
            case 'Source':
                return 'requirement_src';
            case 'Course':
                return 'courses';
            case 'Parent':
                return 'parents';
            case 'KSAT ID':
                return 'ksats_id';
            case 'KSAT Type':
                return 'ksat_type';
            case 'Milestone':
                return 'oam';
            default:
                return '';
        }
    };
    const getRealRealFilter = (filter: string) => {
        switch (filter) {
            case 'work_roles':
                return 'work_roles';
            case 'topic':
                return 'topic';
            case 'requirement_owner':
                return 'requirement_owner';
            case 'requirement_src':
                return 'requirement_src';
            case 'courses':
                return 'courses';
            case 'parents':
                return 'parents';
            case 'ksats_id':
                return 'ksat_id';
            case 'ksat_type':
                return 'ksat_type';
            case 'oam':
                return 'oam';
            default:
                return '';
        }
    };

    const [filterSelected, setFilterSelected] = useState('');
    const [searchField, setSearchField] = useState('');

    return (
        <>
            <div id="m1" className="navbar second-level">
                <div className="form-control d-inline search-field ml-5">
                    <div id="search-input">
                        <div className="ml-5 search-boxes my-auto custom-mat">
                            <span>
                                <FontAwesomeIcon icon={faSearch} className="ml-5 my-auto" />
                            </span>
                            <TextField
                                value={searchField}
                                onChange={(e) => setSearchField(e.target.value)}
                                onKeyDown={(e) => {
                                    if (e.key == 'Enter') {
                                        // TODO: if search doesn't already exist
                                        setSearches([...searches, searchField]);
                                    }
                                }}
                            ></TextField>
                            {searches.map((search, index) => {
                                return (
                                    <div
                                        key={index}
                                        onClick={() => {
                                            setSearches(searches.filter((thisSearch) => thisSearch !== search));
                                        }}
                                    >
                                        {search}
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
                <Divider orientation="vertical" className="vert-divider bg-light-dm ml-10 mr-10" />
                <div className="form-control w-auto mr-5">
                    <FormControl className="w-150" id="search-input">
                        <FormLabel>Filter By</FormLabel>
                        <Select label="Filter" value={filterSelected} onChange={(e) => setFilterSelected(e.target.value)}>
                            {filterOptions.map((filter, index) => {
                                return (
                                    <MenuItem value={getRealFilter(filter)} key={index}>
                                        {filter}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                </div>
                <div className="form-control filter-field mr-5">
                    <div id="search-input">
                        <div className="ml-5 search-boxes my-auto custom-mat">
                            <Autocomplete
                                options={autocomplete === null || filterSelected === '' ? [] : autocomplete[`${filterSelected}`]}
                                renderInput={(params) => <TextField {...params} label="Filter" />}
                                onChange={(e) => {
                                    // @ts-ignore
                                    if (e.target.textContent === '') return;
                                    // if (!filters.includes())
                                    // console.log(e);
                                    // @ts-ignore // TODO: better setup for values and onChange
                                    setFilters([...filters, { column: getRealRealFilter(filterSelected), value: e.target.textContent }]);
                                }}
                            />
                            {filters.map((filter, index) => {
                                return (
                                    <div
                                        key={index}
                                        onClick={() => {
                                            // TODO: put this function outside of this?
                                            setFilters(filters.filter((thisFilter) => thisFilter !== filter));
                                        }}
                                    >
                                        {filter.value}
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
                <Divider orientation="vertical" className="vert-divider bg-light-dm ml-10 mr-10" />
                <div id="clear_filter">
                    <button
                        id="clear_filter"
                        className="btn"
                        onClick={() => {
                            setFilters([]);
                            setSearches([]);
                        }}
                    >
                        Clear All
                    </button>
                </div>
            </div>

            <div className="navbar third-level">
                <ul className="navbar-nav float-left d-none d-md-flex" id="top-navbar-btns">
                    <li className="nav-item">
                        <a href="https://gitlab.com/spenceradolph/mttl-refactor">
                            <button className="btn">
                                <span className="fa-stack" style={{ verticalAlign: 'top' }}>
                                    <FontAwesomeIcon icon={faGitlab} className="fas fa-stack-4x" />
                                    <FontAwesomeIcon icon={faCode} className="fas fa-stack-2x codebutton" />
                                </span>
                            </button>
                        </a>
                    </li>
                </ul>

                <div id="coverage-legend">
                    <span
                        id="m4a"
                        className="training-and-eval"
                        style={{ width: `${trainingAndEvalPercent}%` }}
                        onClick={() => {
                            if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'coverage', value: 'training-and-eval' }))) return;
                            setFilters([...filters, { column: 'coverage', value: 'training-and-eval' }]);
                        }}
                    >
                        <div className="percentage">{trainingAndEvalPercent}%</div>
                        <div className="desc">Training and Eval</div>
                    </span>
                    <span
                        id="m4b"
                        className="eval-only"
                        style={{ width: `${evalPercent}%` }}
                        onClick={() => {
                            if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'coverage', value: 'eval-no-training' }))) return;
                            setFilters([...filters, { column: 'coverage', value: 'eval-no-training' }]);
                        }}
                    >
                        <div className="percentage">{evalPercent}%</div>
                        <div className="desc">Eval</div>
                    </span>
                    <span
                        id="m4c"
                        className="training-only"
                        style={{ width: `${trainingPercent}%` }}
                        onClick={() => {
                            if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'coverage', value: 'training-no-eval' }))) return;
                            setFilters([...filters, { column: 'coverage', value: 'training-no-eval' }]);
                        }}
                    >
                        <div className="percentage">{trainingPercent}%</div>
                        <div className="desc">Training</div>
                    </span>
                    <span
                        id="m4d"
                        className="no-training-and-eval"
                        style={{ width: `${noCoveragePercent}%` }}
                        onClick={() => {
                            if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'coverage', value: 'no-coverage' }))) return;
                            setFilters([...filters, { column: 'coverage', value: 'no-coverage' }]);
                        }}
                    >
                        <div className="percentage">{noCoveragePercent}%</div>
                        <div className="desc">Not Covered</div>
                    </span>
                </div>

                <ul className="navbar-nav float-right d-none d-md-flex" id="top-navbar-btns">
                    <li className="nav-item">
                        <Tooltip title="Add KSAT">
                            <button
                                id="m6"
                                className="btn"
                                onClick={() => {
                                    if (!userData) {
                                        // TODO: make this better
                                        alert('must be logged in to use this feature...');
                                        return;
                                    }
                                    setNewKsatOpen(true);
                                }}
                            >
                                <FontAwesomeIcon icon={faPlusSquare} />
                            </button>
                        </Tooltip>
                    </li>
                    <li className="nav-item">
                        <div id="csv-selector" className={`dropdown ${exportDropdown ? 'show' : ''}`} onClick={() => setExportDropdown(!exportDropdown)}>
                            <Tooltip title="Export Data">
                                <button className="btn" id="nav-link-dropdown-toggle">
                                    <FontAwesomeIcon icon={faFileExport} />
                                </button>
                            </Tooltip>
                            <div id="m7" className="dropdown-menu dropdown-menu-right">
                                <button
                                    className="btn export-button"
                                    onClick={() => {
                                        downloadFile('csv', JSON.parse(JSON.stringify(mttl)));
                                    }}
                                >
                                    {' '}
                                    Export CSV Data{' '}
                                </button>
                                <button
                                    className="btn export-button"
                                    onClick={() => {
                                        // downloadFile('xlsx', JSON.parse(JSON.stringify(mttl))); // TODO: fix this
                                        alert('currently broken');
                                    }}
                                >
                                    {' '}
                                    Export XLSX Data{' '}
                                </button>
                            </div>
                        </div>
                    </li>
                    <li className="nav-item">
                        <Tooltip title="Report a bug">
                            <button
                                className="btn"
                                onClick={() => {
                                    if (!userData) {
                                        // TODO: make this better
                                        alert('must be logged in to use this feature...');
                                        return;
                                    }
                                    setBugReportOpen(true);
                                }}
                            >
                                <FontAwesomeIcon icon={faBug} />
                            </button>
                        </Tooltip>
                    </li>
                    <li className="nav-item">
                        <div id="columns-selector" className="dropdown">
                            <Tooltip title="Select viewable columns">
                                <button className="btn" onClick={() => setColumnSelectOpen(true)}>
                                    <FontAwesomeIcon icon={faList} />
                                </button>
                            </Tooltip>
                        </div>
                    </li>
                </ul>
            </div>
            <BugReport bugReportOpen={bugReportOpen} setBugReportOpen={setBugReportOpen} />
            <NewKsat newKsatOpen={newKsatOpen} setNewKsatOpen={setNewKsatOpen} autocomplete={autocomplete} />
        </>
    );
};
