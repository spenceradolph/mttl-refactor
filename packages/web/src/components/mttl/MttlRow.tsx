import { faBook, faChalkboardTeacher, faCheckDouble, faChild, faEdit, faMapMarker, faPlus, faStopwatch, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MTTL_Type } from '@mttl/common';
import { Tooltip } from '@mui/material';
import { FilterType } from './ksatFilter';
import { KSATCardData, KsatCarousel, ResourceCarousel } from './carousel';
import { useEffect, useState } from 'react';

const classNameCoverageSelector = (KSAT: MTTL_Type) => {
    const { training_links, eval_links } = KSAT;
    if (training_links.length > 0 && eval_links.length > 0) return 'training-and-eval';
    if (training_links.length > 0 && eval_links.length === 0) return 'training-no-eval';
    if (training_links.length === 0 && eval_links.length > 0) return 'eval-no-training';
    return 'no-coverage';
};

const iconCoverageSelector = (KSAT: MTTL_Type) => {
    const { training_links, eval_links } = KSAT;
    if (training_links.length > 0 && eval_links.length > 0) return <FontAwesomeIcon icon={faCheckDouble} scale={0.5} />;
    if (training_links.length > 0 && eval_links.length === 0) return <FontAwesomeIcon icon={faChalkboardTeacher} scale={0.5} />;
    if (training_links.length === 0 && eval_links.length > 0) return <FontAwesomeIcon icon={faStopwatch} scale={0.5} />;
    if (training_links.length === 0 && eval_links.length === 0) return <FontAwesomeIcon icon={faMapMarker} scale={0.5} />;
};

type MttlRowProps = {
    ksats: MTTL_Type[];
    KSAT: MTTL_Type;
    selectedCols: string[];
    filters: FilterType[];
    setFilters: any;
    editKsat: any;
    createLink: any;
    globalTFilter: any;
    globalEFilter: any;
    setGlobalTFilter: any;
    setGlobalEFilter: any;
};

export const MttlRow = (Props: MttlRowProps) => {
    const { KSAT, selectedCols, filters, setFilters, editKsat, createLink, ksats, globalTFilter, setGlobalTFilter, globalEFilter, setGlobalEFilter } = Props;
    const {
        description,
        ksat_id,
        topic,
        requirement_owner,
        requirement_src,
        work_roles,
        child_count,
        training_count,
        parents,
        eval_count,
        courses,
        oam,
        comments,
        children,
        training_links,
        eval_links,
        proficiency_data,
    } = KSAT;

    const [childCarouselVisible, setChildCarouselVisible] = useState(false);
    const [parentCarouselVisible, setParentCarouselVisible] = useState(false);
    const [trainingCarouselVisible, setTrainingCarouselVisible] = useState(false);
    const [evalCarouselVisible, setEvalCarouselVisible] = useState(false);
    // TODO: figure out coverage type

    const coverageCell = (
        <td className="mat-cell cdk-cell cdk-column-ksat_id mat-column-ksat_id" style={{ height: '65px', maxHeight: '65px' }}>
            <span
                className={`coverage ${classNameCoverageSelector(KSAT)}`}
                style={{ maxWidth: '42px !important', height: '65px', maxHeight: '65px' }}
                onClick={() => {
                    if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'coverage', value: classNameCoverageSelector(KSAT) }))) return;
                    setFilters([...filters, { column: 'coverage', value: classNameCoverageSelector(KSAT) }]);
                }}
            >
                <div className="coverage-desc">{iconCoverageSelector(KSAT)}</div>
            </span>
        </td>
    );

    const ksatCell = (
        <td className="mat-cell cdk-cell cdk-column-ksat_id mat-column-ksat_id ng-tns-c130-0 ng-star-inserted">
            <Tooltip title="Request KSAT Modification">
                <span className="clickable-element" onClick={() => editKsat(ksat_id)}>
                    <FontAwesomeIcon icon={faEdit} />
                </span>
            </Tooltip>
            {` ${ksat_id} `}
        </td>
    );

    const descriptionCell = <td className="mat-cell cdk-cell cdk-column-description mat-column-description ng-tns-c130-0 ng-star-inserted">{description}</td>;

    const topicCell = (
        <td>
            {topic.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'topic', value: item }))) return;
                        setFilters([...filters, { column: 'topic', value: item }]);
                    }}
                >
                    {item}
                </div>
            ))}
        </td>
    );

    const ownerCell = (
        <td>
            {requirement_owner.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'requirement_owner', value: item }))) return;
                        setFilters([...filters, { column: 'requirement_owner', value: item }]);
                    }}
                >
                    {item}
                </div>
            ))}
        </td>
    );

    const sourceCell = (
        <td>
            {requirement_src.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'requirement_src', value: item }))) return;
                        setFilters([...filters, { column: 'requirement_src', value: item }]);
                    }}
                >
                    {item}
                </div>
            ))}
        </td>
    );

    const workRoleCell = (
        <td>
            {work_roles.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'work_roles', value: item }))) return;
                        setFilters([...filters, { column: 'work_roles', value: item }]);
                    }}
                >
                    {item}
                </div>
            ))}
        </td>
    );

    const proficiencyCell = (
        <td>
            {proficiency_data.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'work_roles', value: item.workrole }))) return;
                        setFilters([...filters, { column: 'work_roles', value: item.workrole }]);
                    }}
                >
                    {item.workrole} : {item.proficiency}
                </div>
            ))}
        </td>
    );

    const childrenCell = (
        <td>
            <span
                className="clickable-element"
                onClick={() => {
                    setParentCarouselVisible(false);
                    setTrainingCarouselVisible(false);
                    setEvalCarouselVisible(false);
                    if (child_count === '0') return;
                    setChildCarouselVisible(!childCarouselVisible);
                }}
            >
                {/* TODO: just use children.length, don't mess with child_count (even though its part of the data? (maybe work to remove it...)) */}
                <FontAwesomeIcon icon={faChild} /> {child_count === '0' ? ` None` : ` ${children[0]}`} {parseInt(child_count) > 1 ? `+ ${children.length - 1} more` : ``}
            </span>
        </td>
    );

    const parentsCell = (
        <td>
            <span
                className="clickable-element"
                onClick={() => {
                    // TODO: should have some function to 'close all carousels' or similar...hard to keep track if there will eventually be other carousels to close...
                    // could also have a generic 'handle click children' type of thing, but same concept
                    setChildCarouselVisible(false);
                    setTrainingCarouselVisible(false);
                    setEvalCarouselVisible(false);
                    if (parents.length === 0) return; // TODO: why do we have parents array but not parent_count? seems like a disconnect
                    setParentCarouselVisible(!parentCarouselVisible);
                }}
            >
                <FontAwesomeIcon icon={faUsers} /> {parents.length === 0 ? ` None` : ` ${parents[0]}`} {parents.length > 1 ? `+ ${parents.length - 1} more` : ``}
            </span>
        </td>
    );

    const trainingCell = (
        <>
            <td>
                <span
                    className="clickable-element"
                    onClick={() => {
                        setChildCarouselVisible(false);
                        setParentCarouselVisible(false);
                        setEvalCarouselVisible(false);
                        if (training_count === '0') return;
                        setTrainingCarouselVisible(!trainingCarouselVisible);
                    }}
                >
                    <FontAwesomeIcon icon={faBook} /> {training_count} Trainings
                </span>
            </td>
            <td>
                <Tooltip title="Add Training">
                    <div onClick={() => createLink(ksat_id, 'Training')}>
                        <FontAwesomeIcon className="clickable-element" icon={faPlus} />
                    </div>
                </Tooltip>
            </td>
        </>
    );

    const evaluationCell = (
        <>
            <td>
                <span
                    className="clickable-element"
                    onClick={() => {
                        setChildCarouselVisible(false);
                        setParentCarouselVisible(false);
                        setTrainingCarouselVisible(false);
                        if (eval_count === '0') return;
                        setEvalCarouselVisible(!evalCarouselVisible);
                    }}
                >
                    <FontAwesomeIcon icon={faStopwatch} /> {eval_count} Evals
                </span>
            </td>
            <td>
                <Tooltip title="Add Eval">
                    <div onClick={() => createLink(ksat_id, 'Eval')}>
                        <FontAwesomeIcon className="clickable-element" icon={faPlus} />
                    </div>
                </Tooltip>
            </td>
        </>
    );

    const coursesCell = (
        <td>
            {courses.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'courses', value: item }))) return;
                        setFilters([...filters, { column: 'courses', value: item }]);
                    }}
                >
                    {item}
                </div>
            ))}
        </td>
    );

    const milestoneCell = (
        <td>
            {oam.map((item, index) => (
                <div
                    className="clickable-filter"
                    key={index}
                    onClick={() => {
                        if (filters.some((filter) => JSON.stringify(filter) === JSON.stringify({ column: 'oam', value: item }))) return;
                        setFilters([...filters, { column: 'oam', value: item }]);
                    }}
                >
                    {item}
                </div>
            ))}
        </td>
    );

    const commentsCell = <td>{comments}</td>;

    const descFromKsatId = (ksatId: string) => {
        let output = '';
        ksats.forEach((ksat) => {
            if (ksat.ksat_id === ksatId) {
                output = ksat.description;
            }
        });
        return output;
    };

    const constructKSATCardData = (children: MTTL_Type['children']) => {
        const tempArray: KSATCardData[] = [];
        for (const ksat of children) {
            tempArray.push({ ksatId: ksat, description: descFromKsatId(ksat) });
        }
        return tempArray;
    };

    const [localTFilter, setLocalTFilter] = useState({ active: false, profLevel: 'any', resourceType: 'any', showCommercial: true }); // training
    const [localEFilter, setLocalEFilter] = useState({ active: false, profLevel: 'any', resourceType: 'any', showCommercial: true }); // eval

    /**
     * Row inside the table
     */
    return (
        <>
            <tr className="mat-row cdk-row queue-element-row ng-tns-c130-62 ng-star-inserted" style={{ height: '0', fontSize: '10', lineHeight: '0' }}>
                {coverageCell}
                {selectedCols.includes('ksat_id') ? ksatCell : null}
                {selectedCols.includes('description') ? descriptionCell : null}
                {selectedCols.includes('topic') ? topicCell : null}
                {selectedCols.includes('requirement_owner') ? ownerCell : null}
                {selectedCols.includes('requirement_src') ? sourceCell : null}
                {selectedCols.includes('work_roles') && !selectedCols.includes('wr_proficiency')
                    ? workRoleCell
                    : selectedCols.includes('work_roles') && selectedCols.includes('wr_proficiency')
                    ? proficiencyCell
                    : null}
                {selectedCols.includes('child_count') ? childrenCell : null}
                {selectedCols.includes('parent_count') ? parentsCell : null}
                {selectedCols.includes('training_count') ? trainingCell : null}
                {selectedCols.includes('eval_count') ? evaluationCell : null}
                {selectedCols.includes('courses') ? coursesCell : null}
                {selectedCols.includes('oam') ? milestoneCell : null}
                {selectedCols.includes('comments') ? commentsCell : null}
            </tr>
            <tr>
                <td style={{ minWidth: 'max-content' }} colSpan={13}>
                    {/* Children Carousel */}
                    <KsatCarousel visible={childCarouselVisible} setFilters={setFilters} filters={filters} content={constructKSATCardData(children)} />
                    {/* Parents Carousel */}
                    <KsatCarousel visible={parentCarouselVisible} setFilters={setFilters} filters={filters} content={constructKSATCardData(parents)} />
                    {/* Training Carousel */}
                    <ResourceCarousel
                        visible={trainingCarouselVisible}
                        carouselType="Training"
                        content={training_links}
                        filter={null}
                        globalFilter={globalTFilter}
                        setGlobalFilter={setGlobalTFilter}
                        localFilter={localTFilter}
                        setLocalFilter={setLocalTFilter}
                    />
                    {/* Eval Carousel */}
                    <ResourceCarousel
                        visible={evalCarouselVisible}
                        carouselType="Eval"
                        content={eval_links}
                        filter={null}
                        globalFilter={globalEFilter}
                        setGlobalFilter={setGlobalEFilter}
                        localFilter={localEFilter}
                        setLocalFilter={setLocalEFilter}
                    />
                </td>
            </tr>
        </>
    );
};
