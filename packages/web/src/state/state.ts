import { MTTL_Type } from '@mttl/common';

export type AppState = {
    mttl: MTTL_Type[] | null;
    autocomplete: any;
    milestones: any;
    ksats_by_work_role: any;
    ksats_no_work_role: any;
    userData: any;
};

// TODO: could cache this state using a cookie or localstorage, and initialize in a different way
export const initialState: AppState = {
    mttl: null,
    autocomplete: null,
    milestones: null,
    ksats_by_work_role: null,
    ksats_no_work_role: null,
    userData: null,
};
