import { AllMTTLActions } from './mttlActions';

/**
 * Every action requires a type for the reducer to switch on.
 */
export type BaseAction = {
    type: string;
};

export type AllActions = AllMTTLActions;
export type Dispatch = React.Dispatch<AllActions>;
