// TODO: rename this file to something more specific to something inside it?
// If more actions are put here, consider breaking them out into separate files based on component or state or something else...

import { AppState } from '..';
import { BaseAction } from '../actions';

type UpdateMTTLAction = BaseAction & {
    type: 'update-mttl';
    mttlData: AppState['mttl'];
};

type UpdateMilestonesAction = BaseAction & {
    type: 'update-milestones';
    milestoneData: AppState['milestones'];
};

type UpdateAutocompleteAction = BaseAction & {
    type: 'update-autocomplete';
    autocompleteData: AppState['autocomplete'];
};

type UpdateKsatsByWorkRoleAction = BaseAction & {
    type: 'update-ksats-by-work-role';
    ksatData: AppState['ksats_by_work_role'];
};

type UpdateKsatsNoWorkRoleAction = BaseAction & {
    type: 'update-ksats-no-work-role';
    ksatData: AppState['ksats_no_work_role'];
};

type UpdateUserDataAction = BaseAction & {
    type: 'update-user-data';
    userData: AppState['userData'];
};

export type AllMTTLActions = UpdateMTTLAction | UpdateMilestonesAction | UpdateAutocompleteAction | UpdateKsatsByWorkRoleAction | UpdateKsatsNoWorkRoleAction | UpdateUserDataAction;
