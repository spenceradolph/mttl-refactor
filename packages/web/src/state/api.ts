/**
 * All Endpoints and types that return from them can be configured here, in a single place.
 * TODO: consider putting this file in parent directory?
 */

import { AppState } from './state';

const fetchData = async (endpoint: RequestInfo) => {
    const requestBody = await fetch(endpoint);
    const requestData = await requestBody.json();
    return requestData;
};

type apiTypings = {
    fetchMttl: () => Promise<AppState['mttl']>; // TODO: get this type from somewhere else? (probably @mttl/common) ... make sure this isn't typed in 2 separate places...
    fetchAutocomplete: () => Promise<AppState['autocomplete']>;
    fetchMilestones: () => Promise<AppState['milestones']>;
    fetchKsatsByWorkRole: () => Promise<AppState['ksats_by_work_role']>;
    fetchKsatsNoWorkRole: () => Promise<AppState['ksats_no_work_role']>;
    fetchUserData: () => Promise<AppState['userData']>;
    submitBugReport: (title: string, desc: string) => Promise<any>; // TODO: confirm this object via server typings? (could possibly contain {error: ''})
    submitContactUs: (reason: string, explanation: string, notes: string) => Promise<any>;
    submitModifyKsat: (desc: string, ksat_id: string) => Promise<any>;
    submitNewLink: (jsonData: string) => Promise<any>;
    submitNewKsat: (jsonData: string) => Promise<any>;
};

// TODO: could also even make these endpoints somehow linked to the backend using CONSTANTS via the @mttl/common, but maybe overkill...
export const api: apiTypings = {
    fetchMttl: () => fetchData('/api/mttl'),
    fetchAutocomplete: () => fetchData('/api/mttl/autocomplete'),
    fetchMilestones: () => fetchData('/api/mttl/milestones/ksats'),
    fetchKsatsByWorkRole: () => fetchData('/api/mttl/ksats_by_work_role'),
    fetchKsatsNoWorkRole: () => fetchData('/api/mttl/ksats_no_work_role'),
    fetchUserData: () => fetchData('/auth/profile'),
    submitBugReport: (title: string, desc: string) => fetchData(`/api/forms/bugReport?title=${title}&desc=${desc}`),
    submitContactUs: (reason: string, explanation: string, notes?: string) => fetchData(`/api/forms/newContact?reason=${reason}&explanation=${explanation}&notes=${notes ?? 'N/A'}`),
    submitModifyKsat: (desc: string, ksat_id: string) => fetchData(`/api/forms/modifyKSAT?desc=${desc}&ksat_id=${ksat_id}`),
    submitNewLink: (jsonData: string) => fetchData(`/api/forms/newLink?jsonData=${jsonData}`),
    submitNewKsat: (jsonData: string) => fetchData(`/api/forms/newKsat?jsonData=${jsonData}`),
};
