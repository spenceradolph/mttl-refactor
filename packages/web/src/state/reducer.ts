import { AllActions } from './actions';
import { AppState } from './state';

export const reducer = (currentState: AppState, action: AllActions): AppState => {
    switch (action.type) {
        case 'update-mttl': {
            return { ...currentState, mttl: action.mttlData };
        }

        case 'update-milestones': {
            return { ...currentState, milestones: action.milestoneData };
        }

        case 'update-autocomplete': {
            return { ...currentState, autocomplete: action.autocompleteData };
        }

        case 'update-ksats-by-work-role': {
            return { ...currentState, ksats_by_work_role: action.ksatData };
        }

        case 'update-ksats-no-work-role': {
            return { ...currentState, ksats_no_work_role: action.ksatData };
        }

        case 'update-user-data': {
            return { ...currentState, userData: action.userData };
        }

        default: {
            const exhaustiveCheck: never = action;
            throw new Error(`Unhandled Action Case: ${exhaustiveCheck}`);
        }
    }
};
