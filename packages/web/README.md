# @mttl/web

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

The components directory contains all React components used, with most* sub-directories indicating a frontend [route](https://reactrouter.com/).

Some components are easily managed, while the other more complex pages have been further split into sub-components.

State management is handled with react [hooks](https://reactjs.org/docs/hooks-intro.html). For api-calls, the results are stored in a root level 'state' object, and is managed in the 'state' directory via a [reducer](https://reactjs.org/docs/hooks-reference.html#usereducer).

The original intent of using a reducer was to reduce complexity of managing complex changing state within and across components. For now, only state that involves api calls is managed here, since this data is often used by multiple components. Other component's local state could be moved here as well, which would increase reducer complexity but decrease component complexity.

# TODO:

-   Fix CSS
