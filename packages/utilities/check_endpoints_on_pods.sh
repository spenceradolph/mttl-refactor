#!/bin/bash -e

 apk update
 apk add postgresql
 apk add postgresql-dev
 apk add python3
 apk add py3-pip
 pip3 install --upgrade pip
 pip3 install jsonschema
 pip3 install urllib3

export PODNAME=$(kubectl get pods --namespace $KUBE_NAMESPACE |grep -v "NAME\|RESTARTS\|erminating\|postgresql-0\|cm-acme"|grep -o "^[^ ]*")
echo "PODNAME is $PODNAME"
echo "Namespace is $KUBE_NAMESPACE"
kubectl exec -i $PODNAME --namespace "$KUBE_NAMESPACE"  -- python3 /app/packages/database/scripts/import_to_db.sh
echo "finishing check_endpoints_on_pods.sh"
exit 0
