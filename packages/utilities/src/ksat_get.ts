#! /usr/local/bin/ts-node
import { selectKsat } from '@mttl/database';
import { program } from 'commander';
import { exit } from 'process';

const main = async () => {
    program.description('Get Individual KSATs by ID').requiredOption('-i, --id <ids...>', 'KSAT ID(s)');

    program.parse(); // TODO: debugger integration with vscode and ts-node, doesn't seem to print errors before exiting

    const ids: string[] = program.opts().id;

    for (const ksat_id of ids) {
        const result = await selectKsat(ksat_id);
        console.log(result);
    }

    exit(0);
};

// TODO: top level await configurations
// Currently, top-level await is only for more modern versions of node / typescript, but a certain tsconfig and package.json should allow top-level await without this call
main();
