#! /usr/local/bin/ts-node
import { manualQuery } from '@mttl/database';
import { InvalidArgumentError, InvalidOptionArgumentError, Option, program } from 'commander';
import { exit } from 'process';

/**
 * TODO: This can likely be better setup to prevent mistakes and make it more clear what the options are
 * See https://github.com/tj/commander.js/ for more information
 */

const main = async () => {
    program
        .description('Add ksat to the database')
        .option('--id <id>', `Override KSAT id used for the new KSAT\n\n`)
        .addOption(new Option('--type <type>', `Specify KSAT Type\n\n`).choices(['Knowledge', 'Skill', 'Ability', 'Task']).makeOptionMandatory())
        .requiredOption('--description <description>', `KSAT Description\n\n`)
        .requiredOption('--topic <topic>', `Specify the KSATs Topic\n\n`)
        .requiredOption('--work-role <work-role proficiency milestone...>', `Specify Work Role Information\n\n`)
        .requiredOption('--source-owner <source owner...>', `Specify the KSAT's source and owner\n\n`)
        .option('--parents <parents...>', `Specify parents\n\n`)
        .option('--children <children...>', `Specify children\n\n`)
        .option('--link <map-type type topic name description url proficiency course course-network...>', `Specify training or eval link info\n\n`)
        .option('--comment <comment>', `Specify the KSAT comment\n\n`);

    program.parse();

    const { id, type, description, topic, workRole, sourceOwner, parents, children, link, comment } = program.opts();

    if (workRole.length !== 3 || sourceOwner.length !== 2 || (link && link.length !== 9)) throw new Error(`Got different number of arguments for either work-role, source-owner, or link`);

    exit(0);
};

// TODO: top level await configurations
// Currently, top-level await is only for more modern versions of node / typescript, but a certain tsconfig and package.json should allow top-level await without this call
main();
