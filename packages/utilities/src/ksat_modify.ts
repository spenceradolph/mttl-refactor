#! /usr/local/bin/ts-node
import { manualQuery } from '@mttl/database';
import { InvalidArgumentError, InvalidOptionArgumentError, Option, program } from 'commander';
import { exit } from 'process';

/**
 * TODO: This can likely be better setup to prevent mistakes and make it more clear what the options are
 * See https://github.com/tj/commander.js/ for more information
 */

const main = async () => {
    program
        .description('Modify Individual KSAT by ID')
        .usage(`[id] [options]`)
        .argument('<id>', 'KSAT ID')
        .option('--description <new-description>', `New Description\n\n`)
        .option('--topic <new-topic>', `New Topic\n\n`)
        .option('--add-work-role <work-role proficiency milestone...>', `Add work-role:  --add-work-role  work-role  proficiency  milestone\n\n`)
        .option('--remove-work-role <work-role>', `Remove work-role\n\n`)
        .option('--add-source-owner <source owner...>', `Add source/owner to KSAT:  --add-source-owner  source  owner\n\n`)
        .option('--remove-source-owner <source>', `Remove source/owner from KSAT (Only specify source)\n\n`)
        .option('--add-parents <parents...>', `Add 1 or more Parents:  --add-parents  parent1  parent2  etc...\n\n`)
        .option('--remove-parents <parents..>', `Remove 1 or more Parents:  --remove-parents  parent1  parent2  etc...\n\n`)
        .option('--add-children <children...>', `Add 1 or more Children:  --add-children  child1  child2  etc...\n\n`)
        .option('--remove-children <children...>', `Remove 1 or more Children:  --remove-children  child1  child2  etc...\n\n`)
        .option('--add-link <map-type type topic name description url proficiency course course-network...>', `Specify KSAT training or link information\n\n`)
        .option('--remove-link <link>', `Remove Link (use ksat_to_relationship_link id)\n\n`)
        .option('--comment', `Add/Change comment\n\n`);

    program.parse();

    const [id] = program.args;
    // TODO: validation on id

    const { description, topic, addWorkRole, removeWorkRole, addSourceOwner, removeSourceOwner, addParents, removeParents, addChildren, removeChildren, addLink, removeLink, comment } = program.opts();

    if (description) {
        // update description
        const results = await manualQuery('UPDATE ksats SET description = $1 WHERE id = $2', [description, id]);
    }

    if (topic) {
        // update topic
    }

    if (addWorkRole) {
        // TODO: could do cleaner error handling / throwing
        if (addWorkRole.length !== 3) throw new Error(`Expected 3 values but got ${addWorkRole.length}:  --add-work-role  work_role  proficiency  milestone`);
        const [work_role, proficiency, milestone] = addWorkRole;

        // add work-role
    }

    if (removeWorkRole) {
        // remove work-role
    }

    if (addSourceOwner) {
        if (addSourceOwner.length !== 2) throw new Error(`Expected 2 values but got ${addSourceOwner.length}:  --add-source-owner  source  owner`);
        const [source, owner] = addSourceOwner;
    }

    if (removeSourceOwner) {
        // remove source
    }

    if (addParents) {
        for (const parent of addParents) {
            // add parent
        }
    }

    if (removeParents) {
        for (const parent of removeParents) {
            // remove parent
        }
    }

    if (addChildren) {
        for (const child of addChildren) {
            // add child
        }
    }

    if (removeChildren) {
        for (const child of removeChildren) {
            // remove child
        }
    }

    if (addLink) {
        // <map-type type topic name description url proficiency course course-network...>
        if (addLink.length !== 9)
            throw new Error(`Expected 9 values but got ${addSourceOwner.length}:  --add-link  map-type  type  topic  name  description  url  proficiency  course  course-network`);

        // add link
    }

    if (removeLink) {
        // If we have the link ID, why do we need to do this SELECT?
        // find_ksats_to_relatipship_links_id = """ SELECT * FROM ksats_to_relationship_links WHERE id = %s AND ksats_id = %s """
        // delete_ksats_to_relationship_links = """ DELETE FROM ksats_to_relationship_links WHERE id = %s AND ksats_id = %s RETURNING * """
        // remove link
    }

    if (comment) {
        // update comment
        const results = await manualQuery('UPDATE ksats SET comments = $1 WHERE id = $2 RETURNING *', [comment, id]);
        // TODO: do the 'updated on' change...
    }

    console.log(`\n\nDone Updating...`);

    exit(0);
};

// TODO: top level await configurations
// Currently, top-level await is only for more modern versions of node / typescript, but a certain tsconfig and package.json should allow top-level await without this call
main();
