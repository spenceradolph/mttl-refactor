#! /usr/local/bin/ts-node
import { deleteKsat } from '@mttl/database';
import { program } from 'commander';
import { exit } from 'process';

const main = async () => {
    program.description('Remove Individual KSAT by ID').requiredOption('-i, --id <id>', 'KSAT ID');

    program.parse();

    const id: string = program.opts().id;

    const result = await deleteKsat(id);

    console.log(result); // TODO: check for success or failure and log any errors

    exit(0);
};

// TODO: top level await configurations
// Currently, top-level await is only for more modern versions of node / typescript, but a certain tsconfig and package.json should allow top-level await without this call
main();
