# @mttl/utilities

Package with helpful utilities. Most scripts are geared towards database interactions.

People managing ksats (get, add, update, remove) will use the scripts in here.

These utilities are based on the 90th's previous 'mttl.py' script (and sub-scripts).

***Any scripts that are not yet converted to typescript were copied over as the original python and refactored to work in the new environment.
These currently include add.py, remove.py, modify.py, get.py as well as DB import and export scripts.

Scripts are still a work in progress. The python commands will eventually be converted to typescript or otherwise phased out.

# Using Utility Commands

Commands are currently found under ./src and can be run directly through the command line.
Some commands can also be run via the NPM Scripts side-panel to show their options.

Example (run from ./src inside the VSCode integrated terminal):
```
./ksat_get.ts --id T0001
```

# Libraries Used

[ts-node](https://github.com/TypeStrong/ts-node) is the library that is used to directly run typescript without compiling and running a .js file.
[commander](https://github.com/tj/commander.js) is the library that is used for argument parsing. The above examples showcase using different methods of specifying arguments.
