These files & folders inside this ./old directory were copied directly from the root of the previous version.

Likely will have references to the old version (such as directories or filenames) that don't exist (or never existed) in this version.

The intent is to continue to use these during transition and eventually convert everything to typescript.
Several scripts have already been converted in /src.

These were the scripts needed.

-   ./scripts
    -   /ksat
        -   /get.py
        -   /add.py
        -   /modify.py
        -   /remove.py
-   ./server
    -   /scripts
        -   /import_to_db.py
        -   /export_from_db.py
