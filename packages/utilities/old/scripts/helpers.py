import os
import sys
import psycopg2
from urllib.parse import urlparse


select_ksat = """
    SELECT
        json_build_object(
            'ksat_id', ksats.id,
            'ksat_type', ksats.type,
            'description', ksats.description,
            'comments', ksats.comments,
            'created_on', ksats.created_on,
            'updated_on', ksats.updated_on,
            'parents', (
                SELECT coalesce(array_agg(p.to_ksats_id), '{}')
                FROM ksat_edges as p
                WHERE p.from_ksats_id = ksats.id
                LIMIT 1
            ),
            'parent_count', (
                SELECT count(p.to_ksats_id)
                FROM ksat_edges as p
                WHERE p.from_ksats_id = ksats.id
                LIMIT 1
            ),
            'children', (
                SELECT coalesce(array_agg(c.from_ksats_id), '{}')
                FROM ksat_edges as c
                WHERE c.to_ksats_id = ksats.id
                LIMIT 1
            ),
            'child_count', (
                SELECT count(c.from_ksats_id)
                FROM ksat_edges as c
                WHERE c.to_ksats_id = ksats.id
                LIMIT 1
            ),
            'courses', (
                SELECT coalesce(fin.course, '{}')
                FROM (
                    SELECT array_agg(DISTINCT cs.course) AS course
                    FROM relationship_links AS rls
                    JOIN courses AS cs ON rls.courses_id = cs.id
                    JOIN ksats_to_relationship_links AS ktrls ON rls.id = ktrls.relationship_links_id
                    WHERE ktrls.ksats_id = ksats.id
                    LIMIT 1
                ) AS fin
            ),
            'work_roles', (
                SELECT coalesce(array_agg(ktwr.work_roles_id ORDER BY ktwr.work_roles_id ASC), '{}')
                FROM ksat_to_work_roles as ktwr
                WHERE ktwr.ksats_id = ksats.id
                LIMIT 1
            ),
            'proficiency_data', (
                SELECT coalesce(array_agg(
                    json_build_object(
                        'workrole', ktwr.work_roles_id,
                        'proficiency', ktwr.proficiencies_id
                    ) ORDER BY ktwr.work_roles_id ASC
                ), '{}')
                FROM ksat_to_work_roles as ktwr
                WHERE ktwr.ksats_id = ksats.id
                LIMIT 1
            ),
            'oam', (
                SELECT coalesce(array_agg(ms.milestone || ' - ' || ktwr.work_roles_id), '{}')
                FROM ksat_to_work_roles as ktwr
                JOIN milestones AS ms ON ktwr.milestones_id = ms.id
                WHERE ktwr.ksats_id = ksats.id
                LIMIT 1
            ),
            'topic', (
                SELECT coalesce(array_agg(t.topic), '{}')
                FROM ksats AS r
                JOIN topics AS t ON r.topic_id = t.id
                WHERE r.id = ksats.id
                LIMIT 1
            ),
            'training_links', (
                SELECT coalesce(array_agg(
                    json_build_object(
                        'id', ktrls.id,
                        'url', rls.url,
                        'proficiency', ktrls.proficiencies_id,
                        'title', rls.topic,
                        'type', rls.type,
                        'description', rls.description
                    )
                ), '{}')
                FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
                WHERE ktrls.ksats_id = ksats.id AND (rls.map_type = 'training' OR rls.map_type = 'training_supplementary')
                LIMIT 1
            ),
            'training_count', (
                SELECT count(ktrls.ksats_id)
                FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
                WHERE ktrls.ksats_id = ksats.id AND (rls.map_type = 'training' OR rls.map_type = 'training_supplementary')
                LIMIT 1
            ),
            'eval_links', (
                SELECT coalesce(array_agg(
                    json_build_object(
                        'id', ktrls.id,
                        'url', rls.url,
                        'proficiency', ktrls.proficiencies_id,
                        'title', rls.topic,
                        'type', rls.type,
                        'description', rls.description
                    )
                ), '{}')
                FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
                WHERE ktrls.ksats_id = ksats.id AND rls.map_type = 'eval'
                LIMIT 1
            ),
            'eval_count', (
                SELECT count(ktrls.ksats_id)
                FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
                WHERE ktrls.ksats_id = ksats.id AND rls.map_type = 'eval'
                LIMIT 1
            ),
            'requirement_src', (
                SELECT coalesce(array_agg(s.source), '{}')
                FROM ksat_to_sources AS kts
                JOIN sources AS s ON kts.sources_id = s.id
                WHERE kts.ksats_id = ksats.id
                LIMIT 1
            ),
            'requirement_owner', (
                SELECT coalesce(array_agg(s.owner), '{}')
                FROM ksat_to_sources AS kts
                JOIN sources AS s ON kts.sources_id = s.id
                WHERE kts.ksats_id = ksats.id
                LIMIT 1
            )
        )
    FROM
        ksats
    WHERE id = %s
    LIMIT 1
"""

select_just_ksat = """ SELECT * FROM ksats WHERE id = %s """
delete_ksat = """ DELETE FROM ksats WHERE id = %s RETURNING * """


def values_to_select_json(ksat_id, ksat_type, description, comments, created_on,
                updated_on, parents, parent_count, children, child_count, courses,
                work_roles, proficiency_data, oam, topic, training_links, training_count,
                eval_links, eval_count, requirement_src, requirement_owner):
    select_json = {}
    for key, value in locals().items():
        if key != 'select_json':
            select_json[key] = value

    return select_json


def connect_to_database(env: str) -> psycopg2.extensions.connection:
    try:
        pgconn = os.getenv(env)
        result = urlparse(pgconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return conn
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)
