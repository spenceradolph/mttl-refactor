#!/usr/bin/python3
import os
import sys
import argparse
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database, delete_ksat


class KsatRemove:
    def __init__(self, mode='normal', test_param=None) -> None:
        self.mode = mode
        self.client = connect_to_database('PGCONN')
        self.parse_args(mode, test_param)

    def __del__(self):
        self.client.close()

    def parse_args(self, mode, test_param):
        self.ksat = None
        self.problem = False
        if mode == 'unittest' or mode == 'unittest-debug':
            if test_param is None:
                print('Missing test params')
                return
            self.ksat_id = test_param['id']
        else:
            parser = argparse.ArgumentParser(
                description="""Remove ksat from the database. Make sure you export PGCONN""",
                formatter_class=argparse.RawTextHelpFormatter)
            parser.add_argument('id', type=str, metavar='KSAT_ID', nargs='+',
                                help='KSAT id that you want remove')
            parsed_args = parser.parse_args()
            self.ksat_id = parsed_args.id

        if self.client:
            for ksat in self.ksat_id:
                if not self.remove_ksat(ksat):
                    break
            if not self.problem:
                self.client.commit()

    def print_debug(self, msg: str):
        if self.mode == 'normal' or self.mode == 'unittest-debug':
            print(msg)

    def remove_ksat(self, ksat_id):
        cursor = self.client.cursor()
        cursor.execute(delete_ksat, (ksat_id,))
        ksat = cursor.fetchone()
        if ksat:
            self.print_debug(f'Ksat \'{ksat[0]}\' has been deleted from the database')
        else:
            self.print_debug(f'Ksat \'{ksat_id}\' does not exist')
            self.problem = True
            return

        self.ksat = ksat
        return ksat


if __name__ == "__main__":
    app = KsatRemove()
