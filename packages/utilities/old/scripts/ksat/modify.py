#!/usr/bin/python3
import os
import sys
import psycopg2
import argparse
from datetime import datetime
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database, select_just_ksat


class KsatModify:

    def __init__(self, mode='normal', test_param=None) -> None:
        self.mode = mode
        self.client = connect_to_database('PGCONN')
        self.parse_args(mode, test_param)

    def __del__(self) -> None:
        self.client.close()

    def parse_args(self, mode, test_param):
        self.problem = False
        if mode == 'unittest' or mode == 'unittest-debug':
            if test_param is None:
                print('Missing test params')
                return

            self.ksat_id = test_param['id']
            self.description = test_param['description'] if 'description' in test_param else None
            self.topic = test_param['topic'] if 'topic' in test_param else None
            self.add_work_role = test_param['add_work_role'] if 'add_work_role' in test_param else None
            self.remove_work_role = test_param['remove_work_role'] if 'remove_work_role' in test_param else None
            self.add_source_owner = test_param['add_source_owner'] if 'add_source_owner' in test_param else None
            self.remove_source_owner = test_param['remove_source_owner'] if 'remove_source_owner' in test_param else None
            self.add_parents = test_param['add_parents'] if 'add_parents' in test_param else None
            self.remove_parents = test_param['remove_parents'] if 'remove_parents' in test_param else None
            self.add_children = test_param['add_children'] if 'add_children' in test_param else None
            self.remove_children = test_param['remove_children'] if 'remove_children' in test_param else None
            self.add_link = test_param['add_link'] if 'add_link' in test_param else None
            self.remove_link = test_param['remove_link'] if 'remove_link' in test_param else None
            self.comment = test_param['comment'] if 'comment' in test_param else None

        else:
            parser = argparse.ArgumentParser(
                description="""Get ksat data from the database
        Make sure you export PGCONN""",
                formatter_class=argparse.RawTextHelpFormatter)
            parser.add_argument('id', type=str, metavar='KSAT_ID',
                                help="""KSAT id that you want modify""")
            parser.add_argument('--description', type=str,
                                help='Specify the KSAT\'s new description')
            parser.add_argument('--topic', type=str,
                                help='Specify the KSAT\'s new topic')
            parser.add_argument('--add-work-role', type=str, action='append', nargs=3,
                                metavar=('work-role', 'proficiency', 'milestone'),
                                help='Specify the KSAT\' work-role information')
            parser.add_argument('--remove-work-role', type=str,
                                metavar=('WORK_ROLE'),
                                help='Specify the KSAT\' work-role information')
            parser.add_argument('--add-source-owner', type=str, nargs=2,
                                help='Add source / owner to KSAT')
            parser.add_argument('--remove-source-owner', type=str,
                                metavar="SOURCE", help='Remove source / owner from KSAT (Only specify source)')
            parser.add_argument('--add-parents', type=str, nargs='+',
                                metavar="PARENT", help='Specify the KSAT\'s parents')
            parser.add_argument('--remove-parents', type=str, nargs='+',
                                metavar="PARENT", help='Specify the KSAT\'s parents')
            parser.add_argument('--add-children', type=str, nargs='+',
                                metavar="CHILD", help='Specify the KSAT\'s children')
            parser.add_argument('--remove-children', type=str, nargs='+',
                                metavar="CHILD", help='Specify the KSAT\'s children')
            parser.add_argument('--add-link', type=str, action='append', nargs=9,
                                metavar=('map-type', 'type', 'topic', 'name', 'description', 'url', 'proficiency',
                                'course', 'course-network'), help="""Specify the KSAT\'s training or eval link information
            map-type: training or eval (required)
            type: commercial, internal, IDF, IQT, or self-study (required)
            topic: topic of the material (required)
            name: name for the link
            descripton: brief description of the material
            url: url to the material (required)
            proficiency: proficiency of the material
            course: name of the course if this is one
            course-network: name of the network the course is on

            **Put "" if any fields are blank**
            """)
            parser.add_argument('--remove-link', type=str, nargs='+', metavar=('KSAT_TO_REL_LINK_ID'),
                                help="""The ksat_to_relationship_link ID used for the link""")
            parser.add_argument('--comment', type=str, help="""Specify the KSAT\'s comment""")
            parsed_args = parser.parse_args()

            self.ksat_id = parsed_args.id
            self.description = parsed_args.description
            self.topic = parsed_args.topic
            self.add_work_role = parsed_args.add_work_role
            self.remove_work_role = parsed_args.remove_work_role
            self.add_source_owner = parsed_args.add_source_owner
            self.remove_source_owner = parsed_args.remove_source_owner
            self.add_parents = parsed_args.add_parents
            self.remove_parents = parsed_args.remove_parents
            self.add_children = parsed_args.add_children
            self.remove_children = parsed_args.remove_children
            self.add_link = parsed_args.add_link
            self.remove_link = parsed_args.remove_link
            self.comment = parsed_args.comment

        cursor = self.client.cursor()
        try:
            cursor.execute(select_just_ksat, (self.ksat_id,))
            cursor.fetchone()[0]
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            self.error_msg = f'\'{self.ksat_id}\' does not exist'
            self.print_debug(self.error_msg)
            return

        self.client_runner()

        if not self.problem:
            self.client.commit()

    def client_runner(self):
        if self.client:
            if self.description:
                if not self.change_ksat_desc(): return
            if self.topic:
                if not self.change_ksat_topic(): return
            if self.add_work_role:
                for work_role in self.add_work_role:
                    if not self.add_ksat_work_role(*work_role): return
            if self.remove_work_role:
                if not self.remove_ksat_work_role(): return
            if self.add_source_owner:
                if not self.add_ksat_source(*self.add_source_owner): return
            if self.remove_source_owner:
                if not self.remove_ksat_source(self.remove_source_owner): return
            if self.add_parents:
                for parent in self.add_parents:
                    if not self.add_ksat_parents(parent): return
            if self.remove_parents:
                for parent in self.remove_parents:
                    if not self.remove_ksat_parents(parent): return
            if self.add_children:
                for child in self.add_children:
                    if not self.add_ksat_children(child): return
            if self.remove_children:
                for child in self.remove_children:
                    if not self.remove_ksat_children(child): return
            if self.add_link:
                for link in self.add_link:
                    if link[-2] == 'NULL' or link[-2] == 'null' or len(link[-2]) == 0:
                        link[-2] = None
                    if not self.add_ksat_link(*link): return
            if self.remove_link:
                for link_id in self.remove_link:
                    if not self.remove_ksat_link(link_id): return
            if self.comment is not None:
                if not self.change_ksat_comment(): return

    def print_debug(self, msg: str):
        if self.mode == 'normal' or self.mode == 'unittest-debug':
            print(msg)

    def ksat_updated_on(self, ksat_id=None):
        if not ksat_id:
            ksat_id = self.ksat_id
        cursor = self.client.cursor()
        update_query = """ UPDATE ksats SET updated_on = %s WHERE id = %s RETURNING id,updated_on """

        cursor.execute(update_query, (datetime.now(), ksat_id,))
        ksat = cursor.fetchone()

        return ksat

    def change_ksat_desc(self):
        cursor = self.client.cursor()
        update_query = """ UPDATE ksats SET description = %s, updated_on = %s WHERE id = %s RETURNING id, description """
        # We only want to update the description on the specified ksat
        try:
            cursor.execute(update_query, (self.description, datetime.now(), self.ksat_id,))
            ksat = cursor.fetchone()
            self.print_debug(f'Updated the description of \'{ksat[0]}\' to \'{ksat[1]}\'')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            self.print_debug(f'KSAT \'{self.ksat_id}\' does not exist')
            self.problem = True
            return

        return ksat

    def change_ksat_topic(self):
        cursor = self.client.cursor()
        get_topic_id_query = """ SELECT topic_id FROM ksats WHERE id = %s """
        update_query = """ UPDATE topics SET topic = %s WHERE id = %s RETURNING id, topic """
        # First, we need to find out what the topic_id on the specified ksat is
        cursor.execute(get_topic_id_query, (self.ksat_id,))
        topic_id = cursor.fetchone()[0]

        # Next, we need to update the topics tables
        cursor.execute(update_query, (self.topic, topic_id,))
        topic = cursor.fetchone()
        if topic:
            self.print_debug(f'Updated the topic on \'{self.ksat_id}\' to \'{topic[1]}\'')
            self.ksat_updated_on()
        # The ksat will always have a topic_id according to the schema
        # No need to account for failure
        return topic

    def add_ksat_work_role(self, work_role, proficiency, milestone):
        cursor = self.client.cursor()
        select_work_role = """ SELECT * FROM work_roles WHERE id = %s """
        select_ksat_to_work_role = """
            SELECT ktwr.ksats_id, ktwr.work_roles_id, ktwr.proficiencies_id, m.milestone, ktwr.milestones_id
            FROM ksat_to_work_roles AS ktwr
            JOIN milestones AS m ON id = milestones_id
            WHERE ktwr.ksats_id = %s AND ktwr.work_roles_id = %s
            """
        find_milestone = """ SELECT id FROM milestones WHERE milestone = %s """
        insert_milestone = """ INSERT INTO milestones (milestone) VALUES (%s) RETURNING id,milestone """
        insert_query = """ INSERT INTO ksat_to_work_roles (ksats_id,work_roles_id,milestones_id,proficiencies_id) VALUES (%s,%s,%s,%s) RETURNING * """
        # See if the work-role exists
        try:
            cursor.execute(select_work_role, (work_role,))
            cursor.fetchone()[0]
        except (psycopg2.errors.ForeignKeyViolation, TypeError):
            self.print_debug(f'\'{work_role}\' does not exist')
            self.problem = True
            return

        # We need to check if the work-role is already attached to the ksat
        cursor.execute(select_ksat_to_work_role, (self.ksat_id, work_role,))
        curr_work_role = cursor.fetchone()
        if curr_work_role:
            self.print_debug(f'Found the work-role \'{work_role}\' attached to \'{self.ksat_id}\'')
            self.print_debug(f'\t{curr_work_role}')
            return curr_work_role

        # Next, if the work-role was not found, we need to add it
        # We need to check if the milestone is already present and get the id
        try:
            cursor.execute(find_milestone, (milestone,))
            milestone_id = cursor.fetchone()[0]
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            cursor.execute(insert_milestone, (milestone,))
            milestone_id = cursor.fetchone()[0]

        # Lastly, insert the work-role
        try:
            cursor.execute(insert_query, (self.ksat_id, work_role, milestone_id, proficiency,))
            new_work_role = cursor.fetchone()
            self.print_debug(f'Added work-role with the following information {new_work_role}')
            self.ksat_updated_on()
        except (psycopg2.errors.ForeignKeyViolation, TypeError) as err:
            self.print_debug(err)
            self.problem = True
            return

        return 1

    def remove_ksat_work_role(self):
        cursor = self.client.cursor()
        delete_query = """ DELETE FROM ksat_to_work_roles WHERE ksats_id = %s AND work_roles_id = %s
            RETURNING work_roles_id, proficiencies_id, milestones_id """
        # We simply need to remove the row linking the ksat to the work_role
        # in the ksat_to_work_roles table
        try:
            cursor.execute(delete_query, (self.ksat_id, self.remove_work_role,))
            deleted = cursor.fetchone()[0]
            self.print_debug(f'Deleted \'{deleted}\' from \'{self.ksat_id}\'')
            self.ksat_updated_on()
        except (psycopg2.errors.InFailedSqlTransaction, TypeError) as err:
            self.print_debug(err)

        return 1

    def add_ksat_source(self, source, owner):
        cursor = self.client.cursor()
        find_source = """ SELECT * FROM sources WHERE source = %s """
        find_ksat_to_source = """ SELECT * FROM ksat_to_sources WHERE ksats_id = %s AND sources_id = %s """
        insert_source = """ INSERT INTO sources (source,owner) VALUES (%s,%s) RETURNING id,source,owner """
        insert_ksat_to_source = """ INSERT INTO ksat_to_sources (ksats_id,sources_id) VALUES (%s,%s) RETURNING id,ksats_id,sources_id """

        # First, see if the source already exists and get the sources_id
        try:
            cursor.execute(find_source, (source,))
            sources_id = cursor.fetchone()[0]
            self.print_debug(f'Found source with owner \'{source}/{owner}\' with ID:{sources_id}')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            # insert new source/owner
            cursor.execute(insert_source, (source, owner,))
            sources_id = cursor.fetchone()[0]
            self.print_debug(f'Inserted new source/owner \'{source}/{owner}\' with ID:{sources_id}')

        # Next, we need to see if the source_id is already attached to the ksat
        try:
            cursor.execute(find_ksat_to_source, (self.ksat_id, sources_id,))
            kts = cursor.fetchone()
            self.print_debug(f'Found ID \'{kts[0]}\' attached to Ksat \'{self.ksat_id}\'')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            # Finally, we can insert the new ksat_to_source
            cursor.execute(insert_ksat_to_source, (self.ksat_id, sources_id,))
            kts = cursor.fetchone()
            self.print_debug(f'Inserted sources_id \'{kts[2]}\' on Ksat \'{kts[1]}\'')
            self.ksat_updated_on()

        return kts

    def remove_ksat_source(self, source):
        cursor = self.client.cursor()
        find_source_id = """ SELECT * FROM sources WHERE source = %s """
        delete_ksat_to_source = """ DELETE FROM ksat_to_sources WHERE ksats_id = %s AND sources_id = %s RETURNING * """

        # First, we need to find the source_id from the source name
        try:
            cursor.execute(find_source_id, (source,))
            source_id = cursor.fetchone()[0]
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            self.print_debug(f'Source \'{source}\' was not found in the sources table')
            return

        # We only need to remove the ksat_to_sources link
        try:
            cursor.execute(delete_ksat_to_source, (self.ksat_id, source_id,))
            kts = cursor.fetchone()
            self.print_debug(f'Deleted sources_id \'{kts[2]}\' from \'{self.ksat_id}\'')
            self.ksat_updated_on()
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            self.print_debug(f'Source \'{source}\' was not attached to \'{self.ksat_id}\'')

        return kts

    def add_ksat_parents(self, parent_id):
        cursor = self.client.cursor()
        find_parent = """ SELECT * FROM ksat_edges WHERE from_ksats_id = %s AND to_ksats_id = %s """
        insert_parent = """ INSERT INTO ksat_edges (from_ksats_id,to_ksats_id) VALUES (%s,%s) RETURNING * """

        # First, we need to see if the parents is already there
        try:
            cursor.execute(find_parent, (self.ksat_id, parent_id,))
            edge = cursor.fetchone()
            self.print_debug(f'Found the parent \'{edge[1]}\' on Ksat \'{edge[2]}\' with ID:{edge[0]}\'')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            try:
                # Insert the parent
                cursor.execute(insert_parent, (self.ksat_id, parent_id,))
                edge = cursor.fetchone()
                self.print_debug(f'Inserted parent \'{edge[1]}\' on Ksat \'{edge[2]}\' with ID:{edge[0]}')
                self.ksat_updated_on()
                self.ksat_updated_on(parent_id)
            except (psycopg2.errors.ForeignKeyViolation) as err:
                self.print_debug(err)
                self.problem = True
                return

        return edge

    def remove_ksat_parents(self, parent_id):
        cursor = self.client.cursor()
        find_parent = """ SELECT * FROM ksat_edges WHERE from_ksats_id = %s AND to_ksats_id = %s """
        delete_parent = """ DELETE FROM ksat_edges WHERE from_ksats_id = %s AND to_ksats_id = %s RETURNING * """

        # First, we need to see if the parent actually exists
        try:
            cursor.execute(find_parent, (self.ksat_id, parent_id,))
            edge = cursor.fetchone()
            edge_id = edge[0]
            edge_from = edge[1]
            edge_to = edge[2]
        except (psycopg2.errors.InFailedSqlTransaction, TypeError) as err:
            self.print_debug(err)
            self.problem = True
            return

        # Delete the parent
        cursor.execute(delete_parent, (self.ksat_id, parent_id,))
        edge = cursor.fetchone()
        self.print_debug(f'Deleted parent \'{edge_to}\' on Ksat \'{edge_from}\' with ID:{edge_id}')
        self.ksat_updated_on()
        self.ksat_updated_on(parent_id)

        return edge

    def add_ksat_children(self, child_id):
        cursor = self.client.cursor()
        find_child = """ SELECT * FROM ksat_edges WHERE from_ksats_id = %s AND to_ksats_id = %s """
        insert_child = """ INSERT INTO ksat_edges (from_ksats_id,to_ksats_id) VALUES (%s,%s) RETURNING * """

        # First, we need to see if the child is already there
        try:
            cursor.execute(find_child, (child_id, self.ksat_id,))
            edge = cursor.fetchone()
            self.print_debug(f'Found the child \'{edge[1]}\' on Ksat \'{edge[2]}\' with ID:{edge[0]}\'')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            # Insert the child
            try:
                cursor.execute(insert_child, (child_id, self.ksat_id,))
                edge = cursor.fetchone()
                self.print_debug(f'Inserted child \'{edge[1]}\' on Ksat \'{edge[2]}\' with ID:{edge[0]}')
                self.ksat_updated_on()
                self.ksat_updated_on(child_id)
            except (psycopg2.errors.ForeignKeyViolation) as err:
                self.print_debug(err)
                self.problem = True
                return

        return edge

    def remove_ksat_children(self, child_id):
        cursor = self.client.cursor()
        find_child = """ SELECT * FROM ksat_edges WHERE from_ksats_id = %s AND to_ksats_id = %s """
        delete_child = """ DELETE FROM ksat_edges WHERE from_ksats_id = %s AND to_ksats_id = %s RETURNING * """

        # First, we need to see if the child actually exists
        try:
            cursor.execute(find_child, (child_id, self.ksat_id,))
            edge = cursor.fetchone()
            edge_id = edge[0]
            edge_from = edge[1]
            edge_to = edge[2]
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            self.print_debug(f'Child \'{child_id}\' does not exist with parent \'{self.ksat_id}\'')
            return

        # Delete the child
        cursor.execute(delete_child, (child_id, self.ksat_id,))
        edge = cursor.fetchone()
        self.print_debug(f'Deleted child \'{edge_from}\' on Ksat \'{edge_to}\' with ID:{edge_id}')
        self.ksat_updated_on()
        self.ksat_updated_on(child_id)

        return edge

    def add_ksat_link(self, map_type, link_type, topic, name, description, url, proficiency, course, course_network):
        cursor = self.client.cursor()
        find_rel_link = """ SELECT * FROM relationship_links WHERE map_type = %s AND url = %s """
        find_course = """ SELECT * FROM courses WHERE course = %s """
        insert_course = """ INSERT INTO courses (course,network) VALUES (%s,%s) RETURNING * """
        insert_rel_link = """ INSERT INTO relationship_links (type,map_type,topic,url,name,description,courses_id) VALUES (%s,%s,%s,%s,%s,%s,%s) RETURNING * """
        find_ksat_to_rel_link = """ SELECT * FROM ksats_to_relationship_links WHERE ksats_id = %s AND relationship_links_id = %s """
        insert_ksat_to_rel_link = """ INSERT INTO ksats_to_relationship_links (relationship_links_id,ksats_id,proficiencies_id,created_on,updated_on) VALUES (%s,%s,%s,%s,%s) RETURNING * """

        # First, we need to check if the url has already need linked
        try:
            cursor.execute(find_rel_link, (map_type, url,))
            rel_link = cursor.fetchone()
            self.print_debug(f'Found rel-link ID:{rel_link[0]} with info {rel_link}')
        except (psycopg2.errors.InvalidTextRepresentation) as err:
            # If any of the enum values are incorrect
            self.print_debug(err)
            self.print_debug('Enum values is incorrect')
            self.problem = True
            return
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            # Need to check if the course exists
            if course and len(course) != 0:
                try:
                    cursor.execute(find_course, (course,))
                    course_id = cursor.fetchone()[0]
                except (psycopg2.errors.InFailedSqlTransaction, TypeError):
                    # Insert the new course
                    try:
                        cursor.execute(insert_course, (course, course_network,))
                        course_id = cursor.fetchone()[0]
                    except (psycopg2.errors.InvalidTextRepresentation) as err:
                        # If any of the enum values are incorrect
                        self.print_debug(err)
                        self.problem = True
                        return
            else:
                course_id = None
            # Insert the new rel-link
            try:
                cursor.execute(insert_rel_link, (link_type, map_type, topic, url, name, description, course_id,))
                rel_link = cursor.fetchone()
                self.print_debug(f'Inserted new rel-link ID:{rel_link[0]} with info {rel_link}')
            except (psycopg2.errors.InvalidTextRepresentation) as err:
                # If any of the enum values are incorrect
                self.print_debug(err)
                self.problem = True
                return

        # Now check if the ksat is already linked to the rel-link
        # This will not be the case for new rel-link but should check for existing rel-links
        try:
            cursor.execute(find_ksat_to_rel_link, (self.ksat_id, rel_link[0],))
            ktrl = cursor.fetchone()
            self.print_debug(f'Found a ksats_to_relationship_links entry with ID:{ktrl[0]}')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            # Lastly, we need to add the ksats_to_relationship_links entry
            try:
                cursor.execute(insert_ksat_to_rel_link, (rel_link[0], self.ksat_id, proficiency, datetime.now(), datetime.now(),))
                ktrl = cursor.fetchone()
                self.print_debug(f'Inserted new ksats_to_relationship_links entry with ID:{ktrl[0]}')
                self.ksat_updated_on()
            except (psycopg2.errors.ForeignKeyViolation) as err:
                # If any of the foreign key values are incorrect
                self.print_debug(err)
                self.problem = True
                return

        return ktrl

    def remove_ksat_link(self, link_id):
        cursor = self.client.cursor()
        find_ksats_to_relatipship_links_id = """ SELECT * FROM ksats_to_relationship_links WHERE id = %s AND ksats_id = %s """
        delete_ksats_to_relationship_links = """ DELETE FROM ksats_to_relationship_links WHERE id = %s AND ksats_id = %s RETURNING * """

        # First, we need to see if the ksats_to_relationship_links entry even exists
        cursor.execute(find_ksats_to_relatipship_links_id, (link_id, self.ksat_id,))
        ktrl = cursor.fetchone()
        if not ktrl:
            self.print_debug(f'Did not find ksats_to_relationship_links entry with ID:{link_id} with Ksat \'{self.ksat_id}\'')
            return

        # Lastly, delete the entry in ksats_to_relationship_links
        cursor.execute(delete_ksats_to_relationship_links, (link_id, self.ksat_id,))
        ktrl = cursor.fetchone()
        self.print_debug(f'Deleted ksats_to_relationship_links entry with info {ktrl}')
        self.ksat_updated_on()

        return ktrl

    def change_ksat_comment(self):
        cursor = self.client.cursor()
        update_comment = """ UPDATE ksats SET comments = %s WHERE id = %s RETURNING * """

        # Simply update the comment column in the ksat tuple
        cursor.execute(update_comment, (self.comment, self.ksat_id,))
        ksat = cursor.fetchone()
        self.print_debug(f'Updated the comment on \'{ksat[0]}\' to \'{self.comment}\'')
        self.ksat_updated_on()

        return ksat


if __name__ == "__main__":
    app = KsatModify()
