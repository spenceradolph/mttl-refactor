#!/usr/bin/python3
import os
import sys
import unittest
from remove import KsatRemove
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database, select_just_ksat


class TestKsatRemove(unittest.TestCase):

    def setUp(self):
        self.client = connect_to_database('PGCONN')
        self.cursor = self.client.cursor()

    def test_ksat_remove_success(self):
        test_ksat_id = 'T0001'
        self.cursor.execute(select_just_ksat, (test_ksat_id,))
        test_ksat = self.cursor.fetchone()
        self.assertIsNotNone(test_ksat)
        
        # Remove the ksat
        app = KsatRemove('unittest', {'id':[test_ksat_id]})
        self.assertTupleEqual(app.ksat, test_ksat)

        # See if it's still there
        self.cursor.execute(select_just_ksat, (test_ksat_id,))
        test_ksat = self.cursor.fetchone()
        self.assertIsNone(test_ksat)

    def test_ksat_remove_not_present(self):
        app = KsatRemove('unittest', {'id':['T9999']})
        self.assertIsNone(app.ksat)

    def tearDown(self) -> None:
        self.client.close()


if __name__ == '__main__':
    unittest.main()
