#!/usr/bin/python3
import os
import sys
import json
import argparse
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database, select_ksat


class KsatGet():

    def __init__(self, mode='normal', test_param=None) -> None:
        self.client = connect_to_database('PGCONN')
        self.parse_args(mode, test_param)

    def __del__(self):
        self.client.close()

    def parse_args(self, mode, test_param):
        if mode == 'unittest':
            if test_param is None:
                print('Missing test params')
                return

            self.id = test_param['id']
        else:
            parser = argparse.ArgumentParser(
                description="""Get ksat data from the database. Make sure you export PGCONN""",
                formatter_class=argparse.RawTextHelpFormatter)
            parser.add_argument('id', type=str, metavar='KSAT_ID',
                                help='KSAT id that you want to find')
            parsed_args = parser.parse_args()
            self.id = parsed_args.id

        ksat = self.get_ksat()
        # only print if not unittest
        if mode == 'normal':
            if type(ksat) is dict:
                ksat_formated_str = json.dumps(ksat[0], indent=2)
                print(ksat_formated_str)
            else:
                print(ksat)

    def get_ksat(self):
        ksat_id = self.id
        cursor = self.client.cursor()

        cursor.execute(select_ksat, (ksat_id,))
        ksat = cursor.fetchone()
        if not ksat:
            ksat = f'\'{ksat_id}\' does not exist'

        return ksat


if __name__ == "__main__":
    app = KsatGet()
