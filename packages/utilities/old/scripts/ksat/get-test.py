#!/usr/bin/python3
import os
import sys
import unittest
from get import KsatGet
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database


class TestKsatGet(unittest.TestCase):

    def setUp(self):
        self.client = connect_to_database('PGCONN')

    def test_ksat_present(self):
        app = KsatGet('unittest', {'id':'T0001'})
        ksat = app.get_ksat()[0]
        self.assertEqual(ksat['ksat_id'], 'T0001')
        self.assertEqual(ksat['ksat_type'], 'Task')
        self.assertEqual(ksat['description'], '(U) Modify cyber capabilities to detect and disrupt nation-state cyber threat actors across the threat chain (kill chain) of adversary execution')
        self.assertEqual(ksat['comments'], '')
        self.assertListEqual(ksat['parents'], [])
        self.assertEqual(ksat['parent_count'], 0)
        self.assertListEqual(ksat['children'], [
            'T0005','T0006','T0007','T0008','T0009','T0010','T0011',
            'T0012','T0013','T0014','T0016','T0017','T0018','T0019',
            'T0020','T0021','T0022','T0023','T0024','T0025','T0026',
            'T0027','T0028','T0029','T0030','T0031','T0032','T0033',
            'T0034','T0035','T0036','T0037'
        ])
        self.assertEqual(ksat['child_count'], 32)
        self.assertListEqual(ksat['courses'], [])
        sorted_wr = list(ksat['work_roles'])
        sorted_wr.sort()
        self.assertListEqual(sorted_wr, [
            'CCD',
            'SCCD-L',
            'SCCD-W'
        ])
        self.assertListEqual(ksat['proficiency_data'], [
            {
                'workrole': 'CCD',
                'proficiency': '1'
            },
            {
                'workrole': 'SCCD-L',
                'proficiency': '2'
            },
            {
                'workrole': 'SCCD-W',
                'proficiency': '2'
            }
        ])
        self.assertListEqual(ksat['oam'], [
            'Organizational - SCCD-W',
            'Organizational - SCCD-L',
            'Organzation Overview - CCD'
        ])
        self.assertListEqual(ksat['topic'], [
            'Mission'
        ])
        self.assertListEqual(ksat['training_links'], [])
        self.assertEqual(ksat['training_count'], 0)
        self.assertListEqual(ksat['eval_links'], [])
        self.assertEqual(ksat['eval_count'], 0)
        self.assertListEqual(ksat['requirement_src'], [
            'JCT&CS'
        ])
        self.assertListEqual(ksat['requirement_owner'], [
            'USCYBERCOM/J7'
        ])

    def test_ksat_not_present(self):
        app = KsatGet('unittest', {'id':'T9999'})
        self.assertEqual(app.get_ksat(), '\'T9999\' does not exist')

    def tearDown(self) -> None:
        self.client.close()


if __name__ == '__main__':
    unittest.main()
