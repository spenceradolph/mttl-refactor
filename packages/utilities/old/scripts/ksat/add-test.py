#!/usr/bin/python3
import os
import sys
import unittest
from add import KsatAdd
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database, values_to_select_json, select_ksat, delete_ksat


class TestKsatAdd(unittest.TestCase):

    def setUp(self):
        self.client = connect_to_database('PGCONN')
        self.cursor = self.client.cursor()

    def tearDown(self) -> None:
        self.client.close()

    def test_ksat_add_all_new(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'testing',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['CCD','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T0002','T0003'],
            'children': ['K0001','K0002','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.google.com','3','IDF','Commercial'],
                ['training','commercial','testing2','Testing Link 2','A brief test','www.newegg.com','3','IDF','Commercial'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.eval.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()[0]
        
        courses = list(set([link[7] for link in test_dict['link']]))
        work_roles = [work_role[0] for work_role in test_dict['work-role']]
        proficiency_data = [{
            'workrole': work_role[0],
            'proficiency': work_role[1]
        } for work_role in test_dict['work-role']]
        oam = [f'{work_role[2]} - {work_role[0]}' for work_role in test_dict['work-role']]
        training_links = [{
            'id': ksat['training_links'][i]['id'],
            'url': link[5],
            'proficiency': link[6],
            'title': link[2],
            'type': link[1],
            'description': link[4]
        } for i, link in enumerate(test_dict['link']) if link[0] == 'training']
        eval_links = [{
            'id': ksat['eval_links'][0]['id'],
            'url': link[5],
            'proficiency': link[6],
            'title': link[2],
            'type': link[1],
            'description': link[4]
        } for i, link in enumerate(test_dict['link']) if link[0] == 'eval']
        sources = [source[0] for source in test_dict['source-owner']]
        owners = [source[1] for source in test_dict['source-owner']]

        test_return_ksat = values_to_select_json(
            app.ksat_id,
            app.type,
            app.description,
            app.comment,
            ksat['created_on'],
            ksat['updated_on'],
            app.parents,
            len(app.parents),
            app.children,
            len(app.children),
            courses,
            work_roles,
            proficiency_data,
            oam,
            [app.topic],
            training_links,
            len(training_links),
            eval_links,
            len(eval_links),
            sources,
            owners
        )
        self.assertDictEqual(ksat, test_return_ksat)
        # cleanup
        self.cursor.execute(delete_ksat, (app.ksat_id,))
        self.client.commit()

    def test_ksat_add_topic_present(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'C',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['CCD','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T0002','T0003'],
            'children': ['K0001','K0002','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.google.com','3','IDF','Commercial'],
                ['training','commercial','testing2','Testing Link 2','A brief test','www.newegg.com','3','IDF','Commercial'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.eval.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()[0]
        
        courses = list(set([link[7] for link in test_dict['link']]))
        work_roles = [work_role[0] for work_role in test_dict['work-role']]
        proficiency_data = [{
            'workrole': work_role[0],
            'proficiency': work_role[1]
        } for work_role in test_dict['work-role']]
        oam = [f'{work_role[2]} - {work_role[0]}' for work_role in test_dict['work-role']]
        training_links = [{
            'id': ksat['training_links'][i]['id'],
            'url': link[5],
            'proficiency': link[6],
            'title': link[2],
            'type': link[1],
            'description': link[4]
        } for i, link in enumerate(test_dict['link']) if link[0] == 'training']
        eval_links = [{
            'id': ksat['eval_links'][0]['id'],
            'url': link[5],
            'proficiency': link[6],
            'title': link[2],
            'type': link[1],
            'description': link[4]
        } for i, link in enumerate(test_dict['link']) if link[0] == 'eval']
        sources = [source[0] for source in test_dict['source-owner']]
        owners = [source[1] for source in test_dict['source-owner']]

        test_return_ksat = values_to_select_json(
            app.ksat_id,
            app.type,
            app.description,
            app.comment,
            ksat['created_on'],
            ksat['updated_on'],
            app.parents,
            len(app.parents),
            app.children,
            len(app.children),
            courses,
            work_roles,
            proficiency_data,
            oam,
            [app.topic],
            training_links,
            len(training_links),
            eval_links,
            len(eval_links),
            sources,
            owners
        )
        self.assertDictEqual(ksat, test_return_ksat)
        # cleanup
        self.cursor.execute(delete_ksat, (app.ksat_id,))
        self.client.commit()

    def test_ksat_add_no_work_role(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'testing',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['asdfasdfa','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T0002','T0003'],
            'children': ['K0001','K0002','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.google.com','3','IDF','Commercial'],
                ['training','commercial','testing2','Testing Link 2','A brief test','www.newegg.com','3','IDF','Commercial'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.eval.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()
        self.assertIsNone(ksat)

    def test_ksat_add_nonexistant_parent(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'testing',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['CCD','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T9999','T0003'],
            'children': ['K0001','K0002','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.google.com','3','IDF','Commercial'],
                ['training','commercial','testing2','Testing Link 2','A brief test','www.newegg.com','3','IDF','Commercial'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.eval.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()
        self.assertIsNone(ksat)

    def test_ksat_add_nonexistant_child(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'testing',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['CCD','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T0002','T0003'],
            'children': ['K0001','K9999','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.google.com','3','IDF','Commercial'],
                ['training','commercial','testing2','Testing Link 2','A brief test','www.newegg.com','3','IDF','Commercial'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.eval.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()
        self.assertIsNone(ksat)

    def test_ksat_add_incorrect_link_type(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'testing',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['CCD','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T0002','T0003'],
            'children': ['K0001','K0002','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.asdfasdf.com','3','asdfasdf','Commercial'],
                ['training','asdfasdf','testing2','Testing Link 2','A brief test','www.sdfgsdfz.com','3','IDF','Commercial'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.4356345ywrt.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()
        self.assertIsNone(ksat)

    def test_ksat_add_incorrect_course_network(self):
        test_dict = {
            'type': 'Knowledge',
            'topic': 'testing',
            'description': 'This is a test for ksat-add',
            'work-role': [
                ['CCD','2','testing milestone'],
                ['SCCD-L','3','Something more difficult']
            ],
            'source-owner': [['90COS', '90COS']],
            'parents': ['T0001','T0002','T0003'],
            'children': ['K0001','K0002','K0003'],
            'link': [
                ['training','commercial','testing','Testing Link','A brief test','www.google.com','3','IDF','Commercial'],
                ['training','commercial','testing2','Testing Link 2','A brief test','www.newegg.com','3','asdfasdf','asdfasdf'],
                ['eval','commercial','eval-testing','Testing Eval Link','A brief test','www.eval.com','2','IDF','Commercial']
            ],
            'comment': 'This is a test comment!'
        }
        app = KsatAdd('unittest', test_dict)
        self.cursor.execute(select_ksat, (app.ksat_id,))
        ksat = self.cursor.fetchone()
        self.assertIsNone(ksat)


if __name__ == '__main__':
    unittest.main()
