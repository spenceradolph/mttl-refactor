#!/usr/bin/python3
import os
import sys
import unittest
from modify import KsatModify
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database, select_ksat


class TestKsatModify(unittest.TestCase):

    def setUp(self):
        self.client = connect_to_database('PGCONN')
        self.cursor = self.client.cursor()

    def tearDown(self) -> None:
        self.client.close()

    def test_ksat_modify_nonexistant_ksat(self):
        app = KsatModify('unittest', {'id':'T9999', 'description': 'This is a modification test'})
        self.assertEqual(app.error_msg, '\'T9999\' does not exist')

    def test_ksat_modify_description(self):
        ksat_id = 'T0001'
        new_description = 'This is a modification test'

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'description': new_description})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(new_mod['description'], new_description)

        KsatModify('unittest', {'id':ksat_id, 'description': old_ksat['description']})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(new_mod['description'], old_ksat['description'])

    def test_ksat_modify_comment(self):
        ksat_id = 'T0001'
        new_comment = 'This is a modification of comment'

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'comment': new_comment})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(new_mod['comments'], new_comment)

        KsatModify('unittest', {'id':ksat_id, 'comment': old_ksat['comments']})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(new_mod['comments'], old_ksat['comments'])

    def test_ksat_modify_topic(self):
        ksat_id = 'T0001'
        new_topic = 'my new topic!'

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'topic': new_topic})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(new_mod['topic'][0], new_topic)

        KsatModify('unittest', {'id':ksat_id, 'topic': old_ksat['topic'][0]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(new_mod['topic'][0], old_ksat['topic'][0])

    def test_ksat_modify_remove_add_work_role(self):
        ksat_id = 'T0001'
        remove_work_role = 'CCD'
        old_milestone = None
        old_proficiency = None
        new_milestone = None 
        new_proficiency = None

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        for m in old_ksat['oam']:
            if f'- {remove_work_role}' in m:
                old_milestone = m.split(' - ')[0]
        for p in old_ksat['proficiency_data']:
            if remove_work_role in p['workrole'] and len(p['workrole']) == len(remove_work_role):
                old_proficiency = p['proficiency']
        self.assertIsNotNone(old_milestone)
        self.assertIsNotNone(old_proficiency)

        KsatModify('unittest', {'id':ksat_id, 'remove_work_role': remove_work_role})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        for m in new_mod['oam']:
            if f'- {remove_work_role}' in m:
                new_milestone = m.split(' - ')[0]
        for p in new_mod['proficiency_data']:
            if remove_work_role in p['workrole'] and len(p['workrole']) == len(remove_work_role):
                new_proficiency = p['proficiency']
        self.assertIsNone(new_milestone)
        self.assertIsNone(new_proficiency)

        KsatModify('unittest', {'id':ksat_id, 'add_work_role': [[remove_work_role, old_proficiency, old_milestone]]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        
        for m in new_mod['oam']:
            if f'- {remove_work_role}' in m:
                new_milestone = m.split(' - ')[0]
        for p in new_mod['proficiency_data']:
            if remove_work_role in p['workrole'] and len(p['workrole']) == len(remove_work_role):
                new_proficiency = p['proficiency']

        self.assertEqual(old_proficiency, new_proficiency)
        self.assertEqual(old_milestone, new_milestone)

    def test_ksat_modify_add_wr_nonexistant(self):
        ksat_id = 'T0001'
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'add_work_role': [['asdfasdfasfd', '3', 'test milestone']]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(len(old_ksat['work_roles']), len(new_mod['work_roles']))

    def test_ksat_modify_add_wr_proficiency_nonexistant(self):
        ksat_id = 'T0002'
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'add_work_role': [['PO', '5', 'test milestone']]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(len(old_ksat['work_roles']), len(new_mod['work_roles']))

    def test_ksat_modify_remove_add_source_owner(self):
        ksat_id = 'T0001'
        old_source_owner = None

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        old_source_owner = old_ksat['requirement_src'] + old_ksat['requirement_owner']

        KsatModify('unittest', {'id':ksat_id, 'remove_source_owner': old_source_owner[0]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]

        self.assertEqual(len(new_mod['requirement_src']), 0)
        self.assertEqual(len(new_mod['requirement_owner']), 0)

        KsatModify('unittest', {'id':ksat_id, 'add_source_owner': old_source_owner})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        
        self.assertEqual(new_mod['requirement_src'][0], old_source_owner[0])
        self.assertEqual(new_mod['requirement_owner'][0], old_source_owner[1])

    def test_ksat_modify_add_remove_parent(self):
        ksat_id = 'T0001'
        new_parents = ['K0001', 'K0002', 'K0003']

        KsatModify('unittest', {'id':ksat_id, 'add_parents': new_parents})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        for parent in new_parents:
            self.assertIn(parent, new_mod['parents'])

        KsatModify('unittest', {'id':ksat_id, 'remove_parents': new_parents})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['parents']), 0)

    def test_ksat_modify_add_parent_nonexist(self):
        ksat_id = 'T0001'
        new_parents = ['K9997', 'K9998', 'K9999']

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'add_parents': new_parents})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['parents']), len(old_ksat['parents']))

    def test_ksat_modify_add_remove_children(self):
        ksat_id = 'T0001'
        new_children = ['K0001', 'K0002', 'K0003']

        KsatModify('unittest', {'id':ksat_id, 'add_children': new_children})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        for child in new_children:
            self.assertIn(child, new_mod['children'])

        KsatModify('unittest', {'id':ksat_id, 'remove_children': new_children})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        for child in new_children:
            self.assertNotIn(child, new_mod['children'])

    def test_ksat_modify_add_children_nonexist(self):
        ksat_id = 'T0001'
        new_children = ['K9997', 'K9998', 'K9999']

        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]

        KsatModify('unittest', {'id':ksat_id, 'add_children': new_children})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['children']), len(old_ksat['children']))

    def test_ksat_modify_add_remove_link(self):
        ksat_id = 'T0001'
        new_link = [
            'training',
            'commercial',
            'test topic',
            'test name',
            'test description',
            'www.my-test-url1.com',
            '2',
            'test-course1',
            'Commercial'
        ]
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        self.assertEqual(len(old_ksat['training_links']), 0)

        KsatModify('unittest', {'id':ksat_id, 'add_link': [new_link]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['training_links']), 1)
        new_mod_tl = new_mod['training_links'][0]
        self.assertEqual(new_link[1], new_mod_tl['type'])
        self.assertEqual(new_link[2], new_mod_tl['title'])
        self.assertEqual(new_link[4], new_mod_tl['description'])
        self.assertEqual(new_link[5], new_mod_tl['url'])
        self.assertEqual(new_link[6], new_mod_tl['proficiency'])

        link_id = new_mod_tl['id']
        KsatModify('unittest', {'id':ksat_id, 'remove_link': [link_id]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['training_links']), 0)

    def test_ksat_modify_add_link_wrong_type(self):
        ksat_id = 'T0001'
        new_link = [
            'mine',
            'commercial',
            'test topic',
            'test name',
            'test description',
            'www.my-test-url2.com',
            '2',
            'test-course1',
            'Commercial'
        ]
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        self.assertEqual(len(old_ksat['training_links']), 0)

        KsatModify('unittest', {'id':ksat_id, 'add_link': [new_link]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['training_links']), 0)

    def test_ksat_modify_add_link_wrong_linktype(self):
        ksat_id = 'T0001'
        new_link = [
            'training',
            'adsfasfdasdf',
            'test topic',
            'test name',
            'test description',
            'www.my-test-url3.com',
            '5',
            'test-course1',
            'Commercial'
        ]
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        self.assertEqual(len(old_ksat['training_links']), 0)

        KsatModify('unittest', {'id':ksat_id, 'add_link': [new_link]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['training_links']), 0)

    def test_ksat_modify_add_link_proficiency_nonexist(self):
        ksat_id = 'T0001'
        new_link = [
            'training',
            'commercial',
            'test topic',
            'test name',
            'test description',
            'www.my-test-url4.com',
            '5',
            'test-course1',
            'Commercial'
        ]
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        self.assertEqual(len(old_ksat['training_links']), 0)

        KsatModify('unittest', {'id':ksat_id, 'add_link': [new_link]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['training_links']), 0)

    def test_ksat_modify_add_link_course_network_nonexist(self):
        ksat_id = 'T0001'
        new_link = [
            'training',
            'commercial',
            'test topic',
            'test name',
            'test description',
            'www.my-test-url5.com',
            '3',
            'test-course2',
            'asfasfdasdfasf'
        ]
        self.cursor.execute(select_ksat, (ksat_id,))
        old_ksat = self.cursor.fetchone()[0]
        self.assertEqual(len(old_ksat['training_links']), 0)

        KsatModify('unittest', {'id':ksat_id, 'add_link': [new_link]})
        self.cursor.execute(select_ksat, (ksat_id,))
        new_mod = self.cursor.fetchone()[0]
        self.assertEqual(len(new_mod['training_links']), 0)


if __name__ == '__main__':
    unittest.main()
