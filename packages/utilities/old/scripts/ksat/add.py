#!/usr/bin/python3
import os
import sys
import psycopg2
import argparse
from datetime import datetime
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import connect_to_database


class KsatAdd():

    def __init__(self, mode='normal', test_param=None) -> None:
        self.mode = mode
        self.client = connect_to_database('PGCONN')
        self.parse_args(mode, test_param)

    def __del__(self) -> None:
        self.client.close()

    def parse_args(self, mode, test_param):
        if mode == 'unittest' or mode == 'unittest-debug':
            if test_param is None:
                print('Missing test params')
                return

            self.id = test_param['id'] if 'id' in test_param else None
            self.type = test_param['type'] if 'type' in test_param else None
            self.description = test_param['description'] if 'description' in test_param else None
            self.topic = test_param['topic'] if 'topic' in test_param else None
            self.work_role = test_param['work-role'] if 'work-role' in test_param else None
            self.source_owner = test_param['source-owner'] if 'source-owner' in test_param else None
            self.parents = test_param['parents'] if 'parents' in test_param else None
            self.children = test_param['children'] if 'children' in test_param else None
            self.link = test_param['link'] if 'link' in test_param else None
            self.comment = test_param['comment'] if 'comment' in test_param else None

        else:
            parser = argparse.ArgumentParser(
                description="""Add ksat to the database
        Make sure you export PGCONN""",
                formatter_class=argparse.RawTextHelpFormatter)
            parser.add_argument('--id', type=str, metavar='KSAT_ID',
                                help='Override KSAT id used for the new KSAT')
            parser.add_argument('--type', type=str, choices=['Knowledge', 'Skill', 'Ability', 'Task'],
                                required=True, help='Specify the type of KSAT')
            parser.add_argument('--description', type=str,
                                required=True, help='Specify the KSAT\'s description')
            parser.add_argument('--topic', type=str,
                                required=True, help='Specify the KSAT\'s topic')
            parser.add_argument('--work-role', type=str, action='append', nargs=3,
                                metavar=('work-role', 'proficiency', 'milestone'),
                                required=True, help='Specify the KSAT\' work-role information')
            parser.add_argument('--source-owner', type=str, action='append', nargs=2,
                                metavar=('source', 'owner'),
                                required=True, help='Specify the KSAT\'s source and owner')
            parser.add_argument('--parents', type=str, nargs='+',
                                metavar="PARENT", help='Specify the KSAT\'s parents')
            parser.add_argument('--children', type=str, nargs='+',
                                metavar="CHILD", help='Specify the KSAT\'s children')
            parser.add_argument('--link', type=str, action='append', nargs=9,
                                metavar=('map-type', 'type', 'topic', 'name', 'description', 'url', 'proficiency',
                                'course', 'course-network'), help="""Specify the KSAT\'s training or eval link information
            map-type: training or eval (required)
            type: commercial, internal, IDF, IQT, or self-study (required)
            topic: topic of the material (required)
            name: name for the link
            descripton: brief description of the material
            url: url to the material (required)
            proficiency: proficiency of the material
            course: name of the course if this is one
            course-network: name of the network the course is on

            **Put "" if any fields are blank**
            """)
            parser.add_argument('--comment', type=str, help="""Specify the KSAT\'s comment""")
            parsed_args = parser.parse_args()

            self.id = parsed_args.id
            self.type = parsed_args.type
            self.description = parsed_args.description
            self.topic = parsed_args.topic
            self.work_role = parsed_args.work_role
            self.source_owner = parsed_args.source_owner
            self.parents = parsed_args.parents
            self.children = parsed_args.children
            self.link = parsed_args.link
            self.comment = parsed_args.comment

        self.client_runner()

        if not self.problem:
            self.client.commit()

    def client_runner(self):
        if self.client:
            self.problem = False
            self.topic_id = self.insert_into_topics()
            if self.topic_id:
                self.ksat_id = self.insert_into_ksats(self.topic_id)
            if self.ksat_id:
                for source_owner in self.source_owner:
                    self.insert_into_sources(source_owner, self.ksat_id)  # we will insert into ksat_to_sources here, too
                # work_role is a double array
                for wr in self.work_role:
                    if not self.insert_into_ktwr(*wr, self.ksat_id):  # we will insert into milestones, too
                        self.problem = True
                        return
                if self.link:
                    # link is a double array
                    for rel_data in self.link:
                        if not self.insert_into_rel_links(self.ksat_id, *rel_data):  # we will insert into ksats_to_relationship_links
                            self.problem = True
                            return
                if self.parents or self.children:
                    self.insert_into_ksat_edges(self.ksat_id)

    def print_debug(self, msg: str):
        if self.mode == 'normal' or self.mode == 'unittest-debug':
            print(msg)

    def new_ksat_id(self, cursor, ksat_type):
        select_ksat_query = """ SELECT id FROM ksats WHERE type = %s ORDER BY id DESC"""
        cursor.execute(select_ksat_query, (ksat_type,))
        ksat = cursor.fetchone()[0]
        new_ksat = f'{ksat_type[0]}{int(ksat[1::]) + 1:04}'
        return new_ksat

    def insert_into_topics(self):
        cursor = self.client.cursor()
        select_topic_query = """ SELECT id FROM topics WHERE topic = %s """
        insert_topic_query = """ INSERT INTO topics (topic) VALUES (%s) RETURNING id """
        cursor.execute(select_topic_query, (self.topic,))
        topic_id = cursor.fetchone()
        if topic_id:
            topic_id = topic_id[0]
            self.print_debug(f'Found \'{self.topic}\' in topics table under ID:{topic_id}')

        if not topic_id:
            record_to_insert_topic = (
                self.topic,
            )
            cursor.execute(insert_topic_query, record_to_insert_topic)
            topic_id = cursor.fetchone()[0]
            self.print_debug(f'Inserted \'{self.topic}\' into the topics table under ID:{topic_id}')

        return topic_id

    def insert_into_ksats(self, topic_id):
        cursor = self.client.cursor()
        self.create_updated_on = datetime.now()
        insert_ksat_query = """ INSERT INTO ksats (id, type, description, created_on,\
            updated_on, topic_id, comments, old_id) VALUES \
                (%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id """

        try:
            record_to_insert_ksat = (
                self.new_ksat_id(cursor, self.type),  # create picker to get incremented ID form type
                self.type,
                self.description,
                self.create_updated_on,
                self.create_updated_on,
                topic_id,
                self.comment,
                None,
            )
            cursor.execute(insert_ksat_query, record_to_insert_ksat)
            ksat_id = cursor.fetchone()[0]
            self.print_debug(f'Inserted \'{ksat_id}\' in ksats table')
        except (psycopg2.errors.UniqueViolation) as err:
            self.print_debug(err)
            self.problem = True
            return

        return ksat_id

    def insert_into_sources(self, source_owner, ksat_id):
        cursor = self.client.cursor()
        select_source_query = """ SELECT id FROM sources WHERE source = %s """
        insert_source_query = """ INSERT INTO sources (source, owner)\
            VALUES (%s,%s) RETURNING id """
        cursor.execute(select_source_query, (source_owner[0],))
        source_id = cursor.fetchone()
        # Find the same source or create another
        if source_id:
            source_id = source_id[0]
        if not source_id:
            record_to_insert_source = (
                source_owner[0],
                source_owner[1],
            )
            cursor.execute(insert_source_query, record_to_insert_source)
            source_id = cursor.fetchone()[0]

        insert_ksat_source_query = """ INSERT INTO ksat_to_sources \
            (ksats_id, sources_id) VALUES (%s,%s) """
        record_to_insert_ksat_source = (
            ksat_id,
            source_id,
        )
        cursor.execute(insert_ksat_source_query, record_to_insert_ksat_source)

        return source_id

    def insert_into_ktwr(self, work_role, proficiency, milestone, ksat_id):
        cursor = self.client.cursor()

        # First want to find or insert the milestone
        select = """ SELECT id FROM milestones WHERE milestone = %s """
        insert = """ INSERT INTO milestones (milestone, description)\
            VALUES (%s,%s) """
        milestone_id = None

        cursor.execute(select, (milestone,))
        milestone_id = cursor.fetchone()
        if milestone_id:
            milestone_id = milestone_id[0]
            self.print_debug(f'Found \'{milestone}\' in the milestones table under ID:{milestone_id}')
        else:
            record = (milestone, "",)
            cursor.execute(insert, record)
            cursor.execute(select, (milestone,))
            milestone_id = cursor.fetchone()[0]
            self.print_debug(f'Inserted \'{milestone}\' into the milestones table under ID:{milestone_id}')

        # Second we want to insert the ksat_to_work_role
        select_wr = """ SELECT id FROM work_roles WHERE id = %s """
        insert = """ INSERT INTO ksat_to_work_roles (ksats_id, work_roles_id,\
            proficiencies_id, milestones_id) VALUES (%s,%s,%s,%s) RETURNING * """

        # check if work-role exists
        cursor.execute(select_wr, (work_role,))
        if cursor.fetchone() is None:
            self.print_debug(f'The work-role \'{work_role}\' does not exist in the database')
            return

        record = (
            ksat_id,
            work_role,
            proficiency,
            milestone_id,
        )
        cursor.execute(insert, record)
        ktwr = cursor.fetchone()
        return ktwr

    def insert_into_rel_links(self, ksat_id, map_type, rel_type, topic, name,
                    description, url, proficiency, course, course_network):
        cursor = self.client.cursor()

        for arg in locals().items():
            if type(arg) is str and len(arg) == 0:
                arg = None

        select_course = """ SELECT id FROM courses WHERE course = %s """
        insert_course = """ INSERT INTO courses (course, network) VALUES (%s,%s) RETURNING id """
        select_rel_link_url = """ SELECT * FROM relationship_links WHERE url = %s """
        insert_rel_link = """ INSERT INTO relationship_links (type, map_type, topic,\
            url, name, description, courses_id) VALUES (%s,%s,%s,%s,%s,%s,%s) RETURNING id """
        insert_ksat_to_rel_link = """ INSERT INTO ksats_to_relationship_links (relationship_links_id,\
            ksats_id, proficiencies_id, created_on, updated_on) \
                VALUES (%s,%s,%s,%s,%s) RETURNING id """

        # First we insert course for it's ID then insert relationship_link
        # Last we insert the ksat_to_relationship_link

        # Lets search if the course already exists. Otherwise, insert.
        if course:
            try:
                cursor.execute(select_course, (course,))
                course_id = cursor.fetchone()[0]
                self.print_debug(f'Found \'{course}\' in courses table under ID:{course_id}')
            except (psycopg2.errors.InFailedSqlTransaction, TypeError):
                try:
                    record = (
                        course,
                        course_network,
                    )
                    cursor.execute(insert_course, record)
                    course_id = cursor.fetchone()[0]
                    self.print_debug(f'Inserted \'{course}\' into courses table under ID:{course_id}')
                except (psycopg2.errors.InFailedSqlTransaction,
                psycopg2.errors.InvalidTextRepresentation,
                TypeError) as err:
                    self.print_debug(err)
                    self.problem = True
                    return

        # Next, lets search if the url has been used before. Otherwise, insert.
        try:
            cursor.execute(select_rel_link_url, (url,))
            rel_link_id = cursor.fetchone()[0]
            self.print_debug(f'Found \'{url}\' in relationship_links table under ID:{rel_link_id}')
        except (psycopg2.errors.InFailedSqlTransaction, TypeError):
            try:
                record = (
                    rel_type,
                    map_type,
                    topic,
                    url,
                    name,
                    description,
                    course_id,
                )
                cursor.execute(insert_rel_link, record)
                rel_link_id = cursor.fetchone()[0]
                self.print_debug(f'Inserted \'{url}\' into relationship_links table under ID:{rel_link_id}')
            except (psycopg2.errors.InFailedSqlTransaction,
            psycopg2.errors.InvalidTextRepresentation) as err:
                # There was a problem inserting the link
                # Probably b/c of incorrect enum values
                self.print_debug(err)
                self.problem = True
                return

        # Lastly, we need to insert the ksat to rel_link connection into ksat_to_relationship_links table
        record = (
            rel_link_id,
            ksat_id,
            proficiency,
            self.create_updated_on,
            self.create_updated_on,
        )
        cursor.execute(insert_ksat_to_rel_link, record)
        ksat_to_rel_link_id = cursor.fetchone()[0]
        return ksat_to_rel_link_id

    def insert_into_ksat_edges(self, ksat_id):
        cursor = self.client.cursor()
        parent_edges = []
        child_edges = []

        # This is pretty simple, just insert all parents and children that were passed in
        insert_edge = """ INSERT INTO ksat_edges (from_ksats_id, to_ksats_id) VALUES (%s,%s) RETURNING id """
        for parent in self.parents:
            try:
                cursor.execute(insert_edge, (ksat_id, parent,))
                parent_edges.append((parent, cursor.fetchone()[0]))
            except (psycopg2.errors.ForeignKeyViolation, 
            psycopg2.errors.InFailedSqlTransaction):
                self.print_debug(f'\'{parent}\' is not a valid Ksat, Aborting...')
                self.problem = True
                return
        for child in self.children:
            try:
                cursor.execute(insert_edge, (child, ksat_id,))
                child_edges.append((child, cursor.fetchone()[0]))
            except (psycopg2.errors.ForeignKeyViolation,
            psycopg2.errors.InFailedSqlTransaction):
                self.print_debug(f'\'{child}\' is not a valid Ksat, Aborting...')
                self.problem = True
                return

        if len(parent_edges) > 0:
            self.print_debug(f'Inserted {parent_edges} parents into the ksat_edges table')
        if len(child_edges) > 0:
            self.print_debug(f'Inserted {child_edges} children into the ksat_edges table')

        return (parent_edges, child_edges)


if __name__ == "__main__":
    app = KsatAdd()
