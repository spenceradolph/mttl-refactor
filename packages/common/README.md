# @mttl/common

Project to hold common values, constants, types, classes, data, or anything else that might be shared between projects.

# Usage

Everything under ./src should be exported for other packages to use directly.

The clearest example is when both the server and frontend are handling data sent from the database. 
When querying for the large list of ksats used in the mttl table, having the variables correctly typed leads to editor [IntelliSense](https://code.visualstudio.com/docs/editor/intellisense) and faster development and debugging. Ex: The editor will clearly show errors if you ever used an object property that didn't exist. 

Yarn can add this package to a workspace by using 'yarn workspace workspace_name add @mttl/common@1.0.0', but current packages should already have it installed if needed.
This should appear in the target's package.json and be referenced via symlinks in node_modules.

# TODO

-   Database table schema typed
-   Database query results typed
-   Data between web / server typed (such as /api/forms or /auth/profile)
