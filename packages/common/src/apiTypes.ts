// TODO: this package structure will likely change soon
// But the concept of using a separate package to keep the types used by both the front and back is good.

// Ex: When react frontend uses 'fetch' to grab data from the backend, it can assign the type and work with the data easier
// When the express backend or postgres database package ever needs to manipulate the data or get it ready to send, it will also use the same types

export type MTTL_Type = {
    child_count: string;
    children: string[];
    comments: string;
    courses: string[];
    description: string;
    eval_count: string;
    eval_links: {
        description: string;
        proficiency: string;
        title: string;
        type: string;
        url: string;
    }[];
    ksat_id: string;
    ksat_type: string;
    oam: string[];
    parent_count: string;
    parents: string[];
    proficiency_data: {
        workrole: string;
        proficiency: string;
    }[];
    requirement_owner: string[];
    requirement_src: string[];
    topic: string[];
    training_count: string;
    training_links: {
        description: string;
        proficiency: string;
        title: string;
        type: string;
        url: string;
    }[];
    updated_on: string;
    work_roles: string[];
};
