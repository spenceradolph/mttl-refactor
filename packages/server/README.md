# @mttl/server

ExpressJS backend server to handle requests from the frontend.
Passport is used as an authentication framework, and imports a gitlab authentication strategy.
Gitlab api package is also used for issue creation.

# Structure

The server 'app' is contained within ./src/app.ts and the entrypoint for running it is within ./src/index.ts.

All routes are found within ./src/routes, which contains a series of Express routers. Each directory within /routes represents a section of the url endpoint.

Ex:
```
./src/routes/api/mttl/index.ts contains the endpoint for GET /api/mttl/autocomplete
./src/routes/auth contains the endpoint for GET /auth/profile
```

This makes it easier to find the files for particular routes.

# TODO

-   Use more typescript types! (probably from @mttl/common) to ensure correct data flows from database to server to client and vice-versa
-   Better error handling / try catch
-   Better logs (currently only have simple 'METHOD /route' to the console.log)
-   Separate files for /api/forms/index.ts endpoints, since these can get long and verbose. Make it easier to find.
