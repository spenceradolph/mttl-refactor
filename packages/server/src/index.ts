import { app } from './app';

// TODO: should list all environment variables used in the .env.example, but explain that some are set by the Dockerfile in production?
const port = process.env.PORT || '80';

/**
 * Start the server
 */
app.listen(port, () => {
    console.log(`Listening on port ${port}...`);
});
