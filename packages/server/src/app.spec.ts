import request from 'supertest';
import { app } from './app';
import { expect } from 'chai';

// https://stackoverflow.com/questions/14285201/whats-the-least-resistance-path-to-debugging-mocha-tests
// TODO: https://dev.to/wakeupmh/debugging-mocha-tests-in-vscode-468a

describe('GET /api/badroute', () => {
    it('Returns 404 status when requesting unknown route.', async () => {
        const response = await request(app).get('/api/badroute');

        const { status, type, body } = response;

        expect(status).to.eql(404);
        expect(type).to.eql('application/json');
        expect(body).to.eql({ error: 'Resource not found.' });
    });
});

describe('GET /api/forms/bugReport', () => {
    it('Gives error when unauthenticated.', async () => {
        const response = await request(app).get('/api/forms/bugReport');

        const { status, type, body } = response;

        expect(status).to.eql(403);
        expect(type).to.eql('application/json');
        expect(body).to.eql({ error: 'You are not yet authenticated. Please go to /auth/gitlab.' });
    });

    it('Gives error when missing parts of query.', async () => {
        // TODO: stub/mock an authenticated request instead of actually authenticating
    });
});
