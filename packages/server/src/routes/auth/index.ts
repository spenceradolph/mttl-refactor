import { Router } from 'express';
import passport from 'passport';
const GitLabStrategy = require('passport-gitlab2').Strategy;

// https://stackoverflow.com/questions/27637609/understanding-passport-serialize-deserialize
passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user: any, done) {
    done(null, user);
});

// TODO: TEST when these are not set, gracefully fail, ability to set and restart...etc
const clientID = process.env.GITLAB_OAUTH_CID;
const clientSecret = process.env.GITLAB_OAUTH_SECRET;
const baseUrlDomain = process.env.BASE_DOMAIN || 'localhost';
const exposedPort = process.env.EXPOSED_PORT || '80';
const callbackURL = `http://${baseUrlDomain}:${exposedPort}/auth/gitlab/callback`;

passport.use(
    // @ts-ignore
    new GitLabStrategy(
        {
            clientID,
            clientSecret,
            callbackURL,
            scope: ['read_user'], // TODO: they use openid on the 90th's (but I think that includes read_user scope as well?)
        },
        (accessToken: string, refreshToken: string, profile: any, cb: any) => {
            return cb(null, profile); // TODO: get types from somewhere (passport-oauth2? is where this strategy is made from...)
        }
    )
);

export const authRouter = Router();

authRouter.get('/gitlab', passport.authenticate('gitlab'));

authRouter.get(
    '/gitlab/callback',
    passport.authenticate('gitlab', {
        failureRedirect: '/auth/profile', // TODO: some sort of error message saying 'failure to login with gitlab'.... don't really use a login page on our own... // TODO: may be bugs with redirecting to react dev on :3000
    }),
    (req, res) => {
        res.redirect('/mttl'); // successful authentication! now have access to the user information... (stored in .user?)
    }
);

// Sends the requestor what we know about their user-data / authentication
authRouter.get('/profile', (req, res, next) => {
    if (!req.isAuthenticated()) return next(new Error('UNAUTHENTICATED'));
    res.status(200).json(req.user);
});

authRouter.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/home'); // TODO: will eventually just send data response to trigger reducer (90cos doesn't hard refresh) but may involve more frontend work
});

authRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));
