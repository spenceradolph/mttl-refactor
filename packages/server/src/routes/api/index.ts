import { Router } from 'express';
import { formsRouter } from './forms';
import { modulesRouter } from './modules';
import { mttlRouter } from './mttl';

export const apiRouter = Router();

apiRouter.use('/modules', modulesRouter);
apiRouter.use('/mttl', mttlRouter);
apiRouter.use('/forms', formsRouter);

apiRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));
