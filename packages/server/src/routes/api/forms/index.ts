import { Router } from 'express';
import { Gitlab } from '@gitbeaker/node';

// TODO: TEST when these are not set, setting and restarting the server...failing gracefully...etc
const token = process.env.GITLAB_API_TOKEN || '';
const projectId = process.env.GITLAB_PROJECT_ID || '';

const gitlab_api = new Gitlab({
    token,
    requestTimeout: 3000,
});

export const formsRouter = Router();

formsRouter.get('/bugReport', async (req, res, next) => {
    if (!req.isAuthenticated()) return next(new Error('UNAUTHENTICATED'));
    if (!req.query.title || !req.query.desc) return next(new Error('BAD_REQUEST'));

    // @ts-ignore
    const username: string = req.user.username; // TODO: typescript typings for gitlab user info
    const { title, desc } = req.query; // TODO: consider typescript typings for query parameters as well

    try {
        const results = await gitlab_api.Issues.create(projectId, {
            title: `Bug Report: ${title}`,
            description: `@${username} submitted a bug report

Description: ${desc}`,
            labels: 'Bug',
        });
        res.json({ url: results.web_url }); // TODO: standardize responses (or check if 90th has a specific response)
    } catch (error) {
        console.log(error);
        return next(new Error('GITLAB_FAIL'));
    }
});

formsRouter.get('/newContact', async (req, res, next) => {
    // Users clicked Contact Us popup and filled out the info
    // explaination, reason, username, notes

    if (!req.isAuthenticated()) return next(new Error('UNAUTHENTICATED'));
    if (!req.query.reason || !req.query.explanation || !req.query.notes) return next(new Error('BAD_REQUEST'));

    // @ts-ignore
    const username: string = req.user.username;
    const { reason, explanation, notes } = req.query;

    try {
        // TODO: still needs more formatting to match original 90th stuff
        const date_ob = new Date();
        const results = await gitlab_api.Issues.create(projectId, {
            title: `User Contact from Form - ${date_ob.getMonth()}/${date_ob.getDate()}/${date_ob.getFullYear()}`,
            description: `@${username} Submitted Contact Form: 

Reason: ${reason}

Explanation: ${explanation}

Notes: ${notes}`,
            labels: 'Contact',
        });
        res.json(results); // TODO: standardize responses (or check if 90th has a specific response)
    } catch (error) {
        console.log(error);
        return next(new Error('GITLAB_FAIL'));
    }
});

formsRouter.get('/modifyKSAT', async (req, res, next) => {
    // Users clicked modifyKsat popup and filled out the info
    // ksat_id, desc, username

    if (!req.isAuthenticated()) return next(new Error('UNAUTHENTICATED'));
    if (!req.query.desc || !req.query.ksat_id) return next(new Error('BAD_REQUEST'));

    // @ts-ignore
    const username: string = req.user.username;
    const { desc, ksat_id } = req.query;

    try {
        // TODO: still needs more formatting to match original 90th stuff
        const results = await gitlab_api.Issues.create(projectId, {
            title: `Modify KSAT: ${ksat_id}`,
            description: `@${username} Submitted: ${desc}`,
            labels: 'mttl::ksat',
        });
        res.json(results); // TODO: standardize responses (or check if 90th has a specific response)
    } catch (error) {
        console.log(error);
        return next(new Error('GITLAB_FAIL'));
    }
});

formsRouter.get('/newLink', async (req, res, next) => {
    // Users clicked either add training link or add eval link
    // username, json

    if (!req.isAuthenticated()) return next(new Error('UNAUTHENTICATED'));
    if (!req.query.jsonData) return next(new Error('BAD_REQUEST'));

    // @ts-ignore
    const username: string = req.user.username;
    const { jsonData } = req.query;

    // const data = JSON.parse(req.query.jsonData);

    try {
        // TODO: still needs more formatting to match original 90th stuff
        const results = await gitlab_api.Issues.create(projectId, {
            title: `New Link Submission`,
            description: `@${username} submitted a new link

JsonData: ${jsonData}`,
            labels: 'mttl::ksat',
        });
        res.json(results); // TODO: standardize responses (or check if 90th has a specific response)
    } catch (error) {
        console.log(error);
        return next(new Error('GITLAB_FAIL'));
    }
});

formsRouter.get('/newKsat', async (req, res, next) => {
    // Users filled out the new ksat popup
    // name, topic, workrole, owner, source, parents, children, training_link, training_ref, evals, desc, safety*, username

    if (!req.isAuthenticated()) return next(new Error('UNAUTHENTICATED'));
    if (!req.query.jsonData) return next(new Error('BAD_REQUEST'));

    // @ts-ignore
    const username: string = req.user.username;
    const { jsonData } = req.query;

    // const data = JSON.parse(req.query.jsonData);

    try {
        // TODO: still needs more formatting to match original 90th stuff
        const results = await gitlab_api.Issues.create(projectId, {
            title: `New Ksat Submission`,
            description: `@${username} submitted a new ksat

JsonData: ${jsonData}`,
            labels: 'mttl::ksat',
        });
        res.json(results); // TODO: standardize responses (or check if 90th has a specific response)
    } catch (error) {
        console.log(error);
        return next(new Error('GITLAB_FAIL'));
    }
});

formsRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));
