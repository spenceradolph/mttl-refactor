import * as db from '@mttl/database';
import { Router } from 'express';

export const modulesRouter = Router();

modulesRouter.get('/', async (_req, res) => {
    res.json(await db.getModules());
});

modulesRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));
