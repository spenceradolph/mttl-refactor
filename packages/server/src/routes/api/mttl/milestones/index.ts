import * as db from '@mttl/database';
import { Router } from 'express';

export const milestonesRouter = Router();

milestonesRouter.get('/ksats', async (req, res) => {
    res.send(await db.getMilestones());
});

milestonesRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));
