import * as db from '@mttl/database';
import { Router } from 'express';
import { milestonesRouter } from './milestones';

export const mttlRouter = Router();

mttlRouter.use('/milestones', milestonesRouter);

mttlRouter.get('/', async (req, res, next) => {
    // Example of potential error handling implementation
    try {
        return res.json(await db.getKsats()); // TODO: all await's should probably be inside a try/catch like this, look into best practices...
    } catch (error) {
        return next(new Error('DATABASE_FAIL'));
    }
});

mttlRouter.get('/autocomplete', async (req, res) => {
    res.json(await db.getAutoComplete());
});

mttlRouter.get('/ksats_by_work_role', async (req, res) => {
    res.json(await db.getKsatsByWorkRole());
});

mttlRouter.get('/ksats_no_work_role', async (req, res) => {
    res.json(await db.getKsatsNoWorkRole());
});

mttlRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));
