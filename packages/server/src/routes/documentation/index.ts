import express from 'express';

export const docRouter = express.Router();

docRouter.use('/', express.static(`${__dirname}/../../documentation`)); // Expected to be put there by the root workspace build script

docRouter.use((req, res, next) => next(new Error('UNKNOWN_ROUTE')));