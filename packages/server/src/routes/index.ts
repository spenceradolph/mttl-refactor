import express, { Router } from 'express';
import path from 'path';
import { apiRouter } from './api';
import { authRouter } from './auth';
import { docRouter } from './documentation';

export const router = Router();

// Backend Routes

router.use('/api', apiRouter);
router.use('/auth', authRouter);
router.use('/documentation', docRouter);

// Frontend Routes

router.use('/', express.static(`${__dirname}/../build`)); // Expected to be put there by the root build script
router.use((req, res) => {
    if (process.env.NODE_ENV == 'production') {
        res.sendFile(path.resolve(`${__dirname}/../build/index.html`));
    } else {
        res.redirect(`http://localhost:3000${req.url}`); // Useful for concurrent react development
    }
});
