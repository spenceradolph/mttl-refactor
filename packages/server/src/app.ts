import express, { NextFunction, Request, Response } from 'express';
import session from 'express-session';
import helmet from 'helmet';
import passport from 'passport';
import { router } from './routes';

export const app = express();

app.use(helmet());

// Logging
app.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
});

// Authentication
app.use(
    session({
        secret: process.env.SESSION_SECRET || 'defaultSecret!@#$1234',
        resave: true,
        saveUninitialized: false,
    })
);
app.use(passport.initialize());
app.use(passport.session());

// Routing
app.use('/', router);

// Error Handling
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    // TODO: fix these and make them constants (for easy importing into routes)
    switch (err.message) {
        case 'UNAUTHENTICATED': {
            return res.status(403).json({ error: 'You are not yet authenticated. Please go to /auth/gitlab.' });
        }

        case 'BAD_REQUEST': {
            return res.status(400).json({ error: 'Received an unexpected or malformed request.' });
        }

        case 'GITLAB_FAIL': {
            return res.status(500).json({ error: 'Backend Gitlab api failed.' });
        }

        case 'DATABASE_FAIL': {
            return res.status(500).json({ error: 'DB Failure' });
        }

        case 'UNKNOWN_ROUTE': {
            return res.status(404).json({ error: 'Resource not found.' });
        }

        default: {
            next();
        }
    }
});
