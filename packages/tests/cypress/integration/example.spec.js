/// <reference types="cypress" />

describe('Site Displays Content', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('Homepage Element Exists', () => {
        cy.get('#homepage');
    });
});
