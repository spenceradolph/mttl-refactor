# @mttl/tests

Package for integration / end-to-end testing.

Currently built with [Cypress](https://www.cypress.io/)

# Running the tests

`yarn test` to start the testing in headless mode.

Tests can be found in ./cypress/integration

\*Some errors are shown, but these are expected and should be ignored. The tests should still run.
https://docs.cypress.io/guides/references/troubleshooting#Run-the-Cypress-app-by-itself

\*testing expects the compiled project (yarn build) be running (yarn start), and doesn't auto build/start it for us.
CI/CD will be expected to handle starting the server for testing purposes after the build stage is complete.
Configurations can be made to change which site (and port) gets checked. (see cypress.json)

# Cypress Electron

See [here](https://docs.cypress.io/guides/core-concepts/test-runner)...

Cypress offers an additional electron app with a smooth UI for running and creating tests.
You can even record new tests through interacting with the app!

Due to containerization of our app, this feature is not easily exposed to us and we need a local version of cypress.
Recommend local (global) [install](https://docs.cypress.io/guides/getting-started/installing-cypress) if you choose to use this feature.

example

```
npm i -g cypress
cd packages/tests
cypress open
```

# TODO

-   Convert to typescript
-   Learn more about cypress and how to best use it
