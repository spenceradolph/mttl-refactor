# @mttl/database

Contains scripts that connect to the postgresql container and functions to be imported by other packages for making queries.

It exposes functionality to run common queries, currently being used by both server and utilities packages.

See package.json for available scripts.

See ./src/queries.ts for available functions.

# TODO

-   Standardize queries / queryStrings (naming conventions...etc)
-   Utilize typings (probably from @mttl/common)
-   Error handling
