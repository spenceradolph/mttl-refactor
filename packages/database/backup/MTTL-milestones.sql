--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: milestones; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.milestones (id, milestone, description) FROM stdin;
1	None	Placeholder for ksat_to_work_roles with no milestone
2	USCC Basic Info	
3	Software Testing	
4	Cross Conextual Programming Proficiency	
5	Software Engineering	
6	Reverse Engineering	
7	Network Programming and Security	
8	Project Management	
9	Vulnerability Research	
10	Secure Coding	
11	Masterpiece	
12	Organizational	
13	Kernel Mode	
14	User Mode	
15	General	
16	CNO	
17	Agile 1 (Processes)	
18	90 COS	
19	Agile 2 (Leadership)	
20	Organization Overview	
21	Policy and Compliance	
22	Agile 3 (Troubleshooting/Efficiency)	
23	Project Management Tools	
24	Agile Basics	
25	Agile for POs PT. 1 (Processes)	
26	Scrum Specifics	
27	Agile for POs PT. 2 (Stakeholders)	
28	Agile Proficiency	
29	Cyber Mission Forces	
30	90 MQT	
31	Cloud Services	
32	Day 2 Operations	
33	Securing Cloud Services	
34	Software Architecture	
35	OPSEC	
36	Organizational Overview	
37	Agile for Scrum Masters	
38	MetaTotal	
39	Pre-Eval	
40	Eval	
41	Post Eval - Other	
42	Post Eval - Debrief Tester	
43	Foundations of Containers	
44	DevOps / CI/CD	
45	Testing Process	
46	Applied Docker	
47	Docker Foundations	
48	Docker Use	
49	Pseudocode	
50	C Mastery	
51	Introduction to Python	
52	Introduction to C	
53	Intermediate Python	
54	Special Topics	
55	(Python) Networking	
56	DevOps	
57	Introduction to Git	
58	Networking Concepts 	
59	Intermediate C	
60	Operating System	
61	Assembly	
62	Agile	
63	Organzation Overview	
64	Technical Writing	
\.


--
-- Name: milestones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.milestones_id_seq', 64, true);


--
-- PostgreSQL database dump complete
--

