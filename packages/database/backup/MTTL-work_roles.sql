--
-- Sorted PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: work_roles; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.work_roles (id, work_role, description) FROM stdin;
90COS	90th Cyberspace Operations Squadron	90COS inbound personnel.
AC	Agile Coach	Agile Coach (AC). Agile Coaches are subject matter experts in Agile and organizational improvement. They have demonstrated their ability to invent and further the craft and are now expected to coach, train, mentor, guide, architect and otherwise oversee teams of practitioners. Senior Product Owners and Scrum Masters progress to this role after they have successfully led a major improvement project for the organization and achieved experience in both roles.
CCD	Cyber Capability Developer	CCD - Basic Level. At this level, CCDs develop, create, maintain, and write/code new (or modifies existing) computer applications, software, or specialized utility programs.
CST	Client Systems Technician	Client Systems Technician - Basic Level. Client Systems Technicians (CSTs) perform the day to day maintenance and updates to the client systems (workstations, monitors, phones, etc) in use at the 90th covering both enterprise and development networks. CSTs are also the main triage point for the 90 COS Service Desk.
Instructor	Instructor	Instructors develop and conduct training or education of personnel.
JSRE	Junior Site Reliability Engineer	Junior Site Reliability Engineer - Basic Level. Junior Site Reliability Engineers (JSREs) are responsible for the development and configuration of Infrastructure as Code and its deployment (where applicable) through automated pipelines both on-prem and in the cloud. This covers a wide range of technologies and covers all layer of the stack including the infrastructure, platforms, networks, and services themselves. JSREs are overseen by SSREs.
MCCD	Master Cyber Capability Developer	Master CCD (MCC). MCCD are subject matter experts in capability development. They are expected to coach, train, mentor, guide, architect and otherwise oversee teams of developers at various stages of their development.
PO	Product Owner	Product Owner - Basic Level (PO). At the basic proficiency, PO coordinate, negotiate, confirm, clarify, and communicate a mission's value propositions via the creation and prioritization of backlog product items, user stories, and/or tasks.
SAD	Specialty Area Developer	Specialty Area Developer encompasses all other developers/categories not specific to an operating system or test automation engineering.
SCCD-L	Senior Cyber Capability Developer - Linux	Senior CCD in Linux. (SCCD-L) are experienced developers who have demonstrated the ability to perform the full range of capability development and achieved a specialization in the Linux operating system.
SCCD-W	Senior Cyber Capability Developer - Windows	Senior CCD in Windows. (SCCD-W) are experienced developers who have demonstrated the ability to perform the full range of capability development and achieved a specialization in the Windows operating system.
SEE	Standardization/Evaluation Evaluator	A Standardization/Evaluation Examiner (SEE) is an appointed expert, considered to be one of the top unit operational trainers, who evaluates crew personnel to ensure the minimum operational standards are met. A SEE also assists the Stan/Eval office in providing input to some of the Stan/Eval programs and ensuring operational materials are up-to-date.
SM	Scrum Master	Scrum Master - Basic Level (SM). At the basic proficiency, SM coordinate, facilitate, schedule, conduct, and communicate scrum ceremonies and agile processes and practices.
SPO	Senior Product Owner	Senior Product Owner (SPO) are experienced product owners who have demonstrated the ability to perform for single projects and teams. As Seniors they may be asked to lead newly formed and/or immature teams, larger efforts involving a scrum of scrums, be assigned as front door staff, or oversee complex projects with subordinate and interrelated projects and teams. 
SSM	Senior Scrum Master	Senior Scrum Master (SSM) are experienced scrum masters who have demonstrated the ability to perform for single projects and teams. As Seniors they may be asked to lead newly formed and/or immature teams, larger efforts involving a scrum of scrums, or oversee complex projects with subordinate and interrelated projects and teams.
SSRE	Senior Site Reliability Engineer	Senior Site Reliability Engineer - Senior Level. Senior Site Reliability Engineers (SSREs) are responsible for the development and configuration of Infrastructure as Code and its deployment (where applicable) through automated pipelines both on-prem and in the cloud and for mentoring JRSEs. This covers a wide range of technologies and covers all layer of the stack including the infrastructure, platforms, networks, and services themselves.
Sysadmin	System Administrator	System Administrator - Basic Level. System Administrators (SAs) perform maintenance and updates to all 90 COS development systems and services that are not currently covered by an automated Infrastructure as Code pipeline (note this may include running code or scripts that are currently manually executed). This covers a wide range of technologies and covers all layer of the stack including the infrastructure, platforms, networks, and services themselves. It is also expected that SAs receive exposure to the JSRE role over time for configuring network services.
TAE	Test Automation Engineer	Test Automation Engineer (TAE). TAE are experienced developers who have demonstrated specialization in testing and automation using modern pipeline and framework best practices. They specialize in the design, development, and integration of automated functional tests throughout the planning and development process to validate deliverables meet all acceptance criteria.
VR		Specialty Area Developer
\.

--
-- Sorted dump complete
--
