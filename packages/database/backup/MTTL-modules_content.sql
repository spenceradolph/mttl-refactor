--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: modules_content; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.modules_content (id, modules_id, description, url) FROM stdin;
1	1	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/index.html	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/index.html
2	2	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html
3	2	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Debugging_ASM_pt1.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Debugging_ASM_pt1.html
4	2	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/index.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/index.html
5	2	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Adv_types.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Adv_types.html
6	2	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Data_Types.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Data_Types.html
7	3	IDF Agile course content	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/slides/#/
8	3	IDF Agile course outline	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html
9	3	https://www.simform.com/functional-testing-types/	https://www.simform.com/functional-testing-types/
10	3	https://www.mountaingoatsoftware.com/agile/user-stories	https://www.mountaingoatsoftware.com/agile/user-stories
11	3	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/howtoaskquestions/Howtoaskquestions.html	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/howtoaskquestions/Howtoaskquestions.html
12	3	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/
13	3	https://www.scrum.org/resources/what-is-a-daily-scrum	https://www.scrum.org/resources/what-is-a-daily-scrum
14	3	https://www.romanpichler.com/blog/the-product-backlog-refinement-steps	https://www.romanpichler.com/blog/the-product-backlog-refinement-steps
15	4	List pointer structures in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Lists_Pointer_Structures.html
16	4	Singly Linked Lists in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Singly_Linked_List.html
17	4	Doubly Linked Lists in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Doubly_Linked_List.html
18	4	Using Stacks in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Stacks_Lesson.html
19	4	Queues in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Queue_Lesson.html
20	4	Tree structure and traversal in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Trees_Lesson.html
21	5	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/The_Stack.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/The_Stack.html
22	5	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/index.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/index.html
23	5	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html
24	6	Algorithm Analysis	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Algorithm_Analysis.html
25	6	Algorithms Design	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Algorithm_Design.html
26	6	Big O Notation	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Big_O_notation.html
27	6	Big O Analysis	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Big_O_Analysis_Demos.html
28	6	Searching Algorithms	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Searching_Algorithms.html
29	6	Sorting Algorithms	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Sort.html
30	7	Modules in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/modules.html
31	7	Packages in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/packages.html
32	7	User Classes Pt. 1	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/user_classes.html
33	7	User Classes Pt. 2	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/user_classes_pt2.html
34	7	Exception handling in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/exceptions.html
35	7	Object Oriented Principles in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/oop_principles.html
36	7	Object Oriented Terminology Review	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/oop_terminology.html
37	8	Using Python's virtual environment	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/virtualenv.html
38	9	IDF lessons for flow control - operators	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/
39	9	IDF lessons for flow control - if, elif, else	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/if_elif_else.html
40	9	IDF lessons for flow control - while	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/while_loops.html
41	9	IDF lessons for flow control - for loops	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/for_loops.html
42	9	IDF lessons for flow control - break and continue	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/break_continue.html
43	10	Ctypes and structures in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/ctypes.html
44	10	Regular Expressions in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/regular_expressions.html
45	10	Additional Libraries in Python 	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/additional_libraries_modules.html
46	10	Multithreading in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/multithreading.html
47	10	Unit testing in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/unit_testing.html
48	10	Metaclasses in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/metaclasses.html
49	11	Design Patterns in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/designpatterns.html
50	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/04_creating_a_repo.html	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/04_creating_a_repo.html
51	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/06_version_control.html	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/06_version_control.html
52	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/git_configuration_files.html	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/git_configuration_files.html
53	12	https://docs.gitlab.com/ee/user/project/issues/	https://docs.gitlab.com/ee/user/project/issues/
54	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/slides/#/8	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/slides/#/8
55	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/05_branching.html	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/05_branching.html
56	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/gitlab_cicd_intro.html	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/gitlab_cicd_intro.html
57	12	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/01_Introduction.html	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/01_Introduction.html
58	13	Introduction to Programming	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/Intro_to_Programming/index.html
59	13	Input Validation	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=Input%20validation
60	13	Loop Structure	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=loop%20structure
61	13	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=structures
62	13	Free Code Camp recursion example	https://www.freecodecamp.org/news/how-recursion-works-explained-with-flowcharts-and-a-video-de61f40cb7f9/
63	13	Recursion example	https://www.techiedelight.com/reverse-a-string-using-recursion/
64	14	Calling conventions, name mangling, etc. for Microsoft and Sys V in Assembly	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Calls.html
65	14	Informational Only: System Calls in Assembly	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html
66	14	Understanding WinDBG	https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg
67	15	Understanding and using flags in Assembly 	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Flags.html
68	15	Understanding and using control flow e.g., call, ret, cmp, jcc, etc. in Assembly	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/index.html
69	15	Understanding and using string instructions in Assembly	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Strings_Calls.html
70	15	Calling conventions, name mangling, etc. for Microsoft and Sys V in Assembly	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Calls.html
71	16	Introduction to working with Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/python_features/index.html
72	17	Understanding scope in Python functions	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/scope.html
73	17	User functions in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/user_functions.html
74	17	lambda functions in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/lambda_functions.html
75	17	List comprehension in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/list_comprehension.html
76	17	Recursion in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/recursion.html
77	17	Closures, iterators, and generators in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/closures_iterators_generators.html
\.


--
-- Name: modules_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.modules_content_id_seq', 77, true);


--
-- PostgreSQL database dump complete
--

