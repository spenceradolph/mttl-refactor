--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: modules_performance; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.modules_performance (id, modules_id, description, url) FROM stdin;
1	1	Store specified data in a tuple.	https://www.w3schools.com/python/python_tuples.asp
2	1	Create a program using strings and string methods	https://www.w3schools.com/python/python_strings.asp
3	1	Utilize string methods to manipulate strings	https://www.w3schools.com/python/python_strings_modify.asp
4	1	Store user input as a string	https://www.geeksforgeeks.org/taking-input-in-python/
5	1	Use the Python interpreter to identify the data type	https://www.geeksforgeeks.org/type-isinstance-python/
6	1	Use arithmetic operators to modify Python program functionality	https://www.geeksforgeeks.org/python-arithmetic-operators/
7	1	Use bitwise operators to modify Python program functionality	https://www.geeksforgeeks.org/python-bitwise-operators/
8	1	Use type conversion to modify Python program functionality	https://www.geeksforgeeks.org/type-conversion-python/
9	1	Use Python to create, access and manipulate a list	https://www.geeksforgeeks.org/python-list/
10	1	Use Python to create, access and manipulate a Multi-dimensional list	https://www.geeksforgeeks.org/python-using-2d-arrays-lists-the-right-way/
11	2	Write programs to move, replace, and swap values in registers using Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_1/Lab1.nasm
12	2	Write programs partially copying data - leveraging and adapting across registers of different sizes.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm
13	2	Identify and access different registers appropriately in Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm
14	3	Review question seeking strategies - describe the strategies used to complete this task	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Labs/QuestionScavengerHunt.html
15	3	Review core Agile and Scrum terms	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview_MC.html
16	3	Reflect on core Agile and Scrum terms	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview_Checkpoints.html
17	3	 Practice creating User Stories	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/slides/#/9/1
18	4	Implement linked lists in Python 	https://www.geeksforgeeks.org/linked-list-set-1-introduction/
19	4	Use stack functions in Python 	https://www.geeksforgeeks.org/stack-in-python/
20	4	Implement binary tree traversal in Python	https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
21	5	Apply knowledge of the stack through commands in Assembly	TBD
22	6	Implement search and sort algorithms in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Big_0_Analysis_Perf_Labs.html
23	6	Recognize the implications of big O notation	https://towardsdatascience.com/understanding-time-complexity-with-python-examples-2bda6e8158a7
24	6	Implement common sorting algorithms in Python	https://medium.com/@george.seif94/a-tour-of-the-top-5-sorting-algorithms-with-python-code-43ea9aa02889
25	7	Install and utilize a Python package via PIP.	https://packaging.python.org/tutorials/installing-packages/
26	7	Write a class in Python	https://www.geeksforgeeks.org/python-classes-and-objects/
27	7	Expand class functionality using getter and setter functions.	https://www.geeksforgeeks.org/getter-and-setter-in-python/
28	7	Write a program to demonstrate exception handling in Python.	https://www.geeksforgeeks.org/python-exception-handling/
29	10	Create a structure to pass to an imported CDLL.	https://www.geeksforgeeks.org/how-to-call-c-c-from-python/
30	10	Import a C library (.DLL/.SO) to utilize a C compiled function.	https://www.geeksforgeeks.org/how-to-call-c-c-from-python/
31	10	Create a Python script that interacts with system processes.	https://www.geeksforgeeks.org/kill-a-process-by-name-using-python/
32	10	Create a Python script that interacts with system-level information.	https://www.tutorialsteacher.com/python/sys-module
33	10	Given a set of requirements, implement a metaclass.	https://realpython.com/python-metaclasses/
34	10	Utilize the Thread library to implement a simple multithreaded program.	https://realpython.com/intro-to-python-threading/
35	10	Use the Python 're' library to match a specified pattern.	https://regexone.com/references/python
36	10	Design and implement a unit test	https://realpython.com/python-testing/
37	11		TBD
38	12	Demonstrate proficiency with fundamental git commands	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Git/gitLab_issue_creation/instructions.md
39	12	Implement more advanced git commands and functionality.	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Git/git_command_line/instructions.md
40	13	Draw a flowchart for pseudocode.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/labs/pseudolab1.md
41	13	Draw hierarchy chart to plan the logic for a bank program.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab2.md
42	13	Use pseudocode to control logical flow.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab3.md
43	13	Design pseudocode for a program that accepts and manipulates user input.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab4.md
44	13	Design logic for a program to adhere to the requirements list.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab5.md
45	13	Use pseudocode to alter programs to meet new specifications.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab6.md
46	13	Design functions that will incorporate pass by value and pass by reference.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab7.1.md
47	13	Using pseudocode solve three programming projects.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab8.1.md
48	15	Leverage conditional branching to solve problems in Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_6/Lab6.nasm
49	15	Utilize common string instructions in Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_7/Lab7.nasm
50	15	In Assembly, understand name mangling and access predefined external utility functions.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_9/Lab9.nasm
51	16	Launch Python from the command line	https://39iosdev.gitlab.io/ccd-iqt/idf/python/python_features/lab1a.html
52	17	Write a program to parse command line arguments in Python.	https://docs.python.org/3/howto/argparse.html
53	17	Write a program to return multiples values in Python.	https://www.geeksforgeeks.org/g-fact-41-multiple-return-values-in-python/
54	17	Write a program to receive input parameters in Python.	https://www.tutorialspoint.com/python/python_command_line_arguments.htm
55	17	Create and implement functions to meet a requirement in Python.	https://www.learnpython.org/en/Functions
\.


--
-- Name: modules_performance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.modules_performance_id_seq', 55, true);


--
-- PostgreSQL database dump complete
--

