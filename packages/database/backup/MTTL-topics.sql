--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: topics; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.topics (id, topic) FROM stdin;
1	Mission
2	Agile
3	Kernel Development
4	C
5	Python
6	Network Analysis/Programming
7	Software Engineering
8	Version Control
9	DevOps
10	Software Testing
11	Assembly Programming
12	Reverse Engineering
13	Programming
14	Debugging
15	Vulnerability Research
16	Data Structures
17	Implement Security Measures
18	Secure Coding
19	Standards
20	Technical Writing
21	Project Management
22	Project Management Tools
23	Lessons Learned
24	
25	Stan/Eval
26	security classification
27	Git
28	Software Engineering fundamentals
29	Programming Fundamentals
30	Algorithms
31	Networking Concepts
32	Unicode
33	Operating System
34	Development Operations
35	Drivers
36	Windows
37	Linux
38	Risk Management
39	Laws & Ethics
40	Privacy
41	Threats & Vulnerabilities
42	Software Engineering Fundamentals
43	STIGs
44	Software Development Process
45	Architecture Concepts & Models
46	Quality Assurance
47	Critical Infrastructure
48	Penetration Testing
49	90 MQT
50	Organization Overview
51	Cyber Mission Forces
52	Policy and Compliance
53	Windows Operating System
54	CNO User Mode Development
55	Windows Kernel Development
56	*nix
57	Linux Kernel Development
58	C++
59	PowerShell
60	PowerShell injection/injection techniques
61	PowerShell case studies: Domain Name System (DNS)
62	Bash Scripting
63	Wiki
64	Immunity
65	Metasploit
66	JIRA Fundamentals
67	Hacking Methodologies
68	Testing Process
69	USCC Basic Info
70	Organizational Partner Info
71	Supported Organization Processes
72	Data Serialization
73	Network Security
74	Cryptography
75	MetaTotal
76	GitLab
77	Docker
78	Best Practices
79	Day 2 Operations Concept
80	Cloud Services
81	Securing Cloud Services
82	Software Architecture
83	CYT Requirements
84	PO
85	Scrum
86	Jira
87	Agile Development
88	Containerization
89	Advanced Math
90	Organizational Security
91	CI/CD
92	Computer Architecture
93	Windows Internals
94	SSH
\.


--
-- Name: topics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.topics_id_seq', 94, true);


--
-- PostgreSQL database dump complete
--

