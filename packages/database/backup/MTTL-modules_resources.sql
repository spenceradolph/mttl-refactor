--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: modules_resources; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.modules_resources (id, modules_id, title, type, description, url) FROM stdin;
1	1	Using the Python Dictionaries	reference	Reference from IDF	https://www.w3schools.com/python/python_dictionaries.asp
2	1	Using Tuples in Python	reference	Reference from IDF	https://www.w3schools.com/python/python_tuples.asp
3	1	Overview of data structures in Python i.e. sets, lists, tuples, dictionaries	reference	Reference from IDF	https://docs.python.org/3/tutorial/datastructures.html
4	1	Variable types in Python	reference	Reference from IDF	https://www.tutorialspoint.com/python/python_variable_types.htm
5	1	Strings in Python	reference	Reference from IDF	https://www.tutorialspoint.com/python3/python_strings.htm
6	1	Dictionaries in Python	reference	Reference from IDF	https://realpython.com/python-dicts/
7	1	Python String Reference	reference	Reference from IDF	https://docs.python.org/3/library/string.html
8	1	Understanding lists in Python	reference	Reference from IDF	https://www.w3schools.com/python/python_lists.asp
9	1	Basic data types in Python	reference	Reference from IDF	https://docs.python.org/3/library/stdtypes.html
10	1	Python reference for numeric bases	reference	Reference from IDF	https://docs.python.org/3/library/numbers.html
11	2	https://www.csee.umbc.edu/courses/undergraduate/313/spring05/burt_katz/lectures/Lect10/structuresInAsm.html	reference	Reference from IDF	https://www.csee.umbc.edu/courses/undergraduate/313/spring05/burt_katz/lectures/Lect10/structuresInAsm.html
12	2	https://www.gnu-pascal.de/gpc/Endianness.html	reference	Reference from IDF	https://www.gnu-pascal.de/gpc/Endianness.html
13	2	https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm
14	2	https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture	reference	Reference from IDF	https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture
15	2	http://www.c-jump.com/CIS77/ASM/DataTypes/lecture.html	reference	Reference from IDF	http://www.c-jump.com/CIS77/ASM/DataTypes/lecture.html
16	2	https://unix.stackexchange.com/questions/297982/how-to-step-into-step-over-and-step-out-with-gdb	reference	Reference from IDF	https://unix.stackexchange.com/questions/297982/how-to-step-into-step-over-and-step-out-with-gdb
17	2	https://www.tutorialspoint.com/assembly_programming/assembly_variables.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_variables.htm
18	2	https://courses.cs.washington.edu/courses/cse351/13su/lectures/12-structs.pdf	reference	Reference from IDF	https://courses.cs.washington.edu/courses/cse351/13su/lectures/12-structs.pdf
19	2	https://www.geeksforgeeks.org/assembly-language-program-find-largest-number-array/	reference	Reference from IDF	https://www.geeksforgeeks.org/assembly-language-program-find-largest-number-array/
20	2	https://stackoverflow.com/questions/43562980/swapping-two-int-pointers-in-assembly-x86	reference	Reference from IDF	https://stackoverflow.com/questions/43562980/swapping-two-int-pointers-in-assembly-x86
21	3	What is RTFM?	reference	Reference from IDF	http://www.catb.org/jargon/html/R/RTFM.html
22	3	What is scrum?	reference	Reference from IDF	https://digital.ai/resources/agile-101/what-is-scrum
23	3	Stack Overflow guide to asking questions	reference	Reference from IDF	https://stackoverflow.com/help/how-to-ask
24	3	Understanding user stories in Agile	reference	Reference from IDF	https://www.mountaingoatsoftware.com/agile/user-stories
25	3	Be agile vs. do Agile	reference	Reference from IDF	https://medium.com/@Intersog/how-being-agile-is-different-from-doing-agile-9098e8b679f1
26	3	Functional testing	reference	Reference from IDF	https://www.simform.com/functional-testing-types/
27	3	The Agile Manifesto	reference	Reference from IDF	https://www.agilemanifesto.org
28	3	What is a daily scrum?	reference	Reference from IDF	https://www.scrum.org/resources/what-is-a-daily-scrum
29	3	Agile 101 Website	reference	Reference from IDF	https://www.agilealliance.org/agile101/
30	3	Why asking good questions is important	reference	Reference from IDF	https://en.wikipedia.org/wiki/Eternal_September
31	3	Steps to backlog refinement	reference	Reference from IDF	https://www.romanpichler.com/blog/the-product-backlog-refinement-steps
32	3	Understanding Agile	reference	Reference from IDF	https://www.mountaingoatsoftware.com/agile
33	4	Introduction to Linked Lists	reference	Reference from IDF	https://www.geeksforgeeks.org/linked-list-set-1-introduction/
34	4	Linked Lists in Python	reference	Reference from IDF	https://realpython.com/linked-lists-python/
35	4	Using the stack in Python	reference	Reference from IDF	https://www.geeksforgeeks.org/stack-in-python/
36	4	Implementing the Python stack	reference	Reference from IDF	https://realpython.com/how-to-implement-python-stack/
37	4	Queues in Python	reference	Reference from IDF	https://www.geeksforgeeks.org/queue-in-python/
38	4	Binary trees in Python	reference	Reference from IDF	https://www.tutorialspoint.com/python_data_structure/python_binary_tree.htm
39	4	Understanding trees in Python	reference	Reference from IDF	https://www.openbookproject.net/thinkcs/python/english2e/ch21.html
40	4	The binary tree data structure	reference	Reference from IDF	https://www.geeksforgeeks.org/binary-tree-data-structure/
41	5	https://stackoverflow.com/questions/46790666/how-is-stack-memory-allocated-when-using-push-or-sub-x86-instructions	reference	Reference from IDF	https://stackoverflow.com/questions/46790666/how-is-stack-memory-allocated-when-using-push-or-sub-x86-instructions
42	5	https://c9x.me/x86/html/file_module_x86_id_72.html	reference	Reference from IDF	https://c9x.me/x86/html/file_module_x86_id_72.html
43	5	https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/	reference	Reference from IDF	https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/
44	5	https://www.amd.com/system/files/TechDocs/24594.pdf	reference	Reference from IDF	https://www.amd.com/system/files/TechDocs/24594.pdf
45	5	https://blog.holbertonschool.com/hack-virtual-memory-stack-registers-assembly-code/	reference	Reference from IDF	https://blog.holbertonschool.com/hack-virtual-memory-stack-registers-assembly-code/
46	5	https://www.felixcloutier.com/x86/div	reference	Reference from IDF	https://www.felixcloutier.com/x86/div
47	5	http://www.cs.tau.ac.il/~maon/teaching/2014-2015/seminar/seminar1415a-lec6-runtime.pdf	reference	Reference from IDF	http://www.cs.tau.ac.il/~maon/teaching/2014-2015/seminar/seminar1415a-lec6-runtime.pdf
48	5	https://www.cs.uaf.edu/2012/fall/cs301/lecture/09_21_stack.html	reference	Reference from IDF	https://www.cs.uaf.edu/2012/fall/cs301/lecture/09_21_stack.html
49	5	https://learn.adacore.com/labs/bug-free-coding/chapters/stack.html	reference	Reference from IDF	https://learn.adacore.com/labs/bug-free-coding/chapters/stack.html
50	5	https://www.tutorialspoint.com/assembly_programming/assembly_arithmetic_instructions.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_arithmetic_instructions.htm
51	5	https://www.tutorialspoint.com/operating_system/os_processes.htm	reference	Reference from IDF	https://www.tutorialspoint.com/operating_system/os_processes.htm
52	6	Searches in Python	reference	Reference from IDF	https://github.com/TheAlgorithms/Python/tree/master/searches
53	6	Python Big O Notation	reference	Reference from IDF	https://www.tutorialspoint.com/python_data_structure/python_big_o_notation.htm
54	6	Data Structures in Python	reference	Reference from IDF	https://github.com/TheAlgorithms/Python/tree/master/data_structures
55	6	Sorting Algorithms in Python	reference	Reference from IDF	https://stackabuse.com/sorting-algorithms-in-python/
56	6	Sorts in Python	reference	Reference from IDF	https://github.com/TheAlgorithms/Python/tree/master/sorts
57	6	Python Sorting Algorithms	reference	Reference from IDF	https://www.tutorialspoint.com/python_data_structure/python_sorting_algorithms.htm
58	6	Sorting Algorithms in Python	reference	Reference from IDF	https://realpython.com/sorting-algorithms-python/
59	7	Object Oriented Programming in Python	reference	Reference from IDF	https://realpython.com/python3-object-oriented-programming/
60	7	Exceptions in Python	reference	Reference from IDF	https://docs.python.org/3/library/exceptions.html
61	7	Modules in Python	reference	Reference from IDF	https://docs.python.org/3/tutorial/modules.html
62	7	Handling Exception in Python	reference	Reference from IDF	https://wiki.python.org/moin/HandlingExceptions
63	7	Classes in Python	reference	Reference from IDF	https://www.w3schools.com/python/python_classes.asp
64	7	Using PIP in Python	reference	Reference from IDF	https://pip.pypa.io/en/stable/quickstart/
65	7	Classes in Python	reference	Reference from IDF	https://docs.python.org/3/tutorial/classes.html
66	7	Packages in Python	reference	Reference from IDF	https://docs.python.org/3/tutorial/modules.html#packages
67	8	Using Virtual Environments in Python	reference	Reference from IDF	https://python-docs.readthedocs.io/en/latest/dev/virtualenvs.html
68	10	https://docs.python.org/3/howto/regex.html	reference	Reference from IDF	https://docs.python.org/3/howto/regex.html
69	10	https://realpython.com/intro-to-python-threading/	reference	Reference from IDF	https://realpython.com/intro-to-python-threading/
70	10	https://pythonpedia.com/en/tutorial/9050/ctypes	reference	Reference from IDF	https://pythonpedia.com/en/tutorial/9050/ctypes
71	10	https://python-docs.readthedocs.io/en/latest/dev/virtualenvs.html	reference	Reference from IDF	https://python-docs.readthedocs.io/en/latest/dev/virtualenvs.html
72	10	https://www.w3schools.com/python/python_regex.asp	reference	Reference from IDF	https://www.w3schools.com/python/python_regex.asp
73	10	https://realpython.com/python-metaclasses/	reference	Reference from IDF	https://realpython.com/python-metaclasses/
74	10	https://docs.python.org/3/library/index.html	reference	Reference from IDF	https://docs.python.org/3/library/index.html
75	10	https://www.geeksforgeeks.org/python-design-patterns/	reference	Reference from IDF	https://www.geeksforgeeks.org/python-design-patterns/
76	10	https://realpython.com/python-testing/	reference	Reference from IDF	https://realpython.com/python-testing/
77	10	https://docs.python.org/3/library/ctypes.html	reference	Reference from IDF	https://docs.python.org/3/library/ctypes.html
78	10	https://docs.python.org/3/library/unittest.html	reference	Reference from IDF	https://docs.python.org/3/library/unittest.html
79	10	https://docs.python.org/3/library/threading.html	reference	Reference from IDF	https://docs.python.org/3/library/threading.html
80	11	https://www.geeksforgeeks.org/python-design-patterns/	reference	Reference from IDF	https://www.geeksforgeeks.org/python-design-patterns/
81	12	http://gitforwindows.org/	reference	Git for Windows	http://gitforwindows.org/
82	12	http://rogerdudler.github.io/git-guide/	reference	Git Guide	http://rogerdudler.github.io/git-guide/
83	12	https://docs.gitlab.com/ee/README.html	reference	GitLab documentation	https://docs.gitlab.com/ee/README.html
84	12	https://docs.gitlab.com/ee/ci/introduction/index.html	reference	CI/CD Concepts	https://docs.gitlab.com/ee/ci/introduction/index.html
85	12	https://docs.gitlab.com/ee/ci/introduction/index.html	reference	CI/CD Pipelines	https://docs.gitlab.com/ee/ci/pipelines/index.html
86	12	Keyword reference for Git	reference	Keyword Reference for .gitlab-ci.yml file	https://docs.gitlab.com/ee/ci/yaml/README.html
87	12	https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Basic-Snapshotting	reference	Git commands - Basic Snapshotting	https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Basic-Snapshotting
88	12	https://git-scm.com/docs	reference	Git Reference documentation	https://git-scm.com/docs
89	12	https://git-scm.com/docs/gittutorial	reference	Git tutorial	https://git-scm.com/docs/gittutorial
90	12	https://learngitbranching.js.org/	reference	Learn Git Branching	https://learngitbranching.js.org/
91	12	https://medium.com/@lulu.ilmaknun.q/kompilasi-meme-git-e2fe49c6e33e	reference	Meme Git Compilation	https://medium.com/@lulu.ilmaknun.q/kompilasi-meme-git-e2fe49c6e33e
92	12	https://oer.gitlab.io/oer-courses/vm-oer/03-Git-Introduction.html#/slide-vcs-review	reference	Git Introduction Slides	https://oer.gitlab.io/oer-courses/vm-oer/03-Git-Introduction.html#/slide-vcs-review
93	12	https://tom.preston-werner.com/2009/05/19/the-git-parable.html	reference	The Git Parable	https://tom.preston-werner.com/2009/05/19/the-git-parable.html
94	12	https://try.github.io/levels/1/challenges/1	reference	Github training challenges	https://try.github.io/levels/1/challenges/1
95	12	https://www.atlassian.com/git/tutorials	reference	Become a Git guru	https://www.atlassian.com/git/tutorials
96	12	https://www.atlassian.com/git/tutorials/what-is-git	reference	What is Git	https://www.atlassian.com/git/tutorials/what-is-git
97	12	https://www.tutorialspoint.com/git/index.htm	reference	Learn Git Tutorial	https://www.tutorialspoint.com/git/index.htm
98	12	Introduction to Git	Study Reference	Course Slides	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/slides/#/
99	12	Git Reference	Self-Study	Git Reference Documentation.	https://git-scm.com/docs
100	13	Introduction to Computers, Computing, and Programming	Study Reference	Course Slides	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/slides/#/
101	13	Farrell, Joyce, Programming Logic & Design (2017)	reference	Reference from IDF	Farrell, Joyce, Programming Logic & Design (2017)
102	14	Nebbett, G. (2000). Windows NT/2000 native API reference. Sams Publishing	reference	Reference from IDF	Nebbett, G. (2000). Windows NT/2000 native API reference. Sams Publishing
103	14	https://asmtutor.com/#lesson22	reference	Reference from IDF	https://asmtutor.com/#lesson22
104	14	https://www.cs.uaf.edu/2016/fall/cs301/lecture/11_04_syscall.html	reference	Reference from IDF	https://www.cs.uaf.edu/2016/fall/cs301/lecture/11_04_syscall.html
105	14	https://wiki.osdev.org/Virtual_8086_Mode	reference	Reference from IDF	https://wiki.osdev.org/Virtual_8086_Mode
106	14	https://riptutorial.com/x86/example/12672/real-mode	reference	Reference from IDF	https://riptutorial.com/x86/example/12672/real-mode
107	14	https://wiki.osdev.org/Real_Mode	reference	Reference from IDF	https://wiki.osdev.org/Real_Mode
108	14	https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux#Via_interrupt	reference	Reference from IDF	https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux#Via_interrupt
109	14	https://asmtutor.com/#lesson1	reference	Reference from IDF	https://asmtutor.com/#lesson1
110	14	https://www.tutorialspoint.com/assembly_programming/assembly_basic_syntax.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_basic_syntax.htm
111	14	https://www.codeproject.com/Articles/45788/The-Real-Protected-Long-mode-assembly-tutorial-for	reference	Reference from IDF	https://www.codeproject.com/Articles/45788/The-Real-Protected-Long-mode-assembly-tutorial-for
112	14	https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm
113	14	http://faculty.nps.edu/cseagle/assembly/sys_call.html	reference	Reference from IDF	http://faculty.nps.edu/cseagle/assembly/sys_call.html
114	14	https://www.tutorialspoint.com/assembly_programming/assembly_file_management.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_file_management.htm
115	14	https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux	reference	Reference from IDF	https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux
116	14	https://wiki.osdev.org/System_Management_Mode	reference	Reference from IDF	https://wiki.osdev.org/System_Management_Mode
117	14	https://wiki.osdev.org/Protected_Mode	reference	Reference from IDF	https://wiki.osdev.org/Protected_Mode
118	14	https://software.intel.com/content/www/us/en/develop/articles/intel-sdm.html	reference	Reference from IDF	https://software.intel.com/content/www/us/en/develop/articles/intel-sdm.html
119	14	http://www.c-jump.com/CIS77/ASM/Memory/lecture.html	reference	Reference from IDF	http://www.c-jump.com/CIS77/ASM/Memory/lecture.html
120	14	https://www.researchgate.net/publication/241643659_Using_CPU_System_Management_Mode_to_Circumvent_Operating_System_Security_Functions	reference	Reference from IDF	https://www.researchgate.net/publication/241643659_Using_CPU_System_Management_Mode_to_Circumvent_Operating_System_Security_Functions
121	14	https://resources.infosecinstitute.com/calling-ntdll-functions-directly/#gref	reference	Reference from IDF	https://resources.infosecinstitute.com/calling-ntdll-functions-directly/#gref
122	14	https://wiki.osdev.org/Security#Rings	reference	Reference from IDF	https://wiki.osdev.org/Security#Rings
123	14	https://j00ru.vexillium.org/syscalls/nt/64/	reference	Reference from IDF	https://j00ru.vexillium.org/syscalls/nt/64/
124	14	https://stackoverflow.com/questions/29440225/in-linux-x86-64-are-syscalls-and-int-0x80-related	reference	Reference from IDF	https://stackoverflow.com/questions/29440225/in-linux-x86-64-are-syscalls-and-int-0x80-related
125	14	https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/	reference	Reference from IDF	https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/
126	15	https://www.amd.com/system/files/TechDocs/24594.pdf	reference	Reference from IDF	https://www.amd.com/system/files/TechDocs/24594.pdf
127	15	https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/	reference	Reference from IDF	https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/
128	15	https://security.stackexchange.com/questions/129499/what-does-eip-stand-for	reference	Reference from IDF	https://security.stackexchange.com/questions/129499/what-does-eip-stand-for
129	15	https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-1-manual.pdf	reference	Reference from IDF	https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-1-manual.pdf
130	15	https://wiki.osdev.org/X86-64_Instruction_Encoding#Legacy_Prefixes	reference	Reference from IDF	https://wiki.osdev.org/X86-64_Instruction_Encoding#Legacy_Prefixes
131	15	https://en.wikipedia.org/wiki/FLAGS_register	reference	Reference from IDF	https://en.wikipedia.org/wiki/FLAGS_register
132	15	https://www.quora.com/What-is-POPF-I-can-understand-PUSHF-cause-it-simply-push-flags-but-what-is-POPF-How-does-computer-know-what-is-flag-to-pop-1	reference	Reference from IDF	https://www.quora.com/What-is-POPF-I-can-understand-PUSHF-cause-it-simply-push-flags-but-what-is-POPF-How-does-computer-know-what-is-flag-to-pop-1
133	15	http://www.c-jump.com/CIS77/ASM/Instructions/I77_0070_eflags_bits.htm	reference	Reference from IDF	http://www.c-jump.com/CIS77/ASM/Instructions/I77_0070_eflags_bits.htm
134	15	https://nasm.us/doc/nasmdoc3.html	reference	Reference from IDF	https://nasm.us/doc/nasmdoc3.html
135	15	https://compas.cs.stonybrook.edu/~nhonarmand/courses/sp17/cse506/ref/assembly.html	reference	Reference from IDF	https://compas.cs.stonybrook.edu/~nhonarmand/courses/sp17/cse506/ref/assembly.html
136	15	https://datacadamia.com/computer/cpu/register/eflags	reference	Reference from IDF	https://datacadamia.com/computer/cpu/register/eflags
137	15	https://www.tutorialspoint.com/assembly_programming/assembly_scas_instruction.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_scas_instruction.htm
138	15	https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm	reference	Reference from IDF	https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm
139	15	https://en.wikipedia.org/wiki/X86_calling_conventions	reference	Reference from IDF	https://en.wikipedia.org/wiki/X86_calling_conventions
140	15	https://en.wikibooks.org/wiki/X86_Assembly/Control_Flow	reference	Reference from IDF	https://en.wikibooks.org/wiki/X86_Assembly/Control_Flow
141	15	https://revers.engineering/applied-re-accelerated-assembly-p1/	reference	Reference from IDF	https://revers.engineering/applied-re-accelerated-assembly-p1/
142	15	https://wiki.skullsecurity.org/index.php?title=Registers#eip	reference	Reference from IDF	https://wiki.skullsecurity.org/index.php?title=Registers#eip
143	15	https://www.felixcloutier.com/x86/scas:scasb:scasw:scasd	reference	Reference from IDF	https://www.felixcloutier.com/x86/scas:scasb:scasw:scasd
144	16	Introductory Python documentation	reference	Reference from IDF	https://docs.python.org/3/
145	16	The Python standard library version 2	reference	Reference from IDF	https://docs.python.org/2.7/library/index.html
146	16	Differences between Python 2 and 3 	reference	Reference from IDF	https://wiki.python.org/moin/Python2orPython3?action=recall&rev=96
147	16	Python Standard Library version 3	reference	Reference from IDF	https://docs.python.org/3.7/library/index.html
148	16	Python Version 2 	reference	Reference from IDF	https://docs.python.org/2.7/
149	16	Introduction to Python Scripts	reference	Reference from IDF	https://realpython.com/run-python-scripts/
150	16	Docstrings in Python	reference	Reference from IDF	https://www.pythonforbeginners.com/basics/python-docstrings/
151	16	Python 2 vs. Python 3	reference	Reference from IDF	https://www.guru99.com/python-2-vs-python-3.html
152	16	PEP8 - style guide for Python	reference	Reference from IDF	https://www.python.org/dev/peps/pep-0008/
153	17	Recursion in Python	reference	Reference from IDF	https://pythonspot.com/recursion/
154	17	Recursive Thinking in Python	reference	Reference from IDF	https://realpython.com/python-thinking-recursively/
155	17	Define functions in Python 3 	reference	Reference from IDF	https://www.digitalocean.com/community/tutorials/how-to-define-functions-in-python-3
156	17	Generators in Python	reference	Reference from IDF	https://www.programiz.com/python-programming/generator
157	17	Scope in Python	reference	Reference from IDF	https://www.w3schools.com/PYTHON/python_scope.asp
158	17	Lambda functions in Python	reference	Reference from IDF	https://realpython.com/python-lambda/
159	17	Python Lambda	reference	Reference from IDF	https://www.w3schools.com/python/python_lambda.asp
160	17	Python Iterators	reference	Reference from IDF	https://www.w3schools.com/python/python_iterators.asp
161	17	Closure in Python	reference	Reference from IDF	https://www.programiz.com/python-programming/closure
162	17	Scope in Python	reference	Reference from IDF	https://pythonbasics.org/scope/
\.


--
-- Name: modules_resources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.modules_resources_id_seq', 162, true);


--
-- PostgreSQL database dump complete
--

