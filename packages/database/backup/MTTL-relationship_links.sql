--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: relationship_links; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.relationship_links (id, type, map_type, topic, url, name, description, courses_id) FROM stdin;
1	ccv-knowledge	eval	quick-sort	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481ad	\N	\N	\N
2	ccv-knowledge	eval	algorithms-big-O	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481b3	\N	\N	\N
3	ccv-knowledge	eval	breadth-first	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481b9	\N	\N	\N
4	ccv-knowledge	eval	merge-sort	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481bf	\N	\N	\N
5	ccv-knowledge	eval	heap-sort	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481c5	\N	\N	\N
6	ccv-knowledge	eval	selection-sort	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481cb	\N	\N	\N
7	ccv-knowledge	eval	depth-first	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481d1	\N	\N	\N
8	ccv-knowledge	eval	insertion-sort	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481d7	\N	\N	\N
9	ccv-knowledge	eval	hash-table	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481dd	\N	\N	\N
10	ccv-knowledge	eval	assembly-op-code	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481e3	\N	\N	\N
11	ccv-knowledge	eval	assembly-basics-and-memory_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481ef	\N	\N	\N
12	ccv-knowledge	eval	assembly-arithmetic-instructions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481f5	\N	\N	\N
13	ccv-knowledge	eval	assembly-stores-names	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481fb	\N	\N	\N
14	ccv-knowledge	eval	assembly-basics-and-memory_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248201	\N	\N	\N
15	ccv-knowledge	eval	assembly-arithmetic-instructions_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248207	\N	\N	\N
16	ccv-knowledge	eval	assembly_last_statement	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424820d	\N	\N	\N
17	ccv-knowledge	eval	assembly-control-flow_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248213	\N	\N	\N
18	ccv-knowledge	eval	assembly-stack	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248219	\N	\N	\N
19	ccv-knowledge	eval	assembly-arithmetic-instructions_8	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424821f	\N	\N	\N
20	ccv-knowledge	eval	assembly-basics-and-memory_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248225	\N	\N	\N
21	ccv-knowledge	eval	assembly-basics-and-memory_6	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424822b	\N	\N	\N
22	ccv-knowledge	eval	assembly-arithmetic-instructions_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248231	\N	\N	\N
23	ccv-knowledge	eval	assembly-basics-and-memory_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248237	\N	\N	\N
24	ccv-knowledge	eval	assembly-instructions-movzx	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424823d	\N	\N	\N
25	ccv-knowledge	eval	assembly-control-flow_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248243	\N	\N	\N
26	ccv-knowledge	eval	assembly-basics-and-memory_9	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248249	\N	\N	\N
27	ccv-knowledge	eval	assembly-creating-asm_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424824f	\N	\N	\N
28	ccv-knowledge	eval	assembly-negative-bitwise_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248255	\N	\N	\N
29	ccv-knowledge	eval	assembly-instructions-call	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424825b	\N	\N	\N
30	ccv-knowledge	eval	assembly-arithmetic-instructions_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248261	\N	\N	\N
31	ccv-knowledge	eval	assembly-data-types-and-gdb-part-2_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248267	\N	\N	\N
32	ccv-knowledge	eval	assembly-flags_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248273	\N	\N	\N
33	ccv-knowledge	eval	assembly-negative-bitwise_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248279	\N	\N	\N
34	ccv-knowledge	eval	assembly-advanced-types-and-concepts_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424827f	\N	\N	\N
35	ccv-knowledge	eval	assembly-the-stack_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248285	\N	\N	\N
36	ccv-knowledge	eval	assembly-arithmetic-instructions_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd424828b	\N	\N	\N
37	ccv-knowledge	eval	assembly-data-types-and-gdb-part-2_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248291	\N	\N	\N
38	ccv-knowledge	eval	assembly-computer-basics_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd4248297	\N	\N	\N
39	ccv-knowledge	eval	assembly-computer-basics_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424829d	\N	\N	\N
40	ccv-knowledge	eval	assembly-negative-numbers-and-bitwise_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482a3	\N	\N	\N
41	ccv-knowledge	eval	assembly-arithmetic-instructions_7	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482a9	\N	\N	\N
42	ccv-knowledge	eval	assembly-instructions-shifting	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482af	\N	\N	\N
43	ccv-performance	eval	assembly-instructions-shifting	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/knowledge/Assembly/assembly-instructions-shifting/README.md	\N	\N	\N
44	ccv-knowledge	eval	assembly-negative-bitwise_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482b5	\N	\N	\N
45	ccv-knowledge	eval	assembly-basics-and-memory_8	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482bb	\N	\N	\N
46	ccv-knowledge	eval	assembly-flags_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482c1	\N	\N	\N
47	ccv-knowledge	eval	assembly-data-types_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482c7	\N	\N	\N
659	IQT	training	oop	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/index.html	\N	\N	1
48	ccv-knowledge	eval	assembly-instructions-movs	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482cd	\N	\N	\N
49	ccv-knowledge	eval	assembly-twos-compliment	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482d3	\N	\N	\N
50	ccv-knowledge	eval	assembly-instructions-rotate	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482d9	\N	\N	\N
51	ccv-performance	eval	assembly-instructions-rotate	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/knowledge/Assembly/assembly-instructions-rotate/README.md	\N	\N	\N
52	ccv-knowledge	eval	assembly-basics-and-memory_7	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482df	\N	\N	\N
53	ccv-knowledge	eval	assembly-basics-and-memory_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482eb	\N	\N	\N
54	ccv-knowledge	eval	networking-socketserver_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482f1	\N	\N	\N
55	ccv-knowledge	eval	networking-tcp-process_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482f7	\N	\N	\N
56	ccv-knowledge	eval	networking-http_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482fd	\N	\N	\N
57	ccv-knowledge	eval	networking-osi-layers_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248303	\N	\N	\N
58	ccv-knowledge	eval	networking-dhcp	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248309	\N	\N	\N
59	ccv-knowledge	eval	networking-class-A-address	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424830f	\N	\N	\N
60	ccv-knowledge	eval	networking-order-of-operations_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248315	\N	\N	\N
61	ccv-knowledge	eval	networking-switches_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248321	\N	\N	\N
62	ccv-knowledge	eval	networking-ipv6-addresses_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424832d	\N	\N	\N
63	ccv-knowledge	eval	networking-tcp-header-and-flags_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248333	\N	\N	\N
64	ccv-knowledge	eval	subnetting	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248339	\N	\N	\N
65	ccv-knowledge	eval	networking-session-layer	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424833f	\N	\N	\N
66	ccv-knowledge	eval	networking-osi-layers_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424834b	\N	\N	\N
67	ccv-knowledge	eval	networking-udp	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248351	\N	\N	\N
68	ccv-knowledge	eval	networking-ipv4-addresses_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248357	\N	\N	\N
69	ccv-knowledge	eval	networking-arp_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424835d	\N	\N	\N
70	ccv-knowledge	eval	rfc	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424836f	\N	\N	\N
71	ccv-knowledge	eval	networking-udp_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248375	\N	\N	\N
72	ccv-knowledge	eval	networking-syn-and-ack_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424837b	\N	\N	\N
73	ccv-knowledge	eval	networking-osi-layer-7_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248381	\N	\N	\N
74	ccv-knowledge	eval	networking-tcp-header-and-flags_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248387	\N	\N	\N
75	ccv-knowledge	eval	networking-network-byte-order_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424838d	\N	\N	\N
76	ccv-knowledge	eval	networking-layer-2-devices_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248393	\N	\N	\N
77	ccv-knowledge	eval	networking-data-link-layer	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248399	\N	\N	\N
78	ccv-knowledge	eval	networking-intro-to-tcp_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424839f	\N	\N	\N
79	ccv-knowledge	eval	networking-order-of-operations_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483a5	\N	\N	\N
80	ccv-knowledge	eval	networking-socket-types_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483ab	\N	\N	\N
81	ccv-knowledge	eval	networking-ipv6-addresses-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483b1	\N	\N	\N
82	ccv-knowledge	eval	networking-networks-and-ports_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483b7	\N	\N	\N
83	ccv-knowledge	eval	networking-dns-servers_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483bd	\N	\N	\N
84	ccv-knowledge	eval	networking-networks-and-ports_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483c3	\N	\N	\N
85	ccv-knowledge	eval	networking-order-of-operations_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42483c9	\N	\N	\N
86	ccv-knowledge	eval	networking-network-byte-order_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483cf	\N	\N	\N
87	ccv-knowledge	eval	networking-ping_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483d5	\N	\N	\N
88	ccv-knowledge	eval	networking-dhcp_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483db	\N	\N	\N
89	ccv-knowledge	eval	doubly-linked-list	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483e1	\N	\N	\N
90	ccv-knowledge	eval	queue	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483e7	\N	\N	\N
91	ccv-knowledge	eval	pitfalls	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483ed	\N	\N	\N
92	ccv-knowledge	eval	circularly-linked-list	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483f3	\N	\N	\N
93	ccv-knowledge	eval	stack	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483f9	\N	\N	\N
94	ccv-knowledge	eval	tree	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42483ff	\N	\N	\N
95	ccv-knowledge	eval	weighted-graphs	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248405	\N	\N	\N
96	ccv-knowledge	eval	c-formatted-io_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248411	\N	\N	\N
97	ccv-knowledge	eval	c-void-return-type	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248417	\N	\N	\N
98	ccv-knowledge	eval	c-bit-shift-left	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424841d	\N	\N	\N
99	ccv-knowledge	eval	c-standard-library-functions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248423	\N	\N	\N
100	ccv-knowledge	eval	c_stack_memory	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248429	\N	\N	\N
101	ccv-knowledge	eval	c-bitwise-operators_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424842f	\N	\N	\N
102	ccv-knowledge	eval	c-memory-management_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248435	\N	\N	\N
103	ccv-knowledge	eval	c-main-function	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424843b	\N	\N	\N
104	ccv-knowledge	eval	c-preprocessor	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248441	\N	\N	\N
105	ccv-knowledge	eval	c-by-reference	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248447	\N	\N	\N
106	ccv-knowledge	eval	c-structs_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424844d	\N	\N	\N
107	ccv-knowledge	eval	c-structs_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248453	\N	\N	\N
108	ccv-knowledge	eval	c-bitwise-operators_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248459	\N	\N	\N
109	ccv-knowledge	eval	c-detect-endianness	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424845f	\N	\N	\N
110	ccv-knowledge	eval	c-directives_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248465	\N	\N	\N
111	ccv-knowledge	eval	c-memory-management_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424846b	\N	\N	\N
112	ccv-knowledge	eval	c-pointers-and-arrays_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248471	\N	\N	\N
113	ccv-knowledge	eval	c-hashing	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248477	\N	\N	\N
114	ccv-knowledge	eval	c-pointers-and-arrays_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424847d	\N	\N	\N
115	ccv-knowledge	eval	c-demo-lab-1-2-c_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424852b	\N	\N	\N
116	ccv-knowledge	eval	union	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248483	\N	\N	\N
117	ccv-knowledge	eval	c-directives_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248489	\N	\N	\N
118	ccv-knowledge	eval	c-arithmetic-operators_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424848f	\N	\N	\N
119	ccv-knowledge	eval	return-address	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd4248495	\N	\N	\N
120	ccv-knowledge	eval	c-dereference	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424849b	\N	\N	\N
121	ccv-knowledge	eval	c-file-navigation	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484a1	\N	\N	\N
122	ccv-knowledge	eval	bitwise-ones-compliment	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484b3	\N	\N	\N
123	ccv-knowledge	eval	c-increment-operator	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484b9	\N	\N	\N
124	ccv-knowledge	eval	c-pointers-arrays_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484bf	\N	\N	\N
125	ccv-knowledge	eval	c_enum_values	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484cb	\N	\N	\N
126	ccv-knowledge	eval	c-memory-management_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484d1	\N	\N	\N
127	ccv-knowledge	eval	c-pointer-arithmetic	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484d7	\N	\N	\N
128	ccv-knowledge	eval	c-file-overwrite	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484dd	\N	\N	\N
129	ccv-knowledge	eval	c-2d-array	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484e3	\N	\N	\N
130	ccv-knowledge	eval	c-conditional-statements_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484e9	\N	\N	\N
131	ccv-knowledge	eval	c-data-types_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484ef	\N	\N	\N
132	ccv-knowledge	eval	c-stack-based-memory_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484f5	\N	\N	\N
133	ccv-knowledge	eval	c-strings_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42484fb	\N	\N	\N
134	ccv-knowledge	eval	c-basic-file-handling	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248501	\N	\N	\N
135	ccv-knowledge	eval	c-struct-format_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248507	\N	\N	\N
136	ccv-knowledge	eval	c-struct-declaration_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424850d	\N	\N	\N
137	ccv-knowledge	eval	c-typedef_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248513	\N	\N	\N
138	ccv-knowledge	eval	c-loops_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248519	\N	\N	\N
139	ccv-knowledge	eval	c-pointers-and-arrays_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248525	\N	\N	\N
140	ccv-knowledge	eval	c-arrays_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248531	\N	\N	\N
141	ccv-knowledge	eval	c-functions_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248537	\N	\N	\N
142	ccv-knowledge	eval	c-pointers-and-arrays_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424853d	\N	\N	\N
143	ccv-knowledge	eval	c-strings_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248543	\N	\N	\N
144	ccv-knowledge	eval	c-type-conversions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248549	\N	\N	\N
145	ccv-knowledge	eval	c-static-allocation	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424854f	\N	\N	\N
821	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
146	ccv-knowledge	eval	c-pointers-arrays_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248555	\N	\N	\N
147	ccv-knowledge	eval	c-function-pointers	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424855b	\N	\N	\N
148	ccv-knowledge	eval	c-compile-link	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248561	\N	\N	\N
149	ccv-knowledge	eval	c-loops_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248567	\N	\N	\N
150	ccv-knowledge	eval	c-pointers_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424856d	\N	\N	\N
151	ccv-knowledge	eval	c-directives_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248573	\N	\N	\N
152	ccv-knowledge	eval	c-pointers_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248579	\N	\N	\N
153	ccv-knowledge	eval	c-functions_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424857f	\N	\N	\N
154	ccv-knowledge	eval	c-casting_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248585	\N	\N	\N
155	ccv-knowledge	eval	c-heap_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424858b	\N	\N	\N
156	ccv-knowledge	eval	c-functions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248591	\N	\N	\N
157	ccv-knowledge	eval	c-scope_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248597	\N	\N	\N
158	ccv-knowledge	eval	c-heap_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424859d	\N	\N	\N
159	ccv-knowledge	eval	c-functions_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485a3	\N	\N	\N
160	ccv-knowledge	eval	c-memory-management_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485a9	\N	\N	\N
161	ccv-knowledge	eval	c-pointers-arrays_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485af	\N	\N	\N
162	ccv-knowledge	eval	c-functions_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485b5	\N	\N	\N
163	ccv-knowledge	eval	c-stack_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485bb	\N	\N	\N
164	ccv-knowledge	eval	c-header-files_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485c1	\N	\N	\N
165	ccv-knowledge	eval	bitwise-xor	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485c7	\N	\N	\N
166	ccv-knowledge	eval	c-array-strings_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485cd	\N	\N	\N
167	ccv-knowledge	eval	c-header-files_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485d3	\N	\N	\N
168	ccv-knowledge	eval	c-pointers_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485d9	\N	\N	\N
169	ccv-knowledge	eval	c-directives_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485df	\N	\N	\N
170	ccv-knowledge	eval	c-function-basics_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485e5	\N	\N	\N
171	ccv-knowledge	eval	c-memory-management_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485eb	\N	\N	\N
172	ccv-knowledge	eval	c-binary-search-tree	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485f1	\N	\N	\N
173	ccv-knowledge	eval	c-assign-vs-equal_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485f7	\N	\N	\N
174	ccv-knowledge	eval	c-command-line	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd42485fd	\N	\N	\N
175	ccv-performance	eval	c-command-line	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/knowledge/C_Programming/c-command-line/README.md	\N	\N	\N
176	ccv-knowledge	eval	bitwise-or	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248603	\N	\N	\N
177	ccv-knowledge	eval	python-lists_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248609	\N	\N	\N
178	ccv-knowledge	eval	python-tuple_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248615	\N	\N	\N
179	ccv-knowledge	eval	OO-advanage	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424861b	\N	\N	\N
180	ccv-knowledge	eval	python-scope	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248621	\N	\N	\N
181	ccv-knowledge	eval	python-variables_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd4248627	\N	\N	\N
182	ccv-knowledge	eval	python-std-lib_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424862d	\N	\N	\N
183	ccv-knowledge	eval	type-cast-logical-op	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248633	\N	\N	\N
184	ccv-knowledge	eval	python-file-opening	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248639	\N	\N	\N
185	ccv-knowledge	eval	python-factory-design	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424863f	\N	\N	\N
186	ccv-knowledge	eval	python-list-of-list	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424864b	\N	\N	\N
187	ccv-knowledge	eval	python-data-types_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248651	\N	\N	\N
188	ccv-knowledge	eval	python-operators_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248657	\N	\N	\N
189	ccv-knowledge	eval	python-generator-function	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424865d	\N	\N	\N
190	ccv-knowledge	eval	python-strings_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248663	\N	\N	\N
191	ccv-knowledge	eval	python-getter-setter	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248669	\N	\N	\N
192	ccv-knowledge	eval	python-boolean_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424866f	\N	\N	\N
193	ccv-knowledge	eval	python-lists_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248675	\N	\N	\N
194	ccv-knowledge	eval	python-singleton	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424867b	\N	\N	\N
195	ccv-knowledge	eval	class-attributes	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248681	\N	\N	\N
196	ccv-knowledge	eval	python-multiple-values	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248687	\N	\N	\N
197	ccv-knowledge	eval	python-return-statement	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424868d	\N	\N	\N
198	ccv-knowledge	eval	python-data-types_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248693	\N	\N	\N
199	ccv-knowledge	eval	python-oop_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248699	\N	\N	\N
200	ccv-knowledge	eval	python-mapping-and-other-types_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424860f	\N	\N	\N
201	ccv-knowledge	eval	python-variables_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486a5	\N	\N	\N
202	ccv-knowledge	eval	python-file-stats	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486ab	\N	\N	\N
203	ccv-knowledge	eval	python-lists_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486b1	\N	\N	\N
204	ccv-knowledge	eval	python-pep8	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486b7	\N	\N	\N
205	ccv-knowledge	eval	lists_7	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486bd	\N	\N	\N
206	ccv-knowledge	eval	cast_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486c3	\N	\N	\N
207	ccv-knowledge	eval	dict_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486c9	\N	\N	\N
208	ccv-knowledge	eval	data-types_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486cf	\N	\N	\N
209	ccv-knowledge	eval	lists_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486d5	\N	\N	\N
210	ccv-knowledge	eval	dict_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486db	\N	\N	\N
211	ccv-knowledge	eval	dict-list_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486e1	\N	\N	\N
212	ccv-knowledge	eval	set_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486e7	\N	\N	\N
213	ccv-knowledge	eval	functions_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486ed	\N	\N	\N
214	ccv-knowledge	eval	python-pydocs	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486f3	\N	\N	\N
215	ccv-knowledge	eval	python-flow-control_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486f9	\N	\N	\N
216	ccv-knowledge	eval	python-OO-super	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd42486ff	\N	\N	\N
217	ccv-knowledge	eval	python-flow-control_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248705	\N	\N	\N
218	ccv-knowledge	eval	python-strings_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424870b	\N	\N	\N
219	ccv-knowledge	eval	python-mutable-objects	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248711	\N	\N	\N
220	ccv-knowledge	eval	python-pip-install	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248717	\N	\N	\N
221	ccv-knowledge	eval	python_version	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424871d	\N	\N	\N
222	ccv-knowledge	eval	python-strings_6	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248723	\N	\N	\N
223	ccv-knowledge	eval	python-adapter-design	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248729	\N	\N	\N
224	ccv-knowledge	eval	class	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424872f	\N	\N	\N
225	ccv-knowledge	eval	python-constructor	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248735	\N	\N	\N
226	ccv-knowledge	eval	python-mapping_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424873b	\N	\N	\N
227	ccv-knowledge	eval	python-data-types_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248741	\N	\N	\N
228	ccv-knowledge	eval	python-file-making	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248747	\N	\N	\N
229	ccv-knowledge	eval	python-operators_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd424874d	\N	\N	\N
230	ccv-knowledge	eval	python-strings_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248753	\N	\N	\N
231	ccv-knowledge	eval	python-recursion	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248759	\N	\N	\N
232	ccv-knowledge	eval	python-bridge-design	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424875f	\N	\N	\N
233	ccv-knowledge	eval	python-linked-list	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248765	\N	\N	\N
234	ccv-knowledge	eval	constructor	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424876b	\N	\N	\N
235	ccv-knowledge	eval	super	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248771	\N	\N	\N
236	ccv-knowledge	eval	object	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248777	\N	\N	\N
237	ccv-knowledge	eval	inheritence	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424877d	\N	\N	\N
238	ccv-knowledge	eval	set	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248783	\N	\N	\N
239	ccv-knowledge	eval	attribute	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248789	\N	\N	\N
240	ccv-knowledge	eval	polymorphism	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424878f	\N	\N	\N
241	ccv-knowledge	eval	python-strings_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248795	\N	\N	\N
242	ccv-knowledge	eval	python-lists_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424879b	\N	\N	\N
243	ccv-knowledge	eval	iterators	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487a1	\N	\N	\N
244	ccv-knowledge	eval	python-set-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487a7	\N	\N	\N
245	ccv-knowledge	eval	python-mapping_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487ad	\N	\N	\N
246	ccv-knowledge	eval	python-numbers_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487b3	\N	\N	\N
247	ccv-knowledge	eval	python-flow-control_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487b9	\N	\N	\N
248	ccv-knowledge	eval	python-functions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487bf	\N	\N	\N
249	ccv-knowledge	eval	python-file-searching	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487cb	\N	\N	\N
250	ccv-knowledge	eval	python-else_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487d1	\N	\N	\N
251	ccv-knowledge	eval	python-inheritence	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2898bc9ca2dd4248645	\N	\N	\N
252	ccv-knowledge	eval	python-lambda-function	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487d7	\N	\N	\N
253	ccv-knowledge	eval	python-object-type	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487dd	\N	\N	\N
254	ccv-knowledge	eval	python-flow-control_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487e3	\N	\N	\N
255	ccv-knowledge	eval	python-lists_6	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487e9	\N	\N	\N
256	ccv-knowledge	eval	python-tuple_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487ef	\N	\N	\N
257	ccv-knowledge	eval	python-modules_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487f5	\N	\N	\N
258	ccv-knowledge	eval	purpose	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248801	\N	\N	\N
259	ccv-knowledge	eval	branch-update	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248807	\N	\N	\N
260	ccv-knowledge	eval	clone	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248813	\N	\N	\N
261	ccv-knowledge	eval	merge	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248819	\N	\N	\N
262	ccv-knowledge	eval	create-branch	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424881f	\N	\N	\N
263	ccv-knowledge	eval	changes	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424882b	\N	\N	\N
264	ccv-performance	eval	add_values	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Assembly/add_values/testfile.S	\N	\N	\N
265	ccv-performance	eval	power_of_two	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Assembly/power_of_two/testfile.S	\N	\N	\N
266	ccv-performance	eval	reverse_string	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Assembly/reverse_string/testfile.S	\N	\N	\N
267	ccv-performance	eval	mem_move	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Assembly/mem_move/testfile.S	\N	\N	\N
268	ccv-performance	eval	character_replace	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Assembly/character_replace/character_replace.asm	\N	\N	\N
269	ccv-performance	eval	count_characters	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Assembly/count_characters/testfile.S	\N	\N	\N
270	ccv-performance	eval	TCP_Modes	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Networking/TCP_Modes/client.py	\N	\N	\N
271	ccv-performance	eval	KeyServ_REST	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Networking/KeyServ_REST/README.md	\N	\N	\N
272	ccv-performance	eval	Crypto_Sockets	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Networking/Crypto_Sockets/client.py	\N	\N	\N
273	ccv-performance	eval	Socket_Key_Match	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Networking/Socket_Key_Match/client.py	\N	\N	\N
274	ccv-performance	eval	buffer_alloc	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/buffer_alloc/TestCode.c	\N	\N	\N
275	ccv-performance	eval	bubble_sort	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/bubble_sort/TestCode.c	\N	\N	\N
276	ccv-performance	eval	buy_groceries	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/buy_groceries/TestCode.c	\N	\N	\N
277	ccv-performance	eval	c_stack	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_stack/TestCode.c	\N	\N	\N
278	ccv-performance	eval	set_mode	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/set_mode/TestCode.c	\N	\N	\N
279	ccv-performance	eval	pass_by_reference	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/pass_by_reference/TestCode.c	\N	\N	\N
280	ccv-performance	eval	grades	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/grades/TestCode.c	\N	\N	\N
281	ccv-performance	eval	validate_password	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/validate_password/TestCode.c	\N	\N	\N
282	ccv-performance	eval	lo_shu_magic_square	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/lo_shu_magic_square/TestCode.c	\N	\N	\N
283	ccv-performance	eval	format_phone_numbers	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/format_phone_numbers/TestCode.c	\N	\N	\N
284	ccv-performance	eval	string_copying	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/string_copying/TestCode.c	\N	\N	\N
285	ccv-performance	eval	space_fixer	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/space_fixer/TestCode.c	\N	\N	\N
286	ccv-performance	eval	health_records	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/health_records/TestCode.c	\N	\N	\N
287	ccv-performance	eval	function_pointers	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/function_pointers/TestCode.c	\N	\N	\N
288	ccv-performance	eval	function_pointers	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/blob/master/performance/C_Programming_Converted/function_pointers/TestCode.c	\N	\N	\N
289	ccv-performance	eval	make_sentence	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/make_sentence/TestCode.c	\N	\N	\N
290	ccv-performance	eval	function_pointers_array	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/function_pointers_array/TestCode.c	\N	\N	\N
291	ccv-performance	eval	search_file	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/search_file/TestCode.c	\N	\N	\N
292	ccv-performance	eval	vigenere_cipher	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/vigenere_cipher/TestCode.c	\N	\N	\N
293	ccv-performance	eval	bitwise_operators	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/bitwise_operators/TestCode.c	\N	\N	\N
294	ccv-performance	eval	make_paragraph	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/make_paragraph/TestCode.c	\N	\N	\N
295	ccv-performance	eval	c_circularly_linked_lists	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c	\N	\N	\N
296	ccv-performance	eval	c_binary_search_tree	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c	\N	\N	\N
297	ccv-performance	eval	c_doubly_linked_list	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_doubly_linked_list/TestCode.c	\N	\N	\N
298	ccv-performance	eval	matrix	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/matrix/TestCode.c	\N	\N	\N
299	ccv-performance	eval	unpack_string	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/unpack_string/TestCode.c	\N	\N	\N
300	ccv-performance	eval	check_hand	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/check_hand/TestCode.c	\N	\N	\N
301	ccv-performance	eval	write_names_to_file	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/write_names_to_file/TestCode.c	\N	\N	\N
302	ccv-performance	eval	structure_mod	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/structure_mod/TestCode.c	\N	\N	\N
303	ccv-performance	eval	binary_search	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/binary_search/TestCode.c	\N	\N	\N
304	ccv-performance	eval	pack_char_unpackchar	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/pack_char_unpackchar/TestCode.c	\N	\N	\N
305	ccv-performance	eval	quadrilaterals	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/quadrilaterals/TestCode.c	\N	\N	\N
306	ccv-performance	eval	extract_numbers	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/extract_numbers/TestCode.c	\N	\N	\N
307	ccv-performance	eval	vowel_counter	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/vowel_counter/TestCode.c	\N	\N	\N
308	ccv-performance	eval	c_palindrome	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_palindrome/TestCode.c	\N	\N	\N
309	ccv-performance	eval	letter_frequency	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/letter_frequency/TestCode.c	\N	\N	\N
310	ccv-performance	eval	reverse_bits	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/reverse_bits/TestCode.c	\N	\N	\N
311	ccv-performance	eval	bin_hex_string_to_integer	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/bin_hex_string_to_integer/TestCode.c	\N	\N	\N
312	ccv-performance	eval	memory_management	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/memory_management/TestCode.c	\N	\N	\N
313	ccv-performance	eval	c_linked_list	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_linked_list/TestCode.c	\N	\N	\N
314	ccv-performance	eval	string_to_double	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/string_to_double/TestCode.c	\N	\N	\N
315	ccv-performance	eval	check_syntax	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/check_syntax/TestCode.c	\N	\N	\N
316	ccv-performance	eval	pig_latin	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/pig_latin/TestCode.c	\N	\N	\N
317	ccv-performance	eval	general_tree	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/general_tree/TestCode.c	\N	\N	\N
318	ccv-performance	eval	create_use_header_file	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/create_use_header_file/TestCode.c	\N	\N	\N
319	ccv-performance	eval	array_manipulate	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/array_manipulate/TestCode.c	\N	\N	\N
320	ccv-performance	eval	files	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/files/TestCode.c	\N	\N	\N
321	ccv-performance	eval	delete_word	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/delete_word/TestCode.c	\N	\N	\N
322	ccv-performance	eval	writing_a_check_amount	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/writing_a_check_amount/TestCode.c	\N	\N	\N
323	ccv-performance	eval	c_queue	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_queue/TestCode.c	\N	\N	\N
324	ccv-performance	eval	bit_shift	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/bit_shift/testfile.py	\N	\N	\N
822	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
325	ccv-performance	eval	perfect_num	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/perfect_num/testfile.py	\N	\N	\N
326	ccv-performance	eval	change_for_snacks	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/change_for_snacks/testfile.py	\N	\N	\N
327	ccv-performance	eval	python_recursion	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_recursion/testfile.py	\N	\N	\N
328	ccv-performance	eval	packages	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/packages/testfile.py	\N	\N	\N
329	ccv-performance	eval	number_format_in_string	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/number_format_in_string/testfile.py	\N	\N	\N
330	ccv-performance	eval	C_types	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/C_types/testfile.py	\N	\N	\N
331	ccv-performance	eval	parse_command_line	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/parse_command_line/testfile.py	\N	\N	\N
332	ccv-performance	eval	rectangle	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/rectangle/testfile.py	\N	\N	\N
333	ccv-performance	eval	euler_num	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/euler_num/testfile.py	\N	\N	\N
334	ccv-performance	eval	python_stack	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_stack/testfile.py	\N	\N	\N
335	ccv-performance	eval	reverse_string_and_characters	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/reverse_string_and_characters/testfile.py	\N	\N	\N
336	ccv-performance	eval	directory_dictionary	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/directory_dictionary/testfile.py	\N	\N	\N
337	ccv-performance	eval	rational_number	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/rational_number/testfile.py	\N	\N	\N
338	ccv-performance	eval	volume	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/volume/testfile.py	\N	\N	\N
339	ccv-performance	eval	names_to_file	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/names_to_file/testfile.py	\N	\N	\N
340	ccv-performance	eval	pythagorean_triples	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/pythagorean_triples/testfile.py	\N	\N	\N
341	ccv-performance	eval	duplicates	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/duplicates/testfile.py	\N	\N	\N
342	ccv-performance	eval	manipulate_file	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/manipulate_file/testfile.py	\N	\N	\N
343	ccv-performance	eval	computer_list	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/computer_list/testfile.py	\N	\N	\N
344	ccv-performance	eval	python_circularly_linked_lists	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_circularly_linked_lists/testfile.py	\N	\N	\N
345	ccv-performance	eval	critical_sections	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/critical_sections/testfile.py	\N	\N	\N
346	ccv-performance	eval	python_binary_search_tree	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_binary_search_tree/testfile.py	\N	\N	\N
347	ccv-performance	eval	counting_pieces	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/counting_pieces/testfile.py	\N	\N	\N
348	ccv-performance	eval	python_doubly_linked_list	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_doubly_linked_list/testfile.py	\N	\N	\N
349	ccv-performance	eval	state_parks	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/state_parks/testfile.py	\N	\N	\N
350	ccv-performance	eval	html_tags_count	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/html_tags_count/testfile.py	\N	\N	\N
351	ccv-performance	eval	class	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/class/testfile.py	\N	\N	\N
352	ccv-performance	eval	remove_extra_spaces	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/remove_extra_spaces/testfile.py	\N	\N	\N
353	ccv-performance	eval	c_types_rectangle	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/c_types_rectangle/testfile.py	\N	\N	\N
354	ccv-performance	eval	html_search	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/html_search/testfile.py	\N	\N	\N
355	ccv-performance	eval	python_file_tasks	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_file_tasks/testfile.py	\N	\N	\N
356	ccv-performance	eval	shapes	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/shapes/testfile.py	\N	\N	\N
357	ccv-performance	eval	build_matrix	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/build_matrix/testfile.py	\N	\N	\N
358	ccv-performance	eval	python_palindrome	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_palindrome/testfile.py	\N	\N	\N
359	ccv-performance	eval	perfect_squares	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/perfect_squares/testfile.py	\N	\N	\N
360	ccv-performance	eval	exponential_euler	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/exponential_euler/testfile.py	\N	\N	\N
361	ccv-performance	eval	3_card_guts	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/3_card_guts/testfile.py	\N	\N	\N
362	ccv-performance	eval	python_linked_list	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_linked_list/testfile.py	\N	\N	\N
363	ccv-performance	eval	the_sieve_of_eratosthenes	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/the_sieve_of_eratosthenes/testfile.py	\N	\N	\N
364	ccv-performance	eval	binary_to_decimal	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/binary_to_decimal/testfile.py	\N	\N	\N
365	ccv-performance	eval	count_time	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/count_time/testfile.py	\N	\N	\N
366	ccv-performance	eval	IP_port_match	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/IP_port_match/testfile.py	\N	\N	\N
367	ccv-performance	eval	Command_Line	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/Command_Line/testfile.py	\N	\N	\N
368	ccv-performance	eval	eligible	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/eligible/testfile.py	\N	\N	\N
369	ccv-performance	eval	find_whole_words	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/find_whole_words/testfile.py	\N	\N	\N
370	ccv-performance	eval	python_queue	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/python_queue/testfile.py	\N	\N	\N
371	ccv-performance	eval	NOSCHEMABasic_MysteryArg	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt	\N	\N	\N
372	ccv-performance	eval	NOSCHEMABasic_MagicNum	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt	\N	\N	\N
373	ccv-performance	eval	unique_combinations	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/unique_combinations/testfile.py	\N	\N	\N
374	ccv-performance	eval	filter_substring	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/filter_substring/testfile.py	\N	\N	\N
375	ccv-knowledge	eval	c-file-size	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484ad	\N	\N	\N
376	ccv-knowledge	eval	c-thread-id	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484a7	\N	\N	\N
377	ccv-knowledge	eval	c-zero-out-memory	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2888bc9ca2dd424851f	\N	\N	\N
378	ccv-knowledge	eval	networking-CIDR	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248363	\N	\N	\N
379	ccv-knowledge	eval	networking-NAT	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd424831b	\N	\N	\N
380	ccv-knowledge	eval	SEF-modular-design	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd424840b	\N	\N	\N
381	ccv-knowledge	eval	assembly-assembler	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd42482e5	\N	\N	\N
382	ccv-knowledge	eval	assembly-instructions-ret	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2858bc9ca2dd42481e9	\N	\N	\N
383	ccv-performance	eval	Send_Data	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Networking/Send_Data/client.py	\N	\N	\N
384	ccv-performance	eval	Send_Data	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Networking/Send_Data/client.py	\N	\N	\N
385	ccv-performance	eval	tree_stack_sum	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/tree_stack_sum/TestCode.c	\N	\N	\N
386	ccv-knowledge	eval	c-string-functions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2878bc9ca2dd42484c5	\N	\N	\N
387	ccv-knowledge	eval	git-snapshot	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd4248825	\N	\N	\N
388	ccv-knowledge	eval	git-workflow_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487fb	\N	\N	\N
389	ccv-knowledge	eval	git-workflow_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd424880d	\N	\N	\N
390	ccv-knowledge	eval	networking-osi-layer-seven	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248327	\N	\N	\N
391	ccv-knowledge	eval	networking-osi-layer-three	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248369	\N	\N	\N
392	ccv-knowledge	eval	networking-osi-layer-two	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d2868bc9ca2dd4248345	\N	\N	\N
393	ccv-knowledge	eval	python-CTypes	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#5fa0d28a8bc9ca2dd42487c5	\N	\N	\N
394	ccv-performance	eval	git_command_line	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Git/git_command_line/instructons.md	\N	\N	\N
395	ccv-performance	eval	gitlab_issue_creation	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Git/gitlab_issue_creation/instructons.md	\N	\N	\N
396	ccv-performance	eval	debug	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Python/debug/testfile.py	\N	\N	\N
397	ccv-performance	eval	content_storage	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Networking/content_Storage/server.py	\N	\N	\N
398	ccv-performance	eval	send_profile	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Networking/send_profile/client.py	\N	\N	\N
399	ccv-knowledge	eval	networking-serialization_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601307bc26c442c1edfb0677	\N	\N	\N
400	ccv-knowledge	eval	python-serialization_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601307bb26c442c1edfb0676	\N	\N	\N
401	ccv-knowledge	eval	SEF-atomic-functions_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601307bc26c442c1edfb067a	\N	\N	\N
402	ccv-knowledge	eval	SEF-DRY_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601307bc26c442c1edfb0678	\N	\N	\N
403	ccv-knowledge	eval	SEF-SOLID_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601307bc26c442c1edfb0679	\N	\N	\N
404	ccv-knowledge	eval	algorithms-weighted-graph_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601da38145ab1ebbfb3d5051	\N	\N	\N
405	ccv-knowledge	eval	hashing_algorithms_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601c677c866b56b045c37663	\N	\N	\N
406	ccv-knowledge	eval	inorder_sorting	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601c677c866b56b045c37664	\N	\N	\N
407	ccv-knowledge	eval	pre_vs_post_order_traversal	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601c677c866b56b045c37662	\N	\N	\N
408	ccv-knowledge	eval	SEF-procedural_vs_OOP_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#601daa752ba9e5a3fc1f9c99	\N	\N	\N
409	ccv-performance	eval	c_multithreading	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming_Converted/c_multithreading/TestCode.c	\N	\N	\N
410	ccv-knowledge	eval	c_multithreading_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6058d108a97eddece0f7ee1e	\N	\N	\N
411	ccv-knowledge	eval	c_multithreading_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6058d108a97eddece0f7ee1f	\N	\N	\N
412	ccv-knowledge	eval	c_multithreading_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6058d108a97eddece0f7ee20	\N	\N	\N
413	ccv-knowledge	eval	c_multithreading_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6058d108a97eddece0f7ee21	\N	\N	\N
414	ccv-knowledge	eval	c_multithreading_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6058d108a97eddece0f7ee22	\N	\N	\N
415	ccv-performance	eval	double_weighted_graph	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Python/double_weighted_graph/testfile.py	\N	\N	\N
416	ccv-performance	eval	pseudo_shape_stats	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Pseudocode/pseudo_shape_stats/README.md	\N	\N	\N
417	ccv-performance	eval	word_count	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Python/word_count/testfile.py	\N	\N	\N
418	ccv-performance	eval	Encrypted_Data	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Networking/Encrypted_Data/client.py	\N	\N	\N
419	ccv-performance	eval	c_trie	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_trie/TestCode.c	\N	\N	\N
420	ccv-performance	eval	pseudo_journal	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Pseudocode/pseudo_journal/README.md	\N	\N	\N
421	ccv-knowledge	eval	hash_collisions1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#606f7888b1ce02a14e2b9ed2	\N	\N	\N
422	ccv-knowledge	eval	hash_collisions2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6070583d66e542e33639d320	\N	\N	\N
423	ccv-knowledge	eval	hash_collisions3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6070a932716e27d48500d243	\N	\N	\N
424	ccv-knowledge	eval	hash_data_distribution1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6070a932716e27d48500d244	\N	\N	\N
425	ccv-knowledge	eval	hash_data_distribution2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6070a932716e27d48500d245	\N	\N	\N
426	ccv-performance	eval	pseudo_basic_calculator	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/Pseudocode/pseudo_basic_calculator/README.md	\N	\N	\N
427	ccv-performance	eval	c_command_line	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/c_command_line/TestCode.c	\N	\N	\N
428	ccv-knowledge	eval	assembly-logical_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6092d3fa3c403482bfb573d5	\N	\N	\N
429	ccv-knowledge	eval	assembly-stack-frame_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6092d3fa3c403482bfb573d3	\N	\N	\N
430	ccv-knowledge	eval	assembly-string-commands_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6092d3fa3c403482bfb573d4	\N	\N	\N
431	ccv-knowledge	eval	assembly-loop_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6092d3fa3c403482bfb573d7	\N	\N	\N
432	ccv-knowledge	eval	assembly-jump_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#6092d3fa3c403482bfb573d6	\N	\N	\N
433	ccv-knowledge	eval	python_class-attributes	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/CCD/index.html#610c36159fa0fd87c265bf02	\N	\N	\N
434	ccv-performance	eval	RE_VR_ServerGif	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
435	ccv-performance	eval	WinUM_Multithreaded	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
436	ccv-performance	eval	WinUM_UseNatAPIWalkExpTable	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
437	ccv-performance	eval	WinUM_FindCallDLLFunctions	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
438	ccv-performance	eval	EvalDriver	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
439	ccv-performance	eval	WinUM_DLLInject	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
440	ccv-performance	eval	EvalDriverX	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
441	ccv-performance	eval	RE_VR_ApplicationCreateFile	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
442	ccv-performance	eval	WinUM_Winternals	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
443	ccv-knowledge	eval	sccd-w_pe_format_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3aca2	\N	\N	\N
444	ccv-knowledge	eval	sccd-w_system-service-descriptor-table_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3aca8	\N	\N	\N
445	ccv-knowledge	eval	sccd-w_user_mode_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3ac8a	\N	\N	\N
446	ccv-knowledge	eval	sccd-w_windows_native_api_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3ac90	\N	\N	\N
447	ccv-knowledge	eval	sccd-w_windows_registry_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3ac96	\N	\N	\N
448	ccv-knowledge	eval	sccd-w_windows_registry_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3ac9c	\N	\N	\N
449	ccv-knowledge	eval	sccd-w_windows_registry_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/SCCD-W/index.html#601d4d038215996ad3f3acae	\N	\N	\N
450	ccv-performance	eval	CodeReview2	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
451	ccv-performance	eval	CodeReview1	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
452	ccv-performance	eval	Registry1	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Windows.html	\N	\N	\N
453	ccv-knowledge	eval	po-metrics_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f67	\N	\N	\N
454	ccv-knowledge	eval	po-scrum_5	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f6d	\N	\N	\N
455	ccv-knowledge	eval	po-scrum-master_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f73	\N	\N	\N
456	ccv-knowledge	eval	po-product-backlog_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f91	\N	\N	\N
457	ccv-knowledge	eval	po-sprint-backlog_6	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f97	\N	\N	\N
458	ccv-knowledge	eval	po-sprint-planning_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f9d	\N	\N	\N
459	ccv-knowledge	eval	po-responsibilities_14	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fa3	\N	\N	\N
460	ccv-knowledge	eval	po-backlog-refinement_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247faf	\N	\N	\N
461	ccv-knowledge	eval	po-backlog-refinement_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fb5	\N	\N	\N
462	ccv-knowledge	eval	po-product-backlog_12	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fc1	\N	\N	\N
463	ccv-knowledge	eval	po-product-backlog_8	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fc7	\N	\N	\N
464	ccv-knowledge	eval	po-requirements_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fd9	\N	\N	\N
465	ccv-knowledge	eval	po-responsibilities_9	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fdf	\N	\N	\N
466	ccv-knowledge	eval	po-time_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247ff7	\N	\N	\N
467	ccv-knowledge	eval	po-tech-debt_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247ffd	\N	\N	\N
468	ccv-knowledge	eval	po-sprint-scope_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4248003	\N	\N	\N
469	ccv-knowledge	eval	po-responsibilities_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4248009	\N	\N	\N
470	ccv-knowledge	eval	po-increment_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4248021	\N	\N	\N
471	ccv-knowledge	eval	po-responsibilities_7	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4248033	\N	\N	\N
472	ccv-knowledge	eval	po-product-backlog_16	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd424803f	\N	\N	\N
473	ccv-knowledge	eval	po-product-backlog_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4248045	\N	\N	\N
474	ccv-knowledge	eval	po-product-backlog_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424804b	\N	\N	\N
475	ccv-knowledge	eval	po-functional-reqs	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248051	\N	\N	\N
476	ccv-knowledge	eval	po-performance-reqs	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248069	\N	\N	\N
477	ccv-knowledge	eval	po-product-backlog_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248081	\N	\N	\N
478	ccv-knowledge	eval	po-sprint-backlog_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248087	\N	\N	\N
479	ccv-knowledge	eval	po-performance_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424808d	\N	\N	\N
480	ccv-knowledge	eval	po-velocity-vs-value_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248099	\N	\N	\N
481	ccv-knowledge	eval	po-product-backlog_7	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424809f	\N	\N	\N
482	ccv-knowledge	eval	po-testers_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480a5	\N	\N	\N
483	ccv-knowledge	eval	po-responsibilities_8	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480b1	\N	\N	\N
484	ccv-knowledge	eval	po-responsibilities_11	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480c3	\N	\N	\N
485	ccv-knowledge	eval	po-responsibilities_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480d5	\N	\N	\N
486	ccv-knowledge	eval	po-responsibilities_13	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480db	\N	\N	\N
487	ccv-knowledge	eval	po-sprint-retrospective_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480e7	\N	\N	\N
488	ccv-knowledge	eval	po-scrum_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480ed	\N	\N	\N
489	ccv-knowledge	eval	po-responsibilities_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480f3	\N	\N	\N
490	ccv-knowledge	eval	po-dev-team_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480f9	\N	\N	\N
491	ccv-knowledge	eval	po-scrum_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248111	\N	\N	\N
492	ccv-knowledge	eval	po-po-role_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248117	\N	\N	\N
493	ccv-knowledge	eval	po-product-backlog_6	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424811d	\N	\N	\N
494	ccv-knowledge	eval	po-time-management_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248123	\N	\N	\N
495	ccv-knowledge	eval	po-value_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248135	\N	\N	\N
496	ccv-knowledge	eval	po-sprint-backlog_4	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424813b	\N	\N	\N
497	ccv-knowledge	eval	po-product-backlog_9	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248141	\N	\N	\N
498	ccv-knowledge	eval	po-responsibilities_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248147	\N	\N	\N
499	ccv-knowledge	eval	po-responsibilities_15	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424815f	\N	\N	\N
500	ccv-knowledge	eval	po-responsibilities_17	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248165	\N	\N	\N
501	ccv-knowledge	eval	po-tech-debt_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424816b	\N	\N	\N
502	ccv-knowledge	eval	po-timebox_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd424817d	\N	\N	\N
503	ccv-knowledge	eval	po-responsibilities_16	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd4248183	\N	\N	\N
504	ccv-knowledge	eval	po-po-role_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd4248189	\N	\N	\N
505	ccv-knowledge	eval	po-backlog_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd4248195	\N	\N	\N
506	ccv-knowledge	eval	po-responsibilities_10	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd424819b	\N	\N	\N
507	ccv-knowledge	eval	po-stakeholder_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd42481a1	\N	\N	\N
508	ccv-performance	eval	scenarioA-NewProject	https://90cos.gitlab.io/cyv/admin/index.html#product-owner-po-material	\N	\N	\N
509	ccv-knowledge	eval	po-agile-planning	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480e1	\N	\N	\N
510	ccv-knowledge	eval	po-agile-principles	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fcd	\N	\N	\N
511	ccv-knowledge	eval	po-agile-scrum	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd424801b	\N	\N	\N
512	ccv-knowledge	eval	po-agile-sprint-ceremony-review	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4248027	\N	\N	\N
513	ccv-knowledge	eval	po-agile-sprint-purpose	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2858bc9ca2dd424818f	\N	\N	\N
514	ccv-knowledge	eval	po-agile-commander	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424810b	\N	\N	\N
515	ccv-knowledge	eval	po-agile-stand-ups	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248129	\N	\N	\N
516	ccv-knowledge	eval	po-agile-goal	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248105	\N	\N	\N
517	ccv-knowledge	eval	po-agile-work-performed	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248093	\N	\N	\N
518	ccv-knowledge	eval	po-agile-scrum-roles	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd424802d	\N	\N	\N
519	ccv-knowledge	eval	po-agile-backlog-ordering	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424806f	\N	\N	\N
520	ccv-knowledge	eval	po-agile-cannot-complete	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424807b	\N	\N	\N
521	ccv-knowledge	eval	po-agile-ceremonies	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f85	\N	\N	\N
522	ccv-knowledge	eval	po-agile-estimates	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f79	\N	\N	\N
523	ccv-knowledge	eval	po-scrum-pillars	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f8b	\N	\N	\N
524	ccv-knowledge	eval	po-agile-stakeholder-grps	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fd3	\N	\N	\N
525	ccv-knowledge	eval	po-agile-decision-making	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd424812f	\N	\N	\N
526	ccv-knowledge	eval	po-agile-technical-debt	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd42480c9	\N	\N	\N
527	ccv-knowledge	eval	po-agile-external-organizations	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247f7f	\N	\N	\N
528	ccv-knowledge	eval	po-agile-values	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2848bc9ca2dd4248057	\N	\N	\N
529	ccv-knowledge	eval	po-agile-front-door	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fa0d2838bc9ca2dd4247fe5	\N	\N	\N
530	ccv-knowledge	eval	agile-project-charter-purpose	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc66200d	\N	\N	\N
531	ccv-knowledge	eval	agile-project-charter-description	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661fe5	\N	\N	\N
532	ccv-knowledge	eval	agile-project-charter-description-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661ff6	\N	\N	\N
533	ccv-knowledge	eval	agile-project-charter-owner	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc662007	\N	\N	\N
534	ccv-knowledge	eval	agile-team-working-agreement-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661fdf	\N	\N	\N
535	ccv-knowledge	eval	agile-team-working-agreement-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc66204a	\N	\N	\N
536	ccv-knowledge	eval	agile-stakeholders	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc662028	\N	\N	\N
537	ccv-knowledge	eval	agile-epic	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661fce	\N	\N	\N
538	ccv-knowledge	eval	agile-user-story	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fd28f03ab0bf630da43b026	\N	\N	\N
539	ccv-knowledge	eval	agile-backlog-grooming	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661ff0	\N	\N	\N
540	ccv-knowledge	eval	agile-definition-done	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661fd4	\N	\N	\N
541	ccv-knowledge	eval	agile-acceptance-criteria	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc66203f	\N	\N	\N
542	ccv-knowledge	eval	agile-kickoff-meeting-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc662018	\N	\N	\N
543	ccv-knowledge	eval	agile-kickoff-meeting-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661fc8	\N	\N	\N
544	ccv-knowledge	eval	agile-road-map	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fd28f03ab0bf630da43b025	\N	\N	\N
545	ccv-knowledge	eval	agile-sprint-planning-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fd28f03ab0bf630da43b023	\N	\N	\N
546	ccv-knowledge	eval	agile-sprint-retro-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fd28f03ab0bf630da43b024	\N	\N	\N
547	ccv-knowledge	eval	agile-sprint-retro-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc66202e	\N	\N	\N
548	ccv-knowledge	eval	agile-sprint-review-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc661fbd	\N	\N	\N
549	ccv-knowledge	eval	agile-sprint-retro-3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#5fc8458f0183a1e0fc662034	\N	\N	\N
550	ccv-knowledge	eval	po-scrum-artifacts-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb7318f	\N	\N	\N
551	ccv-knowledge	eval	po-scrum-artifacts-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb7318d	\N	\N	\N
660	IQT	training	functions	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/index.html	\N	\N	1
552	ccv-knowledge	eval	po-scrum-cross-functional	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73194	\N	\N	\N
553	ccv-knowledge	eval	po-scrum-daily-scrum-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73192	\N	\N	\N
554	ccv-knowledge	eval	po-scrum-daily-scrum-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73188	\N	\N	\N
555	ccv-knowledge	eval	po-scrum-definition-done-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73191	\N	\N	\N
556	ccv-knowledge	eval	po-scrum-events-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73193	\N	\N	\N
557	ccv-knowledge	eval	po-scrum-product-goal-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb7318e	\N	\N	\N
558	ccv-knowledge	eval	po-scrum-product-goal-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb7318a	\N	\N	\N
559	ccv-knowledge	eval	po-scrum-refinement-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73195	\N	\N	\N
560	ccv-knowledge	eval	po-scrum-scrum-master-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73190	\N	\N	\N
561	ccv-knowledge	eval	po-scrum-sprint-1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb7318c	\N	\N	\N
562	ccv-knowledge	eval	po-scrum-sprint-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb7318b	\N	\N	\N
563	ccv-knowledge	eval	po-scrum-sprint-3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#601d59d35cae6751ffb73189	\N	\N	\N
564	ccv-performance	eval	scenarioB_MTTL_Project	https://90cos.gitlab.io/cyv/admin/index.html#product-owner-po-material	\N	\N	\N
565	ccv-knowledge	eval	po-dev-backlog-pitch	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19647	\N	\N	\N
566	ccv-knowledge	eval	po-dev-devCrew-tasking	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19643	\N	\N	\N
567	ccv-knowledge	eval	po-dev-devCrew	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19648	\N	\N	\N
568	ccv-knowledge	eval	po-dev-execution-pitch	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265fdfe70fec883c0577bc	\N	\N	\N
569	ccv-knowledge	eval	po-dev-field-product	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19645	\N	\N	\N
570	ccv-knowledge	eval	po-dev-phases_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19646	\N	\N	\N
571	ccv-knowledge	eval	po-dev-phases_2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c1964a	\N	\N	\N
572	ccv-knowledge	eval	po-dev-phases_3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19649	\N	\N	\N
573	ccv-knowledge	eval	po-dev-proj-retro	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c1964c	\N	\N	\N
574	ccv-knowledge	eval	po-dev-project-artifacts	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c1964d	\N	\N	\N
575	ccv-knowledge	eval	po-dev-roles_1	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c19644	\N	\N	\N
576	ccv-knowledge	eval	po-dev-sprint-0	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#61265a8d1a55157036c1964b	\N	\N	\N
577	ccv-knowledge	eval	po-agile-INVEST	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726f4	\N	\N	\N
578	ccv-knowledge	eval	po-agile-kanban	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726f5	\N	\N	\N
579	ccv-knowledge	eval	po-agile-user-story-2	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726f3	\N	\N	\N
580	ccv-knowledge	eval	po-dev-CICD	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726e8	\N	\N	\N
581	ccv-knowledge	eval	po-dev-feature-complete	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726e9	\N	\N	\N
582	ccv-knowledge	eval	po-dev-primary-stakeholder	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726e7	\N	\N	\N
583	ccv-knowledge	eval	po-dev-required-testing	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726ea	\N	\N	\N
584	ccv-knowledge	eval	po-dev-rtm	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726eb	\N	\N	\N
585	ccv-knowledge	eval	po-scrum-capacity	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726f2	\N	\N	\N
586	ccv-knowledge	eval	po-scrum-daily-scrum	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726ec	\N	\N	\N
587	ccv-knowledge	eval	po-scrum-def-ready	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726ee	\N	\N	\N
588	ccv-knowledge	eval	po-scrum-effectiveness	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726ed	\N	\N	\N
589	ccv-knowledge	eval	po-scrum-product-goal-3	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726f1	\N	\N	\N
590	ccv-knowledge	eval	po-scrum-retrospective	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726f0	\N	\N	\N
591	ccv-knowledge	eval	po-scrum-voting	https://90cos.gitlab.io/cyv/knowledge-test-banks/test-banks/PO/index.html#615b07a8805322e9fb2726ef	\N	\N	\N
592	ccv-performance	eval	ojt	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/review.html	\N	\N	\N
593	ccv-performance	eval	linux_kernel_pid_translate	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Linux.html	\N	\N	\N
594	ccv-performance	eval	linux_elf_parsing	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Linux.html	\N	\N	\N
595	ccv-performance	eval	linux_vr1	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Linux.html	\N	\N	\N
596	ccv-performance	eval	linux_userspace_multithreaded2	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Linux.html	\N	\N	\N
597	ccv-performance	eval	linux_userspace_looks_for_file	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Linux.html	\N	\N	\N
598	ccv-performance	eval	linux_kernel_keylogger	https://90cos.gitlab.io/cyv/admin/Exam-Handouts/CCD-Senior/Handout-Perform-Senior-Linux.html	\N	\N	\N
599	ccv-performance	eval	linux_midi_parser	https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/performance/C_Programming/linux_midi_parser/TestCode.c	\N	\N	\N
600	IQT	training	ch4-overloading-and-templates	https://39iosdev.gitlab.io/ccd-iqt/idf/cpp-programming/ch4-overloading-and-templates.html	\N	\N	\N
601	IQT	training	object-oriented-programming-c++	https://39iosdev.gitlab.io/ccd-iqt/idf/cpp-programming/object-oriented-programming-c++.html	\N	\N	1
602	IQT	training	c++-standard-template-library	https://39iosdev.gitlab.io/ccd-iqt/idf/cpp-programming/c++-standard-template-library.html	\N	\N	1
603	IQT	training	Pointers_Arrays	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Pointers_Arrays/index.html	\N	\N	1
604	IQT	training	Control_flow	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Control_flow/index	\N	\N	1
605	IQT	training	Control_flow	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Control_flow/index.html	\N	\N	1
606	IQT	training	Multi_Threading	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Multi_Threading/Intro_to_Threads.html	\N	\N	1
607	IQT	training	Functions	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Functions	\N	\N	1
608	IQT	training	Functions	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Functions/index.html	\N	\N	1
609	IQT	training	Functions	https://www.geeksforgeeks.org/pure-functions/	\N	\N	1
610	IQT	training	Functions	https://www.programiz.com/c-programming/list-all-keywords-c-language	\N	\N	1
611	IQT	training	C_compiler	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/C_compiler/index.html	\N	\N	1
612	IQT	training	C_compiler	https://upload.wikimedia.org/wikipedia/commons/1/1b/Portable_Executable_32_bit_Structure_in_SVG_fixed.svg	\N	\N	1
613	IQT	training	C_compiler	https://upload.wikimedia.org/wikipedia/commons/e/e4/ELF_Executable_and_Linkable_Format_diagram_by_Ange_Albertini.png	\N	\N	1
614	IQT	training	C_compiler	https://resources.infosecinstitute.com/complete-tour-of-pe-and-elf-structure/	\N	\N	1
615	IQT	training	Introduction	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction/index.html	\N	\N	1
616	IQT	training	Introduction	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction/gcc.html?highlight=man,pa#man-pages	\N	\N	1
617	IQT	training	Introduction	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction	\N	\N	1
618	IQT	training	Introduction	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction/Definitions.html?highlight=Integrated,Development,Environment#definitions	\N	\N	1
619	IQT	training	Introduction	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Introduction/Documentation.html	\N	\N	1
620	IQT	training	Structs	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Structs/index.html	\N	\N	1
621	IQT	training	Structs	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Structs/structs.html?highlight=style#coding-style-guide	\N	\N	1
622	IQT	training	Structs	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Structs/linked_lists.html	\N	\N	1
623	IQT	training	Array_Strings	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Arrays_strings/index.html	\N	\N	1
624	IQT	training	Variables	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Variables/index.html	\N	\N	1
625	IQT	training	C-Debugging	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Debugging/index.html	\N	\N	1
626	IQT	training	Bitwise_operators	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Bitwise_operators/index.html	\N	\N	1
627	IQT	training	Data_Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming	\N	\N	1
628	IQT	training	Data_Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Data_Structures/Graphs/slides/#/2	\N	\N	1
629	IQT	training	Data_Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Structs/struct_format.html?highlight=structs#format	\N	\N	1
630	IQT	training	IO_part_2	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_2/index.html	\N	\N	1
631	IQT	training	IO_part_2	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_2/index.html?highlight=dry#recommendations	\N	\N	1
632	IQT	training	Error_handling	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Error_handling/index.html	\N	\N	1
633	IQT	training	Error_handling	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Error_handling/Math_functions.html	\N	\N	1
634	IQT	training	Error_handling	http://www.cs.iit.edu/~cs561/cs115/looping/off-by-one.html	\N	\N	1
635	IQT	training	Error_handling	https://www.gnu.org/software/gnulib/manual/html_node/Safe-Allocation-Macros.html	\N	\N	1
636	IQT	training	Memory_Management	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Memory_Management/index.html	\N	\N	1
637	IQT	training	Memory_Management	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Memory_Management/performance_labs/Valgrind_tool.html	\N	\N	1
638	IQT	training	intro-to-sockets	https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/intro-to-sockets/index.html	\N	\N	1
639	IQT	training	advanced-functionality	https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/advanced-functionality/index.html	\N	\N	1
640	IQT	training	Sockets	https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/advanced-functionality/index.html	\N	\N	1
641	IQT	training	intro-to-networking	https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/intro-to-networking/index.html	\N	\N	1
642	IQT	training	intro-to-networking	https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/intro-to-sockets/bsd-socket-api.html	\N	\N	1
643	IQT	training	Python_Networking	https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/osi-layer-2/lab-2-1.html	\N	\N	1
644	IQT	training	Intro-to-algorithms	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/index.html	\N	\N	1
645	IQT	training	Advanced	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/index.html	\N	\N	1
646	IQT	training	Advanced	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/additional_libraries_modules.html?highlight=serial#paramiko-pip-and-the-hitchhikers-guide	\N	\N	1
647	IQT	training	Advanced	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/additional_libraries_modules.html?highlight=json#json	\N	\N	1
648	IQT	training	Advanced	https://medium.com/@gauravkumarindia/synchronization-and-race-conditions-with-multi-threading-in-python-2d35930bb61e	\N	\N	1
649	IQT	training	Advanced	https://codewithoutrules.com/2017/08/16/concurrency-python/	\N	\N	1
650	IQT	training	Advanced	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/designpatterns.html	\N	\N	1
651	IQT	training	Advanced	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/virtualenv.html	\N	\N	1
652	IQT	training	python_features	https://39iosdev.gitlab.io/ccd-iqt/idf/python/python_features/index.html	\N	\N	1
653	IQT	training	Data_Types	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/index.html	\N	\N	1
654	IQT	training	Algorithms	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/	\N	\N	1
655	IQT	training	Algorithms	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/index.html	\N	\N	1
656	IQT	training	Algorithms	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms	\N	\N	1
657	IQT	training	flow_control	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/index.html	\N	\N	1
658	IQT	training	flow_control	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/	\N	\N	1
661	IQT	training	Intro_to_ASM	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/index.html	\N	\N	1
662	IQT	training	Intro_to_ASM	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Debugging_ASM_pt1.html	\N	\N	1
663	IQT	training	Intro_to_ASM	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html	\N	\N	1
664	IQT	training	Intro_to_ASM	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Data_Types.html	\N	\N	1
665	IQT	training	Intro_to_ASM	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Adv_types.html	\N	\N	1
666	IQT	training	ASM_Control_flow	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/index.html	\N	\N	1
667	IQT	training	ASM_Control_flow	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Strings_Calls.html	\N	\N	1
668	IQT	training	ASM_Control_flow	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Control_Flow.html	\N	\N	1
669	IQT	training	ASM_Control_flow	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Calls.html	\N	\N	1
670	IQT	training	ASM_basic_ops	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/index.html	\N	\N	1
671	IQT	training	ASM_basic_ops	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/The_Stack.html	\N	\N	1
672	IQT	training	ASM_basic_ops	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html	\N	\N	1
673	IQT	training	Input_Validation	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=Input%20validation	\N	\N	1
674	IQT	training	Checkpoints	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=loop%20structure	\N	\N	1
675	IQT	training	Checkpoints	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=structures	\N	\N	1
676	IQT	training	Checkpoints	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/Intro_to_Programming/index.html	\N	\N	1
677	IQT	training	Decisions	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=structures	\N	\N	1
678	IQT	training	Decisions	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/Labs/PseudoLab4.html	\N	\N	1
679	IQT	training	Pseudocode_Functions	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab5.md	\N	\N	1
680	IQT	training	Pseudocode_Functions	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/labs.md#pseudocode-activity-6	\N	\N	1
681	IQT	training	Pseudocode_Functions	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=added%20modules	\N	\N	1
682	IQT	training	Pseudocode_Functions	https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/index.html?search=functions	\N	\N	1
683	IQT	training	Pseudocode_Functions	https://www.freecodecamp.org/news/how-recursion-works-explained-with-flowcharts-and-a-video-de61f40cb7f9/	\N	\N	1
684	IQT	training	Pseudocode_Functions	https://www.techiedelight.com/reverse-a-string-using-recursion/	\N	\N	1
685	IQT	training	Operators_expressions	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Operators_expressions/index.html	\N	\N	1
686	IQT	training	IO_Part_1	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_1/index.html	\N	\N	1
687	IQT	training	IO_Part_1	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_1/string-io.html	\N	\N	1
688	IQT	training	IO_Part_1	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/IO_part_1/Buffer_Overflow.html?highlight=buffer#buffer-overflow-attacks	\N	\N	1
689	IQT	training	Preprocessor	https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Preprocessor/index.html	\N	\N	1
690	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/01_Introduction.html	\N	\N	1
691	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/06_version_control.html	\N	\N	1
692	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/05_branching.html	\N	\N	1
693	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/04_creating_a_repo.html	\N	\N	1
694	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/slides/#/8	\N	\N	1
695	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/git_configuration_files.html	\N	\N	1
696	IQT	training	introduction-to-git	https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/gitlab_cicd_intro.html	\N	\N	1
697	IQT	training	introduction-to-git	https://docs.gitlab.com/ee/user/project/issues/	\N	\N	1
698	IQT	training	agile	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/howtoaskquestions/Howtoaskquestions.html	\N	\N	1
699	IQT	training	agile	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	1
700	IQT	training	agile	https://www.romanpichler.com/blog/the-product-backlog-refinement-steps	\N	\N	1
701	IQT	training	agile	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/	\N	\N	1
702	IQT	training	agile	https://www.scrum.org/resources/what-is-a-daily-scrum	\N	\N	1
703	IQT	training	agile	https://www.simform.com/functional-testing-types/	\N	\N	1
704	IQT	training	agile	https://www.mountaingoatsoftware.com/agile/user-stories	\N	\N	1
705	IQT	training	Debugging	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	1
706	IQT	training	ASM_SystemCalls	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html	\N	\N	1
707	IQT	training	ASM_SystemCalls	https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg	\N	\N	1
708	IQT	training	cryptography	https://cryptopals.com/	\N	\N	1
709	IQT	training	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Lists_Pointer_Structures.html	\N	\N	1
710	IQT	training	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Singly_Linked_List.html	\N	\N	1
711	IQT	training	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Doubly_Linked_List.html	\N	\N	1
712	IQT	training	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Stacks_Lesson.html	\N	\N	1
713	IQT	training	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Queue_Lesson.html	\N	\N	1
714	IQT	training	Data Structures	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Trees_Lesson.html	\N	\N	1
715	commercial	training	Product-Purpose	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	\N
716	commercial	training	Stakeholder-Interactions	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	2
717	commercial	training	Stakeholder-Interactions	https://www.scrum.org/courses/professional-scrum-product-owner-training	\N	\N	2
719	commercial	training	Product-Owner-Role	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	2
721	commercial	training	General-Scrum	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	2
724	commercial	training	Product-Backlog-Management	https://www.scrum.org/courses/professional-scrum-product-owner-training	\N	\N	2
725	commercial	training	Python	https://www.mantech.com/advanced-cyber-training-program	\N	\N	\N
726	commercial	training	Kernel	https://www.mantech.com/advanced-cyber-training-program	\N	\N	3
727	commercial	training	VR	https://www.mantech.com/advanced-cyber-training-program	\N	\N	3
728	commercial	training	Reverse-Engineering	https://www.mantech.com/advanced-cyber-training-program	\N	\N	3
729	commercial	training	Networking	https://www.mantech.com/advanced-cyber-training-program	\N	\N	3
730	commercial	training	Operating-System	https://www.mantech.com/advanced-cyber-training-program	\N	\N	3
731	commercial	training	Assembly	https://www.mantech.com/advanced-cyber-training-program	\N	\N	3
732	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
733	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
734	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
735	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
736	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
737	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
738	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
739	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
740	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
741	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
742	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
743	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
744	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
745	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
746	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
747	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
748	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
749	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
750	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
751	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
752	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
753	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
754	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
755	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
756	internal	training_supplementary	SEE	https://90cos.gitlab.io/cyv/admin/roles-and-responsibility/SEE/training.html	see_training		\N
757	internal	training_supplementary	PO	https://confluence.90cos.cdl.af.mil/display/ACoE/Agile+Fundamentals+Course	Agile Fundamentals Course	Agile introduction on Confluence	\N
758	internal	training_supplementary	PO	https://confluence.90cos.cdl.af.mil/display/ACoE/Agile+Fundamentals+Course	Agile Fundamentals Course	Agile introduction on Confluence	\N
759	internal	training_supplementary	PO	https://confluence.90cos.cdl.af.mil/display/ACoE/Agile+Fundamentals+Course	Agile Fundamentals Course	Agile introduction on Confluence	\N
760	internal	training_supplementary	PO	https://confluence.90cos.cdl.af.mil/display/ACoE/Agile+Fundamentals+Course	Agile Fundamentals Course	Agile introduction on Confluence	\N
761	internal	training_supplementary	PO	https://confluence.90cos.cdl.af.mil/display/ACoE/Agile+Fundamentals+Course	Agile Fundamentals Course	Agile introduction on Confluence	\N
762	self-study	training_supplementary	PO	https://www.atlassian.com/agile/tutorials/creating-your-agile-board	Learn how to create agile boards in Jira Software	Step-by-step instructions on how to work with agile boards in Jira Software	\N
763	internal	training_supplementary	PO	https://confluence.90cos.cdl.af.mil/display/ACoE/Agile+Fundamentals+Course?preview=/20349943/20355872/AgileFundamentals_02.pptx	Agile Fundamentals Course - The Product Backlog	Agile introduction on Confluence to the Product Backlog	\N
764	self-study	training_supplementary	PO	https://support.atlassian.com/jira-software-cloud/docs/configure-estimation-and-tracking	JIRA Software Support - Configure estimation and tracking	You can customize your estimation statistic (story points, time, or issue count, for example) and time tracking settings (remaining time estimate) to suit your project	\N
765	self-study	training_supplementary	PO	https://scrummate.com/difference-between-task-and-user-story	Scrum Mate - The difference between task and user story	The key to productive work is not working on more tasks, but selecting the right ones. The question is, how do we recognize the most appropriate tasks? Let's have a look at how we can achieve this.	\N
766	self-study	training_supplementary	PO	https://www.scrumstudy.com/whyscrum/scrum-principles	ScrumStudy - Scrum Principles	Scrum principles are the core guidelines for applying the Scrum framework and should mandatorily be used in all Scrum projects. They are non-negotiable and must be applied as specified in the SBOK®. Keeping the principles intact and using them appropriately instills confidence in the Scrum framework with regard to attaining the objectives of the project.	\N
767	self-study	training_supplementary	PO	https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/	Agile Alliance - 12 Principles Behind the Agile Manifesto	Guiding practices that support teams in implementing and executing with agility	\N
820	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
722	commercial	training	Unknowns-Require-Validation	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	\N
723	commercial	training	Product-Backlog-Management	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	\N
768	self-study	training_supplementary	PO	https://www.atlassian.com/agile/project-management/epics-stories-themes	Atlassian Agile Coach - Stories, epics, and initiatives	These simple structures help agile teams gracefully manage scope and structure work	\N
769	self-study	training_supplementary	PO	https://www.agile-academy.com/en/agile-dictionary/working-agreements	Agile Acadamy - Working Agreements	Dictionary of Agile terms	\N
770	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
771	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
772	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
773	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
774	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
775	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
776	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
777	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
778	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
779	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
780	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
781	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
782	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
783	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
784	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
785	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
786	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
787	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
788	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
789	self-study	training_supplementary	PO	https://scrumguides.org/scrum-guide.html	Scrum Guides - The 2020 Scrum Guide	The 2020 Scrum Guide	\N
790	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
791	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
792	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
793	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
794	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
795	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
796	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
797	self-study	training_supplementary	PO	https://www.scrum.org/courses/professional-scrum-product-owner-training	Scrum.org - Learn Professional Product Ownership	Learn Professional Product Ownership	\N
798	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/48-advanced-level-test-automation-engineer-documents/201-advanced-test-automation-engineer-syllabus-ga-2016.html	ISTQB Advanced Test Automation		\N
799	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/48-advanced-level-test-automation-engineer-documents/201-advanced-test-automation-engineer-syllabus-ga-2016.html	ISTQB Advanced Test Automation		\N
800	commercial	training_supplementary	TAE	https://www.istqb.org/certification-path-root/advanced-level/advanced-level-test-manager.html	ISTQB Advanced Test Management		\N
801	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/2-foundation-level-documents/281-istqb-ctfl-syllabus-2018-v3-1.html	ISTQB Foundation Level Tester		\N
802	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/2-foundation-level-documents/281-istqb-ctfl-syllabus-2018-v3-1.html	ISTQB Foundation Level Tester		\N
803	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/7-advanced-level-documents/302-advanced-level-syllabus-2019-technical-test-analyst.html	ISTQB Advenced Technical Tester		\N
804	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/7-advanced-level-documents/302-advanced-level-syllabus-2019-technical-test-analyst.html	ISTQB Advenced Technical Tester		\N
805	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/7-advanced-level-documents/302-advanced-level-syllabus-2019-technical-test-analyst.html	ISTQB Advenced Technical Tester		\N
806	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/send/7-advanced-level-documents/302-advanced-level-syllabus-2019-technical-test-analyst.html	ISTQB Advenced Technical Tester		\N
807	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/category/65-advanced-level-agile-technical-tester.html	ISTQB Advenced Agile Tester		\N
808	commercial	training_supplementary	TAE	https://www.istqb.org/downloads/category/65-advanced-level-agile-technical-tester.html	ISTQB Advenced Agile Tester		\N
809	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
810	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
811	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
812	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
813	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
814	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
815	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
816	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
817	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
818	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
819	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
823	commercial	training_supplementary	TAE	https://www.istqb.org/	47th Course		\N
824	commercial	training_supplementary	TAE	https://docs.gitlab.com/ee/ci/docker/using_docker_images.html	Containers in GitLab		\N
825	commercial	training_supplementary	TAE	https://docs.gitlab.com/ee/ci/docker/using_docker_images.html	Containers in GitLab		\N
826	commercial	training_supplementary	TAE	https://docs.gitlab.com/ee/ci/caching/	Containers in GitLab		\N
827	commercial	training_supplementary	TAE	https://docs.gitlab.com/ee/ci/caching/	Containers in GitLab		\N
828	commercial	training_supplementary	TAE	https://docker-curriculum.com/#introduction	Docker Intro		\N
829	commercial	training_supplementary	TAE	https://docker-curriculum.com/#introduction	Docker Intro		\N
830	commercial	training_supplementary	TAE	https://docker-curriculum.com/#introduction	Docker Intro		\N
831	commercial	training_supplementary	TAE	https://docker-curriculum.com/#introduction	Docker Intro		\N
832	commercial	training_supplementary	TAE	https://docker-curriculum.com/#introduction	Docker Intro		\N
833	commercial	training_supplementary	TAE	https://docker-curriculum.com/#hello-world	Docker Hello World		\N
834	commercial	training_supplementary	TAE	https://docker-curriculum.com/#hello-world	Docker Hello World		\N
835	commercial	training_supplementary	TAE	https://docker-curriculum.com/#hello-world	Docker Hello World		\N
836	commercial	training_supplementary	TAE	https://docker-curriculum.com/#hello-world	Docker Hello World		\N
837	commercial	training_supplementary	TAE	https://docker-curriculum.com/#hello-world	Docker Hello World		\N
838	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
839	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
840	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
841	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
842	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
843	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
844	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
845	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
846	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
847	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
848	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
849	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
850	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
851	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
852	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
853	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
854	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
855	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
856	commercial	training_supplementary	TAE	https://docker-curriculum.com/#webapps-with-docker	Docker Webapps		\N
857	commercial	training_supplementary	TAE	https://stackoverflow.com/questions/23735149/what-is-the-difference-between-a-docker-image-and-a-container	Docker Webapps		\N
858	commercial	training_supplementary	TAE	https://docker-curriculum.com/#multi-container-environments	Multi-Container Environments		\N
859	commercial	training_supplementary	TAE	https://docker-curriculum.com/#multi-container-environments	Multi-Container Environments		\N
860	commercial	training_supplementary	TAE	https://docs.docker.com/engine/swarm/	Multi-Container Environments		\N
861	commercial	training_supplementary	TAE	https://nickjanetakis.com/blog/docker-tip-2-the-difference-between-copy-and-add-in-a-dockerile	Multi-Container Environments		\N
862	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
863	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
864	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
865	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
866	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
867	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
868	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
869	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
870	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
871	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
872	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
873	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
874	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
875	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
876	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
877	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/builder/	Dockerfile Reference		\N
878	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/images/	Docker CLI Documentation		\N
879	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/ps/	Docker CLI Documentation		\N
880	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/login/	Docker CLI Documentation		\N
881	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/pull/	Docker CLI Documentation		\N
882	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/push/	Docker CLI Documentation		\N
883	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/rm/	Docker CLI Documentation		\N
884	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/rmi/	Docker CLI Documentation		\N
885	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/tag/	Docker CLI Documentation		\N
886	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/exec/	Docker CLI Documentation		\N
887	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/exec/#examples	Docker CLI Documentation		\N
888	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/save/	Docker CLI Documentation		\N
889	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/load/	Docker CLI Documentation		\N
890	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/history/	Docker CLI Documentation		\N
891	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/commit/	Docker CLI Documentation		\N
892	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#name---name	Docker CLI Documentation		\N
893	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#clean-up---rm	Docker CLI Documentation		\N
894	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#volume-shared-filesystems	Docker CLI Documentation		\N
895	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#user	Docker CLI Documentation		\N
896	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities	Docker CLI Documentation		\N
897	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#env-environment-variables	Docker CLI Documentation		\N
898	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#expose-incoming-ports	Docker CLI Documentation		\N
899	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources	Docker CLI Documentation		\N
900	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/run/#network-settings	Docker CLI Documentation		\N
901	commercial	training_supplementary	TAE	https://docs.docker.com/engine/reference/commandline/dockerd/	Dockerd Documentation		\N
902	commercial	training_supplementary	TAE	https://docs.docker.com/config/daemon/systemd/	Dockerd Documentation		\N
903	commercial	training_supplementary	TAE	https://www.docker.com/blog/what-is-containerd-runtime/	Dockerd Documentation		\N
904	commercial	training_supplementary	TAE	https://medium.com/@alenkacz/whats-the-difference-between-runc-containerd-docker-3fc8f79d4d6e	Dockerd Documentation		\N
905	commercial	training_supplementary	TAE	https://docs.docker.com/get-started/	Docker Introduction		\N
906	commercial	training_supplementary	TAE	https://docs.docker.com/get-started/	Docker Introduction		\N
907	commercial	training_supplementary	TAE	https://docs.docker.com/get-started/	Docker Introduction		\N
908	commercial	training_supplementary	TAE	https://docs.docker.com/get-started/	Docker Introduction		\N
909	commercial	training_supplementary	TAE	https://docs.docker.com/get-started/	Docker Introduction		\N
910	commercial	training_supplementary	TAE	https://docs.docker.com/get-started/	Docker Introduction		\N
911	commercial	training_supplementary	TAE	https://www.linode.com/docs/applications/containers/when-and-why-to-use-docker/	Docker Introduction		\N
912	commercial	training_supplementary	TAE	https://docs.docker.com/engine/security/security/	Docker Introduction		\N
913	commercial	training_supplementary	TAE	https://cloud.google.com/solutions/best-practices-for-building-containers	Docker Introduction		\N
914	self-study	training_supplementary	programmer	https://www.futurelearn.com/info/courses/block-to-text-based-programming/0/steps/39492	Introducing Pseudocode		\N
915	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
916	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
917	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
918	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
919	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
920	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
921	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
922	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
923	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
924	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
925	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
926	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
927	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
928	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
929	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
930	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
931	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
932	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
933	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
934	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
935	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
936	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
937	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
938	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
939	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
940	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
941	self-study	training_supplementary	programmer	https://asmtutor.com/	NASM Assembly Language Tutorials	ASM Tutor	\N
942	self-study	training_supplementary	programmer	https://github.com/sysprog21/lkmpg	The Linux Kernel Module Programming Guide		\N
943	self-study	training_supplementary	programmer	https://github.com/sysprog21/lkmpg	The Linux Kernel Module Programming Guide		\N
944	self-study	training_supplementary	programmer	https://github.com/sysprog21/lkmpg	The Linux Kernel Module Programming Guide		\N
945	self-study	training_supplementary	programmer	https://github.com/sysprog21/lkmpg	The Linux Kernel Module Programming Guide		\N
946	self-study	training_supplementary	SCCD-L	https://refspecs.linuxfoundation.org/elf/elf.pdf	ELF Specification: 1.2		\N
947	self-study	training_supplementary	SCCD-L	https://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/	ELF Binaries on Linux Understanding and Analysis		\N
948	self-study	training_supplementary	SCCD-L	http://www.skyfree.org/linux/references/ELF_Format.pdf	ELF Specification: 1.1		\N
949	self-study	training_supplementary	SCCD-L	https://www.intezer.com/blog/research/executable-linkable-format-101-part1-sections-segments/	ELF Format Introduction (4 Parts)		\N
950	self-study	training_supplementary	Ghidra	https://ghidra.re	Ghidra		\N
951	self-study	training_supplementary	devops	https://www.redhat.com/en/topics/devops/what-is-ci-cd	What is CI/CD?		\N
718	commercial	training	Release-and-Sprint-Planning	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html	\N	\N	\N
720	commercial	training	Product-Owner-Role	https://confluence.90cos.cdl.af.mil/display/SG/%28U%29+AFI17-2DEVV3	\N	\N	\N
952	commercial	training	Agile	https://training.atlassian.com/agile-with-jira	Agile With Jira		\N
953	self-study	training	scrum	https://www.mountaingoatsoftware.com/blog/the-difference-between-a-story-and-a-task	Difference between story and task	The Difference Between a Story and a Task	\N
954	self-study	training	scrum	https://www.scrum.org/forum/scrum-forum/38211/jira-story-subtasks-instead-tasks	Jira Story	 Jira story--> Subtasks instead of task	\N
955	self-study	training	scrum	https://medium.com/agile-it/composing-meaningful-tasks-c1ca51064c1a	Meaningful Tasks	Composing Meaningful Tasks	\N
956	self-study	training	scrum	https://www.mountaingoatsoftware.com/blog/stories-epics-and-themes	Stories, Epics, and Themes	 User Stories, Epics, and Themes	\N
957	self-study	training	scrum	https://www.scrum.org/forum/scrum-forum/39541/how-one-should-deal-epics	Dealing with Epics	 How one should deal with Epics	\N
958	self-study	training	scrum	https://www.agilealliance.org/glossary/epic/	Epic Definition	 Epic Definition	\N
959	self-study	training	scrum	https://www.atlassian.com/agile/project-management/epics	Agile Epics	Agile epics: definition, examples, and templates	\N
960	self-study	training	scrum	https://www.techagilist.com/agile/product-owner/velocity-vs-capacity-in-sprint-planning/	Capacity vs. Velocity	Capacity vs. Velocity	\N
961	self-study	training	scrum	https://agilemanifesto.org/principles.html	Agile Principles	The Agile principles	\N
962	self-study	training	scrum	https://www.atlassian.com/agile/kanban/kanban-vs-scrum	Kanban vs. Scrum	Kanban vs. Scrum	\N
963	internal	training	90 COS PO	https://confluence.90cos.cdl.af.mil/pages/viewpage.action?pageId=11338896	Product Charter	Product Charter	\N
964	internal	training	90 COS PO	https://confluence.90cos.cdl.af.mil/display/9CMDP/%28U%29+Guidance+for+Project+Initiation#id-(U)GuidanceforProjectInitiation-PO-project-charter	Create Project Charter	Create Project Charter	\N
965	self-study	training	Jira	https://support.atlassian.com/jira-software-cloud/docs/create-an-issue-and-a-sub-task/	Create an issue and a sub-task	Create an issue and a sub-task	\N
966	self-study	training	Agile	https://www.scruminc.com/product-goal/	Product Goals	Product goals in scrum	\N
967	self-study	training	Scrum	https://www.wrike.com/project-management-guide/faq/what-is-an-agile-roadmap/	Scrum	Within an Agile team, the Product Owner is often responsible for managing not only the product backlog but defining the project’s vision and priorities.	\N
968	self-study	training	Scrum	https://www.romanpichler.com/blog/10-tips-creating-agile-product-roadmap/	Scrum	10 Tips For Creating An Agile Product Roadmap	\N
969	self-study	training	Scrum	https://resources.scrumalliance.org/Article/10-tips-creating-agile-product-roadmap	Scrum	A product roadmap is a powerful tool to describe how a product is likely to grow...	\N
970	self-study	training	Scrum Commitments	https://age-of-product.com/scrum-commitments/	Scrum Commitments	Tying loose ends and shoehorning the definition of done.	\N
971	self-study	training	The Three Scrum Artifacts and Their Commitments	https://resources.scrumalliance.org/Article/scrum-artifacts	The Three Scrum Artifacts and Their Commitments	Scrum uses three artifacts to help manage work.	\N
972	self-study	training	Commitment	https://scrumguides.org/scrum-guide.html	Commitment	The Definition of Done is a formal description of the state...	\N
\.


--
-- Name: relationship_links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.relationship_links_id_seq', 972, true);


--
-- PostgreSQL database dump complete
--

