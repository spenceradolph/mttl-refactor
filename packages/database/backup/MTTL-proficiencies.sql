--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: proficiencies; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.proficiencies (id, description) FROM stdin;
N	No proficiency
A	An 'A' indicates the individual must be able to identify basic fact         and terms about the subject.
B	A 'B' indicates the individual must be able to identify relationships             of basic facts and state general principles about the subject.
C	A 'C' indicates the individual must be able to analyze facts and             principles and draw conclusions about the subject.
D	A 'D' indicates the individual must be able to evaluate conditions             and make proper decisions about the subject.
1	A '1' indicates the individual must be familiar with this competency             and be generally capable of independently handling simple                  tasks or assignments.
2	A '2' indicates the individual must be capable of independently handling some             complex tasks or assignments related to this competency,                  but may need direction and guidance on others.
3	A '3' indicates the individual must be capable of independently             handling a wide variety of complex and or high profile                 tasks or assignments related to this competency.                     Must be an authority in this area and or often sought                         out by others for advice or to teach/mentor                             others on highly complex or challenging                                 tasks or assignments related to this                                     competency.
\.


--
-- PostgreSQL database dump complete
--

