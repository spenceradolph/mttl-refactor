--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: sequences; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.sequences (id, sequence_name, work_roles_id, objective, description, comments, old_id) FROM stdin;
1	CCD Training Sequence	CCD	Cover all CCD expectations for Python - except for VR and RE related topics	Training sequence based on the  IDF sequence	This sequence and constituent parts should be updated as IDF is revised	ccd_0001
2	Python Training Sequence	CCD	Cover all CCD expectations for Python - except for VR and RE related topics	Training sequence designed for example e.g. the IDF sequence	and example comment could be that this should be updated as IDF is revised	seq_0001
\.


--
-- Name: sequences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.sequences_id_seq', 2, true);


--
-- PostgreSQL database dump complete
--

