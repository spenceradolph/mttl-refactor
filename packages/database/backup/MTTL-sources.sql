--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: sources; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.sources (id, source, owner) FROM stdin;
1	JCT&CS	USCYBERCOM/J7
2	ACCMAN17-2CDOv2	90COS
3	ACC CCD CDD TTL	ACC A3/2/6KO
4	NCWF	
5	90COS/CYK	90COS/CYK
6	ACC/A3/2/6K	90COS/CYT
7	2020 Scrum Guide	90COS
8	Squadron Guidance Memorandum	90COS
9	90COS MADP Guidance	90COS/DO
10	Cyberspace Capability Development Operations and Procedures	90COS/DO
11	AFI17-2DEVV3	
\.


--
-- Name: sources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.sources_id_seq', 11, true);


--
-- PostgreSQL database dump complete
--

