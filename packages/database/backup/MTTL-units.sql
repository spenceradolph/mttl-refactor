--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: units; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.units (id, unit_name, estimated_time, description, objective, comments, old_id) FROM stdin;
1	Agile	180	This is an introduction to Agile	Students will understand the fundamental tenets of Agile and Scrum as used at 90 COS 	Valid for multiple work roles	agile_0001
2	Introduction to Git	180	A basic introduction to GitLab and version control	Students will learn the key elements of Git Lab and version control as used at 90 COS	This foundational knowledge is valid for all work roles. However, the required practice may not be relevant to every work role e.g. Scrum Masters	git_0001
3	Introduction to Python	180	This unit  introduces foundational concepts in Python	Students will learn foundational concepts of Python	The progression here is/was originally derived from the IDF progression to CCD qualification	python_0001
4	Introduction to Assembly	720	This unit introduces foundational, intermediate, and some advanced concepts of Assembly programming	Students will articulate and leverage key concepts of Assembly programming	This is focused on x86(-64) programming. Primarily using Intel syntax with NASM (i.e. Linux) though Windows specifics are presented	asm_0001
5	Intermediate Python	360	This unit builds upon foundational Python concepts and introduces more advanced Python topics	Students will leverage OOP,  advanced datatypes, and extended functionality in Python 	Includes design patterns, virtual environments, Ctypes, and other advanced topics	python_0002
6	(Python) Networking	180	This unit introduces networking concepts primarily with Python	Students will learn the foundations of network programming	The concepts themselves, especially with regards to sockets are not necessarily Python dependent	networking_0001
7	Introduction to Pseudocode	300	This unit introduces the foundations and purposes of pseudocode and related topics.	Students will be able to leverage pseudocode and related tools for appropriate tasks	also includes how to ask a question, how to google, and flow charts	pseudocode_0001
\.


--
-- Name: units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.units_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--

