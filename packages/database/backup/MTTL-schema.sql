CREATE TYPE "networkenum" AS ENUM (
  'Commercial',
  'NIPR'
);

CREATE TYPE "ksattypeenum" AS ENUM (
  'Knowledge',
  'Skill',
  'Ability',
  'Task'
);

CREATE TYPE "rellinktypeenum" AS ENUM (
  'commercial',
  'internal',
  'IDF',
  'IQT',
  'self-study',
  'ccv-performance',
  'ccv-knowledge'
);

CREATE TYPE "rellinkmaptypeenum" AS ENUM (
  'training',
  'eval',
  'training_supplementary'
);

CREATE TABLE "ksats" (
  "id" varchar(5) PRIMARY KEY,
  "type" ksattypeenum NOT NULL,
  "topic_id" int NOT NULL,
  "description" varchar NOT NULL,
  "created_on" timestamp NOT NULL,
  "updated_on" timestamp NOT NULL,
  "comments" varchar,
  "old_id" varchar(24),
  UNIQUE ("topic_id", "description")
);

CREATE TABLE "sources" (
  "id" SERIAL,
  "source" varchar(64) UNIQUE NOT NULL,
  "owner" varchar(64),
  PRIMARY KEY ("id")
);

CREATE TABLE "ksat_to_sources" (
  "id" SERIAL,
  "ksats_id" varchar(5),
  "sources_id" int,
  PRIMARY KEY ("id", "ksats_id", "sources_id")
);

CREATE TABLE "ksat_edges" (
  "id" SERIAL,
  "from_ksats_id" varchar(5),
  "to_ksats_id" varchar(5),
  PRIMARY KEY ("id", "from_ksats_id", "to_ksats_id")
);

CREATE TABLE "proficiencies" (
  "id" varchar(1) PRIMARY KEY,
  "description" text NOT NULL
);

CREATE TABLE "work_roles" (
  "id" varchar(16) PRIMARY KEY,
  "work_role" varchar(64) NOT NULL,
  "description" text NOT NULL
);

CREATE TABLE "ksat_to_work_roles" (
  "ksats_id" varchar(5),
  "work_roles_id" varchar(16),
  "milestones_id" int,
  "proficiencies_id" varchar(1),
  PRIMARY KEY ("ksats_id", "work_roles_id", "milestones_id", "proficiencies_id")
);

CREATE TABLE "milestones" (
  "id" SERIAL PRIMARY KEY,
  "milestone" varchar(64) NOT NULL,
  "description" text
);

CREATE TABLE "relationship_links" (
  "id" SERIAL,
  "type" rellinktypeenum NOT NULL,
  "map_type" rellinkmaptypeenum NOT NULL,
  "topic" varchar NOT NULL,
  "url" varchar NOT NULL,
  "name" varchar(64),
  "description" text,
  "courses_id" int,
  PRIMARY KEY ("id")
);

CREATE TABLE "courses" (
  "id" SERIAL PRIMARY KEY,
  "course" varchar(64) UNIQUE,
  "work_roles_id" varchar(6),
  "network" networkenum NOT NULL
);

CREATE TABLE "ksats_to_relationship_links" (
  "id" SERIAL,
  "relationship_links_id" int NOT NULL,
  "ksats_id" varchar(5) NOT NULL,
  "proficiencies_id" varchar(1) NOT NULL,
  "created_on" timestamp NOT NULL,
  "updated_on" timestamp NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "topics" (
  "id" SERIAL PRIMARY KEY,
  "topic" varchar NOT NULL
);

CREATE TABLE "modules" (
  "id" SERIAL PRIMARY KEY,
  "module_name" varchar NOT NULL,
  "description" varchar NOT NULL,
  "objective" varchar NOT NULL,
  "comments" varchar,
  "old_id" varchar NOT NULL
);

CREATE TABLE "modules_content" (
  "id" SERIAL PRIMARY KEY,
  "modules_id" int NOT NULL,
  "description" varchar NOT NULL,
  "url" varchar NOT NULL
);

CREATE TABLE "modules_performance" (
  "id" SERIAL PRIMARY KEY,
  "modules_id" int NOT NULL,
  "description" varchar NOT NULL,
  "url" varchar NOT NULL
);

CREATE TABLE "modules_assessment" (
  "id" SERIAL PRIMARY KEY,
  "modules_id" int NOT NULL,
  "description" varchar NOT NULL,
  "url" varchar NOT NULL
);

CREATE TABLE "modules_resources" (
  "id" SERIAL PRIMARY KEY,
  "modules_id" int NOT NULL,
  "title" varchar NOT NULL,
  "type" varchar NOT NULL,
  "description" varchar NOT NULL,
  "url" varchar NOT NULL
);

CREATE TABLE "modules_ksats" (
  "id" SERIAL PRIMARY KEY,
  "modules_id" int NOT NULL,
  "ksats_id" varchar(5) NOT NULL,
  "proficiencies_id" varchar(1) NOT NULL
);

CREATE TABLE "units" (
  "id" SERIAL PRIMARY KEY,
  "unit_name" varchar NOT NULL,
  "estimated_time" int NOT NULL,
  "description" varchar NOT NULL,
  "objective" varchar NOT NULL,
  "comments" varchar,
  "old_id" varchar NOT NULL
);

CREATE TABLE "unit_modules" (
  "id" SERIAL PRIMARY KEY,
  "units_id" int NOT NULL,
  "modules_id" int NOT NULL,
  UNIQUE ("units_id", "modules_id")
);

CREATE TABLE "sequences" (
  "id" SERIAL,
  "sequence_name" varchar UNIQUE,
  "work_roles_id" varchar(6),
  "objective" varchar,
  "description" varchar NOT NULL,
  "comments" varchar,
  "old_id" varchar NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "sequence_units" (
  "id" SERIAL,
  "sequences_id" int NOT NULL,
  "units_id" int NOT NULL,
  UNIQUE ("sequences_id", "units_id"),
  PRIMARY KEY ("id")
);	

ALTER TABLE "ksat_to_work_roles" ADD FOREIGN KEY ("work_roles_id") REFERENCES "work_roles" ("id");

ALTER TABLE "ksat_to_work_roles" ADD FOREIGN KEY ("ksats_id") REFERENCES "ksats" ("id") ON DELETE CASCADE;

ALTER TABLE "ksat_to_work_roles" ADD FOREIGN KEY ("proficiencies_id") REFERENCES "proficiencies" ("id");

ALTER TABLE "ksats_to_relationship_links" ADD FOREIGN KEY ("relationship_links_id") REFERENCES "relationship_links" ("id");
	
ALTER TABLE "ksats_to_relationship_links" ADD FOREIGN KEY ("ksats_id") REFERENCES "ksats" ("id") ON DELETE CASCADE;

ALTER TABLE "ksats_to_relationship_links" ADD FOREIGN KEY ("proficiencies_id") REFERENCES "proficiencies" ("id");

ALTER TABLE "relationship_links" ADD FOREIGN KEY ("courses_id") REFERENCES "courses" ("id");

ALTER TABLE "courses" ADD FOREIGN KEY ("work_roles_id") REFERENCES "work_roles" ("id");

ALTER TABLE "ksat_edges" ADD FOREIGN KEY ("from_ksats_id") REFERENCES "ksats" ("id") ON DELETE CASCADE;

ALTER TABLE "ksat_edges" ADD FOREIGN KEY ("to_ksats_id") REFERENCES "ksats" ("id") ON DELETE CASCADE;

ALTER TABLE "modules_content" ADD FOREIGN KEY ("modules_id") REFERENCES "modules" ("id");

ALTER TABLE "modules_performance" ADD FOREIGN KEY ("modules_id") REFERENCES "modules" ("id");

ALTER TABLE "modules_assessment" ADD FOREIGN KEY ("modules_id") REFERENCES "modules" ("id");

ALTER TABLE "modules_resources" ADD FOREIGN KEY ("modules_id") REFERENCES "modules" ("id");

ALTER TABLE "sequences" ADD FOREIGN KEY ("work_roles_id") REFERENCES "work_roles" ("id");

ALTER TABLE "sequence_units" ADD FOREIGN KEY ("sequences_id") REFERENCES "sequences" ("id");

ALTER TABLE "sequence_units" ADD FOREIGN KEY ("units_id") REFERENCES "units" ("id");

ALTER TABLE "unit_modules" ADD FOREIGN KEY ("units_id") REFERENCES "units" ("id");

ALTER TABLE "unit_modules" ADD FOREIGN KEY ("modules_id") REFERENCES "modules" ("id");

ALTER TABLE "modules_ksats" ADD FOREIGN KEY ("modules_id") REFERENCES "modules" ("id");

ALTER TABLE "modules_ksats" ADD FOREIGN KEY ("ksats_id") REFERENCES "ksats" ("id") ON DELETE CASCADE;

ALTER TABLE "ksat_to_work_roles" ADD FOREIGN KEY ("milestones_id") REFERENCES "milestones" ("id");

ALTER TABLE "ksats" ADD FOREIGN KEY ("topic_id") REFERENCES "topics" ("id");

-- ALTER TABLE "sources" ADD FOREIGN KEY ("id") REFERENCES "ksat_to_sources" ("source_id");
ALTER TABLE "ksat_to_sources" ADD FOREIGN KEY ("sources_id") REFERENCES "sources" ("id");

-- ALTER TABLE "ksats" ADD FOREIGN KEY ("id") REFERENCES "ksat_to_sources" ("ksats_id") ON DELETE CASCADE;
ALTER TABLE "ksat_to_sources" ADD FOREIGN KEY ("ksats_id") REFERENCES "ksats" ("id") ON DELETE CASCADE;

ALTER TABLE "modules_ksats" ADD FOREIGN KEY ("proficiencies_id") REFERENCES "proficiencies" ("id");


COMMENT ON COLUMN "ksats"."id" IS 'This is not autogenerated!';

COMMENT ON COLUMN "ksat_edges"."from_ksats_id" IS 'child ksat id';

COMMENT ON COLUMN "ksat_edges"."to_ksats_id" IS 'parent ksat id';

COMMENT ON COLUMN "proficiencies"."id" IS 'This is not autogenerated!';

COMMENT ON COLUMN "work_roles"."id" IS 'WR acronym';

COMMENT ON COLUMN "work_roles"."work_role" IS 'Full WR name';

COMMENT ON COLUMN "relationship_links"."type" IS 'either commercial, IQT, self-study';

COMMENT ON COLUMN "relationship_links"."courses_id" IS 'rel-links do not need to be connected to a course';

COMMENT ON COLUMN "relationship_links"."map_type" IS 'either training or eval';

COMMENT ON COLUMN "modules_content"."url" IS 'This is the training';

COMMENT ON COLUMN "modules_performance"."url" IS 'These are labs';

COMMENT ON COLUMN "modules_assessment"."url" IS 'These are tests';

COMMENT ON COLUMN "modules_resources"."type" IS 'currently only *reference* but staged to expand';

COMMENT ON COLUMN "modules_resources"."url" IS 'This is supplimentary information';

COMMENT ON COLUMN "units"."estimated_time" IS 'time in minutes';
