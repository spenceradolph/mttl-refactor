--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: unit_modules; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.unit_modules (id, units_id, modules_id) FROM stdin;
1	1	3
2	2	12
3	3	1
4	3	17
5	3	9
6	3	16
7	4	2
8	4	5
9	4	15
10	4	14
11	5	7
12	5	6
13	5	4
14	5	10
15	5	11
17	7	13
\.


--
-- Name: unit_modules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.unit_modules_id_seq', 17, true);


--
-- PostgreSQL database dump complete
--

