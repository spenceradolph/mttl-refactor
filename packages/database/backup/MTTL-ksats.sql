--
-- Sorted PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: ksats; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.ksats (id, type, topic_id, description, created_on, updated_on, comments, old_id) FROM stdin;
A0004	Ability	29	Write pseudocode to solve a programming problem	2021-12-10 18:39:06.959707	2021-12-10 18:39:06.959708		5f457f1e1ea90ba9adb32a35
A0018	Ability	28	Analyze a problem to formulate a software solution.	2021-12-10 18:39:06.960643	2021-12-10 18:39:06.960644	I.e. Identify functions, variables, inputs, outputs, memory considerations, modularity, error conditions, etc	5f457f1e1ea90ba9adb32a4b
A0019	Ability	28	Integrate functionality between multiple software components.	2021-12-10 18:39:06.961487	2021-12-10 18:39:06.961488	Take multiple blocks of code that they have developed/functions and integrate them together	5f457f1e1ea90ba9adb32a49
A0047	Ability	5	Implement file management operations.	2021-12-10 18:39:06.962343	2021-12-10 18:39:06.962344	In addition to the program package itself (program plus comments, documentation, etc) must PRESENT the functionality to an evaluator.	5f457f1e1ea90ba9adb32a3b
A0061	Ability	29	Create and implement functions to meet a requirement.	2021-12-10 18:39:06.963245	2021-12-10 18:39:06.963247	In addition to the program package itself (program plus comments, documentation, etc) must PRESENT the functionality to an evaluator.	5f457f1e1ea90ba9adb32a3c
A0077	Ability	87	Create and execute unit testing to validate program functionality	2021-12-10 18:39:06.964284	2021-12-10 18:39:06.964285		5f457f1e1ea90ba9adb32a28
A0082	Ability	4	Identify and eliminate memory leaks.	2021-12-10 18:39:06.965075	2021-12-10 18:39:06.965076		5f457f1e1ea90ba9adb32a39
A0087	Ability	30	Create and implement a sort routine.	2021-12-10 18:39:06.965864	2021-12-10 18:39:06.965864	Pick which algorithm to implement; "create" could mean finding pseudocode online and using it	5f457f1e1ea90ba9adb32a3a
A0093	Ability	30	Analyze data structures and implement most efficient Big-O sorting routine.	2021-12-10 18:39:06.966648	2021-12-10 18:39:06.966649		5f457f1e1ea90ba9adb32a3d
A0116	Ability	31	Debug IP network traffic.	2021-12-10 18:39:06.967529	2021-12-10 18:39:06.96753		5f457f1e1ea90ba9adb32a3f
A0117	Ability	5	Develop client/server network programs.	2021-12-10 18:39:06.968509	2021-12-10 18:39:06.96851		5f457f1e1ea90ba9adb32a36
A0179	Ability	4	Implement file management operations.	2021-12-10 18:39:06.969132	2021-12-10 18:39:06.969133		5f457f1e1ea90ba9adb32a41
A0343	Ability	12	Conduct static analysis of a binary to determine its functionality.	2021-12-10 18:39:06.969698	2021-12-10 18:39:06.969699	Something 'simple' like hello world	5f457f1e1ea90ba9adb32a42
A0344	Ability	12	Conduct dynamic analysis of a binary to determine its functionality.	2021-12-10 18:39:06.970377	2021-12-10 18:39:06.970377	Something 'simple' like hello world	5f457f1e1ea90ba9adb32a3e
A0350	Ability	88	Utilize Containerization to build infrastructure as code	2021-12-10 18:39:06.971177	2021-12-10 18:39:06.971178	e.g. Docker	5f457f1e1ea90ba9adb32a40
A0351	Ability	34	Build a CI pipeline using containers	2021-12-10 18:39:06.971948	2021-12-10 18:39:06.97195		5f457f1e1ea90ba9adb32a24
A0353	Ability	34	Perform test driven development	2021-12-10 18:39:06.972735	2021-12-10 18:39:06.972737		5f457f1e1ea90ba9adb32a25
A0355	Ability	34	Package an application/tool for delivery.	2021-12-10 18:39:06.97352	2021-12-10 18:39:06.973521	Should this be a task?	5f457f1e1ea90ba9adb32a26
A0356	Ability	34	Write an application/tool user guide	2021-12-10 18:39:06.974165	2021-12-10 18:39:06.974165		5f457f1e1ea90ba9adb32a27
A0358	Ability	34	Write application/tool installation documentation	2021-12-10 18:39:06.975102	2021-12-10 18:39:06.975103		5f457f1e1ea90ba9adb32a29
A0359	Ability	34	Utilize ticketing tool to structure team activities	2021-12-10 18:39:06.976054	2021-12-10 18:39:06.976055	jira, gitlab	5f457f1e1ea90ba9adb32a2a
A0360	Ability	34	Utilize knowledge management tool to document internal team knowledge and end user documentation	2021-12-10 18:39:06.977019	2021-12-10 18:39:06.97702	confluence, wiki etc	5f457f1e1ea90ba9adb32a2b
A0459	Ability	2	Create, Maintain, and Refine the Product Backlog	2021-12-10 18:39:06.977785	2021-12-15 23:34:05.650917		5f457f1e1ea90ba9adb32a50
A0466	Ability	53	Develop an application using Win32 Project	2021-12-10 18:39:06.978379	2021-12-10 18:39:06.97838		5f457f1e1ea90ba9adb32a51
A0479	Ability	53	Develop a Win32 application that is Win64 compatible	2021-12-10 18:39:06.979045	2021-12-10 18:39:06.979046		5f457f1e1ea90ba9adb32a52
A0508	Ability	55	Develop a Simple Driver	2021-12-10 18:39:06.979629	2021-12-10 18:39:06.97963		5f457f1e1ea90ba9adb32a54
A0543	Ability	57	Develop a system program (Kernel)	2021-12-10 18:39:06.980294	2021-12-10 18:39:06.980295		5f457f1e1ea90ba9adb32a53
A0544	Ability	57	Develop a shell program	2021-12-10 18:39:06.981168	2021-12-10 18:39:06.981168		5f457f1e1ea90ba9adb32a55
A0554	Ability	57	Develop a basic driver	2021-12-10 18:39:06.981734	2021-12-10 18:39:06.981734		5f457f1e1ea90ba9adb32a56
A0561	Ability	5	Demonstrate the ability to create, reuse and import modules	2021-12-10 18:39:06.982301	2021-12-10 18:39:06.982302		5f457f1e1ea90ba9adb32a57
A0563	Ability	5	Programming with Functions and Metaclasses	2021-12-10 18:39:06.983152	2021-12-10 18:39:06.983153		5f457f1e1ea90ba9adb32a5c
A0564	Ability	5	Multi-threading	2021-12-10 18:39:06.984197	2021-12-10 18:39:06.9842		5f457f1e1ea90ba9adb32a5a
A0579	Ability	58	Shared Pointers	2021-12-10 18:39:06.984897	2021-12-10 18:39:06.984898		5f457f1e1ea90ba9adb32a58
A0580	Ability	58	Unique Pointers	2021-12-10 18:39:06.985664	2021-12-10 18:39:06.985665		5f457f1e1ea90ba9adb32a59
A0581	Ability	58	Classes	2021-12-10 18:39:06.986508	2021-12-10 18:39:06.98651		5f457f1e1ea90ba9adb32a5b
A0582	Ability	58	Standard Template Library	2021-12-10 18:39:06.987156	2021-12-10 18:39:06.987157		5f457f1e1ea90ba9adb32a5e
A0604	Ability	31	Packet Captures	2021-12-10 18:39:06.987791	2021-12-10 18:39:06.987792		5f457f1e1ea90ba9adb32a62
A0605	Ability	31	Packet Capture (PCAP) File Analysis	2021-12-10 18:39:06.988427	2021-12-10 18:39:06.988428		5f457f1e1ea90ba9adb32a60
A0606	Ability	31	Capture Filters	2021-12-10 18:39:06.989056	2021-12-10 18:39:06.989057		5f457f1e1ea90ba9adb32a64
A0607	Ability	31	Trace Connection	2021-12-10 18:39:06.989703	2021-12-10 18:39:06.989704		5f457f1e1ea90ba9adb32a63
A0608	Ability	31	Secure vs Non-secure Traffic (encrypted vs non-encrypted)	2021-12-10 18:39:06.99045	2021-12-10 18:39:06.990452		5f457f1e1ea90ba9adb32a68
A0609	Ability	12	WinDbg Dynamic Analysis	2021-12-10 18:39:06.991355	2021-12-10 18:39:06.991356		5f457f1e1ea90ba9adb32a69
A0610	Ability	12	Disassemble Binary	2021-12-10 18:39:06.991999	2021-12-10 18:39:06.992		5f457f1e1ea90ba9adb32a66
A0611	Ability	12	Static Analysis	2021-12-10 18:39:06.992769	2021-12-10 18:39:06.992771		5f457f1e1ea90ba9adb32a67
A0612	Ability	12	Dynamic Analysis	2021-12-10 18:39:06.993836	2021-12-10 18:39:06.993837		5f457f1e1ea90ba9adb32a6a
A0613	Ability	12	Areas of Interest	2021-12-10 18:39:06.994751	2021-12-10 18:39:06.994752		5f457f1e1ea90ba9adb32a65
A0614	Ability	12	Disassembly and C Programming	2021-12-10 18:39:06.995379	2021-12-10 18:39:06.99538		5f457f1e1ea90ba9adb32a83
A0615	Ability	12	Combine Dynamic and Static Analysis	2021-12-10 18:39:06.99601	2021-12-10 18:39:06.996011		5f457f1e1ea90ba9adb32a87
A0616	Ability	12	Static/Dynamic Tools	2021-12-10 18:39:06.996648	2021-12-10 18:39:06.996649		5f457f1e1ea90ba9adb32a8a
A0621	Ability	15	Implement Shell Code for Unique Exploitation Environments	2021-12-10 18:39:06.997327	2021-12-29 15:19:40.322271		5f457f1e1ea90ba9adb32a88
A0622	Ability	15	Structured Exception Handling (SEH) Overwrite	2021-12-10 18:39:06.998094	2021-12-10 18:39:06.998095		5f457f1e1ea90ba9adb32a82
A0623	Ability	15	Heap Spraying/Heap Corruption	2021-12-10 18:39:06.998784	2021-12-10 18:39:06.998785		5f457f1e1ea90ba9adb32a84
A0626	Ability	31	String-based Protocol	2021-12-10 18:39:06.999369	2021-12-10 18:39:06.99937		5f457f1e1ea90ba9adb32a8b
A0627	Ability	31	Bitwise Protocol	2021-12-10 18:39:07.000112	2021-12-10 18:39:07.000113		5f457f1e1ea90ba9adb32a70
A0628	Ability	4	Demonstrate the ability to develop C client/server network programs.	2021-12-10 18:39:07.001007	2021-12-10 18:39:07.001008		5f457f1e1ea90ba9adb32a6b
A0650	Ability	86	Utilize Jira for project and task management.	2021-12-10 18:39:07.001812	2021-12-15 23:27:49.997916		5f457f1e1ea90ba9adb32a71
A0730	Ability	12	Ability to tailor code analysis for application-specific concerns.	2021-12-10 18:39:07.002828	2021-12-10 18:39:07.00283	SP-DEV-001	5f457f1e1ea90ba9adb32a78
A0731	Ability	89	Ability to use and understand complex mathematical concepts (e.g., discrete math).	2021-12-10 18:39:07.004234	2021-12-10 18:39:07.004236	SP-DEV-001	5f457f1e1ea90ba9adb32a7b
A0732	Ability	18	Ability to develop secure software according to secure software deployment methodologies, tools, and practices.	2021-12-10 18:39:07.005457	2021-12-10 18:39:07.005458	SP-DEV-001	5f457f1e1ea90ba9adb32a7a
A0733	Ability	90	Ability to apply cybersecurity and privacy principles to organizational requirements (relevant to confidentiality, integrity, availability, authentication, non-repudiation).	2021-12-10 18:39:07.006631	2021-12-10 18:39:07.006631	SP-DEV-001	5f457f1e1ea90ba9adb32a79
A0734	Ability	73	Ability to identify critical infrastructure systems with information communication technology that were designed without system security considerations.	2021-12-10 18:39:07.007723	2021-12-10 18:39:07.007725	SP-DEV-001	5f457f1e1ea90ba9adb32a7d
A0736	Ability	68	Demonstrate the ability to train team members on how to use & configure automated frameworks & tools.	2021-12-10 18:39:07.008585	2021-12-10 18:39:07.008586	(e.g. DART, DEADCODE, METATOTAL, etc.).	5f457f1e1ea90ba9adb32a7e
A0737	Ability	68	Demonstrate the ability to integrate individual tests, jobs, and/or scripts into testing frameworks, tools, and CI pipelines.	2021-12-10 18:39:07.009172	2021-12-10 18:39:07.009173		5f457f1e1ea90ba9adb32a80
A0738	Ability	68	Demonstrate the ability to leverage automated testing frameworks and tools to orchestrate multiple parallel tests at scale.	2021-12-10 18:39:07.009772	2021-12-10 18:39:07.009773	(e.g. 10s, 100s, 1000s of tests)	5f457f1e1ea90ba9adb32a8c
A0739	Ability	68	Demonstrate the ability to build and configure virtual machines, containers, and/or hardware systems used as target devices/systems.	2021-12-10 18:39:07.010556	2021-12-10 18:39:07.010557		5f457f1e1ea90ba9adb32a8f
A0740	Ability	68	Demonstrate the ability to design and document operationally representative environments consisting of multiple target devices and systems and necessary networking.	2021-12-10 18:39:07.011306	2021-12-10 18:39:07.011307		5f457f1e1ea90ba9adb32a93
A0741	Ability	77	Judge when it is appropriate to use a docker container instead of a virtual machine or bare metal installation.	2021-12-10 18:39:07.012048	2021-12-10 18:39:07.012049		5f457f1e1ea90ba9adb32a91
A0742	Ability	77	Create a service in docker (http, database, etc.) with persistent storage	2021-12-10 18:39:07.012851	2021-12-10 18:39:07.012852		5f457f1e1ea90ba9adb32a92
A0743	Ability	80	Configure a virtual machine (EC2) with a block storage volume in a VPC	2021-12-10 18:39:07.013505	2021-12-10 18:39:07.013506		5f457f1e1ea90ba9adb32a8d
A0744	Ability	80	Configure object storage (S3) with correct permissions	2021-12-10 18:39:07.014108	2021-12-10 18:39:07.014109		5f457f1e1ea90ba9adb32a90
A0745	Ability	86	Analyze and interpret data from Agile reports: Burndown Chart, Control Chart, Cumulative Flow Diagram, Epic Burndown, Epic Report, Sprint Report, Release Burndown, Velocity Chart, Version Report.	2021-12-10 18:39:07.014672	2021-12-10 18:39:07.014673		5f457f1e1ea90ba9adb32a94
A0746	Ability	9	Create a requirements traceability matrix	2021-12-10 18:39:07.015317	2021-12-10 18:39:07.015318		6156040987421678df5dec5c
A0747	Ability	9	Derive requirements from a Cyber Needs Form (CNF)	2021-12-10 18:39:07.017311	2021-12-10 18:39:07.017312		615610e7f2c422d8748ffe1c
A0748	Ability	9	Create and present a Product Retrospective.	2021-12-10 18:39:07.016429	2021-12-10 18:39:07.01643		61560ed16a7c091fa6cee3a1
K0001	Knowledge	27	Understand different Git workflows	2021-12-10 18:39:06.399669	2021-12-10 18:39:06.399671		5f457f1e1ea90ba9adb32a9a
K0002	Knowledge	28	Describe Modular Design	2021-12-10 18:39:06.400814	2021-12-10 18:39:06.400815		5f457f1e1ea90ba9adb32a9d
K0003	Knowledge	28	Understand the purpose of breaking down requirements to atomic functions.	2021-12-10 18:39:06.401639	2021-12-10 18:39:06.40164	Making sure that you don't have one massive function that does everything, but breaking down the functions by discrete tasks	5f457f1e1ea90ba9adb32aa2
K0004	Knowledge	28	Know accepted code styling standards used at the 90 COS.	2021-12-10 18:39:06.402464	2021-12-10 18:39:06.402465		5f457f1e1ea90ba9adb32a9c
K0005	Knowledge	28	Describe the principles of DRY and SOLID design.	2021-12-10 18:39:06.403338	2021-12-10 18:39:06.40334	i.e. Don't repeat yourself...	5f457f1e1ea90ba9adb32aad
K0007	Knowledge	29	Understand the purpose of the return statement	2021-12-10 18:39:06.404324	2021-12-10 18:39:06.404325		5f457f1e1ea90ba9adb32a9b
K0008	Knowledge	5	Describe the declaration and implementation of data types	2021-12-10 18:39:06.405284	2021-12-10 18:39:06.405286	Integer, Float, Boolean, and String	5f457f1e1ea90ba9adb32ab1
K0009	Knowledge	29	Understand how to create and implement functions.	2021-12-10 18:39:06.406315	2021-12-10 18:39:06.406317		5f457f1e1ea90ba9adb32ab6
K0010	Knowledge	29	Understand how to use parameters in a function.	2021-12-10 18:39:06.407375	2021-12-10 18:39:06.407376		5f457f1e1ea90ba9adb32ab3
K0011	Knowledge	29	Understand the scope of a declared variable.	2021-12-10 18:39:06.408373	2021-12-10 18:39:06.408374		5f457f1e1ea90ba9adb32aaf
K0012	Knowledge	29	Understand how return values are used in a function.	2021-12-10 18:39:06.409345	2021-12-10 18:39:06.409346		5f457f1e1ea90ba9adb32ab0
K0013	Knowledge	5	Understand how to import modules and module components.	2021-12-10 18:39:06.41033	2021-12-10 18:39:06.410332		5f457f1e1ea90ba9adb32abb
K0014	Knowledge	5	Understand how to declare and implement a Dictionary.	2021-12-10 18:39:06.411314	2021-12-10 18:39:06.411315		5f457f1e1ea90ba9adb32aba
K0015	Knowledge	5	Understand how to declare and implement a single and multi-dimensional List.	2021-12-10 18:39:06.41229	2021-12-10 18:39:06.412292		5f457f1e1ea90ba9adb32ac6
K0016	Knowledge	5	Understand how to declare and implement a Tuple.	2021-12-10 18:39:06.413283	2021-12-10 18:39:06.413284		5f457f1e1ea90ba9adb32ab7
K0017	Knowledge	5	Understand how to declare and implement a Set.	2021-12-10 18:39:06.414196	2021-12-10 18:39:06.414198		5f457f1e1ea90ba9adb32ab5
K0018	Knowledge	5	Understand the term mutable and data types it applies to.	2021-12-10 18:39:06.414908	2021-12-10 18:39:06.414909		5f457f1e1ea90ba9adb32ab9
K0019	Knowledge	5	Understand the term immutable and data types it applies to.	2021-12-10 18:39:06.415894	2021-12-10 18:39:06.415895		5f457f1e1ea90ba9adb32abd
K0021	Knowledge	5	Describe standards in Python Enhancement Proposal (PEP8)	2021-12-10 18:39:06.416884	2021-12-10 18:39:06.416885		5f457f1e1ea90ba9adb32a9e
K0022	Knowledge	5	Describe the purpose and use of PyDocs	2021-12-10 18:39:06.417855	2021-12-10 18:39:06.417856		5f457f1e1ea90ba9adb32a9f
K0023	Knowledge	5	Identify differences between Python 2 vs Python 3	2021-12-10 18:39:06.418843	2021-12-10 18:39:06.418844		5f457f1e1ea90ba9adb32aa0
K0024	Knowledge	5	Understand how to use the Python Standard Library	2021-12-10 18:39:06.419794	2021-12-10 18:39:06.419796		5f457f1e1ea90ba9adb32aa1
K0026	Knowledge	5	Describe the purpose and use of Iterators	2021-12-10 18:39:06.420762	2021-12-10 18:39:06.420763	Add to 3.2.1 in JQR	5f457f1e1ea90ba9adb32aa6
K0027	Knowledge	5	Describe the purpose and use of Generators	2021-12-10 18:39:06.421721	2021-12-10 18:39:06.421722	Add to 3.2.1 in JQR	5f457f1e1ea90ba9adb32aa3
K0028	Knowledge	5	Describe the purpose and use of Lambda functions	2021-12-10 18:39:06.422705	2021-12-10 18:39:06.422706	Add to 3.2.1 in JQR	5f457f1e1ea90ba9adb32aa4
K0029	Knowledge	5	Describe the purpose and use of C-types	2021-12-10 18:39:06.423692	2021-12-10 18:39:06.423694	Add to 3.2.1 in JQR	5f457f1e1ea90ba9adb32aa8
K0030	Knowledge	5	Describe the terms and fundamentals associated with object-oriented programming using Python.	2021-12-10 18:39:06.424637	2021-12-10 18:39:06.424638	Add to 3.2.9 in JQR	5f457f1e1ea90ba9adb32aa5
K0031	Knowledge	5	Describe the Object-oriented principles necessary to implement polymorphism.	2021-12-10 18:39:06.425442	2021-12-10 18:39:06.425443	Add to 3.2.9 in JQR	5f457f1e1ea90ba9adb32aab
K0032	Knowledge	5	Understand how to create a Python class and describe the necessary components	2021-12-10 18:39:06.425991	2021-12-10 18:39:06.425991	Provide a case study scenario (ex. A bank wants you to create a program with users, account numbers, accounts, etc and they have different banks, etc)... write an essay on how you would use _____ to implement your program.	5f457f1e1ea90ba9adb32aa7
K0033	Knowledge	5	Describe how to create and use an object	2021-12-10 18:39:06.426532	2021-12-10 18:39:06.426532	Provide a case study scenario (ex. A bank wants you to create a program with users, account numbers, accounts, etc and they have different banks, etc)... write an essay on how you would use _____ to implement your program.	5f457f1e1ea90ba9adb32aa9
K0034	Knowledge	5	Describe differences between an object and a class	2021-12-10 18:39:06.427163	2021-12-10 18:39:06.427164		5f457f1e1ea90ba9adb32aae
K0035	Knowledge	5	Identify advantages of object-oriented programming	2021-12-10 18:39:06.428021	2021-12-10 18:39:06.428022		5f457f1e1ea90ba9adb32aaa
K0036	Knowledge	5	Inheritance	2021-12-10 18:39:06.4288	2021-12-10 18:39:06.428801		5f457f1e1ea90ba9adb32aac
K0037	Knowledge	5	Understand the purpose and use of the keyword "Super"	2021-12-10 18:39:06.429614	2021-12-10 18:39:06.429615		5f457f1e1ea90ba9adb32ab4
K0038	Knowledge	5	Recognize the signature and purpose of the initialization function of a constructor.	2021-12-10 18:39:06.43043	2021-12-10 18:39:06.43043		5f457f1e1ea90ba9adb32ab2
K0039	Knowledge	5	Describe the purpose of getter and setter functions	2021-12-10 18:39:06.431352	2021-12-10 18:39:06.431353		5f457f1e1ea90ba9adb32ab8
K0040	Knowledge	5	Describe the purpose of a class attribute.	2021-12-10 18:39:06.432248	2021-12-10 18:39:06.432249		5f457f1e1ea90ba9adb32abc
K0041	Knowledge	5	Describe the behavior of factory design pattern.	2021-12-10 18:39:06.433023	2021-12-10 18:39:06.433024		5f457f1e1ea90ba9adb32abe
K0042	Knowledge	5	Describe the behavior of a singleton design pattern.	2021-12-10 18:39:06.433864	2021-12-10 18:39:06.433865		5f457f1e1ea90ba9adb32abf
K0043	Knowledge	5	Describe the behavior of an adapter design pattern.	2021-12-10 18:39:06.434779	2021-12-10 18:39:06.434781		5f457f1e1ea90ba9adb32ac0
K0044	Knowledge	5	Describe the behavior of a bridge design pattern.	2021-12-10 18:39:06.435954	2021-12-10 18:39:06.435956		5f457f1e1ea90ba9adb32ac1
K0045	Knowledge	30	Describe the concepts associated with traversal techniques.	2021-12-10 18:39:06.437265	2021-12-10 18:39:06.437266		5f457f1e1ea90ba9adb32ac2
K0046	Knowledge	30	Describe the behavior of depth first traversal.	2021-12-10 18:39:06.438671	2021-12-10 18:39:06.438673		5f457f1e1ea90ba9adb32ac3
K0047	Knowledge	30	Describe the behavior of breadth first traversal.	2021-12-10 18:39:06.439709	2021-12-10 18:39:06.43971		5f457f1e1ea90ba9adb32ac4
K0048	Knowledge	30	Describe techniques to determine the weight of a given path in a weighted graph.	2021-12-10 18:39:06.440849	2021-12-10 18:39:06.440852		5f457f1e1ea90ba9adb32ac5
K0049	Knowledge	30	Describe how to determine the most efficient path for traversing a graph	2021-12-10 18:39:06.44193	2021-12-10 18:39:06.441931		5f457f1e1ea90ba9adb32ac7
K0050	Knowledge	30	Describe the concepts associated with hashing.	2021-12-10 18:39:06.442953	2021-12-10 18:39:06.442955	Hashing for data structures (not cryptographic hashing)	5f457f1e1ea90ba9adb32ac8
K0051	Knowledge	30	Describe data distribution as it relates to hashing	2021-12-10 18:39:06.443874	2021-12-10 18:39:06.443875	Hashing for data structures (not cryptographic hashing)	5f457f1e1ea90ba9adb32ac9
K0052	Knowledge	30	Know the efficiency of a hash function.	2021-12-10 18:39:06.444538	2021-12-10 18:39:06.444539	Hashing for data structures (not cryptographic hashing)	5f457f1e1ea90ba9adb32acc
K0053	Knowledge	30	Describe the causes and techniques to avoid hash collisions	2021-12-10 18:39:06.445293	2021-12-10 18:39:06.445294	Hashing for data structures (not cryptographic hashing)	5f457f1e1ea90ba9adb32aca
K0055	Knowledge	16	Describe how to implement and use a Binary search tree	2021-12-10 18:39:06.446307	2021-12-10 18:39:06.446309	Maps to army JQR 3.3.1.a https://17d.cybbh.io/bolc/internal/course/instructor/course/E-Basic-Development/06-C-Data-Structures/	5f457f1e1ea90ba9adb32acb
K0056	Knowledge	16	Describe how to implement and use a Linked list	2021-12-10 18:39:06.447328	2021-12-10 18:39:06.447329		5f457f1e1ea90ba9adb32acd
K0057	Knowledge	16	Describe how to implement and use a Doubly linked list	2021-12-10 18:39:06.448341	2021-12-10 18:39:06.448342		5f457f1e1ea90ba9adb32ace
K0058	Knowledge	16	Describe how to implement and use a Circularly linked list	2021-12-10 18:39:06.449295	2021-12-10 18:39:06.449296		5f457f1e1ea90ba9adb32ad1
K0059	Knowledge	16	Describe how to implement and use a Weighted Graph	2021-12-10 18:39:06.450258	2021-12-10 18:39:06.450259		5f457f1e1ea90ba9adb32acf
K0060	Knowledge	16	Identify common pitfalls when using linked lists, queues, trees, and graphs	2021-12-10 18:39:06.4512	2021-12-10 18:39:06.451201		5f457f1e1ea90ba9adb32ad0
K0061	Knowledge	16	Describe the meaning of First in First Out (FIFO) and Last in First Out (LIFO)	2021-12-10 18:39:06.452252	2021-12-10 18:39:06.452253		5f457f1e1ea90ba9adb32ad2
K0062	Knowledge	5	Describe the libraries commonly used to aid in serialization.	2021-12-10 18:39:06.453259	2021-12-10 18:39:06.45326	protobuf	5f457f1e1ea90ba9adb32ad4
K0063	Knowledge	31	Understand how to utilize sockets	2021-12-10 18:39:06.454318	2021-12-10 18:39:06.45432		5f457f1e1ea90ba9adb32ad3
K0064	Knowledge	31	Understand constructs of the Transmission Control Protocol (TCP) 	2021-12-10 18:39:06.455063	2021-12-10 18:39:06.455065		5f457f1e1ea90ba9adb32ad5
K0065	Knowledge	31	Understand constructs of the User Datagram Protocol (UDP)	2021-12-10 18:39:06.456077	2021-12-10 18:39:06.456078		5f457f1e1ea90ba9adb32ad6
K0066	Knowledge	31	Describe the Open Systems Interconnect (OSI) model	2021-12-10 18:39:06.457105	2021-12-10 18:39:06.457106		5f457f1e1ea90ba9adb32ad8
K0068	Knowledge	31	Understand how to use functions and components of the Socket Library	2021-12-10 18:39:06.458086	2021-12-10 18:39:06.458087	for both Python and C	5f457f1e1ea90ba9adb32ad7
K0071	Knowledge	31	Describe the purpose of OSI Layer 3	2021-12-10 18:39:06.459028	2021-12-10 18:39:06.459029		5f457f1e1ea90ba9adb32adc
K0072	Knowledge	31	Describe the purpose of subnetting	2021-12-10 18:39:06.459825	2021-12-10 18:39:06.459825		5f457f1e1ea90ba9adb32ada
K0073	Knowledge	31	Describe the purpose of OSI Layer 2	2021-12-10 18:39:06.460585	2021-12-10 18:39:06.460586		5f457f1e1ea90ba9adb32ad9
K0075	Knowledge	31	Describe the purpose of OSI Layer 7	2021-12-10 18:39:06.461366	2021-12-10 18:39:06.461367		5f457f1e1ea90ba9adb32adb
K0081	Knowledge	31	Understand how an IPv4 address is constructed.	2021-12-10 18:39:06.462124	2021-12-10 18:39:06.462124		5f457f1e1ea90ba9adb32ade
K0082	Knowledge	31	Understand how an IPv6 address is constructed.	2021-12-10 18:39:06.462895	2021-12-10 18:39:06.462895		5f457f1e1ea90ba9adb32add
K0086	Knowledge	31	Understand different types of ethernet configurations.	2021-12-10 18:39:06.463698	2021-12-10 18:39:06.463699	LANs, WANs, MANs etc.	5f457f1e1ea90ba9adb32adf
K0087	Knowledge	31	Understand the behavior of Network address translation (NAT)	2021-12-10 18:39:06.464576	2021-12-10 18:39:06.464577	Add to 3.7.1 in JQR	5f457f1e1ea90ba9adb32ae1
K0088	Knowledge	31	Understand how to apply CIDR notation to an IP address.	2021-12-10 18:39:06.465377	2021-12-10 18:39:06.465378	Add to 3.7.1 in JQR	5f457f1e1ea90ba9adb32ae0
K0089	Knowledge	31	Describe common network devices	2021-12-10 18:39:06.466235	2021-12-10 18:39:06.466236	Add to 3.7.1 in JQR	5f457f1e1ea90ba9adb32ae3
K0090	Knowledge	31	Understand how to implement serialization	2021-12-10 18:39:06.467088	2021-12-10 18:39:06.467089	Add to 3.7.1 in JQR	5f457f1e1ea90ba9adb32ae2
K0091	Knowledge	31	Understand how to account for endianness between differing systems.	2021-12-10 18:39:06.467964	2021-12-10 18:39:06.467965	Add to 3.7.1 in JQR	5f457f1e1ea90ba9adb32ae5
K0093	Knowledge	4	Understand the purpose and use of the main() function	2021-12-10 18:39:06.468838	2021-12-10 18:39:06.46884		5f457f1e1ea90ba9adb32ae4
K0095	Knowledge	4	Understand how to implement macro guards with the preprocessor.	2021-12-10 18:39:06.469894	2021-12-10 18:39:06.469896		5f457f1e1ea90ba9adb32ae6
K0096	Knowledge	4	Know how to declare and implement data types	2021-12-10 18:39:06.470845	2021-12-10 18:39:06.470846	short, int, float, char, double, and long	5f457f1e1ea90ba9adb32ae9
K0101	Knowledge	4	Describe how to create and use header files	2021-12-10 18:39:06.471726	2021-12-10 18:39:06.471727		5f457f1e1ea90ba9adb32ae7
K0102	Knowledge	4	Describe the purpose of keywords.	2021-12-10 18:39:06.47286	2021-12-10 18:39:06.472862	static, extern, etc (this KSAT seems vague and too broad)	5f457f1e1ea90ba9adb32ae8
K0103	Knowledge	4	Understand how to create and utilize pointers	2021-12-10 18:39:06.473925	2021-12-10 18:39:06.473926		5f457f1e1ea90ba9adb32aee
K0104	Knowledge	4	Understand how to implement and use a single and multi-dimensional array	2021-12-10 18:39:06.474941	2021-12-10 18:39:06.474942		5f457f1e1ea90ba9adb32aea
K0105	Knowledge	4	Understand the purpose of the pre-processor.	2021-12-10 18:39:06.475946	2021-12-10 18:39:06.475947		5f457f1e1ea90ba9adb32aeb
K0106	Knowledge	4	Understand how to utilize type casting	2021-12-10 18:39:06.476931	2021-12-10 18:39:06.476932		5f457f1e1ea90ba9adb32aec
K0107	Knowledge	29	Understand how to implement and use looping constructs.	2021-12-10 18:39:06.477917	2021-12-10 18:39:06.477918		5f457f1e1ea90ba9adb32af0
K0108	Knowledge	4	Understand the term endianness and its effect on data.	2021-12-10 18:39:06.478924	2021-12-10 18:39:06.478925		5f457f1e1ea90ba9adb32aed
K0109	Knowledge	4	Understand the components of Multi-threading.	2021-12-10 18:39:06.479914	2021-12-10 18:39:06.479916		5f457f1e1ea90ba9adb32aef
K0110	Knowledge	16	Describe how a hash table functions.	2021-12-10 18:39:06.480762	2021-12-10 18:39:06.480764		5f457f1e1ea90ba9adb32af1
K0111	Knowledge	29	Identify and understand bitwise operators. 	2021-12-10 18:39:06.481563	2021-12-10 18:39:06.481564		5f457f1e1ea90ba9adb32af4
K0119	Knowledge	4	Describes how memory is mapped for a given C program	2021-12-10 18:39:06.482565	2021-12-10 18:39:06.482566	aka. Heap is provided in virtual memory, stack grows up in memory map. Given a scenario need to know where memory gets allocated	5f457f1e1ea90ba9adb32af3
K0120	Knowledge	4	Describe how and when stack memory is allocated.	2021-12-10 18:39:06.48353	2021-12-10 18:39:06.483531	Given a scenario need to know where memory gets allocated	5f457f1e1ea90ba9adb32af2
K0121	Knowledge	4	Describe heap memory allocation.	2021-12-10 18:39:06.484506	2021-12-10 18:39:06.484507	Given a scenario need to know where memory gets allocated	5f457f1e1ea90ba9adb32af9
K0122	Knowledge	4	Identify the differences between static and dynamic allocation	2021-12-10 18:39:06.485496	2021-12-10 18:39:06.485497	Given a scenario need to know where memory gets allocated	5f457f1e1ea90ba9adb32af6
K0123	Knowledge	29	Understand different approaches to error handling	2021-12-10 18:39:06.486511	2021-12-10 18:39:06.486512	buffer overflow, array index out of bounds, divide by zero etc.	5f457f1e1ea90ba9adb32af5
K0124	Knowledge	4	Describe how to create and utilize a struct composite data type.	2021-12-10 18:39:06.487478	2021-12-10 18:39:06.487479	Add to 3.1.1 in JQR	5f457f1e1ea90ba9adb32af7
K0125	Knowledge	4	Describe how to create and utilize a union composite data type	2021-12-10 18:39:06.488468	2021-12-10 18:39:06.488469	Add to 3.1.1 in JQR	5f457f1e1ea90ba9adb32b00
K0126	Knowledge	4	Describe how to create and utilize an Enum composite data type.	2021-12-10 18:39:06.489458	2021-12-10 18:39:06.48946	Add to 3.1.1 in JQR	5f457f1e1ea90ba9adb32af8
K0127	Knowledge	4	Describe how to create and utilize a TypeDef	2021-12-10 18:39:06.49047	2021-12-10 18:39:06.490471	Add to 3.1.1 in JQR	5f457f1e1ea90ba9adb32afa
K0128	Knowledge	4	Understand how to implement and use a char * array (string).	2021-12-10 18:39:06.491421	2021-12-10 18:39:06.491422	Add to 3.1.1 in JQR	5f457f1e1ea90ba9adb32afc
K0129	Knowledge	32	Describe the storage considerations of using Unicode	2021-12-10 18:39:06.492355	2021-12-10 18:39:06.492356		5f457f1e1ea90ba9adb32afb
K0130	Knowledge	29	Interpret logic from pseudocode	2021-12-10 18:39:06.493044	2021-12-10 18:39:06.493045	i.e. Critical thinking to Pseudocode through a problem	5f457f1e1ea90ba9adb32afd
K0132	Knowledge	30	Understand the varying degrees of Asymptotic notation (Big-O) pertaining to algorithms.	2021-12-10 18:39:06.493641	2021-12-10 18:39:06.493642	Given scenario know when or when not to use certain algorithms due to processing time	5f457f1e1ea90ba9adb32b01
K0133	Knowledge	30	Know the runtime efficiency for an Insertion Sort	2021-12-10 18:39:06.494403	2021-12-10 18:39:06.494403		5f457f1e1ea90ba9adb32afe
K0134	Knowledge	30	Know the runtime efficiency for a Selection Sort	2021-12-10 18:39:06.495184	2021-12-10 18:39:06.495185		5f457f1e1ea90ba9adb32aff
K0135	Knowledge	30	Know the runtime efficiency for a Merge Sort	2021-12-10 18:39:06.496032	2021-12-10 18:39:06.496033		5f457f1e1ea90ba9adb32b05
K0136	Knowledge	30	Know the runtime efficiency for a Heap Sort	2021-12-10 18:39:06.496968	2021-12-10 18:39:06.496969		5f457f1e1ea90ba9adb32b02
K0137	Knowledge	30	Know the runtime efficiency for a Quick Sort	2021-12-10 18:39:06.497853	2021-12-10 18:39:06.497854		5f457f1e1ea90ba9adb32b03
K0139	Knowledge	30	Know the runtime efficiency for a Hashing function.	2021-12-10 18:39:06.498622	2021-12-10 18:39:06.498623		5f457f1e1ea90ba9adb32b04
K0140	Knowledge	4	Describe concepts of compiling, linking, debugging, and executables.	2021-12-10 18:39:06.49942	2021-12-10 18:39:06.499421		5f457f1e1ea90ba9adb32b07
K0141	Knowledge	4	Describe a portable executable (PE) file.	2021-12-10 18:39:06.500283	2021-12-10 18:39:06.500283		5f457f1e1ea90ba9adb32b06
K0142	Knowledge	16	Describe components of an Executable and Linkable Format (ELF) file.	2021-12-10 18:39:06.501268	2021-12-10 18:39:06.50127		5f457f1e1ea90ba9adb32b09
K0143	Knowledge	16	Contrast PE and ELF files.	2021-12-10 18:39:06.502586	2021-12-10 18:39:06.502588		5f457f1e1ea90ba9adb32b0b
K0144	Knowledge	4	Contrast a library and a regular executable program	2021-12-10 18:39:06.503638	2021-12-10 18:39:06.50364		5f457f1e1ea90ba9adb32b0a
K0145	Knowledge	4	Describe calling a library module via its Application Binary Interface (ABI)	2021-12-10 18:39:06.504753	2021-12-10 18:39:06.504754		5f457f1e1ea90ba9adb32b08
K0148	Knowledge	33	Describe creating and controlling processes	2021-12-10 18:39:06.505965	2021-12-10 18:39:06.505966		5f457f1e1ea90ba9adb32b0d
K0149	Knowledge	33	Describe CPU scheduling management	2021-12-10 18:39:06.507028	2021-12-10 18:39:06.507029		5f457f1e1ea90ba9adb32b16
K0150	Knowledge	33	Understand how paging tables are used	2021-12-10 18:39:06.508042	2021-12-10 18:39:06.508043		5f457f1e1ea90ba9adb32b15
K0151	Knowledge	33	Describe caching	2021-12-10 18:39:06.50894	2021-12-10 18:39:06.508942		5f457f1e1ea90ba9adb32b0c
K0152	Knowledge	33	Differentiate between kernel and user-mode memory	2021-12-10 18:39:06.509921	2021-12-10 18:39:06.509923		5f457f1e1ea90ba9adb32b22
K0154	Knowledge	33	Describe threading.	2021-12-10 18:39:06.510904	2021-12-10 18:39:06.510905		5f457f1e1ea90ba9adb32b25
K0155	Knowledge	33	Describe lock implementation	2021-12-10 18:39:06.511934	2021-12-10 18:39:06.511935		5f457f1e1ea90ba9adb32b20
K0156	Knowledge	33	Identify causes of race conditions	2021-12-10 18:39:06.512974	2021-12-10 18:39:06.512975		5f457f1e1ea90ba9adb32b27
K0157	Knowledge	33	Identify causes of deadlocks	2021-12-10 18:39:06.513935	2021-12-10 18:39:06.513937		5f457f1e1ea90ba9adb32b28
K0158	Knowledge	33	Differentiate between types of scheduling algorithms.	2021-12-10 18:39:06.514903	2021-12-10 18:39:06.514904	(e.g. round-robin, etc)	5f457f1e1ea90ba9adb32b0e
K0160	Knowledge	33	Describe Von Neumann Architecture	2021-12-10 18:39:06.515918	2021-12-10 18:39:06.515919	Data persistence not CNO persistence	5f457f1e1ea90ba9adb32b23
K0161	Knowledge	33	Describe Harvard Architecture	2021-12-10 18:39:06.516888	2021-12-10 18:39:06.516889	Data persistence not CNO persistence	5f457f1e1ea90ba9adb32b29
K0162	Knowledge	33	Understand file system structures	2021-12-10 18:39:06.517883	2021-12-10 18:39:06.517884	Data persistence not CNO persistence	5f457f1e1ea90ba9adb32b11
K0163	Knowledge	33	Understand the boot process	2021-12-10 18:39:06.518917	2021-12-10 18:39:06.518919	Data persistence not CNO persistence	5f457f1e1ea90ba9adb32b52
K0164	Knowledge	33	Understand Inter Process Communication (IPC) fundamentals	2021-12-10 18:39:06.519907	2021-12-10 18:39:06.519909	Both Windows and Linux	5f457f1e1ea90ba9adb32b0f
K0165	Knowledge	33	Describe shared memory utilization 	2021-12-10 18:39:06.520881	2021-12-10 18:39:06.520882	Both Windows and Linux	5f457f1e1ea90ba9adb32b26
K0166	Knowledge	33	Describe output redirection using pipes	2021-12-10 18:39:06.521878	2021-12-10 18:39:06.521879	Both Windows and Linux	5f457f1e1ea90ba9adb32b2b
K0168	Knowledge	33	Understand operating system memory hierarchy	2021-12-10 18:39:06.522828	2021-12-10 18:39:06.522829		5f457f1e1ea90ba9adb32b10
K0170	Knowledge	33	Describe virtual memory utilization 	2021-12-10 18:39:06.523797	2021-12-10 18:39:06.523798		5f457f1e1ea90ba9adb32b12
K0171	Knowledge	33	Understand operating system virtualization concepts	2021-12-10 18:39:06.524587	2021-12-10 18:39:06.524588		5f457f1e1ea90ba9adb32b13
K0173	Knowledge	4	Understand how to implement a thread in multi-threading programs.	2021-12-10 18:39:06.52529	2021-12-10 18:39:06.52529		5f457f1e1ea90ba9adb32b37
K0174	Knowledge	4	Understand how to implement a pthread in multi-threading programs.	2021-12-10 18:39:06.525804	2021-12-10 18:39:06.525805		5f457f1e1ea90ba9adb32b21
K0175	Knowledge	4	Understand how to implement a fork in multi-threading programs.	2021-12-10 18:39:06.526386	2021-12-10 18:39:06.526387		5f457f1e1ea90ba9adb32b14
K0176	Knowledge	4	Understand how to implement a join in multi-threading programs.	2021-12-10 18:39:06.526986	2021-12-10 18:39:06.526987		5f457f1e1ea90ba9adb32b24
K0178	Knowledge	4	Understand how to exit a thread in multi-threading programs.	2021-12-10 18:39:06.527572	2021-12-10 18:39:06.527573		5f457f1e1ea90ba9adb32b18
K0179	Knowledge	4	Understand how to detach from a thread in multi-threading programs.	2021-12-10 18:39:06.528192	2021-12-10 18:39:06.528193		5f457f1e1ea90ba9adb32b17
K0180	Knowledge	4	Understand how to implement a mutex in multi-threading programs.	2021-12-10 18:39:06.528765	2021-12-10 18:39:06.528765		5f457f1e1ea90ba9adb32b2c
K0181	Knowledge	4	Understand how to implement a semaphore in multi-threading programs.	2021-12-10 18:39:06.529358	2021-12-10 18:39:06.529359		5f457f1e1ea90ba9adb32b19
K0182	Knowledge	4	Understand how to handle race conditions in multi-threading programs.	2021-12-10 18:39:06.529938	2021-12-10 18:39:06.529939		5f457f1e1ea90ba9adb32b1a
K0183	Knowledge	4	Understand how to handle a deadlock in multi-threading programs.	2021-12-10 18:39:06.530578	2021-12-10 18:39:06.530578		5f457f1e1ea90ba9adb32b1b
K0184	Knowledge	4	Understand how to implement a thread-safe multi-threading program.	2021-12-10 18:39:06.53116	2021-12-10 18:39:06.53116		5f457f1e1ea90ba9adb32b1c
K0185	Knowledge	4	Understand how to obtain the thread id of a thread.	2021-12-10 18:39:06.531809	2021-12-10 18:39:06.531811		5f457f1e1ea90ba9adb32b1d
K0187	Knowledge	4	Describe use of common string handling functions	2021-12-10 18:39:06.53283	2021-12-10 18:39:06.532831	strlen, strncpy, strnstr etc.	5f457f1e1ea90ba9adb32b1e
K0188	Knowledge	4	Describe which functions guarantee null terminated strings	2021-12-10 18:39:06.533785	2021-12-10 18:39:06.533786		5f457f1e1ea90ba9adb32b1f
K0189	Knowledge	18	Understand how to prevent an off-by-one error	2021-12-10 18:39:06.534738	2021-12-10 18:39:06.534739		5f457f1e1ea90ba9adb32b2a
K0190	Knowledge	18	Understand how to prevent an integer overflow	2021-12-10 18:39:06.535618	2021-12-10 18:39:06.535619		5f457f1e1ea90ba9adb32b2d
K0191	Knowledge	4	Understand how to prevent a buffer overflow	2021-12-10 18:39:06.536619	2021-12-10 18:39:06.536621		5f457f1e1ea90ba9adb32b31
K0192	Knowledge	18	Understand how to prevent use-after-free errors.	2021-12-10 18:39:06.537761	2021-12-10 18:39:06.537763		5f457f1e1ea90ba9adb32b2e
K0193	Knowledge	18	Understand the concept of resource acquisition is initialization (RAII)	2021-12-10 18:39:06.538861	2021-12-10 18:39:06.538862		5f457f1e1ea90ba9adb32b2f
K0194	Knowledge	18	Differentiate between a regular expression and a context-free grammar.	2021-12-10 18:39:06.53991	2021-12-10 18:39:06.539911		5f457f1e1ea90ba9adb32b30
K0195	Knowledge	18	Understand the difference between input validation vs input sanitization.	2021-12-10 18:39:06.540948	2021-12-10 18:39:06.540949		5f457f1e1ea90ba9adb32b34
K0196	Knowledge	18	Describe pure functions and function side-effects	2021-12-10 18:39:06.54199	2021-12-10 18:39:06.541991		5f457f1e1ea90ba9adb32b32
K0197	Knowledge	18	Understand low-level crypto basics	2021-12-10 18:39:06.543047	2021-12-10 18:39:06.543048		5f457f1e1ea90ba9adb32b33
K0198	Knowledge	18	Describe characteristics of a secure cryptographic hash	2021-12-10 18:39:06.54408	2021-12-10 18:39:06.544081		5f457f1e1ea90ba9adb32b36
K0201	Knowledge	11	Describe specifics about the x86 architecture.	2021-12-10 18:39:06.545099	2021-12-10 18:39:06.5451	Needs additional section	5f457f1e1ea90ba9adb32b38
K0202	Knowledge	11	Describe specifics about the x86-64 architecture	2021-12-10 18:39:06.546111	2021-12-10 18:39:06.546112	Needs additional section	5f457f1e1ea90ba9adb32b41
K0203	Knowledge	11	Understand how to use the stack.	2021-12-10 18:39:06.547119	2021-12-10 18:39:06.547121	Needs additional section	5f457f1e1ea90ba9adb32b3a
K0204	Knowledge	11	Differentiate between types of memory (registers, cache, system memory)	2021-12-10 18:39:06.548144	2021-12-10 18:39:06.548145	Needs additional section	5f457f1e1ea90ba9adb32b4d
K0205	Knowledge	11	Describe use of virtual memory	2021-12-10 18:39:06.549155	2021-12-10 18:39:06.549156	Needs additional section	5f457f1e1ea90ba9adb32b39
K0206	Knowledge	11	Understand the purpose of an instruction/opcode.	2021-12-10 18:39:06.550075	2021-12-10 18:39:06.550076	Needs additional section	5f457f1e1ea90ba9adb32b43
K0207	Knowledge	11	Know what an operand is as a part of an instruction.	2021-12-10 18:39:06.551053	2021-12-10 18:39:06.551054	Needs additional section	5f457f1e1ea90ba9adb32b4c
K0209	Knowledge	11	Understand the purpose of an assembler.	2021-12-10 18:39:06.552049	2021-12-10 18:39:06.55205	Needs additional section	5f457f1e1ea90ba9adb32b49
K0210	Knowledge	11	Understand the term endianness and its effect on data.	2021-12-10 18:39:06.553087	2021-12-10 18:39:06.553088	Needs additional section	5f457f1e1ea90ba9adb32b3b
K0211	Knowledge	11	Describe how to find two's complement of a binary number.	2021-12-10 18:39:06.554045	2021-12-10 18:39:06.554046	Needs additional section	5f457f1e1ea90ba9adb32b3c
K0213	Knowledge	11	Identify and describe the 64-bit registers	2021-12-10 18:39:06.554978	2021-12-10 18:39:06.55498		5f457f1e1ea90ba9adb32b3d
K0214	Knowledge	11	Identify and describe the 32-bit registers	2021-12-10 18:39:06.555978	2021-12-10 18:39:06.555979		5f457f1e1ea90ba9adb32b3e
K0215	Knowledge	11	Identify and describe the lower 16-bit registers	2021-12-10 18:39:06.55682	2021-12-10 18:39:06.556821		5f457f1e1ea90ba9adb32b3f
K0216	Knowledge	11	Identify and describe th high 8-bit registers	2021-12-10 18:39:06.557591	2021-12-10 18:39:06.557592		5f457f1e1ea90ba9adb32b40
K0217	Knowledge	11	Identify and describe the lower 8-bit registers	2021-12-10 18:39:06.558439	2021-12-10 18:39:06.55844	Needs additional section	5f457f1e1ea90ba9adb32b42
K0218	Knowledge	11	Describe the purpose and use of access memory and how to dereference addresses.	2021-12-10 18:39:06.559116	2021-12-10 18:39:06.559116		5f457f1e1ea90ba9adb32b46
K0219	Knowledge	11	Data sizes	2021-12-10 18:39:06.559711	2021-12-10 18:39:06.559712		5f457f1e1ea90ba9adb32b45
K0220	Knowledge	11	Identify the underlying restrictions to low level memory access	2021-12-10 18:39:06.560569	2021-12-10 18:39:06.56057		5f457f1e1ea90ba9adb32b44
K0221	Knowledge	11	Iteration of consecutive memory addresses	2021-12-10 18:39:06.561442	2021-12-10 18:39:06.561443		5f457f1e1ea90ba9adb32b48
K0222	Knowledge	11	Structures	2021-12-10 18:39:06.562301	2021-12-10 18:39:06.562302		5f457f1e1ea90ba9adb32b47
K0223	Knowledge	11	With references and required resources, describe the purpose, use, and concepts of the NASM assembler.	2021-12-10 18:39:06.563103	2021-12-10 18:39:06.563104		5f457f1e1ea90ba9adb32b4a
K0224	Knowledge	11	Generating opcodes	2021-12-10 18:39:06.563874	2021-12-10 18:39:06.563875		5f457f1e1ea90ba9adb32b4b
K0225	Knowledge	11	How the assembler works	2021-12-10 18:39:06.56489	2021-12-10 18:39:06.564891		5f457f1e1ea90ba9adb32b5a
K0226	Knowledge	11	Differences between assemblers	2021-12-10 18:39:06.565745	2021-12-10 18:39:06.565746		5f457f1e1ea90ba9adb32b4f
K0227	Knowledge	11	Describe the concepts, use, and terms associated with the process stack.	2021-12-10 18:39:06.566665	2021-12-10 18:39:06.566667		5f457f1e1ea90ba9adb32b50
K0228	Knowledge	11	Stack frame	2021-12-10 18:39:06.567525	2021-12-10 18:39:06.567527		5f457f1e1ea90ba9adb32b51
K0229	Knowledge	11	Push/pop/ret	2021-12-10 18:39:06.56848	2021-12-10 18:39:06.568481		5f457f1e1ea90ba9adb32b63
K0230	Knowledge	11	Manual allocation/deallocation	2021-12-10 18:39:06.569574	2021-12-10 18:39:06.569575		5f457f1e1ea90ba9adb32b4e
K0231	Knowledge	11	Relationship to functions	2021-12-10 18:39:06.570624	2021-12-10 18:39:06.570625		5f457f1e1ea90ba9adb32b73
K0232	Knowledge	11	With references and required resources, describe the purpose and use of functions with respect to use in assembly.	2021-12-10 18:39:06.571639	2021-12-10 18:39:06.571641		5f457f1e1ea90ba9adb32b61
K0233	Knowledge	11	Calling conventions	2021-12-10 18:39:06.572702	2021-12-10 18:39:06.572703		5f457f1e1ea90ba9adb32b57
K0234	Knowledge	11	Arguments in relation to stack	2021-12-10 18:39:06.57376	2021-12-10 18:39:06.573762		5f457f1e1ea90ba9adb32b60
K0235	Knowledge	11	Call stack handling	2021-12-10 18:39:06.5748	2021-12-10 18:39:06.574801		5f457f1e1ea90ba9adb32b55
K0236	Knowledge	11	Name mangling	2021-12-10 18:39:06.575803	2021-12-10 18:39:06.575804		5f457f1e1ea90ba9adb32b62
K0237	Knowledge	11	With references and required resources, describe the purpose and use of the various system flag registers.	2021-12-10 18:39:06.57679	2021-12-10 18:39:06.576792		5f457f1e1ea90ba9adb32b68
K0238	Knowledge	11	How assembly programs set flags	2021-12-10 18:39:06.577806	2021-12-10 18:39:06.577808		5f457f1e1ea90ba9adb32b6b
K0239	Knowledge	11	How to manually set the flags	2021-12-10 18:39:06.578791	2021-12-10 18:39:06.578792		5f457f1e1ea90ba9adb32b65
K0240	Knowledge	11	Relation to conditional branch instructions	2021-12-10 18:39:06.579757	2021-12-10 18:39:06.579758		5f457f1e1ea90ba9adb32b69
K0241	Knowledge	11	With references and required resources, describe the purpose and use of system interrupts.	2021-12-10 18:39:06.5807	2021-12-10 18:39:06.580701		5f457f1e1ea90ba9adb32b6e
K0242	Knowledge	11	System interrupts	2021-12-10 18:39:06.581681	2021-12-10 18:39:06.581683		5f457f1e1ea90ba9adb32b75
K0243	Knowledge	11	With references and required resources, describe the purpose, use, and terms of operating system calls (syscall) to request kernel mode services.	2021-12-10 18:39:06.582702	2021-12-10 18:39:06.582703	e.g. sysenter/sysexit, sys_read, sys_write, sys_open, sys_mmap, sys_brk, sys_exit	5f457f1e1ea90ba9adb32b54
K0252	Knowledge	11	With references and required resources, describe the purpose and use of memory segmentation and how a system or process' memory is laid into a section.	2021-12-10 18:39:06.583679	2021-12-10 18:39:06.58368		5f457f1e1ea90ba9adb32b53
K0253	Knowledge	11	Memory segmentation/process memory	2021-12-10 18:39:06.584772	2021-12-10 18:39:06.584773		5f457f1e1ea90ba9adb32b58
K0254	Knowledge	11	With references and required resources, describe the purpose and use of SIMD instructions. Utilize use of registers and instructions that can operate on multiple data items at once.	2021-12-10 18:39:06.585769	2021-12-10 18:39:06.58577		5f457f1e1ea90ba9adb32b56
K0255	Knowledge	11	XMM0-XMM7	2021-12-10 18:39:06.586719	2021-12-10 18:39:06.58672		5f457f1e1ea90ba9adb32b5e
K0256	Knowledge	12	With references and required resources, describe the purpose and use of IDA Pro additional features.	2021-12-10 18:39:06.587525	2021-12-10 18:39:06.587526		5f457f1e1ea90ba9adb32b59
K0257	Knowledge	12	IDA extensions	2021-12-10 18:39:06.58853	2021-12-10 18:39:06.588531		5f457f1e1ea90ba9adb32b5b
K0258	Knowledge	12	IDA scripts via Python	2021-12-10 18:39:06.589477	2021-12-10 18:39:06.589478		5f457f1e1ea90ba9adb32b5c
K0259	Knowledge	12	Advanced IDA features	2021-12-10 18:39:06.590366	2021-12-10 18:39:06.590367		5f457f1e1ea90ba9adb32b5f
K0260	Knowledge	2	Identify steps to create or refine a product backlog item	2021-12-10 18:39:06.591141	2021-12-10 18:39:06.591142		5f457f1e1ea90ba9adb32b5d
K0261	Knowledge	2	Describe the Agile Developent Concept	2021-12-10 18:39:06.591775	2021-12-20 16:53:54.371421		5f457f1e1ea90ba9adb32b64
K0262	Knowledge	2	Describe Scrum Terms and Methodology	2021-12-10 18:39:06.592723	2021-12-20 16:58:09.249074		5f457f1e1ea90ba9adb32b66
K0263	Knowledge	34	Describe Development Operations (DevOps)	2021-12-10 18:39:06.593818	2021-12-10 18:39:06.593819		5f457f1e1ea90ba9adb32b67
K0264	Knowledge	34	Describe the required elements of package delivery	2021-12-10 18:39:06.594716	2021-12-10 18:39:06.594718		5f457f1e1ea90ba9adb32b6a
K0265	Knowledge	35	Describe the functionality of driver kits	2021-12-10 18:39:06.595796	2021-12-10 18:39:06.595797	Windows Driver Kit (WDK); Device Driver Kit (DDK)	5f457f1e1ea90ba9adb32b6c
K0268	Knowledge	36	Describe calling conventions used in Windows API (32it & 64bit)	2021-12-10 18:39:06.597065	2021-12-10 18:39:06.597067	Stdcall; cdecl; fastcall; thiscall	5f457f1e1ea90ba9adb32b6f
K0273	Knowledge	36	Understand the different Windows APIs	2021-12-10 18:39:06.598245	2021-12-10 18:39:06.598247	Describe the Nt API; Describe the Zw API, Describe the Rtl API; Describe the Ldr API	5f457f1e1ea90ba9adb32b70
K0278	Knowledge	36	Understand naming conventions and macro theory associated with Windows API	2021-12-10 18:39:06.59939	2021-12-10 18:39:06.599391	LPCTSTR decoded...; Describe the date/time convention; Describe how using Unicode affects Windows API function calls and data types; Describe the Windows API macro theory; Describe the Windows naming;  conventions; Describe Windows data types	5f457f1e1ea90ba9adb32b71
K0284	Knowledge	36	Understand the Windows Registry	2021-12-10 18:39:06.600458	2021-12-10 18:39:06.600459	Describe registry key; Describe registry hive	5f457f1e1ea90ba9adb32b6d
K0287	Knowledge	37	Understand the Linux configuration startup	2021-12-10 18:39:06.601739	2021-12-10 18:39:06.60174	etc, init, rcsripts, systemd	5f457f1e1ea90ba9adb32b72
K0292	Knowledge	37	Understand process fundamentals in Linux	2021-12-10 18:39:06.60276	2021-12-10 18:39:06.602761	parentage, daemons, resource limits	5f457f1e1ea90ba9adb32b7c
K0296	Knowledge	37	Understand signal fundamentals in Linux	2021-12-10 18:39:06.603773	2021-12-10 18:39:06.603774	ignore, catch, handle, default actions, supported signals	5f457f1e1ea90ba9adb32b7b
K0302	Knowledge	31	Knowledge of computer networking concepts and protocols, and network security methodologies. 	2021-12-10 18:39:06.604929	2021-12-10 18:39:06.60493	SP-DEV-001	5f457f1e1ea90ba9adb32bbd
K0303	Knowledge	38	Knowledge of risk management processes (e.g., methods for assessing and mitigating risk).	2021-12-10 18:39:06.606165	2021-12-10 18:39:06.606166	SP-DEV-001	5f457f1e1ea90ba9adb32bb0
K0304	Knowledge	39	Knowledge of laws, regulations, policies, and ethics as they relate to cybersecurity and privacy. 	2021-12-10 18:39:06.607366	2021-12-10 18:39:06.607367	SP-DEV-001	5f457f1e1ea90ba9adb32b89
K0305	Knowledge	40	Knowledge of cybersecurity and privacy principles.	2021-12-10 18:39:06.60855	2021-12-10 18:39:06.608551	SP-DEV-001	5f457f1e1ea90ba9adb32b7e
K0306	Knowledge	41	Knowledge of cyber threats and vulnerabilities. 	2021-12-10 18:39:06.609723	2021-12-10 18:39:06.609725	SP-DEV-001	5f457f1e1ea90ba9adb32b84
K0307	Knowledge	41	Knowledge of specific operational impacts of cybersecurity lapses.	2021-12-10 18:39:06.610705	2021-12-10 18:39:06.610707	SP-DEV-001	5f457f1e1ea90ba9adb32b7f
K0308	Knowledge	16	Knowledge of complex data structures.	2021-12-10 18:39:06.611728	2021-12-10 18:39:06.611729	SP-DEV-001	5f457f1e1ea90ba9adb32b86
K0309	Knowledge	42	Knowledge of computer programming principles 	2021-12-10 18:39:06.612908	2021-12-10 18:39:06.61291	SP-DEV-001	5f457f1e1ea90ba9adb32b80
K0310	Knowledge	38	Knowledge of organization's enterprise information security architecture.	2021-12-10 18:39:06.613953	2021-12-10 18:39:06.613955	SP-DEV-001	5f457f1e1ea90ba9adb32b82
K0311	Knowledge	38	Knowledge of organization's evaluation and validation requirements.	2021-12-10 18:39:06.614937	2021-12-10 18:39:06.614938	SP-DEV-001	5f457f1e1ea90ba9adb32b8b
K0312	Knowledge	40	Knowledge of cybersecurity and privacy principles and methods that apply to software development.	2021-12-10 18:39:06.61595	2021-12-10 18:39:06.615951	SP-DEV-001	5f457f1e1ea90ba9adb32b8c
K0313	Knowledge	40	Knowledge of cybersecurity and privacy principles and organizational requirements (relevant to confidentiality, integrity, availability, authentication, non-repudiation).	2021-12-10 18:39:06.616949	2021-12-10 18:39:06.61695	SP-DEV-001	5f457f1e1ea90ba9adb32b81
K0314	Knowledge	31	Knowledge of local area and wide area networking principles and concepts including bandwidth management.	2021-12-10 18:39:06.617748	2021-12-10 18:39:06.617749	SP-DEV-001	5f457f1e1ea90ba9adb32b83
K0315	Knowledge	7	Knowledge of low-level computer languages (e.g., assembly languages).	2021-12-10 18:39:06.618542	2021-12-10 18:39:06.618543	SP-DEV-001	5f457f1e1ea90ba9adb32baf
K0316	Knowledge	33	Knowledge of operating systems.	2021-12-10 18:39:06.619318	2021-12-10 18:39:06.619319	SP-DEV-001	5f457f1e1ea90ba9adb32b85
K0317	Knowledge	40	Knowledge of Privacy Impact Assessments.	2021-12-10 18:39:06.620092	2021-12-10 18:39:06.620092	SP-DEV-001	5f457f1e1ea90ba9adb32b88
K0318	Knowledge	42	Knowledge of programming language structures and logic.	2021-12-10 18:39:06.620871	2021-12-10 18:39:06.620872	SP-DEV-001	5f457f1e1ea90ba9adb32b87
K0319	Knowledge	41	Knowledge of system and application security threats and vulnerabilities (e.g., buffer overflow, mobile code, cross-site scripting, Procedural Language/Structured Query Language [PL/SQL] and injections, race conditions, covert channel, replay, return-oriented attacks, malicious code).	2021-12-10 18:39:06.621643	2021-12-10 18:39:06.621644	SP-DEV-001	5f457f1e1ea90ba9adb32b8a
K0320	Knowledge	43	Knowledge of secure configuration management techniques. (e.g., Security Technical Implementation Guides (STIGs), cybersecurity best practices on cisecurity.org).	2021-12-10 18:39:06.622759	2021-12-10 18:39:06.62276	SP-DEV-001	5f457f1e1ea90ba9adb32b90
K0321	Knowledge	14	Knowledge of software debugging principles.	2021-12-10 18:39:06.623623	2021-12-10 18:39:06.623624	SP-DEV-001	5f457f1e1ea90ba9adb32b8d
K0322	Knowledge	42	Knowledge of software design tools, methods, and techniques.	2021-12-10 18:39:06.624488	2021-12-10 18:39:06.624489	SP-DEV-001	5f457f1e1ea90ba9adb32b8f
K0323	Knowledge	44	Knowledge of software development models (e.g., Waterfall Model, Spiral Model).	2021-12-10 18:39:06.625511	2021-12-10 18:39:06.625513	SP-DEV-001	5f457f1e1ea90ba9adb32b8e
K0324	Knowledge	42	Knowledge of software engineering.	2021-12-10 18:39:06.626366	2021-12-10 18:39:06.626367	SP-DEV-001	5f457f1e1ea90ba9adb32bb4
K0325	Knowledge	44	Knowledge of structured analysis principles and methods.	2021-12-10 18:39:06.627342	2021-12-10 18:39:06.627342	SP-DEV-001	5f457f1e1ea90ba9adb32bb3
K0326	Knowledge	42	Knowledge of system design tools, methods, and techniques, including automated systems analysis and design tools.	2021-12-10 18:39:06.628307	2021-12-10 18:39:06.628308	SP-DEV-001	5f457f1e1ea90ba9adb32bb2
K0327	Knowledge	45	Knowledge of web services (e.g., service-oriented architecture, Simple Object Access Protocol, and web service description language).	2021-12-10 18:39:06.629361	2021-12-10 18:39:06.629361	SP-DEV-001	5f457f1e1ea90ba9adb32bb9
K0328	Knowledge	42	Knowledge of interpreted and compiled computer languages.	2021-12-10 18:39:06.630204	2021-12-10 18:39:06.630204	SP-DEV-001	5f457f1e1ea90ba9adb32bb6
K0329	Knowledge	18	Knowledge of secure coding techniques.	2021-12-10 18:39:06.631061	2021-12-10 18:39:06.631063	SP-DEV-001	5f457f1e1ea90ba9adb32bba
K0330	Knowledge	45	Knowledge of software related information technology (IT) security principles and methods (e.g., modularization, layering, abstraction, data hiding, simplicity/minimization).	2021-12-10 18:39:06.632151	2021-12-10 18:39:06.632153	SP-DEV-001	5f457f1e1ea90ba9adb32bd0
K0331	Knowledge	46	Knowledge of software quality assurance process.	2021-12-10 18:39:06.633494	2021-12-10 18:39:06.633495	SP-DEV-001	5f457f1e1ea90ba9adb32b91
K0332	Knowledge	38	Knowledge of supply chain risk management standards, processes, and practices.	2021-12-10 18:39:06.634399	2021-12-10 18:39:06.6344	SP-DEV-001	5f457f1e1ea90ba9adb32b94
K0333	Knowledge	47	Knowledge of critical infrastructure systems with information communication technology that were designed without system security considerations.	2021-12-10 18:39:06.635388	2021-12-10 18:39:06.635389	SP-DEV-001	5f457f1e1ea90ba9adb32b93
K0334	Knowledge	45	Knowledge of network security architecture concepts including topology, protocols, components, and principles (e.g., application of defense-in-depth).	2021-12-10 18:39:06.636221	2021-12-10 18:39:06.636222	SP-DEV-001	5f457f1e1ea90ba9adb32b96
K0335	Knowledge	45	Knowledge of security architecture concepts and enterprise architecture reference models (e.g., Zachman, Federal Enterprise Architecture [FEA]).	2021-12-10 18:39:06.636998	2021-12-10 18:39:06.636999	SP-DEV-001	5f457f1e1ea90ba9adb32b97
K0336	Knowledge	31	Knowledge of the application firewall concepts and functions (e.g., Single point of authentication/audit/policy enforcement, message scanning for malicious content, data anonymization for PCI and PII compliance, data loss protection scanning, accelerated cryptographic operations, SSL security, REST/JSON processing).	2021-12-10 18:39:06.63778	2021-12-10 18:39:06.63778	SP-DEV-001	5f457f1e1ea90ba9adb32b99
K0337	Knowledge	19	Knowledge of Personally Identifiable Information (PII) data security standards.	2021-12-10 18:39:06.638558	2021-12-10 18:39:06.638559	SP-DEV-001	5f457f1e1ea90ba9adb32b98
K0338	Knowledge	19	Knowledge of Payment Card Industry (PCI) data security standards.	2021-12-10 18:39:06.639316	2021-12-10 18:39:06.639317	SP-DEV-001	5f457f1e1ea90ba9adb32b9b
K0339	Knowledge	19	Knowledge of Personal Health Information (PHI) data security standards.	2021-12-10 18:39:06.640261	2021-12-10 18:39:06.640262	SP-DEV-001	5f457f1e1ea90ba9adb32b9a
K0340	Knowledge	38	Knowledge of information technology (IT) risk management policies, requirements, and procedures.	2021-12-10 18:39:06.641191	2021-12-10 18:39:06.641193	SP-DEV-001	5f457f1e1ea90ba9adb32b9c
K0341	Knowledge	24	Knowledge of embedded systems.	2021-12-10 18:39:06.642024	2021-12-10 18:39:06.642025	SP-DEV-001	5f457f1e1ea90ba9adb32b9e
K0342	Knowledge	31	Knowledge of network protocols such as TCP/IP, Dynamic Host Configuration, Domain Name System (DNS), and directory services.	2021-12-10 18:39:06.642866	2021-12-10 18:39:06.642867	SP-DEV-001	5f457f1e1ea90ba9adb32b9f
K0343	Knowledge	48	Knowledge of penetration testing principles, tools, and techniques.	2021-12-10 18:39:06.643923	2021-12-10 18:39:06.643924	SP-DEV-001	5f457f1e1ea90ba9adb32ba2
K0344	Knowledge	38	Knowledge of root cause analysis techniques.	2021-12-10 18:39:06.644769	2021-12-10 18:39:06.64477	SP-DEV-001	5f457f1e1ea90ba9adb32ba1
K0345	Knowledge	41	Knowledge of Application Security Risks (e.g. Open Web Application Security Project Top 10 list)	2021-12-10 18:39:06.645655	2021-12-10 18:39:06.645656	SP-DEV-001	5f457f1e1ea90ba9adb32ba3
K0361	Knowledge	49	Explain the relationships between users stories, acceptance criteria, and the definition of done.	2021-12-10 18:39:06.646643	2021-12-20 16:58:45.370768		5f457f1e1ea90ba9adb32ed1
K0366	Knowledge	2	Describe the responsibilities of the Product Owner.	2021-12-10 18:39:06.647198	2021-12-10 18:39:06.647198		5f457f1e1ea90ba9adb32bab
K0371	Knowledge	2	Describe techniques to collaborate with the key stakeholders.	2021-12-10 18:39:06.648515	2021-12-10 18:39:06.648516		5f457f1e1ea90ba9adb32ba7
K0372	Knowledge	2	Understand decision-making approaches a Product Owner might use.	2021-12-10 18:39:06.649072	2021-12-10 18:39:06.649073		5f457f1e1ea90ba9adb32ba8
K0374	Knowledge	2	Identify the different types of project stakeholders.	2021-12-10 18:39:06.649911	2021-12-10 18:39:06.649912		5f457f1e1ea90ba9adb32ba9
K0377	Knowledge	2	Identify alternatives to open discussion.	2021-12-10 18:39:06.650541	2021-12-10 18:39:06.650542		5f457f1e1ea90ba9adb32bb1
K0383	Knowledge	2	Define technical debt and explain why the Product Owner should be cautious about accumulating technical debt.	2021-12-10 18:39:06.651174	2021-12-10 18:39:06.651175		5f457f1e1ea90ba9adb32bbb
K0392	Knowledge	2	Understand product vision.	2021-12-10 18:39:06.651736	2021-12-20 16:59:08.295299		5f457f1e1ea90ba9adb32bc4
K0395	Knowledge	2	Understand strategies for the incremental delivery of a product.	2021-12-10 18:39:06.652397	2021-12-10 18:39:06.652398		5f457f1e1ea90ba9adb32bc8
K0396	Knowledge	2	Understand techniques to plan product delivery over time.	2021-12-10 18:39:06.653188	2021-12-10 18:39:06.653189		5f457f1e1ea90ba9adb32bcb
K0402	Knowledge	2	Understand aspects of product discovery and identify how each contributes to successful product outcomes.	2021-12-10 18:39:06.653821	2021-12-10 18:39:06.653822		5f457f1e1ea90ba9adb32bd2
K0404	Knowledge	2	Identify benefits of Developers direct interactions.	2021-12-10 18:39:06.654603	2021-12-10 18:39:06.654604		5f457f1e1ea90ba9adb32bd1
K0415	Knowledge	2	Understand value and techniques to measure value.	2021-12-10 18:39:06.65528	2021-12-20 16:59:30.121011		5f457f1e1ea90ba9adb32bdd
K0419	Knowledge	2	Understand criteria for ordering the Product Backlog.	2021-12-10 18:39:06.655919	2021-12-10 18:39:06.65592		5f457f1e1ea90ba9adb32be3
K0420	Knowledge	2	Identify sources of Product Backlog items.	2021-12-10 18:39:06.656573	2021-12-10 18:39:06.656574		5f457f1e1ea90ba9adb32bde
K0422	Knowledge	2	Understand the pros and cons of a "just-in-time" approach for Product Backlog refinement vs. an "all-at-once" approach.	2021-12-10 18:39:06.65716	2021-12-10 18:39:06.657161		5f457f1e1ea90ba9adb32bec
K0452	Knowledge	2	Understand the three Scrum Team accountabilities.	2021-12-10 18:39:06.657812	2021-12-20 16:59:52.229738		5f457f1e1ea90ba9adb32be5
K0453	Knowledge	2	Describe the purpose, composition, and goal of each Agile event.	2021-12-10 18:39:06.65838	2021-12-20 17:00:17.349363	https://www.visual-paradigm.com/scrum/what-are-scrum-ceremonies/	5f457f1e1ea90ba9adb32be7
K0454	Knowledge	2	Understand the six scrum principles.	2021-12-10 18:39:06.658991	2021-12-20 17:00:31.97891		5f457f1e1ea90ba9adb32be6
K0455	Knowledge	2	Define scrum.	2021-12-10 18:39:06.659541	2021-12-20 17:00:39.211606	https://www.scrumguides.org/scrum-guide.html	5f457f1e1ea90ba9adb32be8
K0456	Knowledge	2	Understand the three Scrum pillars. (Transparency, inspection, and adaptation)	2021-12-10 18:39:06.660204	2021-12-20 17:00:49.741513	https://www.scrumguides.org/scrum-guide.html	5f457f1e1ea90ba9adb32bea
K0457	Knowledge	50	90 COS Mission and Vision	2021-12-10 18:39:06.661153	2021-12-10 18:39:06.661154		5f457f1e1ea90ba9adb32bf0
K0458	Knowledge	50	90 COS Organizational Structure	2021-12-10 18:39:06.661774	2021-12-10 18:39:06.661775		5f457f1e1ea90ba9adb32beb
K0459	Knowledge	50	Real-Time Operations and Innovation (RTO&I) Development Requirements	2021-12-10 18:39:06.662328	2021-12-10 18:39:06.662329		5f457f1e1ea90ba9adb32bee
K0460	Knowledge	50	Capability Development Initiation	2021-12-10 18:39:06.662866	2021-12-10 18:39:06.662867		5f457f1e1ea90ba9adb32bed
K0461	Knowledge	50	Flight Missions	2021-12-10 18:39:06.663373	2021-12-10 18:39:06.663374		5f457f1e1ea90ba9adb32bef
K0462	Knowledge	50	90 COS-Supported Organizations	2021-12-10 18:39:06.663946	2021-12-10 18:39:06.663947		5f457f1e1ea90ba9adb32bf1
K0463	Knowledge	50	Organizations Supporting 90 COS	2021-12-10 18:39:06.664708	2021-12-10 18:39:06.664709		5f457f1e1ea90ba9adb32bf6
K0464	Knowledge	50	Mission Area Responsibilities of Supported/Supporting Organizations	2021-12-10 18:39:06.665323	2021-12-10 18:39:06.665324		5f457f1e1ea90ba9adb32bf3
K0466	Knowledge	51	Cyber Mission Forces (CMF) Teams	2021-12-10 18:39:06.666097	2021-12-10 18:39:06.666097		5f457f1e1ea90ba9adb32bf2
K0467	Knowledge	51	90 COS CMF-Roles	2021-12-10 18:39:06.666648	2021-12-10 18:39:06.666649		5f457f1e1ea90ba9adb32bf5
K0468	Knowledge	51	Interaction Between CMF Teams	2021-12-10 18:39:06.667214	2021-12-10 18:39:06.667215		5f457f1e1ea90ba9adb32bf7
K0469	Knowledge	51	90 COS CMF Teams	2021-12-10 18:39:06.667897	2021-12-10 18:39:06.667898		5f457f1e1ea90ba9adb32bf8
K0470	Knowledge	51	90 COS CMF Team Back-Shop Support Requests/Receipt	2021-12-10 18:39:06.66874	2021-12-10 18:39:06.66874		5f457f1e1ea90ba9adb32bf9
K0473	Knowledge	52	Executive Order	2021-12-10 18:39:06.669628	2021-12-10 18:39:06.669629		5f457f1e1ea90ba9adb32bfb
K0474	Knowledge	52	DoD Directive	2021-12-10 18:39:06.670259	2021-12-10 18:39:06.67026		5f457f1e1ea90ba9adb32c01
K0475	Knowledge	52	NSA/CSS Policy	2021-12-10 18:39:06.670938	2021-12-10 18:39:06.670939		5f457f1e1ea90ba9adb32bfc
K0476	Knowledge	52	Federal Act	2021-12-10 18:39:06.67155	2021-12-10 18:39:06.671551		5f457f1e1ea90ba9adb32bfe
K0477	Knowledge	52	Signals Intelligence (SIGINT)	2021-12-10 18:39:06.672294	2021-12-10 18:39:06.672295		5f457f1e1ea90ba9adb32c02
K0478	Knowledge	52	USSID	2021-12-10 18:39:06.672956	2021-12-10 18:39:06.672957		5f457f1e1ea90ba9adb32bfd
K0479	Knowledge	52	Classification Markings	2021-12-10 18:39:06.673632	2021-12-10 18:39:06.673632		5f457f1e1ea90ba9adb32c00
K0480	Knowledge	52	United States Code Titles	2021-12-10 18:39:06.674224	2021-12-10 18:39:06.674225		5f457f1e1ea90ba9adb32c03
K0481	Knowledge	52	Legal Targeting	2021-12-10 18:39:06.674954	2021-12-10 18:39:06.674955		5f457f1e1ea90ba9adb32c04
K0483	Knowledge	53	Basic Administration Using The Graphic User Interface (GUI)	2021-12-10 18:39:06.675811	2021-12-10 18:39:06.675812		5f457f1e1ea90ba9adb32c06
K0484	Knowledge	53	Basic Administration Using Command Line (CLI)	2021-12-10 18:39:06.676439	2021-12-10 18:39:06.67644		5f457f1e1ea90ba9adb32c05
K0485	Knowledge	53	Registry	2021-12-10 18:39:06.677035	2021-12-10 18:39:06.677035		5f457f1e1ea90ba9adb32c07
K0486	Knowledge	53	Vectored Exception Handling	2021-12-10 18:39:06.677706	2021-12-10 18:39:06.677707		5f457f1e1ea90ba9adb32c08
K0487	Knowledge	54	User Mode Computer Network Operations (CNO) Overview	2021-12-10 18:39:06.678403	2021-12-10 18:39:06.678404		5f457f1e1ea90ba9adb32c09
K0488	Knowledge	55	Alternate Driver Architectures	2021-12-10 18:39:06.679087	2021-12-10 18:39:06.679088		5f457f1e1ea90ba9adb32c0d
K0489	Knowledge	55	Advanced Configuration and Power Interface (ACPI) Plug & Play	2021-12-10 18:39:06.679585	2021-12-10 18:39:06.679585		5f457f1e1ea90ba9adb32c0a
K0490	Knowledge	55	Direct Memory Access (DMA)	2021-12-10 18:39:06.680341	2021-12-10 18:39:06.680342		5f457f1e1ea90ba9adb32c0c
K0491	Knowledge	55	Windows Graphics Subsystem	2021-12-10 18:39:06.681065	2021-12-10 18:39:06.681066		5f457f1e1ea90ba9adb32c0f
K0492	Knowledge	55	Shadow System Service Dispatch Table (SSDT)	2021-12-10 18:39:06.681765	2021-12-10 18:39:06.681766		5f457f1e1ea90ba9adb32c0b
K0493	Knowledge	55	Windows Executive Systems	2021-12-10 18:39:06.682505	2021-12-10 18:39:06.682506		5f457f1e1ea90ba9adb32c16
K0494	Knowledge	55	Cygwin	2021-12-10 18:39:06.68315	2021-12-10 18:39:06.683151		5f457f1e1ea90ba9adb32c0e
K0496	Knowledge	56	Makefiles	2021-12-10 18:39:06.683983	2021-12-10 18:39:06.683983		5f457f1e1ea90ba9adb32c17
K0497	Knowledge	56	Special File Systems	2021-12-10 18:39:06.684728	2021-12-10 18:39:06.684729		5f457f1e1ea90ba9adb32c11
K0498	Knowledge	56	Filename Generation	2021-12-10 18:39:06.685446	2021-12-10 18:39:06.685447		5f457f1e1ea90ba9adb32c10
K0499	Knowledge	56	Boot Process and SYSV INIT	2021-12-10 18:39:06.686216	2021-12-10 18:39:06.686217		5f457f1e1ea90ba9adb32c12
K0500	Knowledge	57	Describe how to implement basic driver development	2021-12-10 18:39:06.687113	2021-12-10 18:39:06.687114		5f457f1e1ea90ba9adb32c14
K0501	Knowledge	57	Identify and describe kernel structures	2021-12-10 18:39:06.687698	2021-12-10 18:39:06.687699		5f457f1e1ea90ba9adb32c18
K0502	Knowledge	57	Understand how to utilize file system and drivers	2021-12-10 18:39:06.688292	2021-12-10 18:39:06.688293		5f457f1e1ea90ba9adb32c13
K0503	Knowledge	57	Input/Output Control (ioctl)	2021-12-10 18:39:06.688957	2021-12-10 18:39:06.688958		5f457f1e1ea90ba9adb32c19
K0504	Knowledge	57	Understand how to implement block and memory device drivers	2021-12-10 18:39:06.689541	2021-12-10 18:39:06.689542		5f457f1e1ea90ba9adb32c15
K0505	Knowledge	14	Describe common debugging techniques	2021-12-10 18:39:06.690471	2021-12-10 18:39:06.690472		5f457f1e1ea90ba9adb32c1a
K0507	Knowledge	57	Files System Management Utilities	2021-12-10 18:39:06.691199	2021-12-10 18:39:06.6912		5f457f1e1ea90ba9adb32c24
K0508	Knowledge	57	Variables	2021-12-10 18:39:06.691874	2021-12-10 18:39:06.691875		5f457f1e1ea90ba9adb32c37
K0509	Knowledge	57	Special Variables	2021-12-10 18:39:06.692484	2021-12-10 18:39:06.692485		5f457f1e1ea90ba9adb32c1d
K0510	Knowledge	57	Shell Features	2021-12-10 18:39:06.693274	2021-12-10 18:39:06.693275		5f457f1e1ea90ba9adb32c33
K0511	Knowledge	57	Monitoring & Troubleshooting	2021-12-10 18:39:06.694168	2021-12-10 18:39:06.694169		5f457f1e1ea90ba9adb32c2a
K0512	Knowledge	57	Pre-Installation Considerations/Analysis 	2021-12-10 18:39:06.694833	2021-12-10 18:39:06.694833		5f457f1e1ea90ba9adb32c32
K0513	Knowledge	57	Install Community Enterprise Operating System(CentOS)	2021-12-10 18:39:06.695505	2021-12-10 18:39:06.695506		5f457f1e1ea90ba9adb32c36
K0514	Knowledge	5	Describe how to use Python from the command line	2021-12-10 18:39:06.696228	2021-12-10 18:39:06.696229		5f457f1e1ea90ba9adb32c3b
K0515	Knowledge	58	Virtual Functions	2021-12-10 18:39:06.697068	2021-12-10 18:39:06.697069		5f457f1e1ea90ba9adb32c1e
K0516	Knowledge	58	Virtual Classes	2021-12-10 18:39:06.697776	2021-12-10 18:39:06.697776		5f457f1e1ea90ba9adb32c1b
K0517	Knowledge	58	Polymorphism	2021-12-10 18:39:06.698467	2021-12-10 18:39:06.698468		5f457f1e1ea90ba9adb32c1c
K0518	Knowledge	58	Operator Overloading	2021-12-10 18:39:06.699206	2021-12-10 18:39:06.699206		5f457f1e1ea90ba9adb32c1f
K0519	Knowledge	58	Inheritance	2021-12-10 18:39:06.699877	2021-12-10 18:39:06.699878		5f457f1e1ea90ba9adb32c26
K0520	Knowledge	58	Templates	2021-12-10 18:39:06.700599	2021-12-10 18:39:06.700599		5f457f1e1ea90ba9adb32c20
K0521	Knowledge	58	History	2021-12-10 18:39:06.701427	2021-12-10 18:39:06.701428		5f457f1e1ea90ba9adb32c21
K0524	Knowledge	11	Syscall	2021-12-10 18:39:06.702196	2021-12-10 18:39:06.702196		5f457f1e1ea90ba9adb32c25
K0525	Knowledge	11	Sysenter	2021-12-10 18:39:06.702894	2021-12-10 18:39:06.702895		5f457f1e1ea90ba9adb32c22
K0526	Knowledge	11	Interrupts	2021-12-10 18:39:06.703447	2021-12-10 18:39:06.703448		5f457f1e1ea90ba9adb32c2f
K0527	Knowledge	11	Memory segmentation	2021-12-10 18:39:06.704015	2021-12-10 18:39:06.704016		5f457f1e1ea90ba9adb32c23
K0528	Knowledge	11	Describe how to use x86 64 Calling Conventions	2021-12-10 18:39:06.704583	2021-12-10 18:39:06.704584		5f457f1e1ea90ba9adb32c3a
K0529	Knowledge	11	Single Instruction Multiple Data (SIMD)	2021-12-10 18:39:06.705149	2021-12-10 18:39:06.705149		5f457f1e1ea90ba9adb32c28
K0530	Knowledge	11	Privilege Levels	2021-12-10 18:39:06.705714	2021-12-10 18:39:06.705714		5f457f1e1ea90ba9adb32c27
K0532	Knowledge	59	Filter file contents	2021-12-10 18:39:06.706402	2021-12-10 18:39:06.706402		5f457f1e1ea90ba9adb32c29
K0533	Knowledge	59	Pipe output	2021-12-10 18:39:06.706936	2021-12-10 18:39:06.706936		5f457f1e1ea90ba9adb32c2b
K0534	Knowledge	59	PS1 file, interactive prompt	2021-12-10 18:39:06.707435	2021-12-10 18:39:06.707435		5f457f1e1ea90ba9adb32c2d
K0535	Knowledge	59	Rules: protection of scripts (execution prevention)	2021-12-10 18:39:06.70826	2021-12-10 18:39:06.708261		5f457f1e1ea90ba9adb32c2c
K0536	Knowledge	59	.Net Compilation within PowerShell (needs .Net Installed)	2021-12-10 18:39:06.70902	2021-12-10 18:39:06.70902		5f457f1e1ea90ba9adb32c2e
K0537	Knowledge	60	Functions	2021-12-10 18:39:06.709959	2021-12-10 18:39:06.70996		5f457f1e1ea90ba9adb32c30
K0538	Knowledge	60	Params	2021-12-10 18:39:06.710532	2021-12-10 18:39:06.710533		5f457f1e1ea90ba9adb32c34
K0539	Knowledge	60	Variables	2021-12-10 18:39:06.711157	2021-12-10 18:39:06.711158		5f457f1e1ea90ba9adb32c31
K0540	Knowledge	60	Data Types	2021-12-10 18:39:06.711724	2021-12-10 18:39:06.711724		5f457f1e1ea90ba9adb32c35
K0541	Knowledge	60	Data Structures	2021-12-10 18:39:06.712316	2021-12-10 18:39:06.712317		5f457f1e1ea90ba9adb32c38
K0542	Knowledge	60	Flow Control	2021-12-10 18:39:06.713006	2021-12-10 18:39:06.713007		5f457f1e1ea90ba9adb32c3c
K0543	Knowledge	60	Filtering/matching	2021-12-10 18:39:06.71363	2021-12-10 18:39:06.713631		5f457f1e1ea90ba9adb32c3d
K0544	Knowledge	60	Get-Help, Get-Member, Get-Command, Get-Cmdlet, etc.	2021-12-10 18:39:06.714327	2021-12-10 18:39:06.714327		5f457f1e1ea90ba9adb32c3f
K0545	Knowledge	60	Exception Handling	2021-12-10 18:39:06.714921	2021-12-10 18:39:06.714922		5f457f1e1ea90ba9adb32c3e
K0546	Knowledge	60	Closures, Lambdas, Script Blocks	2021-12-10 18:39:06.71563	2021-12-10 18:39:06.715631		5f457f1e1ea90ba9adb32c41
K0547	Knowledge	60	Interaction Common Information Model /Windows Management Instrumentation	2021-12-10 18:39:06.716255	2021-12-10 18:39:06.716256		5f457f1e1ea90ba9adb32c40
K0548	Knowledge	60	Managing Remote Systems via PowerShell	2021-12-10 18:39:06.716886	2021-12-10 18:39:06.716886		5f457f1e1ea90ba9adb32c43
K0549	Knowledge	60	PowerShell and Dynamic Link Libraries (DLLs)	2021-12-10 18:39:06.717723	2021-12-10 18:39:06.717724		5f457f1e1ea90ba9adb32c42
K0550	Knowledge	60	PowerShell and Component Object Model (COM)	2021-12-10 18:39:06.718449	2021-12-10 18:39:06.71845		5f457f1e1ea90ba9adb32c45
K0551	Knowledge	60	PowerShell case studies: Managing CA database via PowerShell	2021-12-10 18:39:06.719093	2021-12-10 18:39:06.719094		5f457f1e1ea90ba9adb32c47
K0552	Knowledge	60	PowerShell case studies: Active Directory	2021-12-10 18:39:06.719765	2021-12-10 18:39:06.719766		5f457f1e1ea90ba9adb32c44
K0553	Knowledge	60	PowerShell case studies: TCP/LDAP communication via PowerShell	2021-12-10 18:39:06.720389	2021-12-10 18:39:06.72039		5f457f1e1ea90ba9adb32c46
K0554	Knowledge	60	PowerShell case studies: Web	2021-12-10 18:39:06.721033	2021-12-10 18:39:06.721033		5f457f1e1ea90ba9adb32c4c
K0555	Knowledge	61	Filter file contents	2021-12-10 18:39:06.721699	2021-12-10 18:39:06.721699		5f457f1e1ea90ba9adb32c48
K0556	Knowledge	61	Describe how to pipe output	2021-12-10 18:39:06.722283	2021-12-10 18:39:06.722284		5f457f1e1ea90ba9adb32c4a
K0557	Knowledge	61	Get-Help, Get-Member, Get-Command, Get-Cmdlet, etc.	2021-12-10 18:39:06.722894	2021-12-10 18:39:06.722895		5f457f1e1ea90ba9adb32c4b
K0558	Knowledge	62	UNIX Processes	2021-12-10 18:39:06.723836	2021-12-10 18:39:06.723837		5f457f1e1ea90ba9adb32c49
K0559	Knowledge	62	Utility Binaries	2021-12-10 18:39:06.724832	2021-12-10 18:39:06.724833		5f457f1e1ea90ba9adb32c52
K0560	Knowledge	62	Variables	2021-12-10 18:39:06.72559	2021-12-10 18:39:06.725591		5f457f1e1ea90ba9adb32c4e
K0561	Knowledge	62	Login Process (.bashrc)	2021-12-10 18:39:06.726257	2021-12-10 18:39:06.726258		5f457f1e1ea90ba9adb32c4d
K0562	Knowledge	62	Special Variables	2021-12-10 18:39:06.727039	2021-12-10 18:39:06.72704		5f457f1e1ea90ba9adb32c4f
K0563	Knowledge	62	Quoting Mechanisms	2021-12-10 18:39:06.727876	2021-12-10 18:39:06.727878		5f457f1e1ea90ba9adb32c50
K0564	Knowledge	62	Functions	2021-12-10 18:39:06.728731	2021-12-10 18:39:06.728732		5f457f1e1ea90ba9adb32c53
K0565	Knowledge	62	Advanced Programming	2021-12-10 18:39:06.729376	2021-12-10 18:39:06.729377		5f457f1e1ea90ba9adb32c60
K0574	Knowledge	63	Search Projects	2021-12-10 18:39:06.730462	2021-12-10 18:39:06.730463		5f457f1e1ea90ba9adb32c51
K0577	Knowledge	64	Scripting	2021-12-10 18:39:06.731313	2021-12-10 18:39:06.731314		5f457f1e1ea90ba9adb32c55
K0578	Knowledge	64	Renaming Functions and comment functions	2021-12-10 18:39:06.73212	2021-12-10 18:39:06.732121		5f457f1e1ea90ba9adb32c58
K0579	Knowledge	64	Tracing code	2021-12-10 18:39:06.733078	2021-12-10 18:39:06.733079		5f457f1e1ea90ba9adb32c54
K0580	Knowledge	64	Searching	2021-12-10 18:39:06.733828	2021-12-10 18:39:06.733829		5f457f1e1ea90ba9adb32c57
K0581	Knowledge	64	Manipulate Execution Flow	2021-12-10 18:39:06.734569	2021-12-10 18:39:06.73457		5f457f1e1ea90ba9adb32c56
K0582	Knowledge	64	Code Analysis	2021-12-10 18:39:06.735292	2021-12-10 18:39:06.735293		5f457f1e1ea90ba9adb32c59
K0583	Knowledge	64	Plugins	2021-12-10 18:39:06.736048	2021-12-10 18:39:06.736049		5f457f1e1ea90ba9adb32c62
K0584	Knowledge	64	Settings	2021-12-10 18:39:06.736919	2021-12-10 18:39:06.73692		5f457f1e1ea90ba9adb32c5c
K0586	Knowledge	65	Shell Payloads	2021-12-10 18:39:06.737605	2021-12-10 18:39:06.737606		5f457f1e1ea90ba9adb32c5e
K0588	Knowledge	66	Applications	2021-12-10 18:39:06.738289	2021-12-10 18:39:06.738289		5f457f1e1ea90ba9adb32c5b
K0589	Knowledge	66	Core Concepts	2021-12-10 18:39:06.738863	2021-12-10 18:39:06.738864		5f457f1e1ea90ba9adb32c5a
K0590	Knowledge	66	Agile Board	2021-12-10 18:39:06.739437	2021-12-10 18:39:06.739438		5f457f1e1ea90ba9adb32c5d
K0591	Knowledge	66	Issue Creation	2021-12-10 18:39:06.74001	2021-12-10 18:39:06.740011		5f457f1e1ea90ba9adb32c5f
K0592	Knowledge	66	WorkFlow	2021-12-10 18:39:06.740592	2021-12-10 18:39:06.740592		5f457f1e1ea90ba9adb32c61
K0593	Knowledge	66	Tracking Issues	2021-12-10 18:39:06.741162	2021-12-10 18:39:06.741163		5f457f1e1ea90ba9adb32c64
K0594	Knowledge	31	Packet Capture (PCAP) based Intrusion Detection System (IDS) Analysis	2021-12-10 18:39:06.741834	2021-12-10 18:39:06.741834		5f457f1e1ea90ba9adb32c63
K0598	Knowledge	67	Hacking operating systems and software	2021-12-10 18:39:06.742735	2021-12-10 18:39:06.742736		5f457f1e1ea90ba9adb32c65
K0599	Knowledge	67	Hacking fuzzing fundamentals	2021-12-10 18:39:06.743362	2021-12-10 18:39:06.743363		5f457f1e1ea90ba9adb32c67
K0600	Knowledge	4	Understand basic concepts on how to remediate format string vulnerabilities	2021-12-10 18:39:06.743962	2021-12-10 18:39:06.743963		5f457f1e1ea90ba9adb32c68
K0601	Knowledge	15	Common Software Exploitation Goals	2021-12-10 18:39:06.744545	2021-12-10 18:39:06.744545		5f457f1e1ea90ba9adb32c7c
K0602	Knowledge	15	Modern Protection Mechanisms	2021-12-10 18:39:06.745156	2021-12-10 18:39:06.745157		5f457f1e1ea90ba9adb32c6a
K0603	Knowledge	15	Modern Circumvention Techniques	2021-12-10 18:39:06.745681	2021-12-10 18:39:06.745681		5f457f1e1ea90ba9adb32c66
K0604	Knowledge	15	Heuristic Design to Achieve Sufficient Targeting Granularity	2021-12-10 18:39:06.746255	2021-12-10 18:39:06.746256		5f457f1e1ea90ba9adb32c69
K0605	Knowledge	15	Triaging Exceptions	2021-12-10 18:39:06.746833	2021-12-10 18:39:06.746833		5f457f1e1ea90ba9adb32c6b
K0606	Knowledge	15	Heap Grooming	2021-12-10 18:39:06.747365	2021-12-10 18:39:06.747365		5f457f1e1ea90ba9adb32c6c
K0607	Knowledge	15	Return-Oriented Programming (ROP)	2021-12-10 18:39:06.74793	2021-12-10 18:39:06.747931		5f457f1e1ea90ba9adb32c6d
K0608	Knowledge	15	Defeating Address Space Layout Randomization (ASLR)	2021-12-10 18:39:06.748509	2021-12-29 15:19:46.739309		5f457f1e1ea90ba9adb32c6f
K0609	Knowledge	15	Data Execution Prevention (DEP)	2021-12-10 18:39:06.74912	2021-12-29 15:19:43.196215		5f457f1e1ea90ba9adb32c6e
K0610	Knowledge	31	Understand how to utilize ports and protocols in network programming.	2021-12-10 18:39:06.749687	2021-12-10 18:39:06.749687		5f457f1e1ea90ba9adb32c70
K0611	Knowledge	31	Address Resolution Protocol (ARP)	2021-12-10 18:39:06.750379	2021-12-10 18:39:06.75038		5f457f1e1ea90ba9adb32c72
K0612	Knowledge	31	Hypertext Transfer Protocol/Secure (HTTP/HTTPS)	2021-12-10 18:39:06.751165	2021-12-10 18:39:06.751165		5f457f1e1ea90ba9adb32c71
K0613	Knowledge	31	Domain Name System (DNS)	2021-12-10 18:39:06.752004	2021-12-10 18:39:06.752005		5f457f1e1ea90ba9adb32c73
K0614	Knowledge	31	Simple Mail Transfer Protocol (SMTP)	2021-12-10 18:39:06.752841	2021-12-10 18:39:06.752842		5f457f1e1ea90ba9adb32c75
K0615	Knowledge	31	Internal Control Message Protocol (ICMP)	2021-12-10 18:39:06.753685	2021-12-10 18:39:06.753686		5f457f1e1ea90ba9adb32c74
K0616	Knowledge	31	Dynamic Host Configuration Protocol (DHCP)	2021-12-10 18:39:06.754537	2021-12-10 18:39:06.754538		5f457f1e1ea90ba9adb32c76
K0617	Knowledge	31	Know the purpose and how to use Reading Request For Comments (RFC)	2021-12-10 18:39:06.755339	2021-12-10 18:39:06.75534		5f457f1e1ea90ba9adb32c77
K0618	Knowledge	31	Understand how to implement POSIX API/BSD Socket connections in network programming.	2021-12-10 18:39:06.756223	2021-12-10 18:39:06.756224		5f457f1e1ea90ba9adb32c78
K0621	Knowledge	2	Scrum Event Distinction and Process Order	2021-12-10 18:39:06.756983	2021-12-10 18:39:06.756984		5f457f1e1ea90ba9adb32c79
K0622	Knowledge	2	Understand the goal of the Product Backlog Refinement Meeting.	2021-12-10 18:39:06.757564	2021-12-20 17:01:26.478546		5f457f1e1ea90ba9adb32c7a
K0623	Knowledge	2	Understand the composition and purpose of the Daily Scrum Event	2021-12-10 18:39:06.758179	2021-12-20 17:01:58.225751		5f457f1e1ea90ba9adb32c7d
K0624	Knowledge	2	Scrum Advantages to Rapid Capability Development	2021-12-10 18:39:06.758729	2021-12-10 18:39:06.75873		5f457f1e1ea90ba9adb32c7e
K0625	Knowledge	2	Understand the details to conduct unit testing	2021-12-10 18:39:06.759247	2021-12-10 18:39:06.759247		5f457f1e1ea90ba9adb32c81
K0626	Knowledge	2	Size and Complexity of Functional Test Items	2021-12-10 18:39:06.759754	2021-12-10 18:39:06.759755		5f457f1e1ea90ba9adb32c82
K0627	Knowledge	2	Explain the proper structure of a User Story	2021-12-10 18:39:06.760365	2021-12-20 17:02:27.676198		5f457f1e1ea90ba9adb32c84
K0628	Knowledge	20	Read Executive Summaries	2021-12-10 18:39:06.760947	2021-12-10 18:39:06.760948		5f457f1e1ea90ba9adb32c80
K0629	Knowledge	20	Write/Modify Executive Summaries	2021-12-10 18:39:06.761513	2021-12-10 18:39:06.761514		5f457f1e1ea90ba9adb32c7f
K0631	Knowledge	20	Write/Modify User Guides	2021-12-10 18:39:06.762083	2021-12-10 18:39:06.762084		5f457f1e1ea90ba9adb32c83
K0633	Knowledge	20	Write/Modify Analysis Reports	2021-12-10 18:39:06.762661	2021-12-10 18:39:06.762661		5f457f1e1ea90ba9adb32c85
K0634	Knowledge	20	Reading Capability Requirements Specifications	2021-12-10 18:39:06.763242	2021-12-10 18:39:06.763243		5f457f1e1ea90ba9adb32c86
K0635	Knowledge	20	Write/Modify Capability Requirements Specifications	2021-12-10 18:39:06.763844	2021-12-10 18:39:06.763846		5f457f1e1ea90ba9adb32c88
K0637	Knowledge	20	Write/Modify Statement of Requirements	2021-12-10 18:39:06.76443	2021-12-10 18:39:06.764431		5f457f1e1ea90ba9adb32c87
K0639	Knowledge	20	Write/Modify Test Plan/Report	2021-12-10 18:39:06.765002	2021-12-10 18:39:06.765003		5f457f1e1ea90ba9adb32c8a
K0640	Knowledge	68	Functional Evaluation Steps	2021-12-10 18:39:06.76571	2021-12-10 18:39:06.76571		5f457f1e1ea90ba9adb32c89
K0641	Knowledge	68	Build Functional Evaluation Steps	2021-12-10 18:39:06.766275	2021-12-10 18:39:06.766276		5f457f1e1ea90ba9adb32c8b
K0642	Knowledge	68	Operational Testing	2021-12-10 18:39:06.766845	2021-12-10 18:39:06.766846		5f457f1e1ea90ba9adb32c8c
K0643	Knowledge	68	Developmental Testing	2021-12-10 18:39:06.767426	2021-12-10 18:39:06.767427		5f457f1e1ea90ba9adb32c8d
K0645	Knowledge	44	Version Control	2021-12-10 18:39:06.767998	2021-12-10 18:39:06.767999		5f457f1e1ea90ba9adb32c8f
K0647	Knowledge	44	Integrated Development Environments	2021-12-10 18:39:06.768569	2021-12-10 18:39:06.76857		5f457f1e1ea90ba9adb32c8e
K0648	Knowledge	14	Identify common debugging Tools	2021-12-10 18:39:06.769144	2021-12-10 18:39:06.769145		5f457f1e1ea90ba9adb32cb1
K0649	Knowledge	2	Outline relationship between the PO and other scrum members.	2021-12-10 18:39:06.769709	2021-12-20 17:02:51.379305		5f457f1e1ea90ba9adb32cb6
K0652	Knowledge	2	Explain Basic Facts and methodologies associated with Software testing	2021-12-10 18:39:06.770289	2021-12-10 18:39:06.770289		5f457f1e1ea90ba9adb32cab
K0653	Knowledge	2	Interpret basic computer architecture and networking concepts	2021-12-10 18:39:06.770864	2021-12-10 18:39:06.770864		5f457f1e1ea90ba9adb32c90
K0656	Knowledge	2	Understand how agile process is used to mitigate risk with customer	2021-12-10 18:39:06.771382	2021-12-20 17:03:17.553117		5f457f1e1ea90ba9adb32ca6
K0658	Knowledge	27	Identify features associated with Git	2021-12-10 18:39:06.771999	2021-12-10 18:39:06.772		5f457f1e1ea90ba9adb32ca7
K0664	Knowledge	2	Distinguish between user stories, tasks, and subtasks.	2021-12-10 18:39:06.772774	2021-12-22 22:20:18.813377		5f457f1e1ea90ba9adb32ca4
K0667	Knowledge	9	Understand Cyber Development and Front-Door policy within the 90th COS	2021-12-10 18:39:06.773413	2021-12-10 18:39:06.773414	https://confluence.90cos.cdl.af.mil/display/SG/%28U%29+AFI17-2DEVV3	5f457f1e1ea90ba9adb32c99
K0668	Knowledge	2	Understand the 12 principles of agile (refer to manifesto)	2021-12-10 18:39:06.774124	2021-12-22 22:21:55.773158		5f457f1e1ea90ba9adb32cac
K0669	Knowledge	2	Identify the difference between the roles of a Product Owner and a Project Manager	2021-12-10 18:39:06.774699	2021-12-10 18:39:06.7747		5f457f1e1ea90ba9adb32cae
K0670	Knowledge	69	(U) Explain US Cyber Command  authorities, responsibilities, and contributions to the National Cyber Mission.	2021-12-10 18:39:06.77551	2021-12-10 18:39:06.77551		5f457f1e1ea90ba9adb32cb4
K0671	Knowledge	70	(U) Describe mission and organizational partners, including information needs, mission, structure, capabilities, etc.	2021-12-10 18:39:06.776477	2021-12-10 18:39:06.776478		5f457f1e1ea90ba9adb32c91
K0672	Knowledge	51	(U) Describe cyber mission force equipment taxonomy (Platform-Access-Payloads/Toolset), capability development process and repository	2021-12-10 18:39:06.777309	2021-12-10 18:39:06.777309		5f457f1e1ea90ba9adb32c92
K0673	Knowledge	69	(U) Describe USCYBERCOM legal, policy, and compliance requirements relevant to capability development.	2021-12-10 18:39:06.778145	2021-12-10 18:39:06.778146		5f457f1e1ea90ba9adb32c94
K0674	Knowledge	41	(U) Identify cyber adversary threat tier taxonomy (2014 National Intelligence Estimate (NIE)), DIA/NSA Standard Cyber Threat Model, etc.)	2021-12-10 18:39:06.778982	2021-12-10 18:39:06.778983		5f457f1e1ea90ba9adb32c96
K0675	Knowledge	69	(U) Describe sources and locations of cyber capability registries and repositories (E.g. Cyber Capability Registry (CCR), Agency and service repositories, etc.)	2021-12-10 18:39:06.779819	2021-12-10 18:39:06.77982		5f457f1e1ea90ba9adb32c95
K0676	Knowledge	69	(U) Describe sources and locations (public and classified) of capability development TTPs and tradecraft information/intelligence used by the US Gov and others.	2021-12-10 18:39:06.78066	2021-12-10 18:39:06.780661		5f457f1e1ea90ba9adb32c97
K0677	Knowledge	31	(U) Describe Network Classes and their key characteristics, and common payload & access approaches (T1, T2, T3)  (General Purpose, Telepresence, Commercial Mobile, Control Systems, Battlefield Systems)	2021-12-10 18:39:06.781499	2021-12-10 18:39:06.7815		5f457f1e1ea90ba9adb32c98
K0678	Knowledge	71	(U) Describe supported organizations requirements processes.	2021-12-10 18:39:06.782458	2021-12-10 18:39:06.782459		5f457f1e1ea90ba9adb32c9a
K0679	Knowledge	71	(U) Describe the supported organization's approval process for operational use of a capability.	2021-12-10 18:39:06.783297	2021-12-10 18:39:06.783298		5f457f1e1ea90ba9adb32c9b
K0680	Knowledge	1	(U) Describe relevant mission processes including version control processes, release processes, documentation requirements, and testing requirements.	2021-12-10 18:39:06.784091	2021-12-10 18:39:06.784091		5f457f1e1ea90ba9adb32c9f
K0681	Knowledge	7	(U) Explain how to apply modern software engineering practices.	2021-12-10 18:39:06.784902	2021-12-10 18:39:06.784903		5f457f1e1ea90ba9adb32c9c
K0682	Knowledge	44	(U) Describe modern software development methodologies (e.g. Continuous Integration (CI), Continuous Delivery (CD), Test Driven Development (TDD), etc.)	2021-12-10 18:39:06.785724	2021-12-10 18:39:06.785725		5f457f1e1ea90ba9adb32c9e
K0683	Knowledge	21	(U) Explain your organizations project management, timeline estimation, and software engineering philosophy (e.g. CI/CD, TDD, etc.)	2021-12-10 18:39:06.78656	2021-12-10 18:39:06.786561		5f457f1e1ea90ba9adb32c9d
K0684	Knowledge	10	(U) Describe principles, methodologies, and tools used to improve quality of software (e.g. regression testing, test coverage, code review, pair programming, etc.)	2021-12-10 18:39:06.787329	2021-12-10 18:39:06.78733		5f457f1e1ea90ba9adb32ca1
K0685	Knowledge	33	(U) Explain terms and concepts of operating system fundamentals (e.g. File systems, I/O, Memory Management, Process Abstraction, etc.)	2021-12-10 18:39:06.788176	2021-12-10 18:39:06.788177		5f457f1e1ea90ba9adb32ca0
K0686	Knowledge	42	(U) Explain basic programming concepts (e.g., levels, structures, compiled vs. interpreted languages).	2021-12-10 18:39:06.788946	2021-12-10 18:39:06.788947		5f457f1e1ea90ba9adb32ca2
K0687	Knowledge	42	(U) Explain procedural and object-oriented programming paradigms.	2021-12-10 18:39:06.789764	2021-12-10 18:39:06.789765		5f457f1e1ea90ba9adb32ca3
K0688	Knowledge	19	(U) Explain how to utilize reference documentation for C, Python, assembly, and other international technical standards and specifications (IEEE, ISO, IETF, etc.)	2021-12-10 18:39:06.790596	2021-12-10 18:39:06.790597		5f457f1e1ea90ba9adb32caa
K0689	Knowledge	4	(U) Explain the purpose and usage of elements and components of the C programming language	2021-12-10 18:39:06.791379	2021-12-10 18:39:06.791379		5f457f1e1ea90ba9adb32cdc
K0690	Knowledge	5	(U) Explain the purpose and usage of elements and components of the Python programming language	2021-12-10 18:39:06.792241	2021-12-10 18:39:06.792241		5f457f1e1ea90ba9adb32cb3
K0691	Knowledge	11	(U) Explain the purpose and usage of elements of Assembly language (e.g., x86 and x86_64, ARM, PowerPC).	2021-12-10 18:39:06.79307	2021-12-10 18:39:06.793071		5f457f1e1ea90ba9adb32cca
K0692	Knowledge	7	(U) Explain the use and application of static and dynamic program analysis.	2021-12-10 18:39:06.793893	2021-12-10 18:39:06.793894		5f457f1e1ea90ba9adb32cb0
K0693	Knowledge	12	(U) Explain what it is to decompile, disassemble, analyze, and reverse engineer compiled binaries.	2021-12-10 18:39:06.794714	2021-12-10 18:39:06.794715		5f457f1e1ea90ba9adb32caf
K0694	Knowledge	12	(U) Identify commonly used tools to decompile, disassemble, analyze, and reverse engineer compiled binaries.	2021-12-10 18:39:06.795485	2021-12-10 18:39:06.795486		5f457f1e1ea90ba9adb32cb2
K0695	Knowledge	7	(U) Explain software optimization techniques.	2021-12-10 18:39:06.796313	2021-12-10 18:39:06.796313		5f457f1e1ea90ba9adb32cb5
K0696	Knowledge	31	(U) Explain terms and concepts of networking protocols (e.g. Ethernet, IP, DHCP, ICMP, SMTP, DNS, TCP/OSI, etc.)	2021-12-10 18:39:06.797136	2021-12-10 18:39:06.797136		5f457f1e1ea90ba9adb32cb7
K0697	Knowledge	31	(U) Describe and explain physical and logical network infrastructure, to include hubs, switches, routers, firewalls, etc.	2021-12-10 18:39:06.797953	2021-12-10 18:39:06.797954		5f457f1e1ea90ba9adb32cba
K0698	Knowledge	31	(U) Explain network defense mechanisms. (e.g., encryption, authentication, detection, perimeter protection).	2021-12-10 18:39:06.798769	2021-12-10 18:39:06.79877		5f457f1e1ea90ba9adb32cb9
K0699	Knowledge	72	(U) Explain data serialization formats (e.g. XML, JSON, etc.)	2021-12-10 18:39:06.799702	2021-12-10 18:39:06.799703		5f457f1e1ea90ba9adb32cb8
K0700	Knowledge	16	(U) Explain the concepts and terminology of data structures and associated algorithms (e.g., search, sort, traverse, insert, delete).	2021-12-10 18:39:06.800563	2021-12-10 18:39:06.800564		5f457f1e1ea90ba9adb32cc0
K0701	Knowledge	18	(U) Explain terms and concepts of secure coding practices.	2021-12-10 18:39:06.801388	2021-12-10 18:39:06.801388		5f457f1e1ea90ba9adb32cbc
K0702	Knowledge	73	(U) Describe capability OPSEC analysis and application (attribution, sanitization, etc.)	2021-12-10 18:39:06.802326	2021-12-10 18:39:06.802327		5f457f1e1ea90ba9adb32cbb
K0703	Knowledge	73	(U) Explain standard, non-standard, specialized, and/or unique communication techniques used to provide confidentiality, integrity, availability, authentication, and non-repudiation .	2021-12-10 18:39:06.803112	2021-12-10 18:39:06.803112		5f457f1e1ea90ba9adb32cbf
K0704	Knowledge	74	(U) Describe common pitfalls surrounding the use, design, and implementation of cryptographic facilities.	2021-12-10 18:39:06.803992	2021-12-10 18:39:06.803993		5f457f1e1ea90ba9adb32cbd
K0705	Knowledge	74	(U) Demonstrate understanding of pervasive cryptographic primitives.	2021-12-10 18:39:06.804795	2021-12-10 18:39:06.804796		5f457f1e1ea90ba9adb32cbe
K0706	Knowledge	21	(U) Explain Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.)	2021-12-10 18:39:06.805615	2021-12-10 18:39:06.805616		5f457f1e1ea90ba9adb32cc1
K0707	Knowledge	69	(U) Identify cyber development conferences used to share development and community of interest information (e.g. CNEDev, MTEM, etc.)	2021-12-10 18:39:06.80645	2021-12-10 18:39:06.806451		5f457f1e1ea90ba9adb32cc8
K0708	Knowledge	75	(U) List the capabilities and limitations of MetaTotal.	2021-12-10 18:39:06.80747	2021-12-10 18:39:06.807471		5f457f1e1ea90ba9adb32cc7
K0709	Knowledge	75	(U) List the capabilities and limitations of Mockingbird.	2021-12-10 18:39:06.808347	2021-12-10 18:39:06.808348		5f457f1e1ea90ba9adb32cc5
K0710	Knowledge	5	Understand how to implement exception handling	2021-12-10 18:39:06.809094	2021-12-10 18:39:06.809094	 try-except, making sure your program can handle errors	5f457f1e1ea90ba9adb32cc2
K0711	Knowledge	5	Understand Strings and String Methods	2021-12-10 18:39:06.809657	2021-12-10 18:39:06.809658		5f457f1e1ea90ba9adb32cc6
K0712	Knowledge	5	Identify and understand type casting.	2021-12-10 18:39:06.810231	2021-12-10 18:39:06.810232		5f457f1e1ea90ba9adb32cc3
K0713	Knowledge	5	Understand how to implement conditional statements.	2021-12-10 18:39:06.810804	2021-12-10 18:39:06.810805		5f457f1e1ea90ba9adb32cc4
K0714	Knowledge	29	Identify and understand logical operators.	2021-12-10 18:39:06.811325	2021-12-10 18:39:06.811326		5f457f1e1ea90ba9adb32ccb
K0715	Knowledge	29	Identify and understand relational operators.	2021-12-10 18:39:06.811829	2021-12-10 18:39:06.81183		5f457f1e1ea90ba9adb32cc9
K0716	Knowledge	29	Identify and understand arithmetic operators (PEMDAS)	2021-12-10 18:39:06.812399	2021-12-10 18:39:06.812399		5f457f1e1ea90ba9adb32ccd
K0720	Knowledge	27	Describe the purpose of a Git repository.	2021-12-10 18:39:06.813014	2021-12-10 18:39:06.813015		5f457f1e1ea90ba9adb32cd1
K0721	Knowledge	27	Describe commands used to snapshot a Git project.	2021-12-10 18:39:06.813613	2021-12-10 18:39:06.813614	add, commit	5f457f1e1ea90ba9adb32ccc
K0722	Knowledge	27	Describe how to create a branch in Git.	2021-12-10 18:39:06.814298	2021-12-10 18:39:06.814299		5f457f1e1ea90ba9adb32ccf
K0723	Knowledge	27	Describe how to merge a branch in Git.	2021-12-10 18:39:06.815106	2021-12-10 18:39:06.815106		5f457f1e1ea90ba9adb32cce
K0724	Knowledge	27	Describe commands used to update a branch in Git.	2021-12-10 18:39:06.815866	2021-12-10 18:39:06.815867	pull, push	5f457f1e1ea90ba9adb32cd8
K0725	Knowledge	27	Describe how to compare changes to a project in Git.	2021-12-10 18:39:06.816677	2021-12-10 18:39:06.816678	diff	5f457f1e1ea90ba9adb32cd0
K0726	Knowledge	27	Describe how to clone a project in Git	2021-12-10 18:39:06.817496	2021-12-10 18:39:06.817497		5f457f1e1ea90ba9adb32cd5
K0728	Knowledge	76	Summarize the purpose of a .gitlab-ci.yml file in GitLab	2021-12-10 18:39:06.818361	2021-12-10 18:39:06.818361		5f457f1e1ea90ba9adb32cd2
K0729	Knowledge	76	Clarify how to configure a .gitlab-ci.yml file to generate an artifact in a GitLab pipeline	2021-12-10 18:39:06.818928	2021-12-10 18:39:06.818929		5f457f1e1ea90ba9adb32cd3
K0730	Knowledge	76	Clarify how to configure a .gitlab-ci.yml file to cache files in a GitLab pipeline	2021-12-10 18:39:06.819444	2021-12-10 18:39:06.819444		5f457f1e1ea90ba9adb32cd4
K0731	Knowledge	76	Clarify how to define stages in a .gitlab-ci.yml file for a GitLab pipeline	2021-12-10 18:39:06.820006	2021-12-10 18:39:06.820007		5f457f1e1ea90ba9adb32cd6
K0732	Knowledge	76	Clarify how to define jobs in a .gitlab-ci.yml file for a GitLab pipeline	2021-12-10 18:39:06.820571	2021-12-10 18:39:06.820572		5f457f1e1ea90ba9adb32cd7
K0733	Knowledge	5	Describe how to utilize PIP to install a package.	2021-12-10 18:39:06.821133	2021-12-10 18:39:06.821134		5f457f1e1ea90ba9adb32cdb
K0734	Knowledge	5	Understand the purpose and benefits of Python Virtual Environments.	2021-12-10 18:39:06.821716	2021-12-10 18:39:06.821716		5f457f1e1ea90ba9adb32cd9
K0735	Knowledge	29	Identify and understand assignment operators.	2021-12-10 18:39:06.822283	2021-12-10 18:39:06.822284		5f457f1e1ea90ba9adb32cec
K0736	Knowledge	5	Understand how to implement a function that returns multiple values.	2021-12-10 18:39:06.822852	2021-12-10 18:39:06.822853		5f457f1e1ea90ba9adb32cda
K0737	Knowledge	29	Understand the constructs of a recursive function.	2021-12-10 18:39:06.823432	2021-12-10 18:39:06.823433		5f457f1e1ea90ba9adb32cde
K0738	Knowledge	29	Understand how functions are implemented that do not return a value.	2021-12-10 18:39:06.824103	2021-12-10 18:39:06.824104		5f457f1e1ea90ba9adb32cdd
K0739	Knowledge	4	Understand the differences between pass by value and pass by reference for a function.	2021-12-10 18:39:06.824674	2021-12-10 18:39:06.824675		5f457f1e1ea90ba9adb32ce2
K0740	Knowledge	4	Understand how a function returns a memory reference.	2021-12-10 18:39:06.825246	2021-12-10 18:39:06.825247		5f457f1e1ea90ba9adb32cdf
K0741	Knowledge	5	Understand how to open and close an existing file.	2021-12-10 18:39:06.825819	2021-12-10 18:39:06.82582		5f457f1e1ea90ba9adb32ce0
K0742	Knowledge	5	Understand how to read, parse write file data.	2021-12-10 18:39:06.826394	2021-12-10 18:39:06.826395		5f457f1e1ea90ba9adb32ce1
K0743	Knowledge	5	Understand how to create and delete a new file.	2021-12-10 18:39:06.82696	2021-12-10 18:39:06.826961		5f457f1e1ea90ba9adb32ce3
K0744	Knowledge	5	Understand how to determine the size of a file.	2021-12-10 18:39:06.827522	2021-12-10 18:39:06.827523		5f457f1e1ea90ba9adb32ce8
K0745	Knowledge	5	Understand how to determine location of content within a file.	2021-12-10 18:39:06.828095	2021-12-10 18:39:06.828095		5f457f1e1ea90ba9adb32ce4
K0746	Knowledge	4	Understand how to open and close an existing file.	2021-12-10 18:39:06.82866	2021-12-10 18:39:06.828661		5f457f1e1ea90ba9adb32ce5
K0747	Knowledge	4	Understand how to read, parse write file data.	2021-12-10 18:39:06.829226	2021-12-10 18:39:06.829227		5f457f1e1ea90ba9adb32ce6
K0748	Knowledge	4	Understand how to create and delete a new file.	2021-12-10 18:39:06.829792	2021-12-10 18:39:06.829792		5f457f1e1ea90ba9adb32ce7
K0749	Knowledge	4	Understand how to determine the size of a file.	2021-12-10 18:39:06.830355	2021-12-10 18:39:06.830356		5f457f1e1ea90ba9adb32ce9
K0750	Knowledge	4	Understand how to determine location of content within a file.	2021-12-10 18:39:06.83093	2021-12-10 18:39:06.830931		5f457f1e1ea90ba9adb32cea
K0751	Knowledge	16	Describe how to implement and use a Stack	2021-12-10 18:39:06.831497	2021-12-10 18:39:06.831498		5f457f1e1ea90ba9adb32ced
K0752	Knowledge	16	Describe how to implement and use a Queue	2021-12-10 18:39:06.832072	2021-12-10 18:39:06.832072		5f457f1e1ea90ba9adb32ceb
K0753	Knowledge	16	Describe how to implement and use a Tree	2021-12-10 18:39:06.832636	2021-12-10 18:39:06.832637	a general tree (other than a Binary Search Tree)	5f457f1e1ea90ba9adb32cef
K0754	Knowledge	29	Understand how to implement and use if and if/else constructs.	2021-12-10 18:39:06.833211	2021-12-10 18:39:06.833212		5f457f1e1ea90ba9adb32cf1
K0755	Knowledge	4	Understand how to implement a switch statement.	2021-12-10 18:39:06.833779	2021-12-10 18:39:06.83378		5f457f1e1ea90ba9adb32cee
K0757	Knowledge	4	Understand how to dynamically allocate memory on the heap.	2021-12-10 18:39:06.834349	2021-12-10 18:39:06.83435	malloc	5f457f1e1ea90ba9adb32cf4
K0758	Knowledge	4	Understand how to release dynamically allocated memory from the heap.	2021-12-10 18:39:06.834918	2021-12-10 18:39:06.834919	free	5f457f1e1ea90ba9adb32cf2
K0759	Knowledge	31	Understand the purpose and how to use Wireshark.	2021-12-10 18:39:06.83548	2021-12-10 18:39:06.83548		5f457f1e1ea90ba9adb32cf0
K0760	Knowledge	4	Understand how to create and use function pointers.	2021-12-10 18:39:06.836182	2021-12-10 18:39:06.836183		5f457f1e1ea90ba9adb32cf3
K0761	Knowledge	4	Understand how to use pointers and pointer arithmetic to traverse an array.	2021-12-10 18:39:06.836817	2021-12-10 18:39:06.836817		5f457f1e1ea90ba9adb32cf6
K0762	Knowledge	4	Understand how the preprocessor is used.	2021-12-10 18:39:06.837495	2021-12-10 18:39:06.837496		5f457f1e1ea90ba9adb32cf5
K0763	Knowledge	11	Understand how to use C datatypes Word, Doubleword, and Quadword.	2021-12-10 18:39:06.838127	2021-12-10 18:39:06.838128		5f457f1e1ea90ba9adb32cf7
K0764	Knowledge	4	Understand the purpose and use of the increment and decrement operators.	2021-12-10 18:39:06.838939	2021-12-10 18:39:06.838939		5f457f1e1ea90ba9adb32cf9
K0765	Knowledge	4	Understand the predefined parameters and how to use them in command line arguments.	2021-12-10 18:39:06.839471	2021-12-10 18:39:06.839472		5f457f1e1ea90ba9adb32cf8
K0766	Knowledge	15	Understand the purpose of a ROP gadget	2021-12-10 18:39:06.840184	2021-12-10 18:39:06.840185		5f457f1e1ea90ba9adb32d08
K0767	Knowledge	11	Understand move instructions.	2021-12-10 18:39:06.840897	2021-12-10 18:39:06.840898	mov, movx, movs, movzx	5f457f1e1ea90ba9adb32cfa
K0770	Knowledge	11	Understand the purpose of the lea instruction.	2021-12-10 18:39:06.841431	2021-12-10 18:39:06.841432		5f457f1e1ea90ba9adb32cfb
K0771	Knowledge	11	Understand the purpose of the xchg instruction.	2021-12-10 18:39:06.841992	2021-12-10 18:39:06.841992		5f457f1e1ea90ba9adb32cfe
K0772	Knowledge	11	Understand stack instructions.	2021-12-10 18:39:06.842523	2021-12-10 18:39:06.842523	push, pop, pushf, popf	5f457f1e1ea90ba9adb32d01
K0776	Knowledge	11	Understand the purpose of the call instruction.	2021-12-10 18:39:06.843105	2021-12-10 18:39:06.843106		5f457f1e1ea90ba9adb32d02
K0777	Knowledge	11	Understand the purpose of the ret instruction.	2021-12-10 18:39:06.8437	2021-12-10 18:39:06.8437		5f457f1e1ea90ba9adb32d03
K0778	Knowledge	11	Understand mathematical instructions.	2021-12-10 18:39:06.84431	2021-12-10 18:39:06.844311	add, sub, div, mul, inc, dec	5f457f1e1ea90ba9adb32d05
K0784	Knowledge	11	Understand the shift instructions.	2021-12-10 18:39:06.844871	2021-12-10 18:39:06.844872	shr, shl, sal, sar	5f457f1e1ea90ba9adb32d0d
K0786	Knowledge	11	Understand logical instructions.	2021-12-10 18:39:06.845473	2021-12-10 18:39:06.845473	and, or, xor, not, test	5f457f1e1ea90ba9adb32d0f
K0790	Knowledge	11	Understand the rotate instructions.	2021-12-10 18:39:06.845991	2021-12-10 18:39:06.845992	ror, rol	5f457f1e1ea90ba9adb32d15
K0794	Knowledge	11	Understand the purpose of the cmp instruction.	2021-12-10 18:39:06.846553	2021-12-10 18:39:06.846554		5f457f1e1ea90ba9adb32d19
K0796	Knowledge	11	Understand the purpose of the jcc and other conditional jump instructions.	2021-12-10 18:39:06.847093	2021-12-10 18:39:06.847094	je/jz, jne/jnz, ja, jb/jc	5f457f1e1ea90ba9adb32d1f
K0797	Knowledge	11	Understand the purpose of the loop instruction.	2021-12-10 18:39:06.847798	2021-12-10 18:39:06.847798		5f457f1e1ea90ba9adb32d1d
K0798	Knowledge	11	Understand string instructions.	2021-12-10 18:39:06.848357	2021-12-10 18:39:06.848358	stos, scas, rep, repne, repe, cmps	5f457f1e1ea90ba9adb32d18
K0800	Knowledge	11	Understand the purpose of the lods instruction.	2021-12-10 18:39:06.84892	2021-12-10 18:39:06.848921		5f457f1e1ea90ba9adb32d16
K0806	Knowledge	11	Understand the purpose of the std instruction.	2021-12-10 18:39:06.84947	2021-12-10 18:39:06.849471		5f457f1e1ea90ba9adb32d22
K0807	Knowledge	11	Understand the purpose of the cld instruction.	2021-12-10 18:39:06.850022	2021-12-10 18:39:06.850023		5f457f1e1ea90ba9adb32d23
K0808	Knowledge	11	Describe how to create and utilize structures.	2021-12-10 18:39:06.850574	2021-12-10 18:39:06.850574		5f457f1e1ea90ba9adb32d24
K0809	Knowledge	11	Understand how to utilize bit operations.	2021-12-10 18:39:06.851128	2021-12-10 18:39:06.851129		5f457f1e1ea90ba9adb32d25
K0810	Knowledge	11	Understand how to utilize flags.	2021-12-10 18:39:06.851689	2021-12-10 18:39:06.85169		5f457f1e1ea90ba9adb32d2a
K0811	Knowledge	11	Describe how to utilize labels.	2021-12-10 18:39:06.852217	2021-12-10 18:39:06.852218		5f457f1e1ea90ba9adb32d27
K0812	Knowledge	11	Understand how to implement conditional control flow.	2021-12-10 18:39:06.852741	2021-12-10 18:39:06.852741		5f457f1e1ea90ba9adb32d29
K0813	Knowledge	11	Understand how to create and implement functions.	2021-12-10 18:39:06.853294	2021-12-10 18:39:06.853295		5f457f1e1ea90ba9adb32d28
K0814	Knowledge	11	Describe common interrupts and how to use them.	2021-12-10 18:39:06.853831	2021-12-10 18:39:06.853832		5f457f1e1ea90ba9adb32d26
K0815	Knowledge	11	Understand how to use pre-defined utility functions.	2021-12-10 18:39:06.854369	2021-12-10 18:39:06.85437	strlen, memcpy, memset, etc	5f457f1e1ea90ba9adb32d2c
K0816	Knowledge	11	Describe how to invoke system calls.	2021-12-10 18:39:06.854894	2021-12-10 18:39:06.854895		5f457f1e1ea90ba9adb32d2b
K0817	Knowledge	11	Understand how to allocate memory.	2021-12-10 18:39:06.855419	2021-12-10 18:39:06.855419		5f457f1e1ea90ba9adb32d2e
K0818	Knowledge	11	Understand how to implement file handling.	2021-12-10 18:39:06.85595	2021-12-10 18:39:06.85595		5f457f1e1ea90ba9adb32d2d
K0819	Knowledge	11	Describe how to use a GDB debugger to identify errors in a program.	2021-12-10 18:39:06.856472	2021-12-10 18:39:06.856473		5f457f1e1ea90ba9adb32d2f
K0820	Knowledge	11	Describe how to use a WinDBG debugger to identify errors in a program.	2021-12-10 18:39:06.857013	2021-12-10 18:39:06.857014		5f457f1e1ea90ba9adb32d32
K0821	Knowledge	11	Understand different processor modes.	2021-12-10 18:39:06.857536	2021-12-10 18:39:06.857537		5f457f1e1ea90ba9adb32d30
K0822	Knowledge	4	Understand how to use the SSL library.	2021-12-10 18:39:06.85809	2021-12-10 18:39:06.85809		5f457f1e1ea90ba9adb32d31
K0823	Knowledge	4	Understand how to zero-out allocated memory.	2021-12-10 18:39:06.858609	2021-12-10 18:39:06.85861		5f457f1e1ea90ba9adb32d35
K0824	Knowledge	4	Understand how to find information in MSDN documents.	2021-12-10 18:39:06.859162	2021-12-10 18:39:06.859163		5f457f1e1ea90ba9adb32d33
K0825	Knowledge	4	Understand how to use man pages in linux to find information.	2021-12-10 18:39:06.85974	2021-12-10 18:39:06.859741		5f457f1e1ea90ba9adb32d34
K0827	Knowledge	4	Understand how to use the C Standard Library	2021-12-10 18:39:06.860341	2021-12-10 18:39:06.860342		5f457f1e1ea90ba9adb32d3b
K0828	Knowledge	29	Understand regular expressions.	2021-12-10 18:39:06.860864	2021-12-10 18:39:06.860865	Define regex, identify/craft regex syntax	5f457f1e1ea90ba9adb32d36
K0829	Knowledge	68	Identify automated testing frameworks & tools used in the 90th and their purpose/function.	2021-12-10 18:39:06.861467	2021-12-10 18:39:06.861467	(e.g. DART, DEADCODE, METATOTAL, etc.)	5f457f1e1ea90ba9adb32d37
K0830	Knowledge	68	Identify and understand how to utilize orchestration technologies and tools.	2021-12-10 18:39:06.861991	2021-12-10 18:39:06.861992		5f457f1e1ea90ba9adb32d39
K0831	Knowledge	68	Identify and describe the differences between automation and orchestration.	2021-12-10 18:39:06.862573	2021-12-10 18:39:06.862574		5f457f1e1ea90ba9adb32d38
K0832	Knowledge	68	Describe the process for scheduling computing resources for systems at the 90th.	2021-12-10 18:39:06.863119	2021-12-10 18:39:06.86312	(e.g. IaaS/SaaS/PaaS Compute, memory, and storage)	5f457f1e1ea90ba9adb32d3c
K0833	Knowledge	68	Identify and describe proper data collection requirements for test scripts and plans.	2021-12-10 18:39:06.863739	2021-12-10 18:39:06.86374		5f457f1e1ea90ba9adb32d3a
K0834	Knowledge	29	Understand the function of a program written in Pseudocode 	2021-12-10 18:39:06.864353	2021-12-10 18:39:06.864354		5f457f1e1ea90ba9adb32d3d
K0835	Knowledge	29	Determine the most efficient and effective solution from multiple Pseudocode solutions,	2021-12-10 18:39:06.865144	2021-12-10 18:39:06.865145		5f457f1e1ea90ba9adb32d43
K0836	Knowledge	77	Summarize the purpose of Docker	2021-12-10 18:39:06.865975	2021-12-10 18:39:06.865976		5f457f1e1ea90ba9adb32d3f
K0837	Knowledge	77	List the limitations of Docker containers	2021-12-10 18:39:06.866512	2021-12-10 18:39:06.866512		5f457f1e1ea90ba9adb32d3e
K0838	Knowledge	77	Summarize Docker vs Virtual Machines	2021-12-10 18:39:06.86705	2021-12-10 18:39:06.867051		5f457f1e1ea90ba9adb32d41
K0839	Knowledge	77	Summarize Docker vs other container technologies/options	2021-12-10 18:39:06.867577	2021-12-10 18:39:06.867578		5f457f1e1ea90ba9adb32d40
K0840	Knowledge	77	Describe containers and containerization	2021-12-10 18:39:06.868155	2021-12-10 18:39:06.868156		5f457f1e1ea90ba9adb32d42
K0841	Knowledge	77	Recall the difference between a docker container and a docker image	2021-12-10 18:39:06.868682	2021-12-10 18:39:06.868683		5f457f1e1ea90ba9adb32d46
K0842	Knowledge	77	Identify the purpose of the FROM instruction	2021-12-10 18:39:06.86921	2021-12-10 18:39:06.869211	https://docs.docker.com/engine/reference/builder/#from	5f457f1e1ea90ba9adb32d49
K0843	Knowledge	77	Identify the purpose of the RUN instruction	2021-12-10 18:39:06.869732	2021-12-10 18:39:06.869732	https://docs.docker.com/engine/reference/builder/#run	5f457f1e1ea90ba9adb32d44
K0844	Knowledge	77	Identify the purpose of the CMD instruction	2021-12-10 18:39:06.870254	2021-12-10 18:39:06.870254	https://docs.docker.com/engine/reference/builder/#cmd	5f457f1e1ea90ba9adb32d45
K0845	Knowledge	77	Identify the purpose of the EXPOSE instruction	2021-12-10 18:39:06.870776	2021-12-10 18:39:06.870777	https://docs.docker.com/engine/reference/builder/#expose	5f457f1e1ea90ba9adb32d47
K0846	Knowledge	77	Identify the purpose of the ENV instruction	2021-12-10 18:39:06.871301	2021-12-10 18:39:06.871301	https://docs.docker.com/engine/reference/builder/#env	5f457f1e1ea90ba9adb32d4d
K0847	Knowledge	77	Identify the purpose of the ADD instruction	2021-12-10 18:39:06.871827	2021-12-10 18:39:06.871828	https://docs.docker.com/engine/reference/builder/#add	5f457f1e1ea90ba9adb32d4c
K0848	Knowledge	77	Identify the purpose of the COPY instruction	2021-12-10 18:39:06.872386	2021-12-10 18:39:06.872386	https://docs.docker.com/engine/reference/builder/#copy	5f457f1e1ea90ba9adb32d4a
K0849	Knowledge	77	Summarize the purpose of the Dockerfile	2021-12-10 18:39:06.872963	2021-12-10 18:39:06.872964	https://docs.docker.com/engine/reference/builder/	5f457f1e1ea90ba9adb32d48
K0850	Knowledge	77	Summarize the difference between ADD and COPY instructions	2021-12-10 18:39:06.873815	2021-12-10 18:39:06.873816		5f457f1e1ea90ba9adb32d4b
K0851	Knowledge	77	Identify the purpose of the ENTRYPOINT instruction	2021-12-10 18:39:06.874502	2021-12-10 18:39:06.874503	https://docs.docker.com/engine/reference/builder/#entrypoint	5f457f1e1ea90ba9adb32d4f
K0852	Knowledge	77	Identify the purpose of the VOLUME instruction	2021-12-10 18:39:06.875039	2021-12-10 18:39:06.87504	https://docs.docker.com/engine/reference/builder/#volume	5f457f1e1ea90ba9adb32d4e
K0853	Knowledge	77	Identify the purpose of the USER instruction	2021-12-10 18:39:06.875716	2021-12-10 18:39:06.875717	https://docs.docker.com/engine/reference/builder/#user	5f457f1e1ea90ba9adb32d51
K0854	Knowledge	77	Identify the purpose of the WORKDIR instruction	2021-12-10 18:39:06.876485	2021-12-10 18:39:06.876486	https://docs.docker.com/engine/reference/builder/#workdir	5f457f1e1ea90ba9adb32d50
K0855	Knowledge	77	Identify the purpose of the ARGS instruction	2021-12-10 18:39:06.876992	2021-12-10 18:39:06.876993	https://docs.docker.com/engine/reference/builder/#arg	5f457f1e1ea90ba9adb32d52
K0856	Knowledge	77	Identify the purpose of the HEALTHCHECK instruction	2021-12-10 18:39:06.877711	2021-12-10 18:39:06.877712	https://docs.docker.com/engine/reference/builder/#healthcheck	5f457f1e1ea90ba9adb32d53
K0857	Knowledge	77	Identify the purpose of the SHELL instruction	2021-12-10 18:39:06.878438	2021-12-10 18:39:06.878439	https://docs.docker.com/engine/reference/builder/#shell	5f457f1e1ea90ba9adb32d55
K0858	Knowledge	77	Identify the purpose of the 'images' CLI command	2021-12-10 18:39:06.878995	2021-12-10 18:39:06.878996	https://docs.docker.com/engine/reference/commandline/images/	5f457f1e1ea90ba9adb32d54
K0859	Knowledge	77	Identify the purpose of the 'ps' CLI command	2021-12-10 18:39:06.879551	2021-12-10 18:39:06.879552	https://docs.docker.com/engine/reference/commandline/ps/	5f457f1e1ea90ba9adb32d56
K0860	Knowledge	77	Identify the purpose of the 'login' command	2021-12-10 18:39:06.880472	2021-12-10 18:39:06.880473	https://docs.docker.com/engine/reference/commandline/login/	5f457f1e1ea90ba9adb32d58
K0861	Knowledge	77	Identify the purpose of the 'pull' CLI command	2021-12-10 18:39:06.881122	2021-12-10 18:39:06.881123	https://docs.docker.com/engine/reference/commandline/pull/	5f457f1e1ea90ba9adb32d5a
K0862	Knowledge	77	Identify the purpose of the 'push' CLI command	2021-12-10 18:39:06.881683	2021-12-10 18:39:06.881684	https://docs.docker.com/engine/reference/commandline/push/	5f457f1e1ea90ba9adb32d57
K0863	Knowledge	77	Identify the purpose of the 'rm' CLI command	2021-12-10 18:39:06.882259	2021-12-10 18:39:06.88226	https://docs.docker.com/engine/reference/commandline/rm/	5f457f1e1ea90ba9adb32d59
K0864	Knowledge	77	Identify the purpose of the 'rmi' CLI command	2021-12-10 18:39:06.882785	2021-12-10 18:39:06.882786	https://docs.docker.com/engine/reference/commandline/rmi/	5f457f1e1ea90ba9adb32d5c
K0865	Knowledge	77	Identify the purpose of the 'tag' CLI command	2021-12-10 18:39:06.883353	2021-12-10 18:39:06.883354	https://docs.docker.com/engine/reference/commandline/tag/	5f457f1e1ea90ba9adb32d5b
K0866	Knowledge	77	Identify the purpose of the 'exec' CLI command	2021-12-10 18:39:06.883911	2021-12-10 18:39:06.883912	https://docs.docker.com/engine/reference/commandline/exec/	5f457f1e1ea90ba9adb32d5d
K0867	Knowledge	77	Identify the purpose of the 'save' CLI command	2021-12-10 18:39:06.884517	2021-12-10 18:39:06.884518	https://docs.docker.com/engine/reference/commandline/save/	5f457f1e1ea90ba9adb32d5f
K0868	Knowledge	77	Identify the purpose of the 'load' CLI command	2021-12-10 18:39:06.885084	2021-12-10 18:39:06.885084	https://docs.docker.com/engine/reference/commandline/load/	5f457f1e1ea90ba9adb32d62
K0869	Knowledge	77	Identify the purpose of the 'history' CLI command	2021-12-10 18:39:06.885672	2021-12-10 18:39:06.885673	https://docs.docker.com/engine/reference/commandline/history/	5f457f1e1ea90ba9adb32d5e
K0870	Knowledge	77	Identify the purpose of the 'commit' CLI command	2021-12-10 18:39:06.886257	2021-12-10 18:39:06.886258	https://docs.docker.com/engine/reference/commandline/commit/	5f457f1e1ea90ba9adb32d63
K0871	Knowledge	77	Identify the purpose of the '--name' flag in the run command	2021-12-10 18:39:06.886807	2021-12-10 18:39:06.886808	https://docs.docker.com/engine/reference/run/#name---name	5f457f1e1ea90ba9adb32d61
K0872	Knowledge	77	Identify the purpose of the '--rm' flag in the run command	2021-12-10 18:39:06.887391	2021-12-10 18:39:06.887392	https://docs.docker.com/engine/reference/run/#clean-up---rm	5f457f1e1ea90ba9adb32d60
K0873	Knowledge	77	Identify the purpose of the '--volume' flag in the run command	2021-12-10 18:39:06.888	2021-12-10 18:39:06.888001	https://docs.docker.com/engine/reference/run/#volume-shared-filesystems	5f457f1e1ea90ba9adb32d64
K0874	Knowledge	77	Identify the purpose of the '--user' flag in the run command	2021-12-10 18:39:06.888584	2021-12-10 18:39:06.888585	https://docs.docker.com/engine/reference/run/#user	5f457f1e1ea90ba9adb32d65
K0875	Knowledge	77	Identify the purpose of the '-e' flag in the run command	2021-12-10 18:39:06.88916	2021-12-10 18:39:06.88916	https://docs.docker.com/engine/reference/run/#env-environment-variables	5f457f1e1ea90ba9adb32d66
K0876	Knowledge	77	Identify the purpose of the '-p' flag in the run command	2021-12-10 18:39:06.890007	2021-12-10 18:39:06.890008	https://docs.docker.com/engine/reference/run/#expose-incoming-ports	5f457f1e1ea90ba9adb32d67
K0877	Knowledge	77	Identify the purpose of runtime privilege flags in the run command	2021-12-10 18:39:06.890727	2021-12-10 18:39:06.890727	https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities	5f457f1e1ea90ba9adb32d69
K0878	Knowledge	77	Identify the purpose of the runtime constraints flags in the run command	2021-12-10 18:39:06.89128	2021-12-10 18:39:06.891281	https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources	5f457f1e1ea90ba9adb32d68
K0879	Knowledge	77	Identify the purpose of the network settings flags in the run command	2021-12-10 18:39:06.891918	2021-12-10 18:39:06.891919	https://docs.docker.com/engine/reference/run/#network-settings	5f457f1e1ea90ba9adb32d6d
K0880	Knowledge	77	Identify docker container best practices and why they're important	2021-12-10 18:39:06.892591	2021-12-10 18:39:06.892592	https://docs.docker.com/develop/dev-best-practices/	5f457f1e1ea90ba9adb32d6a
K0881	Knowledge	77	Identify dockerfile best practices and why they're important	2021-12-10 18:39:06.893177	2021-12-10 18:39:06.893178	https://docs.docker.com/develop/develop-images/dockerfile_best-practices/	5f457f1e1ea90ba9adb32d6b
K0882	Knowledge	77	List what docker orchestration tools are available when using docker in a production environment	2021-12-10 18:39:06.893842	2021-12-10 18:39:06.893843	https://docs.docker.com/get-started/orchestration/	5f457f1e1ea90ba9adb32d6c
K0883	Knowledge	77	Recall how to configure the docker daemon	2021-12-10 18:39:06.894615	2021-12-10 18:39:06.894616	https://docs.docker.com/config/daemon/	5f457f1e1ea90ba9adb32d6e
K0884	Knowledge	77	Recall how to interact with the docker daemon using systemd	2021-12-10 18:39:06.895467	2021-12-10 18:39:06.895468	https://docs.docker.com/config/daemon/systemd/	5f457f1e1ea90ba9adb32d71
K0885	Knowledge	77	Identify the four different security areas when considering Docker security	2021-12-10 18:39:06.896066	2021-12-10 18:39:06.896066	https://docs.docker.com/engine/security/security/	5f457f1e1ea90ba9adb32d6f
K0886	Knowledge	77	Summarize how Docker in swarm mode can be used to scale applications	2021-12-10 18:39:06.896676	2021-12-10 18:39:06.896677	https://docs.docker.com/engine/swarm/	5f457f1e1ea90ba9adb32d70
K0887	Knowledge	77	Summarize how Docker leverages Containerd to manage container lifecycles	2021-12-10 18:39:06.897518	2021-12-10 18:39:06.897519	https://www.inovex.de/blog/containers-docker-containerd-nabla-kata-firecracker/	5f457f1e1ea90ba9adb32d73
K0888	Knowledge	77	Summarize how Docker leverages runc to provide container isolation	2021-12-10 18:39:06.898207	2021-12-10 18:39:06.898208	https://www.inovex.de/blog/containers-docker-containerd-nabla-kata-firecracker/	5f457f1e1ea90ba9adb32d72
K0889	Knowledge	2	Identify the four Agile values.	2021-12-10 18:39:06.899078	2021-12-20 17:04:45.977684		5f457f1e1ea90ba9adb32d74
K0890	Knowledge	76	Recall the purpose of different GitLab repository configuration files 	2021-12-10 18:39:06.899724	2021-12-10 18:39:06.899725	.gitlab-ci.yml, .gitlab folder, .gitignore, README.md, LICENSE.md, CONTRIBUTING.md, etc...	5f457f1e1ea90ba9adb32d76
K0891	Knowledge	78	Recall how to ask a question the right way	2021-12-10 18:39:06.900509	2021-12-10 18:39:06.90051		5f457f1e1ea90ba9adb32d75
K0895	Knowledge	79	Explain the steps that should be taken to mitigate potential downtime when introducing new services	2021-12-10 18:39:06.901202	2021-12-10 18:39:06.901203		5f457f1e1ea90ba9adb32d7a
K0896	Knowledge	80	Identify the high-level purpose of the main AWS services	2021-12-10 18:39:06.901929	2021-12-10 18:39:06.90193	Example: EC2, EBS, S3, ELB, EIP, ECR, ECS, EKS, Elastic Beanstalk, SES, RDS, Cloud Watch, Cloud Trail, IAM, VPCs, Subnets, Gateways, and Security Groups	5f457f1e1ea90ba9adb32d78
K0897	Knowledge	80	Identify the properties of virtualized compute resources (EC2, AMI, ECR, ECS, Elastic Beanstalk)	2021-12-10 18:39:06.902523	2021-12-10 18:39:06.902524		5f457f1e1ea90ba9adb32d79
K0898	Knowledge	80	Identify the properties of block level storage (EBS)	2021-12-10 18:39:06.903158	2021-12-10 18:39:06.903159		5f457f1e1ea90ba9adb32d7b
K0899	Knowledge	80	Identify the properties of object level storage (S3)	2021-12-10 18:39:06.903831	2021-12-10 18:39:06.903832		5f457f1e1ea90ba9adb32d7c
K0900	Knowledge	80	Understand the relationship between VPCs, subnets, and security groups	2021-12-10 18:39:06.904363	2021-12-10 18:39:06.904363		5f457f1e1ea90ba9adb32d7d
K0901	Knowledge	81	Explain the purpose of security risk guides (such as OWASP, etc.) and how to apply their recommendations to cloud applications	2021-12-10 18:39:06.905273	2021-12-10 18:39:06.905273		5f457f1e1ea90ba9adb32d7e
K0902	Knowledge	81	Explain the implications of the vulnerabilities described in the OWASP top 10	2021-12-10 18:39:06.905867	2021-12-10 18:39:06.905867		5f457f1e1ea90ba9adb32d80
K0903	Knowledge	82	Explain the implications of the factors in the 12 factor application methodology	2021-12-10 18:39:06.906664	2021-12-10 18:39:06.906665		5f457f1e1ea90ba9adb32d7f
K0904	Knowledge	82	Explain the purpose of different software architecture methodologies (12 factor, etc.) and how to apply them to cloud applications	2021-12-10 18:39:06.907273	2021-12-10 18:39:06.907274		5f457f1e1ea90ba9adb32d81
K0905	Knowledge	2	Describe the purpose and composition of an Agile Development Sprint	2021-12-10 18:39:06.90792	2021-12-20 17:05:43.153666		5f510c3c14d3cf5e42522436
K0906	Knowledge	2	Identify artifacts of a Sprint Planning meeting.	2021-12-10 18:39:06.908665	2021-12-20 17:06:00.568631		5f510c3c14d3cf5e42522437
K0907	Knowledge	2	Describe what actions to take if the Developers cannot complete work by the end of the Sprint.	2021-12-10 18:39:06.909298	2021-12-20 17:06:24.772606		5f510c3c14d3cf5e42522438
K0908	Knowledge	83	Identify the members of the flight and their roles	2021-12-10 18:39:06.910306	2021-12-10 18:39:06.910306	Flight CC, Chief, UTM, etc.	5f7624863fa286da1f515741
K0909	Knowledge	83	Identify which Policy documents govern our function and where to locate them.	2021-12-10 18:39:06.910908	2021-12-10 18:39:06.910908	JCT&CS, ACCI17-202's, https://confluence.90cos.cdl.af.mil/display/TRAIN/CYT+Policy+and+Analysis	5f7624863fa286da1f515742
K0910	Knowledge	83	Recognize the major courses in each work role's training roadmap.	2021-12-10 18:39:06.911527	2021-12-29 18:58:30.301915	IDF, ACTP, PSPO I	5f7624863fa286da1f515743
K0911	Knowledge	83	Describe the flights products and/or deliverables to the unit.	2021-12-10 18:39:06.912308	2021-12-10 18:39:06.912309	Training courses, Letter of X (LOX) document, CYT flight's homepage, MTTL, Training materials, Training Library, etc.	5f7624863fa286da1f515744
K0912	Knowledge	83	Describe the steps and/or events in evaluations process.	2021-12-10 18:39:06.912871	2021-12-10 18:39:06.912872	From new employee to adding them to the letter of X	5f7624863fa286da1f515745
K0913	Knowledge	83	Summarize the CYT flight's working agreement.	2021-12-10 18:39:06.913627	2021-12-10 18:39:06.913628	Include tests concerning the Communications plan	5f7624863fa286da1f515746
K0914	Knowledge	83	List information about examination scheduling	2021-12-10 18:39:06.914267	2021-12-10 18:39:06.914268	Frequency, Location of Schedule, how to find information about where to sign up	5f7624863fa286da1f515747
K0915	Knowledge	83	Describe the process to contribute to the Master Training Task List (MTTL).	2021-12-10 18:39:06.915049	2021-12-10 18:39:06.91505	Reference Contributing.md	5f7624863fa286da1f515748
K0916	Knowledge	83	Describe the four KSATs (Knowledge, Skills, Abilities, Tasks).	2021-12-10 18:39:06.915828	2021-12-10 18:39:06.915829		5f7624863fa286da1f515749
K0917	Knowledge	83	Describe the process to request training.	2021-12-10 18:39:06.916412	2021-12-10 18:39:06.916413		5f7624863fa286da1f51574a
K0918	Knowledge	83	Describe the process to request a Stan/Eval evaluation.	2021-12-10 18:39:06.917221	2021-12-10 18:39:06.917222		5f7624863fa286da1f51574b
K0919	Knowledge	83	Describe the role of a Stan/Eval Examiner (SEE).	2021-12-10 18:39:06.918051	2021-12-10 18:39:06.918052		5f7624863fa286da1f51574c
K0920	Knowledge	83	Describe a Virtual Training Record and its purpose.	2021-12-10 18:39:06.918566	2021-12-10 18:39:06.918567		5f7624863fa286da1f51574d
K0921	Knowledge	83	Describe the software systems used by the flight to perform its mission	2021-12-10 18:39:06.919155	2021-12-10 18:39:06.919156		5f7624863fa286da1f51574e
K0922	Knowledge	83	Describe the steps and/or events in training process.	2021-12-10 18:39:06.919755	2021-12-10 18:39:06.919755		5f7624873fa286da1f51574f
K0923	Knowledge	83	Describe the Flight methods and systems for task tracking	2021-12-10 18:39:06.920336	2021-12-10 18:39:06.920337		5f7624873fa286da1f515750
K0924	Knowledge	84	Know where to locate the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates on Confluence.	2021-12-10 18:39:06.921159	2021-12-10 18:39:06.921159		5f909bce37597b2eef89001f
K0925	Knowledge	2	Understand the purpose and contents of a Project Charter.	2021-12-10 18:39:06.921691	2021-12-10 18:39:06.921691	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060a8
K0927	Knowledge	2	Understand the Agile Definition of Done and who creates it.	2021-12-10 18:39:06.922323	2021-12-20 17:06:45.832101	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060aa
K0930	Knowledge	2	Describe an Epic in Agile Scrum.	2021-12-10 18:39:06.922876	2021-12-22 22:21:31.585324	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060ad
K0931	Knowledge	2	Understand the different types of project stakeholders.	2021-12-10 18:39:06.923404	2021-12-20 17:07:16.772193	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060ae
K0932	Knowledge	2	Understand the purpose and contents of a Scrum team Working Agreement.	2021-12-10 18:39:06.923917	2021-12-10 18:39:06.923918	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060af
K0933	Knowledge	2	Understand the purpose and goal of the Project Kickoff Meeting.	2021-12-10 18:39:06.924485	2021-12-10 18:39:06.924486	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060b0
K0934	Knowledge	2	Understand the purpose of the Product Roadmap.	2021-12-10 18:39:06.925103	2021-12-29 18:58:30.650647	Found in MADP documentaiton on Confluence	5fc1585f2bee7a1751f060b1
K0935	Knowledge	26	Identify which Security Classification Guides (SCG) apply to squadron's mission	2021-12-10 18:39:06.925847	2021-12-10 18:39:06.925848	cf. DoD Instruction 5200.01	5fcfb3a03d7fc7fc566c2be2
K0937	Knowledge	83	Describe the Location of the Vol 1	2021-12-10 18:39:06.92679	2021-12-10 18:39:06.926791		5ff77ed36ef9fc6003e089ee
K0938	Knowledge	83	Describe training requirements for IQT, MQT, upgrade/special mission, difference qualification, senior officer qualification and requalification	2021-12-10 18:39:06.927576	2021-12-10 18:39:06.927577		5ff77ed36ef9fc6003e089ef
K0939	Knowledge	83	Describe instructor requirements laid out in the Vol 1	2021-12-10 18:39:06.928472	2021-12-10 18:39:06.928473		5ff77ed36ef9fc6003e089f0
K0940	Knowledge	83	Describe currency requirements laid out in the Vol 1	2021-12-10 18:39:06.929354	2021-12-10 18:39:06.929354		5ff77ed36ef9fc6003e089f1
K0941	Knowledge	85	Understand each scrum artifact's related commitment.	2021-12-10 18:39:06.930475	2021-12-29 18:58:30.943984	requirement sources identified by original Scrum KSATs	602d4184e1754883d2237d6c
K0942	Knowledge	85	Understand the composition and purpose of the product goal.	2021-12-10 18:39:06.931315	2021-12-22 22:22:45.135256	requirement sources identified by original Scrum KSATs	602d41d16ab1bad036820ee9
K0943	Knowledge	85	Understand the purpose of an increment in scrum.	2021-12-10 18:39:06.932151	2021-12-20 17:09:19.336621	requirement sources identified by original Scrum KSATs	602d42b35e521690a255bb66
K0945	Knowledge	85	Understand the composition and purpose of the sprint backlog	2021-12-10 18:39:06.933956	2021-12-20 17:09:33.300485	requirement sources identified by original Scrum KSATs	602d4393306921754b492d76
K0946	Knowledge	85	Identify time-boxes associated with each Scrum event.	2021-12-10 18:39:06.934864	2021-12-20 17:09:38.955523	requirement sources identified by original Scrum KSATs	602d4413c4a4c215f06225e1
K0947	Knowledge	85	Understand the composition and capabilities of the Scrum team.	2021-12-10 18:39:06.93569	2021-12-20 17:09:49.666061	requirement sources identified by original Scrum KSATs	602d4470996d765c56f7308d
K0948	Knowledge	85	Understand the three artifacts of scrum.	2021-12-10 18:39:06.936532	2021-12-20 17:09:55.785558		602401e836ca22d17a1b2abd
K0949	Knowledge	52	Describe the processes and coordination required to introduce new mission equipment to the development networks.	2021-12-10 18:39:06.937345	2021-12-10 18:39:06.937345		60269d55cd04c31a7307cdfd
K0950	Knowledge	9	Understand the three phases of how the 90 COS manages development projects.	2021-12-10 18:39:06.93824	2021-12-10 18:39:06.93824		60c279e8b6142e8924fc20bc
K0951	Knowledge	9	Understand the purpose and content of the Front Door Backlog Pitch.	2021-12-10 18:39:06.939099	2021-12-10 18:39:06.939099		60c279e8b6142e8924fc20bd
K0952	Knowledge	9	Understand the purpose and content of the Front Door Execution Pitch.	2021-12-10 18:39:06.939904	2021-12-10 18:39:06.939905		60c279e8b6142e8924fc20be
K0953	Knowledge	9	Understand the role of the Tool Champion.	2021-12-10 18:39:06.940799	2021-12-10 18:39:06.9408		60c279e8b6142e8924fc20bf
K0954	Knowledge	9	Understand the purpose and goal of Sprint 0.	2021-12-10 18:39:06.941693	2021-12-10 18:39:06.941694		60c279e8b6142e8924fc20c0
K0955	Knowledge	9	Describe a Project Retrospective.	2021-12-10 18:39:06.942611	2021-12-10 18:39:06.942612		60c279e9b6142e8924fc20c1
K0956	Knowledge	9	Describe the Project Artifacts for a completed project.	2021-12-10 18:39:06.943474	2021-12-10 18:39:06.943475		60c279e9b6142e8924fc20c2
K0957	Knowledge	9	Describe the composition of a 90COS DevCrew.	2021-12-10 18:39:06.944355	2021-12-10 18:39:06.944356		60c279e9b6142e8924fc20c3
K0958	Knowledge	9	Understand how DevCrews are assigned projects.	2021-12-10 18:39:06.945232	2021-12-10 18:39:06.945233		60c279e9b6142e8924fc20c4
K0959	Knowledge	9	Understand the steps needed to release and field a completed product.	2021-12-10 18:39:06.946166	2021-12-10 18:39:06.946167		60c279e9b6142e8924fc20c5
K0960	Knowledge	86	Explain the effects of configuring estimation and time tracking on Scrum boards	2021-12-10 18:39:06.947346	2021-12-10 18:39:06.947347		5f457f1e1ea90ba9adb32ed6
K0961	Knowledge	86	Explain the concepts of using epics and versions in a Software project.	2021-12-10 18:39:06.948117	2021-12-10 18:39:06.948118		5f457f1e1ea90ba9adb32ed8
K0962	Knowledge	86	Describe unique characteristics of different issue types when used in a Jira Software context (epics, standard issue types, sub-task issue types).	2021-12-10 18:39:06.948797	2021-12-10 18:39:06.948798		5f457f1e1ea90ba9adb32edd
K0963	Knowledge	86	Explain and predict the impacts of modifications to an active sprint (scope changes).	2021-12-10 18:39:06.949536	2021-12-10 18:39:06.949537		5f457f1e1ea90ba9adb32ed9
K0964	Knowledge	86	Explain the concept and impact of ranking issues.	2021-12-10 18:39:06.950162	2021-12-10 18:39:06.950162		5f457f1e1ea90ba9adb32edc
K0965	Knowledge	85	Describe the difference between capacity and velocity	2021-12-10 18:39:06.950773	2021-12-22 22:21:42.607949		61254f42e5b9b9d1130dcc8a
K0966	Knowledge	2	Describe the difference between Kanban and Scrum	2021-12-10 18:39:06.951428	2021-12-22 22:22:04.343378		61254f42e5b9b9d1130dcc8b
K0967	Knowledge	85	Describe the Agile Definition of Ready	2021-12-10 18:39:06.952093	2021-12-10 18:39:06.952094		61254f42e5b9b9d1130dcc8d
K0968	Knowledge	85	Describe the responsibilities of the Scrum Master	2021-12-10 18:39:06.95263	2021-12-10 18:39:06.95263		61254f42e5b9b9d1130dcc8e
K0969	Knowledge	9	Describe the purpose of the Requirements Traceability Matrix	2021-12-10 18:39:06.953379	2021-12-10 18:39:06.95338		61254f42e5b9b9d1130dcc8f
K0970	Knowledge	9	Describe how to determine a product's required tests and whether the tests need to be gated	2021-12-10 18:39:06.954468	2021-12-10 18:39:06.954469		61254f42e5b9b9d1130dcc90
K0971	Knowledge	85	Describe the responsibilities of the Developers.	2021-12-10 18:39:06.955222	2021-12-10 18:39:06.955223		61256071f6bc539c56c786b7
K0972	Knowledge	9	Understand when ELA (Evaluated Level Assurance) testing is required	2021-12-10 18:39:06.958322	2021-12-10 18:39:06.958323		6155f2d90c10df20b46d2564
K0973	Knowledge	85	Explain the composition and purpose of the product backlog.	2021-12-10 18:39:06.956278	2021-12-10 18:39:06.95628		615609b9236ce355389d3e1f
K0974	Knowledge	85	Explain how to interpret a burn-down chart.	2021-12-10 18:39:06.957275	2021-12-10 18:39:06.957277		6155e6df2eca5b72171cf8ea
S0001	Skill	24	Skill in conducting vulnerability scans and recognizing vulnerabilities in security systems.	2021-12-10 18:39:07.01898	2021-12-10 18:39:07.018981	SP-DEV-001	5f457f1e1ea90ba9adb32db1
S0002	Skill	24	Skill in conducting software debugging.	2021-12-10 18:39:07.019885	2021-12-10 18:39:07.019887	SP-DEV-001	5f457f1e1ea90ba9adb32dae
S0003	Skill	24	Skill in creating programs that validate and process multiple inputs including command line arguments, environmental variables, and input streams.	2021-12-10 18:39:07.020716	2021-12-10 18:39:07.020717	SP-DEV-001	5f457f1e1ea90ba9adb32daf
S0004	Skill	24	Skill in designing countermeasures to identified security risks.	2021-12-10 18:39:07.021543	2021-12-10 18:39:07.021544	SP-DEV-001	5f457f1e1ea90ba9adb32dab
S0005	Skill	24	Skill in developing and applying security system access controls.	2021-12-10 18:39:07.022447	2021-12-10 18:39:07.022448	SP-DEV-001	5f457f1e1ea90ba9adb32db2
S0006	Skill	24	Skill in discerning the protection needs (i.e., security controls) of information systems and networks.	2021-12-10 18:39:07.023324	2021-12-10 18:39:07.023325	SP-DEV-001	5f457f1e1ea90ba9adb32db0
S0007	Skill	24	Skill in writing code in a currently supported programming language (e.g., Java, C++).	2021-12-10 18:39:07.024584	2021-12-10 18:39:07.024585	SP-DEV-001	5f457f1e1ea90ba9adb32db3
S0008	Skill	24	Skill in secure test plan design (e. g. unit, integration, system, acceptance).	2021-12-10 18:39:07.025495	2021-12-10 18:39:07.025496	SP-DEV-001	5f457f1e1ea90ba9adb32d87
S0009	Skill	24	Skill in using Public-Key Infrastructure (PKI) encryption and digital signature capabilities into applications (e.g., S/MIME email, SSL traffic).	2021-12-10 18:39:07.026485	2021-12-10 18:39:07.026486	SP-DEV-001	5f457f1e1ea90ba9adb32d94
S0010	Skill	24	Skill in developing applications that can log and handle errors, exceptions, and application faults and logging.	2021-12-10 18:39:07.027351	2021-12-10 18:39:07.027352	SP-DEV-001	5f457f1e1ea90ba9adb32d8c
S0011	Skill	24	Skill in using code analysis tools.	2021-12-10 18:39:07.02838	2021-12-10 18:39:07.028381	SP-DEV-001	5f457f1e1ea90ba9adb32d8d
S0012	Skill	24	Skill in performing root cause analysis.	2021-12-10 18:39:07.029331	2021-12-10 18:39:07.029332	SP-DEV-001	5f457f1e1ea90ba9adb32d88
S0013	Skill	24	Skill to apply cybersecurity and privacy principles to organizational requirements (relevant to confidentiality, integrity, availability, authentication, non-repudiation).	2021-12-10 18:39:07.030227	2021-12-10 18:39:07.030228	SP-DEV-001	5f457f1e1ea90ba9adb32d9a
S0014	Skill	5	Skill in parsing command line arguments using built-in functionality.	2021-12-10 18:39:07.031141	2021-12-10 18:39:07.031142	argparse library; In addition to the program package itself (program plus comments, documentation, etc) must PRESENT the functionality to  an evaluator.	5f457f1e1ea90ba9adb32dad
S0015	Skill	91	Register a Gitlab runner.	2021-12-10 18:39:07.032164	2021-12-10 18:39:07.032165		5f457f1e1ea90ba9adb32dac
S0016	Skill	4	Debug a C program.	2021-12-10 18:39:07.03334	2021-12-10 18:39:07.033342		5f457f1e1ea90ba9adb32d99
S0017	Skill	5	Debug a python script.	2021-12-10 18:39:07.034634	2021-12-10 18:39:07.034636		5f457f1e1ea90ba9adb32d90
S0018	Skill	27	Commit changes to a working branch.	2021-12-10 18:39:07.035418	2021-12-10 18:39:07.035419	status, add, commit	5f457f1e1ea90ba9adb32d89
S0019	Skill	27	Create and checkout a branch.	2021-12-10 18:39:07.036138	2021-12-10 18:39:07.036139	Using Git CLI	5f457f1e1ea90ba9adb32d8a
S0020	Skill	27	Update a project	2021-12-10 18:39:07.036854	2021-12-10 18:39:07.036855	push, pull	5f457f1e1ea90ba9adb32d8b
S0021	Skill	27	Inspect and compare a project	2021-12-10 18:39:07.0374	2021-12-10 18:39:07.0374	diff	5f457f1e1ea90ba9adb32d8e
S0022	Skill	27	Clone a project	2021-12-10 18:39:07.038198	2021-12-10 18:39:07.038199		5f457f1e1ea90ba9adb32d8f
S0023	Skill	5	Declare and implement data types.	2021-12-10 18:39:07.038935	2021-12-10 18:39:07.038935	Integer, Float, String, and Boolean	5f457f1e1ea90ba9adb32d97
S0024	Skill	5	Declare and/or implement container data type.	2021-12-10 18:39:07.039854	2021-12-10 18:39:07.039855	List, Multi-Dimensional List, Dictionary, Tuple, and Set	5f457f1e1ea90ba9adb32d95
S0026	Skill	5	Utilize standard library modules.	2021-12-10 18:39:07.040741	2021-12-10 18:39:07.040742	sys, os, regex et.al.	5f457f1e1ea90ba9adb32d93
S0027	Skill	5	Install for a Python package using PIP.	2021-12-10 18:39:07.041623	2021-12-10 18:39:07.041623		5f457f1e1ea90ba9adb32d91
S0028	Skill	5	Set up and replicate a Python Virtual Environment.	2021-12-10 18:39:07.042515	2021-12-10 18:39:07.042516		5f457f1e1ea90ba9adb32d92
S0029	Skill	29	Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.	2021-12-10 18:39:07.043321	2021-12-10 18:39:07.043321		5f457f1e1ea90ba9adb32d98
S0030	Skill	29	Utilize bitwise operators to manipulate binary values.	2021-12-10 18:39:07.04428	2021-12-10 18:39:07.04428		5f457f1e1ea90ba9adb32d96
S0031	Skill	29	Utilize logical operators to formulate boolean expressions.	2021-12-10 18:39:07.045109	2021-12-10 18:39:07.04511		5f457f1e1ea90ba9adb32d9c
S0032	Skill	29	Utilize relational operators to formulate boolean expressions.	2021-12-10 18:39:07.045979	2021-12-10 18:39:07.04598		5f457f1e1ea90ba9adb32d9f
S0033	Skill	29	Utilize assignment operators to update a variable.	2021-12-10 18:39:07.046608	2021-12-10 18:39:07.046608		5f457f1e1ea90ba9adb32da4
S0034	Skill	4	Declare and implement appropriate data types for program requirements.	2021-12-10 18:39:07.047255	2021-12-10 18:39:07.047256	short, int, float, char, double, and long	5f457f1e1ea90ba9adb32d9b
S0035	Skill	4	Declare and/or implement of arrays and multi-dimensional arrays.	2021-12-10 18:39:07.04801	2021-12-10 18:39:07.048011	data types besides char *	5f457f1e1ea90ba9adb32d9d
S0036	Skill	4	Declare and implement a char * array (string).	2021-12-10 18:39:07.048801	2021-12-10 18:39:07.048802		5f457f1e1ea90ba9adb32da0
S0037	Skill	5	Open and close an existing file.	2021-12-10 18:39:07.049508	2021-12-10 18:39:07.049509		5f457f1e1ea90ba9adb32d9e
S0038	Skill	5	Read, parse, write (append, insert, modify) file data.	2021-12-10 18:39:07.050119	2021-12-10 18:39:07.05012		5f457f1e1ea90ba9adb32da1
S0039	Skill	5	Create and delete a file.	2021-12-10 18:39:07.050657	2021-12-10 18:39:07.050657		5f457f1e1ea90ba9adb32da3
S0040	Skill	5	Determine the size of a file.	2021-12-10 18:39:07.051244	2021-12-10 18:39:07.051245		5f457f1e1ea90ba9adb32da2
S0041	Skill	5	Determine location of content within a file.	2021-12-10 18:39:07.051898	2021-12-10 18:39:07.051899		5f457f1e1ea90ba9adb32da5
S0042	Skill	4	Open and close an existing file.	2021-12-10 18:39:07.052589	2021-12-10 18:39:07.05259		5f457f1e1ea90ba9adb32da6
S0043	Skill	4	Read, parse, write (append, insert, modify) file data.	2021-12-10 18:39:07.053404	2021-12-10 18:39:07.053405		5f457f1e1ea90ba9adb32da9
S0044	Skill	4	Create and delete file.	2021-12-10 18:39:07.054304	2021-12-10 18:39:07.054304		5f457f1e1ea90ba9adb32daa
S0045	Skill	4	Determine the size of a file.	2021-12-10 18:39:07.055142	2021-12-10 18:39:07.055143		5f457f1e1ea90ba9adb32da8
S0046	Skill	4	Determine location of content within a file.	2021-12-10 18:39:07.056061	2021-12-10 18:39:07.056062		5f457f1e1ea90ba9adb32da7
S0047	Skill	5	Implement a function that returns multiple values.	2021-12-10 18:39:07.056895	2021-12-10 18:39:07.056896		5f457f1e1ea90ba9adb32db4
S0048	Skill	29	Implement a function that receives input parameters.	2021-12-10 18:39:07.057685	2021-12-10 18:39:07.057686		5f457f1e1ea90ba9adb32dd3
S0049	Skill	29	Implement a recursive function.	2021-12-10 18:39:07.058547	2021-12-10 18:39:07.058547		5f457f1e1ea90ba9adb32db6
S0050	Skill	29	Implement a function that doesn't return a value.	2021-12-10 18:39:07.059376	2021-12-10 18:39:07.059376	void return type in C	5f457f1e1ea90ba9adb32db5
S0051	Skill	4	Implement a function with pass by reference using the & reference operator.	2021-12-10 18:39:07.060372	2021-12-10 18:39:07.060372		5f457f1e1ea90ba9adb32db8
S0052	Skill	29	Implement a function that returns a single value.	2021-12-10 18:39:07.061282	2021-12-10 18:39:07.061282		5f457f1e1ea90ba9adb32dc4
S0053	Skill	4	Implement a function that returns a memory reference.	2021-12-10 18:39:07.062154	2021-12-10 18:39:07.062154		5f457f1e1ea90ba9adb32dd2
S0054	Skill	16	Create and destroy a Weighted Graph.	2021-12-10 18:39:07.063026	2021-12-10 18:39:07.063027		5f457f1e1ea90ba9adb32dca
S0055	Skill	16	Create and destroy a Linked List.	2021-12-10 18:39:07.063939	2021-12-10 18:39:07.063939		5f457f1e1ea90ba9adb32dc9
S0056	Skill	16	Create and destroy a Doubly Linked List.	2021-12-10 18:39:07.064852	2021-12-10 18:39:07.064852		5f457f1e1ea90ba9adb32dcb
S0057	Skill	16	Create and destroy a Circularly Linked List.	2021-12-10 18:39:07.065734	2021-12-10 18:39:07.065735		5f457f1e1ea90ba9adb32dd1
S0058	Skill	16	Create and destroy a Queue.	2021-12-10 18:39:07.066659	2021-12-10 18:39:07.06666		5f457f1e1ea90ba9adb32db7
S0059	Skill	16	Create and destroy a Tree.	2021-12-10 18:39:07.067506	2021-12-10 18:39:07.067507		5f457f1e1ea90ba9adb32dcc
S0060	Skill	16	Create and destroy a Binary Search Tree.	2021-12-10 18:39:07.068428	2021-12-10 18:39:07.068429		5f457f1e1ea90ba9adb32dd0
S0061	Skill	16	Create and destroy a Hash Table.	2021-12-10 18:39:07.069329	2021-12-10 18:39:07.06933		5f457f1e1ea90ba9adb32dce
S0062	Skill	16	Create and destroy a Stack.	2021-12-10 18:39:07.070207	2021-12-10 18:39:07.070208		5f457f1e1ea90ba9adb32dc8
S0063	Skill	16	Find item in a Weighted Graph.	2021-12-10 18:39:07.071061	2021-12-10 18:39:07.071061		5f457f1e1ea90ba9adb32dcf
S0064	Skill	16	Add and remove nodes from a a Weighted Graph.	2021-12-10 18:39:07.071974	2021-12-10 18:39:07.071975		5f457f1e1ea90ba9adb32dcd
S0065	Skill	16	Find an item in a Linked List.	2021-12-10 18:39:07.072882	2021-12-10 18:39:07.072883		5f457f1e1ea90ba9adb32dbc
S0066	Skill	16	Add and remove nodes from a Linked List.	2021-12-10 18:39:07.073789	2021-12-10 18:39:07.07379		5f457f1e1ea90ba9adb32dbb
S0067	Skill	16	Find an item in a Doubly Linked List.	2021-12-10 18:39:07.074681	2021-12-10 18:39:07.074682		5f457f1e1ea90ba9adb32db9
S0068	Skill	16	Add and remove nodes from a Doubly Linked List.	2021-12-10 18:39:07.075522	2021-12-10 18:39:07.075523		5f457f1e1ea90ba9adb32dba
S0069	Skill	16	Find an item in a Circularly Linked List.	2021-12-10 18:39:07.076546	2021-12-10 18:39:07.076547		5f457f1e1ea90ba9adb32dbd
S0070	Skill	16	Add and remove nodes from a Circularly Linked List.	2021-12-10 18:39:07.077452	2021-12-10 18:39:07.077453		5f457f1e1ea90ba9adb32dbf
S0071	Skill	16	Enqueue and dequeue a Queue.	2021-12-10 18:39:07.07833	2021-12-10 18:39:07.078331		5f457f1e1ea90ba9adb32dbe
S0072	Skill	16	Find an item in a Tree.	2021-12-10 18:39:07.079101	2021-12-10 18:39:07.079102		5f457f1e1ea90ba9adb32dc5
S0073	Skill	16	Add and remove nodes from a Tree.	2021-12-10 18:39:07.079683	2021-12-10 18:39:07.079684		5f457f1e1ea90ba9adb32dc0
S0074	Skill	16	Find an item in a Binary Search Tree.	2021-12-10 18:39:07.080521	2021-12-10 18:39:07.080522		5f457f1e1ea90ba9adb32dc1
S0075	Skill	16	Add and remove nodes from a Binary Search Tree.	2021-12-10 18:39:07.081368	2021-12-10 18:39:07.081369		5f457f1e1ea90ba9adb32dc2
S0076	Skill	16	Find an item in a Hash Table.	2021-12-10 18:39:07.082252	2021-12-10 18:39:07.082253		5f457f1e1ea90ba9adb32dc3
S0077	Skill	16	Add and remove items from a Hash Table.	2021-12-10 18:39:07.083336	2021-12-10 18:39:07.083337		5f457f1e1ea90ba9adb32dc6
S0078	Skill	16	Push and pop a Stack.	2021-12-10 18:39:07.084448	2021-12-10 18:39:07.084448		5f457f1e1ea90ba9adb32dc7
S0079	Skill	29	Validate expected input.	2021-12-10 18:39:07.085406	2021-12-10 18:39:07.085407	Values passed to parameters, command line input, read from user input or a file etc.	5f457f1e1ea90ba9adb32dd7
S0080	Skill	5	Demonstrate the skill to implement exception handling.	2021-12-10 18:39:07.086386	2021-12-10 18:39:07.086387		5f457f1e1ea90ba9adb32dd4
S0081	Skill	29	Implement a looping construct.	2021-12-10 18:39:07.087169	2021-12-10 18:39:07.087169	For, while (do/while in C)	5f457f1e1ea90ba9adb32df2
S0082	Skill	29	Implement conditional control flow constructs.	2021-12-10 18:39:07.088287	2021-12-10 18:39:07.088288	if, if/else, switch etc.	5f457f1e1ea90ba9adb32dfa
S0084	Skill	4	Compile and link a program.	2021-12-10 18:39:07.089243	2021-12-10 18:39:07.089244		5f457f1e1ea90ba9adb32dda
S0085	Skill	4	Use a cross-compiler to compile position-independent code.	2021-12-10 18:39:07.090292	2021-12-10 18:39:07.090293		5f457f1e1ea90ba9adb32dde
S0087	Skill	31	Utilize a JSON Module.	2021-12-10 18:39:07.09143	2021-12-10 18:39:07.091432		5f457f1e1ea90ba9adb32dd6
S0089	Skill	4	Use of Valgrind to identify and correct memory leaks and context errors.	2021-12-10 18:39:07.092495	2021-12-10 18:39:07.092496	with -leak-check=full	5f457f1e1ea90ba9adb32dd5
S0090	Skill	4	Allocate memory on the heap (malloc).	2021-12-10 18:39:07.093279	2021-12-10 18:39:07.093279		5f457f1e1ea90ba9adb32de1
S0091	Skill	4	Unallocating memory from the heap (free).	2021-12-10 18:39:07.093888	2021-12-10 18:39:07.093889		5f457f1e1ea90ba9adb32de5
S0092	Skill	5	Handle partial reads/writes during serialization and de-serialization.	2021-12-10 18:39:07.094814	2021-12-10 18:39:07.094815		5f457f1e1ea90ba9adb32deb
S0093	Skill	5	Serialize fixed size multi-byte types between systems of differing endianness.	2021-12-10 18:39:07.095709	2021-12-10 18:39:07.095709		5f457f1e1ea90ba9adb32dec
S0094	Skill	5	Serialize and de-serialize variable sized data structures between systems of differing endianness.	2021-12-10 18:39:07.096582	2021-12-10 18:39:07.096583		5f457f1e1ea90ba9adb32ddd
S0095	Skill	31	Utilize Wireshark to analyze and debug a client/server program's network traffic.	2021-12-10 18:39:07.09748	2021-12-10 18:39:07.097481		5f457f1e1ea90ba9adb32de3
S0096	Skill	5	Utilize the Socket library.	2021-12-10 18:39:07.098377	2021-12-10 18:39:07.098377		5f457f1e1ea90ba9adb32dfb
S0097	Skill	4	Create and use pointers.	2021-12-10 18:39:07.099198	2021-12-10 18:39:07.099198	Declare, assign, derefernce etc.	5f457f1e1ea90ba9adb32de4
S0098	Skill	4	Implement a function pointer to call another function.	2021-12-10 18:39:07.10005	2021-12-10 18:39:07.100051		5f457f1e1ea90ba9adb32e02
S0099	Skill	4	Use pointer arithmetic to traverse an array.	2021-12-10 18:39:07.100882	2021-12-10 18:39:07.100883	.	5f457f1e1ea90ba9adb32ddf
S0100	Skill	4	Handle partial reads/writes during serialization and de-serialization.	2021-12-10 18:39:07.101713	2021-12-10 18:39:07.101714		5f457f1e1ea90ba9adb32dd8
S0101	Skill	4	Serialize fixed size multi-byte types between systems of differing endianness.	2021-12-10 18:39:07.102436	2021-12-10 18:39:07.102437		5f457f1e1ea90ba9adb32dd9
S0102	Skill	4	Serialize and de-serialize variable sized data structures between systems of differing endianness.	2021-12-10 18:39:07.103104	2021-12-10 18:39:07.103105		5f457f1e1ea90ba9adb32ddb
S0103	Skill	4	Utilize the Socket library to establish network communications between systems.	2021-12-10 18:39:07.103975	2021-12-10 18:39:07.103976		5f457f1e1ea90ba9adb32ddc
S0104	Skill	4	Use preprocessor directives to develop portable programs.	2021-12-10 18:39:07.104918	2021-12-10 18:39:07.104919	#include, #define, #ifndef etc.	5f457f1e1ea90ba9adb32de8
S0105	Skill	11	Create and use C datatypes: Word, DoubleWord, and Quadword.	2021-12-10 18:39:07.106068	2021-12-10 18:39:07.106069		5f457f1e1ea90ba9adb32dfd
S0108	Skill	4	Utilize post and pre increment/decrement operators.	2021-12-10 18:39:07.106883	2021-12-10 18:39:07.106884		5f457f1e1ea90ba9adb32df4
S0109	Skill	4	Implement a program that uses standard main() command line arguments.	2021-12-10 18:39:07.107847	2021-12-10 18:39:07.107848	int argc, char **argv, char **envp	5f457f1e1ea90ba9adb32de0
S0110	Skill	29	Implement error handling.	2021-12-10 18:39:07.108919	2021-12-10 18:39:07.10892	buffer overflow, array index out of bounds, divide by zero etc.	5f457f1e1ea90ba9adb32df8
S0111	Skill	4	Build and utilize a static library.	2021-12-10 18:39:07.109955	2021-12-10 18:39:07.109955		5f457f1e1ea90ba9adb32de2
S0112	Skill	4	Build a statically linked program.	2021-12-10 18:39:07.110759	2021-12-10 18:39:07.11076		5f457f1e1ea90ba9adb32df3
S0113	Skill	4	Sanitize input data.	2021-12-10 18:39:07.1116	2021-12-10 18:39:07.1116	removing leading/trailing whitespace in a string etc.	5f457f1e1ea90ba9adb32df7
S0114	Skill	11	Create and utilize structures.	2021-12-10 18:39:07.112771	2021-12-10 18:39:07.112772		5f457f1e1ea90ba9adb32dfc
S0115	Skill	11	Utilize bit operations.	2021-12-10 18:39:07.113848	2021-12-10 18:39:07.113849		5f457f1e1ea90ba9adb32de6
S0117	Skill	11	Utilize labels for control flow.	2021-12-10 18:39:07.1148	2021-12-10 18:39:07.114801		5f457f1e1ea90ba9adb32de7
S0118	Skill	11	Implement conditional control flow.	2021-12-10 18:39:07.115617	2021-12-10 18:39:07.115617		5f457f1e1ea90ba9adb32df5
S0119	Skill	11	Create and implement functions.	2021-12-10 18:39:07.116476	2021-12-10 18:39:07.116476		5f457f1e1ea90ba9adb32de9
S0120	Skill	11	Use interrupts to execute OS system calls.	2021-12-10 18:39:07.117301	2021-12-10 18:39:07.117302		5f457f1e1ea90ba9adb32dea
S0121	Skill	11	Use pre-defined utility functions.	2021-12-10 18:39:07.118323	2021-12-10 18:39:07.118325	strlen, memcpy, memset, etc	5f457f1e1ea90ba9adb32df6
S0122	Skill	11	Invoke system calls.	2021-12-10 18:39:07.119272	2021-12-10 18:39:07.119273		5f457f1e1ea90ba9adb32ded
S0123	Skill	11	Allocate memory.	2021-12-10 18:39:07.120307	2021-12-10 18:39:07.120308		5f457f1e1ea90ba9adb32df1
S0124	Skill	11	Implement file handling.	2021-12-10 18:39:07.121247	2021-12-10 18:39:07.121248		5f457f1e1ea90ba9adb32df9
S0125	Skill	11	Utilize general purpose instructions.	2021-12-10 18:39:07.12216	2021-12-10 18:39:07.122161		5f457f1e1ea90ba9adb32dee
S0126	Skill	11	Utilize arithmetic instructions.	2021-12-10 18:39:07.123076	2021-12-10 18:39:07.123077		5f457f1e1ea90ba9adb32def
S0128	Skill	11	Utilize common conditional instructions.	2021-12-10 18:39:07.124092	2021-12-10 18:39:07.124093		5f457f1e1ea90ba9adb32df0
S0129	Skill	11	Utilize common string instructions.	2021-12-10 18:39:07.125132	2021-12-10 18:39:07.125133		5f457f1e1ea90ba9adb32dff
S0130	Skill	11	Utilize a GDB debugger to identify errors in a program.	2021-12-10 18:39:07.12601	2021-12-10 18:39:07.126012		5f457f1e1ea90ba9adb32e16
S0131	Skill	11	Utilize WinDBG debugger to identify errors in a program.	2021-12-10 18:39:07.126788	2021-12-10 18:39:07.126789		5f457f1e1ea90ba9adb32dfe
S0132	Skill	11	Write Assembly code for different processor modes.	2021-12-10 18:39:07.127861	2021-12-10 18:39:07.127862		5f457f1e1ea90ba9adb32e14
S0133	Skill	11	Utilize the stack frame	2021-12-10 18:39:07.129016	2021-12-10 18:39:07.129017		5f457f1e1ea90ba9adb32e18
S0134	Skill	11	Utilize registers and sub-registers	2021-12-10 18:39:07.129652	2021-12-10 18:39:07.129653		5f457f1e1ea90ba9adb32e21
S0135	Skill	11	Implement branch instructions.	2021-12-10 18:39:07.130493	2021-12-10 18:39:07.130494		5f457f1e1ea90ba9adb32e1e
S0136	Skill	11	Utilize x86 calling conventions	2021-12-10 18:39:07.131254	2021-12-10 18:39:07.131255		5f457f1e1ea90ba9adb32e20
S0137	Skill	11	Utilize name mangling	2021-12-10 18:39:07.131936	2021-12-10 18:39:07.131937		5f457f1e1ea90ba9adb32e04
S0138	Skill	11	Utilize flags	2021-12-10 18:39:07.132759	2021-12-10 18:39:07.13276		5f457f1e1ea90ba9adb32e13
S0139	Skill	11	Demonstrate the skill in implementing bitwise instructions	2021-12-10 18:39:07.133698	2021-12-10 18:39:07.133698	shl, shr, or, and, xor, not, rol, ror, sar, sal	5f457f1e1ea90ba9adb32e17
S0140	Skill	11	Implement protection mechanisms	2021-12-10 18:39:07.134605	2021-12-10 18:39:07.134606		5f457f1e1ea90ba9adb32e10
S0141	Skill	11	Execute instructions using raw opcodes	2021-12-10 18:39:07.135268	2021-12-10 18:39:07.135269		5f457f1e1ea90ba9adb32e1c
S0142	Skill	11	Implement byte ordering	2021-12-10 18:39:07.135916	2021-12-10 18:39:07.135916		5f457f1e1ea90ba9adb32e2a
S0143	Skill	11	Utilize a debugger to identify errors in a program.	2021-12-10 18:39:07.136689	2021-12-10 18:39:07.13669		5f457f1e1ea90ba9adb32e1d
S0144	Skill	4	Utilize threads in a multi-threaded program.	2021-12-10 18:39:07.137619	2021-12-10 18:39:07.137619		5f457f1e1ea90ba9adb32e15
S0145	Skill	4	Utilize a semaphore in a multi-threaded program.	2021-12-10 18:39:07.138662	2021-12-10 18:39:07.138662		5f457f1e1ea90ba9adb32e00
S0146	Skill	4	Utilize mutexes in a multithreaded program.	2021-12-10 18:39:07.139825	2021-12-10 18:39:07.139825		5f457f1e1ea90ba9adb32e01
S0147	Skill	4	Remediate formatting string vulnerabilities.	2021-12-10 18:39:07.141023	2021-12-10 18:39:07.141024		5f457f1e1ea90ba9adb32e03
S0148	Skill	4	Implement safe buffer size allocation	2021-12-10 18:39:07.141962	2021-12-10 18:39:07.141962		5f457f1e1ea90ba9adb32e09
S0149	Skill	4	Model complex functionality as a state-machine.	2021-12-10 18:39:07.1429	2021-12-10 18:39:07.142904		5f457f1e1ea90ba9adb32e05
S0150	Skill	4	Establish a secure communications channel using an SSL library.	2021-12-10 18:39:07.143839	2021-12-10 18:39:07.143839		5f457f1e1ea90ba9adb32e06
S0151	Skill	4	Zero-out memory securely	2021-12-10 18:39:07.144887	2021-12-10 18:39:07.144888		5f457f1e1ea90ba9adb32e07
S0152	Skill	4	Perform Windows API error analysis	2021-12-10 18:39:07.145993	2021-12-10 18:39:07.145993	GetLastError(), helpmsg	5f457f1e1ea90ba9adb32e08
S0153	Skill	4	Utilize Windows API File IO	2021-12-10 18:39:07.146873	2021-12-10 18:39:07.146873	functions: CreateFile(), HANDLE, FindFirstFile(), FindFirstFileEx(), FindNextFile(), FindClose(), ReadFile(), WriteFiLE(), CloseHandle()	5f457f1e1ea90ba9adb32e0a
S0154	Skill	4	Utilize MSDN Documents to find function details	2021-12-10 18:39:07.147784	2021-12-10 18:39:07.147785	registry, Processes (CreateProcess(), Synchronization: WaitForSingleObject()), Threads (CreateThread(), CreateMutex()), Windows Sockets (WSAStartup), WSACleanup()...), Windows Services (StartServiceCtrlDispatcher()...)	5f457f1e1ea90ba9adb32e0b
S0155	Skill	4	Utilize man pages to find function details and utilize function	2021-12-10 18:39:07.148655	2021-12-10 18:39:07.148655	File IO, File and Directory Management, Date/Time Conventions, Signals, IPC, Sockets, Inline Functions, Buffered IO, Advanced IO, PIDS, Fork(), exec(), wait(), waidpid(), system(), setuid(), setgid(), setreuid(), setreguid(), setpgid(), getpriority(), setpriority(), nice()	5f457f1e1ea90ba9adb32e0d
S0156	Skill	4	Utilize a struct composite data type.	2021-12-10 18:39:07.149433	2021-12-10 18:39:07.149433		5f457f1e1ea90ba9adb32e0f
S0157	Skill	11	Utilize the process stack.	2021-12-10 18:39:07.150002	2021-12-10 18:39:07.150003		5f457f1e1ea90ba9adb32e0c
S0158	Skill	4	Create and use header files.	2021-12-10 18:39:07.150573	2021-12-10 18:39:07.150573		5f457f1e1ea90ba9adb32e0e
S0159	Skill	4	Type cast a variable to a different data type.	2021-12-10 18:39:07.151091	2021-12-10 18:39:07.151092		5f457f1e1ea90ba9adb32e11
S0160	Skill	4	Utilize the standard library.	2021-12-10 18:39:07.151596	2021-12-10 18:39:07.151596	stdlib, string (strncpy, strcpy, strncmp, strcmp, strlen etc.), stdio, ctype, math etc.	5f457f1e1ea90ba9adb32e19
S0161	Skill	29	Write and utilize regular expressions in a program.	2021-12-10 18:39:07.1522	2021-12-10 18:39:07.1522		5f457f1e1ea90ba9adb32e12
S0162	Skill	68	Configure automated testing frameworks & tools within a projects CI pipeline.	2021-12-10 18:39:07.152837	2021-12-10 18:39:07.152838	 (e.g. DART, DEADCODE, METATOTAL, etc.).	5f457f1e1ea90ba9adb32e1a
S0163	Skill	68	Create automated functional tests.	2021-12-10 18:39:07.153629	2021-12-10 18:39:07.15363		5f457f1e1ea90ba9adb32e1b
S0164	Skill	68	Develop individual automated tests, jobs, and/or scripts to validate/satisfy acceptance criteria.	2021-12-10 18:39:07.154247	2021-12-10 18:39:07.154248		5f457f1e1ea90ba9adb32e1f
S0165	Skill	68	Document how automated testing satisfies test requirements within test plans and reports.	2021-12-10 18:39:07.154972	2021-12-10 18:39:07.154973		5f457f1e1ea90ba9adb32e22
S0166	Skill	68	Produce necessary artifacts to support test report generation.	2021-12-10 18:39:07.15558	2021-12-10 18:39:07.155581	(e.g. Log files, Pass/Fail reports, etc.)	5f457f1e1ea90ba9adb32e28
S0167	Skill	68	Manage and version control the automation, orchestration, and configurations for target systems and representative environments.	2021-12-10 18:39:07.156178	2021-12-10 18:39:07.156178		5f457f1e1ea90ba9adb32e23
S0168	Skill	68	Schedule DART test plan batches.	2021-12-10 18:39:07.156996	2021-12-10 18:39:07.156997		5f457f1e1ea90ba9adb32e24
S0169	Skill	68	Conduct analysis of collected data.	2021-12-10 18:39:07.15767	2021-12-10 18:39:07.15767		5f457f1e1ea90ba9adb32e26
S0170	Skill	68	Incorporate analysis of collected data into automated CI/CD process.	2021-12-10 18:39:07.158255	2021-12-10 18:39:07.158256		5f457f1e1ea90ba9adb32e25
S0171	Skill	29	Write Pseudocode for a sequential process	2021-12-10 18:39:07.158875	2021-12-10 18:39:07.158875		5f457f1e1ea90ba9adb32e27
S0172	Skill	29	Write Pseudocode with repeating steps to the process	2021-12-10 18:39:07.159691	2021-12-10 18:39:07.159692		5f457f1e1ea90ba9adb32e29
S0173	Skill	92	Write code that implements interrupts	2021-12-10 18:39:07.160838	2021-12-10 18:39:07.160839	Replace "OS Virtualization" with "Computer Architecture"; writing code that implements interrupt; not sure what is meant by interrupt... process interrupt?...cliff notes to the dinosaur book (See KSAT S0120 - duplicate?)	5f457f1e1ea90ba9adb32e2c
S0174	Skill	92	Write code to implement signal handling	2021-12-10 18:39:07.161688	2021-12-10 18:39:07.161688	Replace "OS Virtualization" with "Computer Architecture"; writing code that implements signal handling...cliff notes to the dinosaur book	5f457f1e1ea90ba9adb32e2d
S0175	Skill	33	Implement a thread in an OS utility.	2021-12-10 18:39:07.162547	2021-12-10 18:39:07.162548		5f457f1e1ea90ba9adb32e2b
S0176	Skill	33	Implement an OS utility that requires a lock.	2021-12-10 18:39:07.163458	2021-12-10 18:39:07.163459		5f457f1e1ea90ba9adb32e2e
S0177	Skill	11	Write assembly code that accomplishes the same functionality as given in a high level language (C or Python) code snippet.	2021-12-10 18:39:07.164292	2021-12-10 18:39:07.164293		5f457f1e1ea90ba9adb32e2f
S0178	Skill	12	Reverse engineer a basic binary program to write high-level pseudocode.	2021-12-10 18:39:07.165064	2021-12-10 18:39:07.165065	Something 'simple' like hello world	5f457f1e1ea90ba9adb32e30
S0179	Skill	12	Reverse engineer a binary using IDA Pro.	2021-12-10 18:39:07.165951	2021-12-10 18:39:07.165952	Something 'simple' like hello world	5f457f1e1ea90ba9adb32e31
S0180	Skill	12	Reverse engineer a binary using WinDBG.	2021-12-10 18:39:07.166874	2021-12-10 18:39:07.166875	Something 'simple' like hello world	5f457f1e1ea90ba9adb32e33
S0181	Skill	12	Analyze binaries to identify function calls	2021-12-10 18:39:07.167601	2021-12-10 18:39:07.167605	Something 'simple' like hello world	5f457f1e1ea90ba9adb32e36
S0182	Skill	12	Reverse engineer a binary using GDB.	2021-12-10 18:39:07.168293	2021-12-10 18:39:07.168294	Something 'simple' like hello world	5f457f1e1ea90ba9adb32e32
S0183	Skill	34	Perform paired programming	2021-12-10 18:39:07.169184	2021-12-10 18:39:07.169184		5f457f1e1ea90ba9adb32e34
S0184	Skill	34	Develop a user story from a requirement.	2021-12-10 18:39:07.169988	2021-12-10 18:39:07.169989		5f457f1e1ea90ba9adb32e35
S0185	Skill	34	Prepare and package a developed tool for delivery	2021-12-10 18:39:07.170597	2021-12-10 18:39:07.170598		5f457f1e1ea90ba9adb32e38
S0186	Skill	34	Validate operability of an application/tool on clean host	2021-12-10 18:39:07.171257	2021-12-10 18:39:07.171258		5f457f1e1ea90ba9adb32e37
S0188	Skill	2	Demonstrate techniques to provide transparency to stakeholders on progress toward goals.	2021-12-10 18:39:07.17219	2021-12-10 18:39:07.172191		5f457f1e1ea90ba9adb32e3c
S0189	Skill	2	Demonstrate techniques to interact with stakeholders over multiple Sprints.	2021-12-10 18:39:07.172912	2021-12-10 18:39:07.172913		5f457f1e1ea90ba9adb32e3b
S0193	Skill	2	Plan a product release.	2021-12-10 18:39:07.17347	2021-12-10 18:39:07.173471		5f457f1e1ea90ba9adb32e40
S0200	Skill	2	Create Product Backlog item that includes description of desired outcome and value.	2021-12-10 18:39:07.174145	2021-12-10 18:39:07.174146		5f457f1e1ea90ba9adb32e44
S0204	Skill	15	Configure Ghidra client to use scripts.	2021-12-10 18:39:07.174822	2021-12-10 18:39:07.174823	Make sure to cover installation and config of 90th scripts.	5f457f1e1ea90ba9adb32e48
S0206	Skill	77	Design a dockerfile that meets certain requirements	2021-12-10 18:39:07.175502	2021-12-10 18:39:07.175503		5f457f1e1ea90ba9adb32e49
S0207	Skill	77	Carry out the docker build command to build a container image	2021-12-10 18:39:07.17622	2021-12-10 18:39:07.176221		5f457f1e1ea90ba9adb32e47
S0208	Skill	77	Carry out the docker run command to start a docker container	2021-12-10 18:39:07.176901	2021-12-10 18:39:07.176902		5f457f1e1ea90ba9adb32e4a
S0209	Skill	77	Carry out the docker exec command to interact with an already running container	2021-12-10 18:39:07.177689	2021-12-10 18:39:07.17769		5f457f1e1ea90ba9adb32e4b
S0210	Skill	77	Use docker documentation to solve docker problem sets	2021-12-10 18:39:07.178364	2021-12-10 18:39:07.178364	https://docs.docker.com/engine/reference/commandline/cli/  https://docs.docker.com/engine/reference/run/	5f457f1e1ea90ba9adb32e4d
S0211	Skill	77	Carry out the configuration of docker commands to use persistent storage	2021-12-10 18:39:07.17894	2021-12-10 18:39:07.17894	volumes, bind mounts, tmpfs https://docs.docker.com/storage/	5f457f1e1ea90ba9adb32e4c
S0212	Skill	77	Carry out the configuration of docker networking	2021-12-10 18:39:07.179578	2021-12-10 18:39:07.179578	specifically, communication with other docker containers, probably with docker-compose https://docs.docker.com/network/	5f457f1e1ea90ba9adb32e4e
S0213	Skill	25	Ensure Examiner Go/No-Go requirements have been met	2021-12-10 18:39:07.180614	2021-12-10 18:39:07.180615		5f457f1e1ea90ba9adb32e4f
S0214	Skill	25	Ensure Examinee Go/No-Go requirements have been met	2021-12-10 18:39:07.181548	2021-12-10 18:39:07.181548		5f457f1e1ea90ba9adb32e51
S0215	Skill	25	Obtain all required evaluation material 	2021-12-10 18:39:07.182464	2021-12-10 18:39:07.182465		5f457f1e1ea90ba9adb32e50
S0216	Skill	25	Brief the overall conduct of the evaluation to include when the evaluation begins/ends	2021-12-10 18:39:07.183355	2021-12-10 18:39:07.183356		5f457f1e1ea90ba9adb32e55
S0217	Skill	25	Brief scenario requirements to include Go/No-Go scenario	2021-12-10 18:39:07.18433	2021-12-10 18:39:07.184331		5f457f1e1ea90ba9adb32e52
S0218	Skill	25	Brief grading criteria and overall grade definitions	2021-12-10 18:39:07.185257	2021-12-10 18:39:07.185258		5f457f1e1ea90ba9adb32e53
S0219	Skill	25	Brief examiner/examinee evaluation roles and responsibilities	2021-12-10 18:39:07.186139	2021-12-10 18:39:07.18614		5f457f1e1ea90ba9adb32e54
S0220	Skill	25	Identify and document all discrepancies in the appropriate evaluation area	2021-12-10 18:39:07.186983	2021-12-10 18:39:07.186984		5f457f1e1ea90ba9adb32e56
S0221	Skill	25	Assign proper area grades based on discrepancy documentation and criteria guidance	2021-12-10 18:39:07.187869	2021-12-10 18:39:07.18787		5f457f1e1ea90ba9adb32e58
S0222	Skill	25	Factor all cumulative deviations in the examinee's performance when assigning overall grade	2021-12-10 18:39:07.188855	2021-12-10 18:39:07.188856		5f457f1e1ea90ba9adb32e5a
S0223	Skill	25	Award the appropriate overall grade based on criteria guidance and governing guidance	2021-12-10 18:39:07.189782	2021-12-10 18:39:07.189783		5f457f1e1ea90ba9adb32e59
S0224	Skill	25	Assign additional training for discrepancies where a debrief is not the proper remedial action.	2021-12-10 18:39:07.190708	2021-12-10 18:39:07.190708		5f457f1e1ea90ba9adb32e57
S0225	Skill	25	Assigned sufficient additional training that will ensure examinee would achieve proper qualification level	2021-12-10 18:39:07.191581	2021-12-10 18:39:07.191582		5f457f1e1ea90ba9adb32e5d
S0226	Skill	25	Debrief a summary of overall evaluation to include the scenario and examinee's tasks supporting the scenario	2021-12-10 18:39:07.192437	2021-12-10 18:39:07.192437		5f457f1e1ea90ba9adb32e5f
S0227	Skill	25	Debrief all key scenario events,  providing instruction and references as required	2021-12-10 18:39:07.193318	2021-12-10 18:39:07.193319		5f457f1e1ea90ba9adb32e5b
S0228	Skill	25	Reconstruct key scenario events and examinee's role in each event	2021-12-10 18:39:07.194191	2021-12-10 18:39:07.194192		5f457f1e1ea90ba9adb32e5c
S0229	Skill	25	Debriefed all discrepancies to include correct actions of each discrepancy	2021-12-10 18:39:07.195188	2021-12-10 18:39:07.195189		5f457f1e1ea90ba9adb32e5e
S0230	Skill	25	Debrief each evaluation area grade and, if warranted,  additional training to include additional training due date	2021-12-10 18:39:07.196287	2021-12-10 18:39:07.196288		5f457f1e1ea90ba9adb32e65
S0231	Skill	25	Debrief overall evaluation grade	2021-12-10 18:39:07.197162	2021-12-10 18:39:07.197162		5f457f1e1ea90ba9adb32e66
S0232	Skill	25	Correctly complete evaluation gradesheet prior to debrief	2021-12-10 18:39:07.198097	2021-12-10 18:39:07.198097		5f457f1e1ea90ba9adb32e61
S0233	Skill	25	Verify accuracy/completion of AF Form 4418 prior to debrief	2021-12-10 18:39:07.199008	2021-12-10 18:39:07.199009		5f457f1e1ea90ba9adb32e60
S0234	Skill	25	Debrief supervisor of examinee's performance to include discrepancies,  remedial action,  area/overall grades,  and additional training (including due date)	2021-12-10 18:39:07.200015	2021-12-10 18:39:07.200016		5f457f1e1ea90ba9adb32e62
S0235	Skill	25	Conduct evaluation as briefed to the examinee	2021-12-10 18:39:07.200959	2021-12-10 18:39:07.20096		5f457f1e1ea90ba9adb32e63
S0236	Skill	25	Does not detract or disrupt the examinee's performance during the evaluation	2021-12-10 18:39:07.201872	2021-12-10 18:39:07.201873		5f457f1e1ea90ba9adb32e64
S0237	Skill	25	Evaluate all criteria areas	2021-12-10 18:39:07.202785	2021-12-10 18:39:07.202786		5f457f1e1ea90ba9adb32e68
S0238	Skill	25	Administer task scenario as required.	2021-12-10 18:39:07.203797	2021-12-10 18:39:07.203798		5f457f1e1ea90ba9adb32e67
S0239	Skill	25	If applicable,  take over examinee position if/when examinee performance exhibited breaches of safety	2021-12-10 18:39:07.204848	2021-12-10 18:39:07.204849		5f457f1e1ea90ba9adb32e69
S0240	Skill	53	Utilize the Windows Application Programming Interface (API) for Events	2021-12-10 18:39:07.20566	2021-12-10 18:39:07.20566		5f457f1e1ea90ba9adb32e6c
S0241	Skill	53	Utilize Windows services	2021-12-10 18:39:07.206561	2021-12-10 18:39:07.206562		5f457f1e1ea90ba9adb32e6e
S0243	Skill	53	Configure and setup a Win32 environment	2021-12-10 18:39:07.207193	2021-12-10 18:39:07.207193		5f457f1e1ea90ba9adb32e6b
S0244	Skill	53	Utilize the user file Application Programming Interface (API)	2021-12-10 18:39:07.207939	2021-12-10 18:39:07.207939		5f457f1e1ea90ba9adb32e6a
S0245	Skill	53	Utilize the Windows registry Application Programming Interface (API)	2021-12-10 18:39:07.208678	2021-12-10 18:39:07.208679		5f457f1e1ea90ba9adb32e6d
S0246	Skill	33	Create and utilize processes	2021-12-10 18:39:07.209384	2021-12-10 18:39:07.209384		5f457f1e1ea90ba9adb32e6f
S0247	Skill	33	Create and utilize threads	2021-12-10 18:39:07.21009	2021-12-10 18:39:07.210091		5f457f1e1ea90ba9adb32e70
S0248	Skill	33	Synchronize threads	2021-12-10 18:39:07.210688	2021-12-10 18:39:07.210689		5f457f1e1ea90ba9adb32e86
S0249	Skill	53	Create and utilize a Dynamic Link Library (DLL)	2021-12-10 18:39:07.211345	2021-12-10 18:39:07.211345		5f457f1e1ea90ba9adb32e8f
S0250	Skill	53	Utilize the virtual alloc Application Programming Interface (API)	2021-12-10 18:39:07.212188	2021-12-10 18:39:07.212189		5f457f1e1ea90ba9adb32e73
S0251	Skill	53	Utilize the heap alloc Application Programming Interface (API)	2021-12-10 18:39:07.212881	2021-12-10 18:39:07.212881		5f457f1e1ea90ba9adb32e90
S0252	Skill	53	Utilize the file mapping Application Programming Interface (API)	2021-12-10 18:39:07.213475	2021-12-10 18:39:07.213475		5f457f1e1ea90ba9adb32e8c
S0253	Skill	53	Create and utilize winsock servers	2021-12-10 18:39:07.214216	2021-12-10 18:39:07.214217		5f457f1e1ea90ba9adb32e8d
S0254	Skill	53	Create and utilize winsock clients	2021-12-10 18:39:07.214881	2021-12-10 18:39:07.214882		5f457f1e1ea90ba9adb32e71
S0256	Skill	53	Implement C++ Structured Exception Handling (SEH)	2021-12-10 18:39:07.2156	2021-12-10 18:39:07.215601		5f457f1e1ea90ba9adb32e8a
S0257	Skill	53	Implement asynchronous Input/Output	2021-12-10 18:39:07.216557	2021-12-10 18:39:07.216557		5f457f1e1ea90ba9adb32e8e
S0259	Skill	93	Utilize native Application Programming Interface (API) Calls	2021-12-10 18:39:07.217396	2021-12-10 18:39:07.217396		5f457f1e1ea90ba9adb32e91
S0260	Skill	93	Manipulate Windows handles	2021-12-10 18:39:07.217905	2021-12-10 18:39:07.217905		5f457f1e1ea90ba9adb32e74
S0261	Skill	93	Configure Structured Exception Handling (SEH) Internals	2021-12-10 18:39:07.218528	2021-12-10 18:39:07.218529		5f457f1e1ea90ba9adb32e88
S0262	Skill	93	Manipulate Processes	2021-12-10 18:39:07.2193	2021-12-10 18:39:07.219301		5f457f1e1ea90ba9adb32e72
S0263	Skill	93	Navigate Portable Executable Images	2021-12-10 18:39:07.219933	2021-12-10 18:39:07.219934		5f457f1e1ea90ba9adb32e7e
S0265	Skill	93	Load and utilize modules	2021-12-10 18:39:07.220593	2021-12-10 18:39:07.220594		5f457f1e1ea90ba9adb32e80
S0266	Skill	93	Implement security using the Windows Security Model	2021-12-10 18:39:07.221137	2021-12-10 18:39:07.221138		5f457f1e1ea90ba9adb32e75
S0267	Skill	93	Utilize the Process Environment Block (PEB)	2021-12-10 18:39:07.221705	2021-12-10 18:39:07.221705		5f457f1e1ea90ba9adb32e76
S0268	Skill	93	Utilize the Thread Environment Block (TEB)	2021-12-10 18:39:07.222238	2021-12-10 18:39:07.222239		5f457f1e1ea90ba9adb32e78
S0270	Skill	93	Create Windows memory	2021-12-10 18:39:07.222835	2021-12-10 18:39:07.222835		5f457f1e1ea90ba9adb32e77
S0271	Skill	93	Manipulate Windows memory	2021-12-10 18:39:07.223362	2021-12-10 18:39:07.223363		5f457f1e1ea90ba9adb32e7d
S0272	Skill	93	Utilize Windows Remote Procedure Call (RPC)	2021-12-10 18:39:07.224033	2021-12-10 18:39:07.224033		5f457f1e1ea90ba9adb32e79
S0274	Skill	54	Inject Position Independent Code (PIC) into memory	2021-12-10 18:39:07.224575	2021-12-10 18:39:07.224576		5f457f1e1ea90ba9adb32e85
S0275	Skill	54	Execute Position Independent Code (PIC)	2021-12-10 18:39:07.225157	2021-12-10 18:39:07.225157		5f457f1e1ea90ba9adb32e7f
S0276	Skill	54	Divert code execution to another memory location	2021-12-10 18:39:07.225823	2021-12-10 18:39:07.225823		5f457f1e1ea90ba9adb32e7b
S0277	Skill	54	Utilize binary hardening techniques	2021-12-10 18:39:07.226394	2021-12-10 18:39:07.226394		5f457f1e1ea90ba9adb32e7a
S0278	Skill	55	Debug a kernel application	2021-12-10 18:39:07.227043	2021-12-10 18:39:07.227043		5f457f1e1ea90ba9adb32e7c
S0279	Skill	55	Utilize synchronization mechanisms	2021-12-10 18:39:07.227612	2021-12-10 18:39:07.227613		5f457f1e1ea90ba9adb32e82
S0280	Skill	55	Utilize paging in memory management	2021-12-10 18:39:07.22826	2021-12-10 18:39:07.228261		5f457f1e1ea90ba9adb32e81
S0281	Skill	55	Utilize Interrupt Request Levels (IRQLs)	2021-12-10 18:39:07.228812	2021-12-10 18:39:07.228812		5f457f1e1ea90ba9adb32e84
S0282	Skill	3	Utilize key Kernel Structures	2021-12-10 18:39:07.229414	2021-12-10 18:39:07.229415		5f457f1e1ea90ba9adb32e83
S0283	Skill	55	Defer kernel work to avoid deadlocks	2021-12-10 18:39:07.229944	2021-12-10 18:39:07.229944		5f457f1e1ea90ba9adb32e87
S0286	Skill	55	Manage input/output request packets (IRP)	2021-12-10 18:39:07.230508	2021-12-10 18:39:07.230508		5f457f1e1ea90ba9adb32e8b
S0287	Skill	55	Utilize the input/output subsystem	2021-12-10 18:39:07.231039	2021-12-10 18:39:07.23104		5f457f1e1ea90ba9adb32e89
S0288	Skill	55	Manage Windows memory	2021-12-10 18:39:07.231595	2021-12-10 18:39:07.231596		5f457f1e1ea90ba9adb32e92
S0289	Skill	55	Utilize the Object Manager for input/output resources	2021-12-10 18:39:07.232217	2021-12-10 18:39:07.232218		5f457f1e1ea90ba9adb32e95
S0290	Skill	55	Utilize System Service Dispatch Table (SSDT) to access kernel routines.	2021-12-10 18:39:07.232805	2021-12-10 18:39:07.232806		5f457f1e1ea90ba9adb32e93
S0291	Skill	55	Analyze a Windbg Crash Dump.	2021-12-10 18:39:07.233363	2021-12-10 18:39:07.233363		5f457f1e1ea90ba9adb32e94
S0292	Skill	55	Utilize the Windows scheduler	2021-12-10 18:39:07.233922	2021-12-10 18:39:07.233923		5f457f1e1ea90ba9adb32e96
S0293	Skill	55	Interface with both documented and undocumented API's	2021-12-10 18:39:07.234448	2021-12-10 18:39:07.234448		5f457f1e1ea90ba9adb32e97
S0294	Skill	56	Navigate files and directories	2021-12-10 18:39:07.235079	2021-12-10 18:39:07.23508		5f457f1e1ea90ba9adb32e9a
S0295	Skill	56	Utilize basic Command Line Interface (CLI)	2021-12-10 18:39:07.23561	2021-12-10 18:39:07.235611		5f457f1e1ea90ba9adb32e9d
S0296	Skill	56	Utilize input/output redirection/pipes	2021-12-10 18:39:07.236202	2021-12-10 18:39:07.236203		5f457f1e1ea90ba9adb32e98
S0297	Skill	56	Create and utilize regular expressions	2021-12-10 18:39:07.23674	2021-12-10 18:39:07.236741		5f457f1e1ea90ba9adb32e99
S0298	Skill	56	Modify file ownership	2021-12-10 18:39:07.237262	2021-12-10 18:39:07.237263		5f457f1e1ea90ba9adb32e9c
S0299	Skill	56	Modify file permissions	2021-12-10 18:39:07.237789	2021-12-10 18:39:07.237789		5f457f1e1ea90ba9adb32e9b
S0300	Skill	56	Create traditional directory structures	2021-12-10 18:39:07.238313	2021-12-10 18:39:07.238313		5f457f1e1ea90ba9adb32e9e
S0301	Skill	31	Advanced Networking	2021-12-10 18:39:07.238836	2021-12-10 18:39:07.238836		5f457f1e1ea90ba9adb32e9f
S0302	Skill	57	Create and utilize personal utilities	2021-12-10 18:39:07.239357	2021-12-10 18:39:07.239358		5f457f1e1ea90ba9adb32ea5
S0303	Skill	57	Utilize text handling utilities	2021-12-10 18:39:07.239879	2021-12-10 18:39:07.23988		5f457f1e1ea90ba9adb32ea3
S0304	Skill	57	Implement file system security	2021-12-10 18:39:07.240444	2021-12-10 18:39:07.240445		5f457f1e1ea90ba9adb32ea0
S0305	Skill	57	Utilize the tee and dd commands	2021-12-10 18:39:07.240963	2021-12-10 18:39:07.240963		5f457f1e1ea90ba9adb32ea1
S0306	Skill	57	Execute basic administration commands using a GUI	2021-12-10 18:39:07.24155	2021-12-10 18:39:07.241551		5f457f1e1ea90ba9adb32ea2
S0307	Skill	57	Install packages using a GUI	2021-12-10 18:39:07.242075	2021-12-10 18:39:07.242076		5f457f1e1ea90ba9adb32ea4
S0308	Skill	57	Remove packages using a GUI	2021-12-10 18:39:07.242629	2021-12-10 18:39:07.24263		5f457f1e1ea90ba9adb32ea6
S0309	Skill	57	Install packages using a CLI	2021-12-10 18:39:07.24315	2021-12-10 18:39:07.24315		5f457f1e1ea90ba9adb32ea7
S0310	Skill	57	Remove packages using a CLI	2021-12-10 18:39:07.243796	2021-12-10 18:39:07.243796		5f457f1e1ea90ba9adb32ea8
S0311	Skill	57	Utilize communication utilities	2021-12-10 18:39:07.244311	2021-12-10 18:39:07.244312		5f457f1e1ea90ba9adb32eaa
S0312	Skill	57	Utilize the shell	2021-12-10 18:39:07.244857	2021-12-10 18:39:07.244857		5f457f1e1ea90ba9adb32eab
S0315	Skill	57	Generate filenames	2021-12-10 18:39:07.24542	2021-12-10 18:39:07.245421		5f457f1e1ea90ba9adb32ea9
S0316	Skill	57	Create and manage processes	2021-12-10 18:39:07.24595	2021-12-10 18:39:07.245951		5f457f1e1ea90ba9adb32eac
S0320	Skill	57	Install and utilize plugins	2021-12-10 18:39:07.246449	2021-12-10 18:39:07.24645		5f457f1e1ea90ba9adb32ead
S0321	Skill	57	Utilize Man pages	2021-12-10 18:39:07.246975	2021-12-10 18:39:07.246975		5f457f1e1ea90ba9adb32eae
S0322	Skill	57	Discover and control Linux hardware	2021-12-10 18:39:07.2475	2021-12-10 18:39:07.2475		5f457f1e1ea90ba9adb32eb3
S0324	Skill	57	Administer a file system	2021-12-10 18:39:07.248027	2021-12-10 18:39:07.248028		5f457f1e1ea90ba9adb32eaf
S0325	Skill	57	Administer users and groups	2021-12-10 18:39:07.248523	2021-12-10 18:39:07.248524		5f457f1e1ea90ba9adb32eb2
S0326	Skill	57	Implement pluggable authentication	2021-12-10 18:39:07.249089	2021-12-10 18:39:07.249089		5f457f1e1ea90ba9adb32eb1
S0327	Skill	57	Administer security	2021-12-10 18:39:07.249631	2021-12-10 18:39:07.249632		5f457f1e1ea90ba9adb32eb0
S0329	Skill	3	Utilize kernel structures	2021-12-10 18:39:07.250266	2021-12-10 18:39:07.250267		5f457f1e1ea90ba9adb32eb5
S0330	Skill	57	Utilize the file system and drivers	2021-12-10 18:39:07.250828	2021-12-10 18:39:07.250829		5f457f1e1ea90ba9adb32eb6
S0331	Skill	57	Utilize ioctl calls	2021-12-10 18:39:07.251341	2021-12-10 18:39:07.251342		5f457f1e1ea90ba9adb32eb4
S0332	Skill	57	Utilize block and memory device drivers	2021-12-10 18:39:07.251843	2021-12-10 18:39:07.251844		5f457f1e1ea90ba9adb32eb7
S0333	Skill	57	Debug a kernel application	2021-12-10 18:39:07.252543	2021-12-10 18:39:07.252544		5f457f1e1ea90ba9adb32ebe
S0335	Skill	2	Assess the skills, capabilities and practices of a Product Owner to help the organization realize value.	2021-12-10 18:39:07.253303	2021-12-10 18:39:07.253304	This seems like a SEE type activity	5f457f1e1ea90ba9adb32eba
S0339	Skill	2	Integrate feedback to generate and order Product Backlog items.	2021-12-10 18:39:07.254026	2021-12-10 18:39:07.254027		5f457f1e1ea90ba9adb32ebc
S0345	Skill	2	Create documentation relaying leadership/customer vision	2021-12-10 18:39:07.254768	2021-12-10 18:39:07.254768		5f457f1e1ea90ba9adb32ec3
S0346	Skill	2	Collaborate with team members to create project charter	2021-12-10 18:39:07.255399	2021-12-10 18:39:07.255399		5f457f1e1ea90ba9adb32ec2
S0348	Skill	2	Establish a galvanizing environment to improve team collaboration	2021-12-10 18:39:07.255954	2021-12-10 18:39:07.255955		5f457f1e1ea90ba9adb32ec6
S0352	Skill	2	Prioritize backlog items	2021-12-10 18:39:07.256528	2021-12-10 18:39:07.256529		5f457f1e1ea90ba9adb32ec8
S0354	Skill	2	Collaborate with tool champion to identify requirements	2021-12-10 18:39:07.257223	2021-12-10 18:39:07.257223		5f457f1e1ea90ba9adb32ecb
S0356	Skill	2	Formulate acceptance criteria.	2021-12-10 18:39:07.257788	2021-12-10 18:39:07.257789		5f457f1e1ea90ba9adb32ec9
S0357	Skill	2	Apply the required principles to create a definition of done	2021-12-10 18:39:07.258349	2021-12-10 18:39:07.25835		5f457f1e1ea90ba9adb32ecc
S0363	Skill	49	Facilitate an effective Project Kickoff Meeting in accordance with squadron standards.	2021-12-10 18:39:07.258912	2021-12-10 18:39:07.258913	http://mpls-as-701v:8090/display/9CMDP/%28U%29+Guidance+for+Facilitating+a+Project+Kickoff+Meeting	5f457f1e1ea90ba9adb32eda
S0364	Skill	49	Create a release plan.	2021-12-10 18:39:07.259494	2021-12-10 18:39:07.259494		5f457f1e1ea90ba9adb32ed2
S0365	Skill	49	Conduct an effective Product Backlog Refinement Meeting	2021-12-10 18:39:07.260119	2021-12-10 18:39:07.260119	http://mpls-as-701v:8090/display/9CMDP/%28U%29+Guidance+for+Facilitating+a+Product+Backlog+Grooming+Meeting	5f457f1e1ea90ba9adb32ed7
S0366	Skill	49	Facilitate an effective Sprint Planning Meeting.	2021-12-10 18:39:07.260687	2021-12-10 18:39:07.260688	http://mpls-as-701v:8090/display/9CMDP/%28U%29+Guidance+For+Facilitating+a+Sprint+Planning+Meeting	5f457f1e1ea90ba9adb32ede
S0367	Skill	49	Develop and present a Backlog Pitch to the DO	2021-12-10 18:39:07.261282	2021-12-10 18:39:07.261283		5f457f1e1ea90ba9adb32ed4
S0368	Skill	49	Utilize velocity data to forecast future delivery of product features.	2021-12-10 18:39:07.261792	2021-12-10 18:39:07.261792		5f457f1e1ea90ba9adb32ed5
S0376	Skill	86	Manage releases (versions/sprints) in Software projects using boards or the Release Hub.	2021-12-10 18:39:07.262387	2021-12-10 18:39:07.262388		5f457f1e1ea90ba9adb32ee0
S0377	Skill	86	Given business requirements recommend the appropriate configuration of project workflows.	2021-12-10 18:39:07.262952	2021-12-10 18:39:07.262952		5f457f1e1ea90ba9adb32edb
S0378	Skill	86	Edit project screens and project workflows to meet business requirements.	2021-12-10 18:39:07.26352	2021-12-10 18:39:07.263521		5f457f1e1ea90ba9adb32edf
S0379	Skill	5	Demonstrate ability to write object-oriented programs	2021-12-10 18:39:07.264168	2021-12-10 18:39:07.264169	Constructor/Destructor; Inheritance; Instanciation	5f457f1e1ea90ba9adb32ee1
S0381	Skill	44	Peer Review code and provide critical feedback to developer.	2021-12-10 18:39:07.264734	2021-12-10 18:39:07.264735		5f457f1e1ea90ba9adb32a6e
S0382	Skill	5	Create a class constructor or destructor	2021-12-10 18:39:07.265321	2021-12-10 18:39:07.265322		5f457f1e1ea90ba9adb32a7c
S0383	Skill	76	Merge a branch.	2021-12-10 18:39:07.265886	2021-12-10 18:39:07.265887	Using Gitlab UI	5f6c9c3db02d633806ba1320
S0384	Skill	76	Create an issue.	2021-12-10 18:39:07.266449	2021-12-10 18:39:07.26645	Using Gitlab UI	5f6c9c3db02d633806ba1321
S0385	Skill	76	Create a merge request.	2021-12-10 18:39:07.26701	2021-12-10 18:39:07.26701	Using Gitlab UI	5f6c9c3db02d633806ba1322
S0386	Skill	27	Checkout an existing branch.	2021-12-10 18:39:07.267573	2021-12-10 18:39:07.267573	Using Git CLI	5f6c9c3db02d633806ba1323
S0389	Skill	76	Commit changes to the working branch.	2021-12-10 18:39:07.268207	2021-12-10 18:39:07.268208	Using Gitlab UI	5f6c9c3db02d633806ba1326
S0390	Skill	27	Merge a branch.	2021-12-10 18:39:07.26879	2021-12-10 18:39:07.268791	Using Git CLI	5f6ca014ea03087fd432c579
S0391	Skill	4	Create and utilize a function prototype	2021-12-10 18:39:07.26938	2021-12-10 18:39:07.269381		5f88854aa9ac0d71db9bb57d
S0392	Skill	2	Use the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates to create a project charter.	2021-12-10 18:39:07.269952	2021-12-22 22:22:26.510879		5f909bce37597b2eef89001d
S0393	Skill	2	Use the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates to create a project kickoff meeting agenda.	2021-12-10 18:39:07.27052	2021-12-10 18:39:07.27052		5f909bce37597b2eef89001e
S0394	Skill	62	Implement conditional control flow constructs.	2021-12-10 18:39:07.271084	2021-12-10 18:39:07.271084		5f457f1e1ea90ba9adb32a5f
S0395	Skill	91	Configure Gitlab CI YAML files to define stages and jobs.	2021-12-10 18:39:07.271799	2021-12-10 18:39:07.271799		5f457f1e1ea90ba9adb32a77
S0396	Skill	91	Configure Gitlab CI YAML files to generate and store artifacts	2021-12-10 18:39:07.272645	2021-12-10 18:39:07.272646		5f457f1e1ea90ba9adb32a76
S0397	Skill	16	Create and use complex Data Structures.	2021-12-10 18:39:07.273494	2021-12-10 18:39:07.273494	Elf Parsing for Senior Linux, Others include Linked Lists, Doubly-Linked Lists, Circularly-Linked Lists, Trees, Binary Search Trees, Weighted Graph, Hash Tables, Queues, and Stacks.	5f457f1e1ea90ba9adb32a7f
S0398	Skill	24	Modify the Master Training Task List (MTTL).	2021-12-10 18:39:07.274295	2021-12-10 18:39:07.274296	Add, update, or remove KSATs	5f7f2db02d93ae2b871b7ecf
S0399	Skill	15	Discover a basic known vulnerability in source	2021-12-10 18:39:07.27486	2021-12-10 18:39:07.27486		5f457f1e1ea90ba9adb32a8e
S0400	Skill	15	Implement a basic fuzzing algorithm.	2021-12-10 18:39:07.275427	2021-12-10 18:39:07.275428		5f457f1e1ea90ba9adb32a81
S0401	Skill	15	Implement a simple buffer overflow.	2021-12-10 18:39:07.275991	2021-12-10 18:39:07.275992		5f457f1e1ea90ba9adb32a86
S0402	Skill	15	Generate shell code.	2021-12-10 18:39:07.276563	2021-12-10 18:39:07.276563		5f457f1e1ea90ba9adb32a85
S0403	Skill	75	Integrate MetaTotal into CI pipeline.	2021-12-10 18:39:07.277202	2021-12-10 18:39:07.277203		5f457f1e1ea90ba9adb32a6f
S0404	Skill	75	Import MetaTotal images into OpenStack environment.	2021-12-10 18:39:07.278037	2021-12-10 18:39:07.278037		5f457f1e1ea90ba9adb32a73
S0405	Skill	75	Submit files to MetaTotal.	2021-12-10 18:39:07.27887	2021-12-10 18:39:07.27887		5f457f1e1ea90ba9adb32a72
S0406	Skill	75	View and interpret MetaTotal scan results.	2021-12-10 18:39:07.279759	2021-12-10 18:39:07.27976		5f457f1e1ea90ba9adb32a74
S0407	Skill	75	Submit files to Mockingbird.	2021-12-10 18:39:07.280588	2021-12-10 18:39:07.280589		5f457f1e1ea90ba9adb32a75
S0408	Skill	83	Install and use appropriate VS Code extensions for mission work	2021-12-10 18:39:07.281426	2021-12-10 18:39:07.281426		5ff77ed36ef9fc6003e089f2
S0409	Skill	94	Setup SSH key pairs and manage existing keys on a server.	2021-12-10 18:39:07.282386	2021-12-10 18:39:07.282387		60182f920742c6afaf89a03d
S0410	Skill	56	Modify files without a text editor (i.e. nano, vi/vim).	2021-12-10 18:39:07.283217	2021-12-10 18:39:07.283217		60182fdddc45cabe5831d004
S0411	Skill	56	Navigate the file system using console i.e. the command line interface (CLI).	2021-12-10 18:39:07.284066	2021-12-10 18:39:07.284067		6018303c3a7d5d0fc3b13092
S0412	Skill	9	Derive and apply the proper security classification for a project from the CNF or other provided requirements.	2021-12-10 18:39:07.285045	2021-12-10 18:39:07.285046	Intended for PO work role; Evaluations:  currently applies to scenarioA-NewProject.	60a82342387488fee7b6ba3d
S0413	Skill	9	Derive and apply the proper Security Classification Guide (SCG) for a project from the CNF or other provided requirements.	2021-12-10 18:39:07.285883	2021-12-10 18:39:07.285884	Intended for PO work role; Evaluations:  currently applies to scenarioA-NewProject.	60a82342387488fee7b6ba3e
S0414	Skill	85	Participate in a sprint planning event	2021-12-10 18:39:07.293521	2021-12-10 18:39:07.293522		6155dedc4f4c5b977d508c71
S0415	Skill	2	Create an Agile Epic.	2021-12-10 18:39:07.28673	2021-12-10 18:39:07.28673		61561080ec2c24e977a227f2
S0416	Skill	2	Create an issue in Jira.	2021-12-10 18:39:07.287579	2021-12-22 22:22:35.760566		615605f447397949b3fc1611
S0417	Skill	85	Create a sprint goal based on sprint backlog items.	2021-12-10 18:39:07.288501	2021-12-10 18:39:07.288502		6155fa98a438a984bc8787d6
S0418	Skill	85	Participate in a sprint retrospective event	2021-12-10 18:39:07.291856	2021-12-10 18:39:07.291857		6155f637da99662ee4bb40a4
S0419	Skill	85	Participate in daily Scrum stand-up meeting	2021-12-10 18:39:07.292681	2021-12-10 18:39:07.292681		61560c6fd00f42b70bfa837b
S0420	Skill	85	Create a Scrum board in Jira.	2021-12-10 18:39:07.289346	2021-12-10 18:39:07.289346		6155d6434e33a33525d64e5a
S0421	Skill	85	Participate in a sprint review event.	2021-12-10 18:39:07.290217	2021-12-10 18:39:07.290217		6155f8f29c7fae5ecd57db87
S0422	Skill	85	Establish a product goal.	2021-12-10 18:39:07.29103	2021-12-10 18:39:07.29103		6155fef685d93a3f9b11c5a6
T0001	Task	1	(U) Modify cyber capabilities to detect and disrupt nation-state cyber threat actors across the threat chain (kill chain) of adversary execution	2021-12-10 18:39:06.351268	2021-12-10 18:39:06.351271		5f457f1e1ea90ba9adb32ee8
T0002	Task	1	(U) Create or enhance cyberspace capabilities to compromise, deny, degrade, disrupt, destroy, or manipulate automated information systems.	2021-12-10 18:39:06.354068	2021-12-10 18:39:06.354069		5f457f1e1ea90ba9adb32eed
T0003	Task	1	(U) Create or enhance cyberspace solutions to enable surveillance and reconnaissance of automated information systems.	2021-12-10 18:39:06.355303	2021-12-10 18:39:06.355305		5f457f1e1ea90ba9adb32f02
T0004	Task	1	(U) Create or enhance cyberspace solutions to enable the defense of national interests.	2021-12-10 18:39:06.356572	2021-12-10 18:39:06.356574		5f457f1e1ea90ba9adb32f05
T0005	Task	2	(U) Interpret customer requirements and evaluate resource and system constraints to create solution design specifications.	2021-12-10 18:39:06.357631	2021-12-10 18:39:06.357632		5f457f1e1ea90ba9adb32ee9
T0006	Task	2	(U) Reference capability repositories and other sources to identify existing capabilities which fully/partially meet customer requirements (with or without modification).	2021-12-10 18:39:06.358529	2021-12-10 18:39:06.35853		5f457f1e1ea90ba9adb32eeb
T0007	Task	2	(U) Collaborate with stakeholders to identify and/or develop appropriate cyberspace solutions.	2021-12-10 18:39:06.359433	2021-12-10 18:39:06.359434		5f457f1e1ea90ba9adb32ef8
T0008	Task	3	(U) Analyze, modify, develop, debug and document software and applications which run in kernel and user space.	2021-12-10 18:39:06.360394	2021-12-10 18:39:06.360395		5f457f1e1ea90ba9adb32eec
T0009	Task	4	(U) Analyze, modify, develop, debug and document software and applications in C programming language.	2021-12-10 18:39:06.361323	2021-12-10 18:39:06.361324		5f457f1e1ea90ba9adb32eea
T0010	Task	5	(U) Analyze, modify, develop, debug and document software and applications in Python programming language.	2021-12-10 18:39:06.362232	2021-12-10 18:39:06.362233		5f457f1e1ea90ba9adb32f04
T0011	Task	6	(U) Analyze, modify, develop, debug and document software and applications utilizing standard, non-standard, specialized, and/or unique network communication protocols.	2021-12-10 18:39:06.363243	2021-12-10 18:39:06.363244		5f457f1e1ea90ba9adb32f08
T0012	Task	7	(U) Analyze, modify, develop, debug and document custom interface specifications between interconnected systems.	2021-12-10 18:39:06.364359	2021-12-10 18:39:06.36436		5f457f1e1ea90ba9adb32f09
T0013	Task	8	(U) Manage source code using version control (e.g. Git)	2021-12-10 18:39:06.365397	2021-12-10 18:39:06.365398		5f457f1e1ea90ba9adb32f0b
T0014	Task	9	(U) Develop, modify, and utilize automation technologies to enable employment of capabilities as efficiently as possible (e.g. TDD, CI/CD, etc.)	2021-12-10 18:39:06.366494	2021-12-10 18:39:06.366496		5f457f1e1ea90ba9adb32f00
T0015	Task	10	(U) Build, test, and modify prototypes using representative environments.	2021-12-10 18:39:06.367657	2021-12-10 18:39:06.36766		5f457f1e1ea90ba9adb32f03
T0016	Task	10	(U) Analyze and correct technical problems encountered during testing and implementation of cyberspace solutions.	2021-12-10 18:39:06.368874	2021-12-10 18:39:06.368877		5f457f1e1ea90ba9adb32f06
T0017	Task	11	(U) Analyze, modify, develop, debug and document software and applications using assembly languages.	2021-12-10 18:39:06.370033	2021-12-10 18:39:06.370034		5f457f1e1ea90ba9adb32f0a
T0018	Task	12	(U) Utilize tools to decompile, disassemble, analyze, and reverse engineer compiled binaries.	2021-12-10 18:39:06.371117	2021-12-10 18:39:06.371119		5f457f1e1ea90ba9adb32f07
T0019	Task	13	(U) Make use of compiler attributes and platform-specific features.	2021-12-10 18:39:06.372295	2021-12-10 18:39:06.372296		5f457f1e1ea90ba9adb32f0c
T0020	Task	14	(U) Perform static and dynamic analysis in order to find errors and flaws.	2021-12-10 18:39:06.37375	2021-12-10 18:39:06.373753		5f457f1e1ea90ba9adb32ef2
T0021	Task	12	(U) Conduct hardware and/or software static and dynamic analysis to reverse engineer malicious or benign systems	2021-12-10 18:39:06.374875	2021-12-10 18:39:06.374877		5f457f1e1ea90ba9adb32eef
T0022	Task	15	(U) Research, reference, discover, analyze, modify, develop and document known and new vulnerabilities in computer systems.	2021-12-10 18:39:06.375889	2021-12-10 18:39:06.37589		5f457f1e1ea90ba9adb32eee
T0023	Task	7	(U) Design and develop data requirements, database structure, process flow, systematic procedures, algorithms, data analysis, and file structures.	2021-12-10 18:39:06.376739	2021-12-10 18:39:06.37674		5f457f1e1ea90ba9adb32ef5
T0024	Task	16	(U) Utilize data structures to organize, sort, and manipulate elements of information	2021-12-10 18:39:06.377677	2021-12-10 18:39:06.377678		5f457f1e1ea90ba9adb32ef0
T0025	Task	7	(U) Design and develop user interfaces (e.g. web pages, GUIs, CLIs, Console Interfaces)	2021-12-10 18:39:06.378534	2021-12-10 18:39:06.378536		5f457f1e1ea90ba9adb32ef1
T0026	Task	7	(U) Utilize secure coding techniques during development of software and applications	2021-12-10 18:39:06.379506	2021-12-10 18:39:06.379507		5f457f1e1ea90ba9adb32ef3
T0027	Task	17	(U) Employ tradecraft to provide confidentiality, integrity, availability, authentication, and non-repudiation of developed accesses, payloads and tools.	2021-12-10 18:39:06.380743	2021-12-10 18:39:06.380744		5f457f1e1ea90ba9adb32ef4
T0028	Task	17	(U) Apply cryptography primitives to protect the confidentiality and integrity of sensitive data.	2021-12-10 18:39:06.381652	2021-12-10 18:39:06.381653		5f457f1e1ea90ba9adb32ef6
T0029	Task	18	(U) Perform code review and analysis to inform OPSEC analysis and application (attribution, sanitization, etc.)	2021-12-10 18:39:06.382725	2021-12-10 18:39:06.382726		5f457f1e1ea90ba9adb32ef7
T0030	Task	18	(U) Produce artifacts to inform risk analysis, acceptance testing, and legal review.	2021-12-10 18:39:06.383714	2021-12-10 18:39:06.383715		5f457f1e1ea90ba9adb32efa
T0031	Task	19	(U) Locate and utilize technical specifications and industry standards (e.g. Internet Engineering Task Force (IETF), IEEE, IEC, International Standards Organization (ISO)).	2021-12-10 18:39:06.384866	2021-12-10 18:39:06.384867		5f457f1e1ea90ba9adb32ef9
T0032	Task	20	(U) Deliver technical documentation (e.g. user guides, design documentation, test plans, change logs, training materials, etc.) to end users.	2021-12-10 18:39:06.385857	2021-12-10 18:39:06.385858		5f457f1e1ea90ba9adb32efb
T0033	Task	21	(U) Implement project management, software engineering philosophies, modern capability development methodologies (Agile, TDD, CI/CD, etc)	2021-12-10 18:39:06.386814	2021-12-10 18:39:06.386815		5f457f1e1ea90ba9adb32efd
T0034	Task	7	(U) Apply software engineering best practices to enable sustainability and extensibility.	2021-12-10 18:39:06.387621	2021-12-10 18:39:06.387622		5f457f1e1ea90ba9adb32efc
T0035	Task	22	(U) Enter work into Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.)	2021-12-10 18:39:06.388657	2021-12-15 23:33:59.188221		5f457f1e1ea90ba9adb32efe
T0036	Task	7	(U) Enhance capability design strategies and tactics by synthesizing information, processes, and techniques in the areas of malicious software, vulnerabilities, reverse engineering, secure software engineering, and exploitation.	2021-12-10 18:39:06.389472	2021-12-10 18:39:06.389473		5f457f1e1ea90ba9adb32f01
T0037	Task	23	(U) Document and communicate tradecraft, best practices, TTPs, training, briefings, presentations, papers, studies, lessons learned, etc. to both technical and non-technical audiences	2021-12-10 18:39:06.390381	2021-12-10 18:39:06.390381		5f457f1e1ea90ba9adb32eff
T0038	Task	24	Create a masterpiece	2021-12-10 18:39:06.391304	2021-12-10 18:39:06.391305		5f457f1e1ea90ba9adb32f0f
T0039	Task	24	Program in C++	2021-12-10 18:39:06.3919	2021-12-10 18:39:06.391902		5f457f1e1ea90ba9adb32f0d
T0040	Task	24	Create PowerShell tools	2021-12-10 18:39:06.392548	2021-12-10 18:39:06.392548		5f457f1e1ea90ba9adb32f0e
T0041	Task	24	Vulnerability Research to develop valuable tools	2021-12-10 18:39:06.393085	2021-12-10 18:39:06.393086		5f457f1e1ea90ba9adb32f11
T0042	Task	24	Utilize virtualization software to develop, emulate, and execute tools and systems	2021-12-10 18:39:06.393771	2021-12-10 18:39:06.393772		5f457f1e1ea90ba9adb32f10
T0043	Task	25	Conduct an Evaluation.	2021-12-10 18:39:06.394982	2021-12-10 18:39:06.394984	Administer, observe, evaluate, and debrief and examination	5f7f2db02d93ae2b871b7ece
T0044	Task	26	Author a project classification guidance memo during project initiation.	2021-12-10 18:39:06.39654	2021-12-10 18:39:06.396541	cf. DoD Instruction 5200.01	5fcfb0da079dcb183770f8cb
\.

--
-- Sorted dump complete
--
