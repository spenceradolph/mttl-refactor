--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: modules_assessment; Type: TABLE DATA; Schema: public; Owner: mttl_user
--

COPY public.modules_assessment (id, modules_id, description, url) FROM stdin;
1	1	Use the Python interpreter to identify the data type	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2a.html
2	1	Use arithmetic operators to modify Python program functionality	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2b_c.html
3	1	Create a program using strings and string methods	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2d_e.html
4	1	Utilize string methods to manipulate strings	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2d_e.html
5	1	Store user input as a string	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2d_e.html
6	1	Use Python to create, access and manipulate a list	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2f.html
7	1	Use Python to create, access, and manipulate dictionaries in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2h.html
8	1	Store specified data in a tuple.	https://www.geeksforgeeks.org/find-the-size-of-a-tuple-in-python/
9	2	Write programs to move, replace, and swap values in registers using Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_1/Lab1.nasm
10	2	Write programs partially copying data - leveraging and adapting across registers of different sizes.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm
11	2	Identify and access different registers appropriately in Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm
12	3	IDF Agile final assessment	https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Labs/AgileLab1.html
13	4	Implement singly linked lists in Python 	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Singly_Linked_List_Lab.html
14	4	Implement doubly linked lists in Python 	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Linked_List_Perf_Lab.html
15	4	Implement and articulate binary tree traversal in Python	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Structures/Trees_Perf_Labs.html
16	5	Apply knowledge of the stack through commands in Assembly	TBD
17	6	Describe and evaluate the implications of big O notation	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Big_0_Analysis_Perf_Labs.html
18	7	Create, reuse and import modules in Python.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5a.html
19	7	Write object-oriented programs in Python, using classes, class constructors, objects, getters, and setters	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5b.html
20	7	Write a program to demonstrate input validation and exception handling in Python.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5c.html
21	7	Write a program to demonstrate exception handling in Python.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5d.html
22	8	Implement the following example and record observations	https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/virtualenv.html
23	9	Utilize assignment operators as part of a Python code solution.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3a.html
24	9	Control the execution of a program with conditional if, elif, else statements.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3c.html
25	9	Use a while loop to repeat code execution a specified number of times	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3d.html
26	9	Iterate through Python elements with a For loop	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3e.html
27	9	Utilize boolean operators as part of a Python code solution.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3f.html
28	10	Create a structure to pass to an imported CDLL.	TBD
29	10	Import a C library (.DLL/.SO) to utilize a C compiled function.	TBD
30	10	Create a Python script that interacts with system processes.	TBD
31	10	Create a Python script that interacts with operating system functionality.	TBD
32	10	Create a Python script that interacts with system-level information.	TBD
33	10	Given a set of requirements, implement a metaclass.	TBD
34	10	Utilize the Thread library to implement a simple multithreaded program.	TBD
35	10	Use the Python 're' library to match a specified pattern.	TBD
36	10	Design a unit test	TBD
37	10	Implement a unit test.	TBD
38	10	Utilize unit test results to guide program debugging.	TBD
39	11	Submit a merge request providing a brief example of when you might use each of the three listed design patterns. 	TBD
40	12	Demonstrate proficiency with fundamental git commands	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Git/gitLab_issue_creation/instructions.md
41	12	Implement more advanced git commands and functionality.	https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Git/git_command_line/instructions.md
42	13	Draw a flowchart for pseudocode.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/labs/pseudolab1.md
43	13	Draw hierarchy chart to plan the logic for a bank program.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab2.md
44	13	Use pseudocode to control logical flow.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab3.md
45	13	Design pseudocode for a program that accepts and manipulates user input.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab4.md
46	13	Design logic for a program to adhere to the requirements list.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab5.md
47	13	Use pseudocode to alter programs to meet new specifications.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab6.md
48	13	Design functions that will incorporate pass by value and pass by reference.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab7.1.md
49	13	Using pseudocode solve three programming projects.	https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/PseudoLab8.1.md
50	14	Create a 'hello world' program in Assembly, that's writes to a file, and explain how it uses system calls.	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html
51	14	Provide a brief written description (in your own words) explaining real, protected, and system management mode in Assembly.	https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html
52	15	Leverage conditional branching to solve problems in Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_6/Lab6.nasm
53	15	Utilize common string instructions in Assembly.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_7/Lab7.nasm
54	15	In Assembly, understand name mangling and access predefined external utility functions.	https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_9/Lab9.nasm
55	16	Launch Python from the command line	Submit a description of running Python from the command line, indicate whether it is Python 2 or 3 and how you know. 
56	17	Create and implement functions to meet a requirement in Python.	https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/lab4a.html
\.


--
-- Name: modules_assessment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mttl_user
--

SELECT pg_catalog.setval('public.modules_assessment_id_seq', 56, true);


--
-- PostgreSQL database dump complete
--

