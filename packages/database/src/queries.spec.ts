import { MTTL_Type } from '@mttl/common';
import chai, { expect } from 'chai';
import jsonSchemaValidator from 'chai-json-schema';
import { selectKsat } from './queries';

// TODO: create this in the @mttl/common and import it (or at least don't keep in this file) (consider creating this from TS (or vice-versa))
// TODO: more accurate schema (array types aren't finished)
const ksatSchema = {
    type: 'object',
    properties: {
        child_count: {
            type: 'string',
        },
        children: {
            type: 'array',
        },
        comments: {
            type: 'string',
        },
        courses: {
            type: 'string',
        },
        description: {
            type: 'string',
        },
        eval_count: {
            type: 'string',
        },
        eval_links: {
            type: 'array',
        },
        ksat_id: {
            type: 'string',
        },
        ksat_type: {
            type: 'string',
        },
        oam: {
            type: 'array',
        },
        parent_count: {
            type: 'string',
        },
        parents: {
            type: 'array',
        },
        proficiencyData: {
            type: 'array',
        },
        requirement_owner: {
            type: 'array',
        },
        requirement_src: {
            type: 'array',
        },
        topic: {
            type: 'array',
        },
        training_count: {
            type: 'string',
        },
        training_links: {
            type: 'array',
        },
        updated_on: {
            type: 'string',
        },
        work_roles: {
            type: 'array',
        },
    },
    required: [
        'child_count',
        'children',
        'comments',
        'courses',
        'description',
        'eval_count',
        'eval_links',
        'ksat_id',
        'ksat_type',
        'oam',
        'parent_count',
        'parents',
        'proficiency_data',
        'requirement_owner',
        'requirement_src',
        'topic',
        'training_count',
        'training_links',
        'updated_on',
        'work_roles',
    ],
};

// https://github.com/chaijs/chai-json-schema#notes
// TODO: look into updating this package for more performance / newer version
chai.use(jsonSchemaValidator);

describe('selectKsat', function () {
    this.timeout(0); // Disable timeouts during this test

    it('Returns a single ksat object', async function () {
        const ksat: MTTL_Type = await selectKsat('T0001');

        expect(ksat.ksat_id).eq('T0001');
        // expect(ksat).to.be.jsonSchema(ksatSchema);
    });

    it('Returns null if given a bad id', async function () {
        const ksat: MTTL_Type = await selectKsat('Z0000');

        expect(ksat).null;
    });
});
