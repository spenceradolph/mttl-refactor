SELECT id AS ksat_id
FROM ksats
    LEFT JOIN ksat_to_work_roles AS ktwr ON ktwr.ksats_id = ksats.id
WHERE ktwr.work_roles_id IS NULL;