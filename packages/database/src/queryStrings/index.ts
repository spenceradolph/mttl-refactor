import { readFileSync } from 'fs';

export const select_ksat_query = readFileSync(__dirname + '/select_ksat_query.sql').toString();

export const delete_ksat_query = readFileSync(__dirname + '/delete_ksat_query.sql').toString();

export const mttlQuery = readFileSync(__dirname + '/mttlQuery.sql').toString();

export const autocompleteQuery = readFileSync(__dirname + '/autocompleteQuery.sql').toString();

export const moduleViewerQuery = readFileSync(__dirname + '/moduleViewerQuery.sql').toString();

export const getMilestoneKsats = readFileSync(__dirname + '/getMilestoneKsats.sql').toString();

export const getKsatsByWorkRoleQuery = readFileSync(__dirname + '/getKsatsByWorkRoleQuery.sql').toString();

export const getKsatsNoWorkRoleQuery = readFileSync(__dirname + '/getKsatsNoWorkRoleQuery.sql').toString();
