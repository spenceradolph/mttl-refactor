SELECT array_agg(ksats.id) AS ksats_id,
    array_agg(ksats.id) AS parents,
    json_agg(DISTINCT ksats.type) AS ksat_type,
    (
        SELECT array_agg(wr.id)
        FROM work_roles AS wr
        LIMIT 1
    ) AS work_roles,
    (
        SELECT array_agg(
                json_build_object(
                    wr.id,
                    json_build_object(
                        'milestones',
                        (
                            SELECT array_agg(DISTINCT m.milestone)
                            FROM ksat_to_work_roles AS ktwr
                                JOIN milestones AS m ON m.id = ktwr.milestones_id
                            WHERE ktwr.work_roles_id = wr.id
                            LIMIT 1
                        )
                    )
                )
            )
        FROM work_roles AS wr
        LIMIT 1
    ) AS milestones_by_work_roles,
    (
        SELECT array_agg(t.topic)
        FROM topics AS t
        LIMIT 1
    ) AS topic,
    (
        SELECT array_agg(s.source)
        FROM sources AS s
        LIMIT 1
    ) AS requirement_src,
    (
        SELECT array_agg(DISTINCT s.owner)
        FROM sources AS s
        LIMIT 1
    ) AS requirement_owner,
    (
        SELECT array_agg(c.course)
        FROM courses AS c
        LIMIT 1
    ) AS courses,
    (
        SELECT array_agg(oam.milestone)
        FROM milestones AS oam
        LIMIT 1
    ) AS oam
FROM ksats
LIMIT 1;