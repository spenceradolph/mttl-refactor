SELECT m.module_name,
    m.description,
    m.objective,
    m.comments,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ksat_id',
                        mks.ksats_id,
                        'proficiency',
                        mks.proficiencies_id
                    )
                ),
                '{}'
            )
        FROM modules_ksats AS mks
        WHERE mks.ksats_id LIKE 'K%'
            AND m.id = mks.modules_id
        LIMIT 1
    ) AS knowledge,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ksat_id',
                        mks.ksats_id,
                        'proficiency',
                        mks.proficiencies_id
                    )
                ),
                '{}'
            )
        FROM modules_ksats AS mks
        WHERE mks.ksats_id LIKE 'S%'
            AND m.id = mks.modules_id
        LIMIT 1
    ) AS skills,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ksat_id',
                        mks.ksats_id,
                        'proficiency',
                        mks.proficiencies_id
                    )
                ),
                '{}'
            )
        FROM modules_ksats AS mks
        WHERE mks.ksats_id LIKE 'A%'
            AND m.id = mks.modules_id
        LIMIT 1
    ) AS abilities,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ksat_id',
                        mks.ksats_id,
                        'proficiency',
                        mks.proficiencies_id
                    )
                ),
                '{}'
            )
        FROM modules_ksats AS mks
        WHERE mks.ksats_id LIKE 'T%'
            AND m.id = mks.modules_id
        LIMIT 1
    ) AS tasks,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ref',
                        mc.url,
                        'description',
                        mc.description
                    )
                ),
                '{}'
            )
        FROM modules_content AS mc
        WHERE m.id = mc.modules_id
        LIMIT 1
    ) AS content,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ref',
                        mp.url,
                        'description',
                        mp.description
                    )
                ),
                '{}'
            )
        FROM modules_performance AS mp
        WHERE m.id = mp.modules_id
        LIMIT 1
    ) AS performance,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ref',
                        ma.url,
                        'description',
                        ma.description
                    )
                ),
                '{}'
            )
        FROM modules_assessment AS ma
        WHERE m.id = ma.modules_id
        LIMIT 1
    ) AS assessment,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'ref',
                        mr.url,
                        'description',
                        mr.description
                    )
                ),
                '{}'
            )
        FROM modules_resources AS mr
        WHERE m.id = mr.modules_id
        LIMIT 1
    ) AS resources
FROM modules AS m;