SELECT m.milestone AS overarching_milestone,
    ktwr.work_roles_id AS work_role,
    (
        SELECT array_agg(
                json_build_object(
                    'ksat',
                    ktwr2.ksats_id,
                    'proficiency',
                    ktwr2.proficiencies_id
                )
            )
        FROM ksat_to_work_roles AS ktwr2
        WHERE ktwr2.work_roles_id = ktwr.work_roles_id
    ) AS ksats
FROM ksat_to_work_roles AS ktwr
    JOIN milestones AS m ON m.id = ktwr.milestones_id
GROUP BY overarching_milestone,
    work_role;