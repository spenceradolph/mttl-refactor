SELECT json_build_object(
        'ksat_id',
        ksats.id,
        'ksat_type',
        ksats.type,
        'description',
        ksats.description,
        'comments',
        ksats.comments,
        'created_on',
        ksats.created_on,
        'updated_on',
        ksats.updated_on,
        'parents',
        (
            SELECT coalesce(array_agg(p.to_ksats_id), '{}')
            FROM ksat_edges as p
            WHERE p.from_ksats_id = ksats.id
            LIMIT 1
        ), 'parent_count', (
            SELECT count(p.to_ksats_id)
            FROM ksat_edges as p
            WHERE p.from_ksats_id = ksats.id
            LIMIT 1
        ), 'children', (
            SELECT coalesce(array_agg(c.from_ksats_id), '{}')
            FROM ksat_edges as c
            WHERE c.to_ksats_id = ksats.id
            LIMIT 1
        ), 'child_count', (
            SELECT count(c.from_ksats_id)
            FROM ksat_edges as c
            WHERE c.to_ksats_id = ksats.id
            LIMIT 1
        ), 'courses', (
            SELECT coalesce(fin.course, '{}')
            FROM (
                    SELECT array_agg(DISTINCT cs.course) AS course
                    FROM relationship_links AS rls
                        JOIN courses AS cs ON rls.courses_id = cs.id
                        JOIN ksats_to_relationship_links AS ktrls ON rls.id = ktrls.relationship_links_id
                    WHERE ktrls.ksats_id = ksats.id
                    LIMIT 1
                ) AS fin
        ),
        'work_roles',
        (
            SELECT coalesce(
                    array_agg(
                        ktwr.work_roles_id
                        ORDER BY ktwr.work_roles_id ASC
                    ),
                    '{}'
                )
            FROM ksat_to_work_roles as ktwr
            WHERE ktwr.ksats_id = ksats.id
            LIMIT 1
        ), 'proficiency_data', (
            SELECT coalesce(
                    array_agg(
                        json_build_object(
                            'workrole',
                            ktwr.work_roles_id,
                            'proficiency',
                            ktwr.proficiencies_id
                        )
                        ORDER BY ktwr.work_roles_id ASC
                    ),
                    '{}'
                )
            FROM ksat_to_work_roles as ktwr
            WHERE ktwr.ksats_id = ksats.id
            LIMIT 1
        ), 'oam', (
            SELECT coalesce(
                    array_agg(ms.milestone || ' - ' || ktwr.work_roles_id),
                    '{}'
                )
            FROM ksat_to_work_roles as ktwr
                JOIN milestones AS ms ON ktwr.milestones_id = ms.id
            WHERE ktwr.ksats_id = ksats.id
            LIMIT 1
        ), 'topic', (
            SELECT coalesce(array_agg(t.topic), '{}')
            FROM ksats AS r
                JOIN topics AS t ON r.topic_id = t.id
            WHERE r.id = ksats.id
            LIMIT 1
        ), 'training_links', (
            SELECT coalesce(
                    array_agg(
                        json_build_object(
                            'id',
                            ktrls.id,
                            'url',
                            rls.url,
                            'proficiency',
                            ktrls.proficiencies_id,
                            'title',
                            rls.topic,
                            'type',
                            rls.type,
                            'description',
                            rls.description
                        )
                    ),
                    '{}'
                )
            FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
            WHERE ktrls.ksats_id = ksats.id
                AND (
                    rls.map_type = 'training'
                    OR rls.map_type = 'training_supplementary'
                )
            LIMIT 1
        ), 'training_count', (
            SELECT count(ktrls.ksats_id)
            FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
            WHERE ktrls.ksats_id = ksats.id
                AND (
                    rls.map_type = 'training'
                    OR rls.map_type = 'training_supplementary'
                )
            LIMIT 1
        ), 'eval_links', (
            SELECT coalesce(
                    array_agg(
                        json_build_object(
                            'id',
                            ktrls.id,
                            'url',
                            rls.url,
                            'proficiency',
                            ktrls.proficiencies_id,
                            'title',
                            rls.topic,
                            'type',
                            rls.type,
                            'description',
                            rls.description
                        )
                    ),
                    '{}'
                )
            FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
            WHERE ktrls.ksats_id = ksats.id
                AND rls.map_type = 'eval'
            LIMIT 1
        ), 'eval_count', (
            SELECT count(ktrls.ksats_id)
            FROM ksats_to_relationship_links AS ktrls
                JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
            WHERE ktrls.ksats_id = ksats.id
                AND rls.map_type = 'eval'
            LIMIT 1
        ), 'requirement_src', (
            SELECT coalesce(array_agg(s.source), '{}')
            FROM ksat_to_sources AS kts
                JOIN sources AS s ON kts.sources_id = s.id
            WHERE kts.ksats_id = ksats.id
            LIMIT 1
        ), 'requirement_owner', (
            SELECT coalesce(array_agg(s.owner), '{}')
            FROM ksat_to_sources AS kts
                JOIN sources AS s ON kts.sources_id = s.id
            WHERE kts.ksats_id = ksats.id
            LIMIT 1
        )
    )
FROM ksats
WHERE id = $1
LIMIT 1;