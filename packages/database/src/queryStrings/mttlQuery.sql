SELECT ksats.id AS ksat_id,
    ksats.type as ksat_type,
    ksats.description,
    ksats.comments,
    (
        SELECT coalesce(array_agg(p.to_ksats_id), '{}')
        FROM ksat_edges as p
        WHERE p.from_ksats_id = ksats.id
        LIMIT 1
    ) AS parents,
    (
        SELECT count(p.to_ksats_id)
        FROM ksat_edges as p
        WHERE p.from_ksats_id = ksats.id
        LIMIT 1
    ) AS parent_count,
    (
        SELECT coalesce(array_agg(c.from_ksats_id), '{}')
        FROM ksat_edges as c
        WHERE c.to_ksats_id = ksats.id
        LIMIT 1
    ) AS children,
    (
        SELECT count(c.from_ksats_id)
        FROM ksat_edges as c
        WHERE c.to_ksats_id = ksats.id
        LIMIT 1
    ) AS child_count,
    (
        SELECT coalesce(fin.course, '{}')
        FROM (
                SELECT array_agg(DISTINCT cs.course) AS course
                FROM relationship_links AS rls
                    JOIN courses AS cs ON rls.courses_id = cs.id
                    JOIN ksats_to_relationship_links AS ktrls ON rls.id = ktrls.relationship_links_id
                WHERE ktrls.ksats_id = ksats.id
                LIMIT 1
            ) AS fin
    ) AS courses,
    (
        SELECT coalesce(array_agg(ktwr.work_roles_id), '{}')
        FROM ksat_to_work_roles as ktwr
        WHERE ktwr.ksats_id = ksats.id
        LIMIT 1
    ) AS work_roles,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'workrole',
                        ktwr.work_roles_id,
                        'proficiency',
                        ktwr.proficiencies_id
                    )
                ),
                '{}'
            )
        FROM ksat_to_work_roles as ktwr
        WHERE ktwr.ksats_id = ksats.id
        LIMIT 1
    ) AS proficiency_data,
    (
        SELECT coalesce(
                array_agg(ms.milestone || ' - ' || ktwr.work_roles_id),
                '{}'
            )
        FROM ksat_to_work_roles as ktwr
            JOIN milestones AS ms ON ktwr.milestones_id = ms.id
        WHERE ktwr.ksats_id = ksats.id
        LIMIT 1
    ) AS oam,
    (
        SELECT coalesce(array_agg(t.topic), '{}')
        FROM ksats AS r
            JOIN topics AS t ON r.topic_id = t.id
        WHERE r.id = ksats.id
        LIMIT 1
    ) AS topic,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'url',
                        rls.url,
                        'proficiency',
                        ktrls.proficiencies_id,
                        'title',
                        rls.topic,
                        'type',
                        rls.type,
                        'description',
                        rls.description
                    )
                ),
                '{}'
            )
        FROM ksats_to_relationship_links AS ktrls
            JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
        WHERE ktrls.ksats_id = ksats.id
            AND (
                rls.map_type = 'training'
                OR rls.map_type = 'training_supplementary'
            )
        LIMIT 1
    ) AS training_links,
    (
        SELECT count(ktrls.ksats_id)
        FROM ksats_to_relationship_links AS ktrls
            JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
        WHERE ktrls.ksats_id = ksats.id
            AND (
                rls.map_type = 'training'
                OR rls.map_type = 'training_supplementary'
            )
        LIMIT 1
    ) AS training_count,
    (
        SELECT coalesce(
                array_agg(
                    json_build_object(
                        'url',
                        rls.url,
                        'proficiency',
                        ktrls.proficiencies_id,
                        'title',
                        rls.topic,
                        'type',
                        rls.type,
                        'description',
                        rls.description
                    )
                ),
                '{}'
            )
        FROM ksats_to_relationship_links AS ktrls
            JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
        WHERE ktrls.ksats_id = ksats.id
            AND rls.map_type = 'eval'
        LIMIT 1
    ) AS eval_links,
    (
        SELECT count(ktrls.ksats_id)
        FROM ksats_to_relationship_links AS ktrls
            JOIN relationship_links AS rls ON ktrls.relationship_links_id = rls.id
        WHERE ktrls.ksats_id = ksats.id
            AND rls.map_type = 'eval'
        LIMIT 1
    ) AS eval_count,
    (
        SELECT coalesce(array_agg(s.source), '{}')
        FROM ksat_to_sources AS kts
            JOIN sources AS s ON kts.sources_id = s.id
        WHERE kts.ksats_id = ksats.id
        LIMIT 1
    ) AS requirement_src,
    (
        SELECT coalesce(array_agg(s.owner), '{}')
        FROM ksat_to_sources AS kts
            JOIN sources AS s ON kts.sources_id = s.id
        WHERE kts.ksats_id = ksats.id
        LIMIT 1
    ) AS requirement_owner,
    ksats.updated_on
FROM ksats;