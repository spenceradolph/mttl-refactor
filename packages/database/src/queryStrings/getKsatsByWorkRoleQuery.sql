SELECT wr.id AS work_role,
    (
        SELECT array_agg(ktw.ksats_id)
        FROM ksat_to_work_roles AS ktw
        WHERE ktw.ksats_id LIKE 'K%'
            AND wr.id = ktw.work_roles_id
        LIMIT 1
    ) AS knowledge,
    (
        SELECT array_agg(ktw.ksats_id)
        FROM ksat_to_work_roles AS ktw
        WHERE ktw.ksats_id LIKE 'S%'
            AND wr.id = ktw.work_roles_id
        LIMIT 1
    ) AS skills,
    (
        SELECT array_agg(ktw.ksats_id)
        FROM ksat_to_work_roles AS ktw
        WHERE ktw.ksats_id LIKE 'A%'
            AND wr.id = ktw.work_roles_id
        LIMIT 1
    ) AS abilities,
    (
        SELECT array_agg(ktw.ksats_id)
        FROM ksat_to_work_roles AS ktw
        WHERE ktw.ksats_id LIKE 'T%'
            AND wr.id = ktw.work_roles_id
        LIMIT 1
    ) AS tasks
FROM work_roles AS wr;