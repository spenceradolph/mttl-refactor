import { dbPool } from './database';
import { autocompleteQuery, delete_ksat_query, getKsatsByWorkRoleQuery, getKsatsNoWorkRoleQuery, getMilestoneKsats, moduleViewerQuery, mttlQuery, select_ksat_query } from './queryStrings';

// TODO: more error checking and typescript types imported

//
// used in @mttl/utilities
//

// TODO: refactor to take a list of strings, fewer network calls for utility when working with lists
export const selectKsat = async (ksat_id: string) => {
    const results = await dbPool.query(select_ksat_query, [ksat_id]);
    if (results.rowCount !== 1) return null;
    return results.rows[0].json_build_object;
};

export const deleteKsat = async (ksat_id: string) => {
    const results = await dbPool.query(delete_ksat_query, [ksat_id]);
    if (results.rowCount !== 1) return null;
    return results;
};

//TODO: better types here
export const manualQuery = async (queryString: string, inserts?: any[]) => {
    const results = await dbPool.query(queryString, inserts);
    return results;
};

//
// used in web / api endpoints
//

export const getKsats = async () => {
    // Example of how error handling could be implemented in the future.
    // Express would catch the error and pass it to our custom Error Handler
    try {
        const results = await dbPool.query(mttlQuery);
        return results.rows;
    } catch (error) {
        throw new Error('DB_QUERY_FAIL');
    }
};

export const getAutoComplete = async () => {
    const results = await dbPool.query(autocompleteQuery);
    return results.rows[0];
};

export const getMilestones = async () => {
    const results = await dbPool.query(getMilestoneKsats);
    return results.rows;
};

export const getModules = async () => {
    const results = await dbPool.query(moduleViewerQuery);
    return results.rows;
};

export const getKsatsByWorkRole = async () => {
    const results = await dbPool.query(getKsatsByWorkRoleQuery);
    return results.rows;
};

export const getKsatsNoWorkRole = async () => {
    const results = await dbPool.query(getKsatsNoWorkRoleQuery);
    return results.rows;
};
