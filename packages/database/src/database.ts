import { Pool } from 'pg';

// TODO: TEST when these aren't set, or incorrect, gracefully error...etc
const host = process.env.POSTGRES_HOST || 'postgres';
const database = process.env.POSTGRES_DB || 'mttl_db';
const user = process.env.POSTGRES_USER || 'mttl_user';
const password = process.env.POSTGRES_PASSWORD || 'mttl_password';

export const dbPool = new Pool({
    host,
    database,
    user,
    password,
    max: 25,
});
