#!/usr/bin/python3

import os
import sys
import psycopg2
from urllib.parse import urlparse


def connect_to_database(env: str) -> psycopg2.extensions.connection:
    # print("getting connection")
    try:
        pgconn = os.getenv(env)
        result = urlparse(pgconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)
