#!/usr/bin/python3

import argparse
import os
import re
import tempfile
import shutil
from enum import Enum


RE_OBJDESC = re.compile(
    '-- (?P<isdata>(Data for )?)Name: (?P<name>.*?); '
    'Type: (?P<type>.*?); '
    'Schema: (?P<schema>.*?); '
    'Owner: (?P<owner>.*)'
)
RE_SEQSET = re.compile("SELECT pg_catalog.setval\('(?P<name>.*?)'.*")


class State(Enum):
    EMPTY = 1
    SETTINGS = 2
    DEF = 3
    DATA = 4
    COPY = 5
    INSERT = 6
    SEQSET = 7


class Buffer(list):
    destdir = None
    st = State.EMPTY
    fname = None
    title = None

    def __init__(self, destdir):
        self.destdir = destdir

    def flushto(self, st, fname, title):
        # print("EVICTING", self.st, "to", self.fname, "New state:", st)

        # Trim ellipsing comments and empty lines
        while self and ('' == self[0] or self[0].startswith('--')):
            del self[0]
        while self and ('' == self[-1] or self[-1].startswith('--')):
            del self[-1]

        if len(self):
            if self.st in (State.COPY, State.INSERT):
                self[:] = sort_datalines(self)

            self[:] = [
                '--',
                self.title,
                '--',
                '',
            ] + self

            with open(os.path.join(self.destdir, self.fname), "w") as out:
                out.writelines([l + '\n' for l in self])

        self.clear()
        self.st = st
        self.fname = fname
        self.title = title

    def proc_comment(self, line):
        # Returns True if the line is a comment, i.e. it has been processed
        if not line.startswith('--'):
            return False

        m = re.match(RE_OBJDESC, line)
        if not m:
            return True

        if 'SEQUENCE SET' == m.group('type'):
            st = State.SEQSET
        elif m.group('isdata'):
            st = State.DATA
        else:
            st = State.DEF

        fname = '%d:%s:%s:%s:%s' % (
                st.value,
                m.group('type'),
                m.group('schema'),
                m.group('name'),
                m.group('owner')
        )

        if 255 < len(fname):
            fname = fname[:255-3] + "..."

        self.flushto(st, fname, line)

        return True


def sort_datalines(lines):
    pre = []
    data = []
    post = []

    state = 0
    ptr = pre
    isins = False
    for line in lines:
        if 0 == state:
            if line.startswith('COPY'):
                ptr.append(line)
                ptr = data
                state = 1
            elif line.startswith('INSERT'):
                ptr = data
                ptr.append(line)
                isins = True
                state = 1
            else:
                ptr.append(line)
        elif 1 == state:
            if isins and '\n' == line or not isins and '\\.\n' == line:
                ptr = post
                ptr.append(line)
            else:
                ptr.append(line)
        else:
            ptr.append(line)

    return pre + sorted(data) + post


def dissect(dump, destdir):
    buf = Buffer(destdir)

    for line in open(dump):
        # trim trailing newline (if any)
        if '\n' == line[-1]:
            line = line[:-1]

        # print(buf.st.name.ljust(10), "\t[%s]" % line)
        if buf.st == State.EMPTY:
            if buf.proc_comment(line):
                pass
            elif '' == line:
                pass
            else:
                buf.flushto(State.SETTINGS, "%d:%s" % (State.SETTINGS.value, "SETTINGS"),
                            '-- Sorted PostgreSQL database dump')
                buf.append(line)

        elif buf.st in (State.SETTINGS, State.DEF, State.INSERT):
            if buf.proc_comment(line):
                pass
            else:
                buf.append(line)

        elif buf.st == State.DATA:
            if line.startswith('COPY '):
                buf.st = State.COPY
            elif line.startswith('INSERT '):
                buf.st = State.INSERT
            buf.append(line)

        elif buf.st == State.COPY:
            buf.append(line)
            if r'\.' == line:
                buf.flushto(State.EMPTY, None, None)

        elif buf.st == State.SEQSET:
            if buf.proc_comment(line):
                pass
            elif line.startswith('SELECT pg_catalog.setval'):
                m = re.match(RE_SEQSET, line)
                line = "SELECT pg_catalog.setval('%s', 1, false);" % m.group(
                    'name')
                buf.append(line)
            else:
                buf.append(line)

        else:
            print("This should not happen")

    buf.flushto(State.EMPTY, None, None)


def recombine(destdir, dump):
    out = open(dump, 'w')

    first = True
    sorted_files = sorted(os.listdir(destdir))
    for fname in sorted_files:
        if first:
            first = False
        else:
            out.write('\n')
        with open(os.path.join(destdir, fname)) as f:
            out.writelines(f.readlines())

    if sorted_files:
        out.writelines([
            '\n',
            '--\n',
            '-- Sorted dump complete\n',
            '--\n',
        ])

    out.close()


def pgdump_sort(dump, sdump):
    destdir = tempfile.mkdtemp(
        suffix=os.path.basename(dump), prefix='pgdump-sort')

    try:
        dissect(dump, destdir)
        recombine(destdir, sdump)

    finally:
        shutil.rmtree(destdir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="""Sort a pgdump file""",
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('dump', type=str,
                        help='The source file paths')
    parser.add_argument('sorted_dump', type=str,
                        help='The destination file paths')
    parsed_args = parser.parse_args()

    dump = parsed_args.dump
    sdump = parsed_args.sorted_dump
    if sdump is None:
        sdump = re.sub(r'\.sql$', '', dump) + '-sorted.sql'

    pgdump_sort(dump, sdump)
