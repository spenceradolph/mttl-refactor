#!/usr/bin/python3
import os
import subprocess
import argparse
from helpers import connect_to_database
from pgdump_sort import pgdump_sort

default_pgconn = 'postgres://mttl_user:mttl_password@postgres:5432/mttl_db'

pgconn = os.getenv('PGCONN', default_pgconn)
default_dbname = pgconn.split('/')[-1]
os.environ['PGCONN'] = pgconn

sort_table_list = [
    'ksats',
    'ksat_to_work_roles',
    'work_roles'
]


class ExportFromDb():

    def __init__(self, mode='normal', test_param=None) -> None:
        self.client = connect_to_database('PGCONN')
        self.parse_args(mode, test_param)

    def __del__(self):
        self.client.close()

    def parse_args(self, mode, test_param):
        if mode == 'unittest':
            if test_param is None:
                print('Missing test params')
                return

            self.dbname = test_param['database']
            self.sort_tables = test_param['sort']
            self.debug = test_param['debug']
        else:
            parser = argparse.ArgumentParser(
                description=f'Backup data from the database. Make sure you export PGCONN. defaults to {default_pgconn}',
                formatter_class=argparse.RawTextHelpFormatter)
            parser.add_argument('--dbname', type=str, default=default_dbname,
                                help='Name of the database that you want to export from')
            parser.add_argument('--sort', nargs='+', default=sort_table_list, metavar='TABLE_NAME',
                                help=f'Space deliminated list of table names to sort. Defaults to {sort_table_list}')
            parser.add_argument('-d', '--debug', action='store_true',
                                help='show debug output')
            parsed_args = parser.parse_args()
            self.dbname = parsed_args.dbname
            self.sort_tables = parsed_args.sort
            self.debug = parsed_args.debug

        self.export_from_database(mode)

    def export_from_database(self, mode):
        backup_dir = 'backup'
        get_tables = """ SELECT table_name FROM information_schema.tables WHERE table_catalog = %s AND table_schema = 'public' """

        cursor = self.client.cursor()
        cursor.execute(get_tables, (self.dbname,))
        tables = cursor.fetchall()
        table_backup = None

        for table in tables:
            table = table[0]
            backup_file = f'MTTL-{table}.sql'
            backup_path = f'{backup_dir}/{backup_file}'

            cmd = f'pg_dump {pgconn} --data-only --table={table}'
            p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            table_backup = self.debug_print(p.communicate())

            with open(f'{backup_path}', 'wb') as table_backup_file:
                # write original backup file
                table_backup_file.write(table_backup)
            # call pgdump-sort to sort backup file
            if table in self.sort_tables:
                pgdump_sort(f'{backup_path}', f'{backup_path}')

            if mode == 'normal' and not self.debug:
                print(f'Wrote \'{table}\' table to backup file {backup_file}')

        return tables

    def debug_print(self, output):
        stdout = output[0]
        stderr = output[1]
        if self.debug:
            print(stdout.decode())
            print(stderr.decode())
        return stdout


if __name__ == "__main__":
    app = ExportFromDb()
