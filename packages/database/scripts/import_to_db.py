#!/usr/bin/python3
import os
import subprocess
import argparse
from helpers import connect_to_database

default_pgconn = 'postgres://mttl_user:mttl_password@postgres:5432/mttl_db'
default_dfconn = 'postgres://mttl_user:mttl_password@postgres:5432/postgres'

pgconn = os.getenv('PGCONN', default_pgconn)
dfconn = os.getenv('DFCONN', default_dfconn)
default_dbname = pgconn.split('/')[-1]
os.environ['PGCONN'] = pgconn
os.environ['DFCONN'] = dfconn

# List of tables in the DB in the order that they must be imported
# Foreign key constraints requires us to import in this order
table_list = [
    'work_roles',
    'courses',
    'topics',
    'ksats',
    'ksat_edges',
    'sources',
    'ksat_to_sources',
    'milestones',
    'proficiencies',
    'ksat_to_work_roles',
    'relationship_links',
    'ksats_to_relationship_links',
    'modules',
    'modules_assessment',
    'modules_content',
    'modules_ksats',
    'modules_performance',
    'modules_resources',
    'sequences',
    'units',
    'sequence_units',
    'unit_modules'
]


class ImportToDb():

    def __init__(self, mode='normal', test_param=None) -> None:
        self.parse_args(mode, test_param)

    def __del__(self):
        return

    def parse_args(self, mode, test_param):
        if mode == 'unittest':
            if test_param is None:
                print('Missing test params')
                return

            self.dbname = test_param['database']
            self.debug = test_param['debug']
        else:
            parser = argparse.ArgumentParser(
                description="""Get ksat data from the database. Make sure you export PGCONN and DFCONN""",
                formatter_class=argparse.RawTextHelpFormatter)
            parser.add_argument('--dbname', type=str, default=default_dbname,
                                help='Name of the database that you want to export from')
            parser.add_argument('--table-list', nargs='+', default=table_list, metavar='TABLE_NAME',
                                help=f'Space deliminated list of tables to import. Tables will be imported in this order. Defaults to {table_list}')
            parser.add_argument('-d', '--debug', action='store_true',
                                help='show debug output')
            parsed_args = parser.parse_args()
            self.dbname = parsed_args.dbname
            self.debug = parsed_args.debug

        self.import_to_database(mode)

    def import_to_database(self, mode):
        backup_dir = 'backup'

        # drop and create the database
        # with force is necessary because of existing connections to the db
        tmp = f'\\set AUTOCOMMIT on\ndrop database if exists {self.dbname} with (force); create database {self.dbname};'
        p = subprocess.Popen(f'psql {dfconn}'.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.debug_print(p.communicate(input=tmp.encode()))

        # import the schema
        with open(f'{backup_dir}/MTTL-schema.sql', 'rb') as schema_file:
            sp = subprocess.Popen(f'psql -q {pgconn}'.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            self.debug_print(sp.communicate(input=schema_file.read()))

        # import each table into the database
        for table in table_list:
            with open(f'{backup_dir}/MTTL-{table}.sql', 'rb') as table_file:
                sp = subprocess.Popen(f'psql {pgconn}'.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                self.debug_print(sp.communicate(input=table_file.read()), True)
            if mode == 'normal' and not self.debug:
                print(f'Imported \'MTTL-{table}.sql\' into {self.dbname} database')

    def debug_print(self, output, rep=False):
        stdout = output[0]
        stderr = output[1]
        if self.debug:
            if rep:
                print(stdout.decode().replace('SET\n', ''))
            else:
                print(stdout.decode())
            print(stderr.decode())
        return stdout


if __name__ == "__main__":
    app = ImportToDb()
