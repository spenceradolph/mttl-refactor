# MTTL (React Version)

This version is setup as a monorepo with [Yarn Workspaces](https://classic.yarnpkg.com/blog/2017/08/02/introducing-workspaces/). The following 'packages' are used in conjunction to build, run, and test the mttl web-application. Typescript has been used for almost all functionality, and Docker is setup for ease of development and deployment.

-   web
    -   [Create-React-App](https://reactjs.org/docs/create-a-new-react-app.html) frontend framework
-   server
    -   [ExpressJS](https://expressjs.com/) handling of web requests / routing
    -   [Passport](https://www.passportjs.org/) framework for authentication
-   database
    -   Exposes functions for server & utilities to make queries
-   common
    -   Typescript types for data used between multiple packages
-   docs
    -   [mdBook](https://rust-lang.github.io/mdBook/) for generating documentation
-   tests
    -   [Cypress](https://www.cypress.io/) end-to-end testing
-   utilities
    -   Helpful dev scripts for DB Interaction (add, modify, import, export)

# Local Dev Setup

## Requirements

-   Docker (Docker Desktop)
-   VSCode with Remote Containers Extension

Previous setup steps can be found [here](https://90cos-mttl.90cos.com/documentation/dev/set-up-env.html) detailing installation of docker & VSCode.

## Steps

1. Clone this repo and launch VSCode

```sh
git clone https://gitlab.com/90cos/mttl
code ./mttl
```

2. Make a copy of `.env.example`, name it `.env` and configure as needed.

The `.env.example` explains how to change each variable for local development. To enable authenticating against a personal gitlab application, please read [this guide](https://docs.gitlab.com/ee/integration/oauth_provider.html#user-owned-applications) and create an application in your profile. To enable the server to create issues from user submitted forms on the frontend, please read [this guide](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) and use a personal token.

3. Open the project in a remote container.

A prompt may have already appeared to open the project in a development container. You can also do this action via VSCode command palette or by clicking in the bottom left blue square.

4. (optional) Select the project.code-workspace file and click "Open Workspace".

To further simplify the development environment, [VSCode Workspaces](https://code.visualstudio.com/docs/editor/workspaces) puts the editor into the packages directory to help with working in our mono-repo.

# Running

From VSCode file explorer, right click the 'OPEN EDITORS' tab to ensure 'NPM SCRIPTS' are enabled. Once enabled, they show all scripts from package's package.json files in a side-panel. Package.json scripts should generally be used to get the project running.

The root package.json also contains useful scripts for building and running the production code, but is not necessary for development (and is hidden when within VSCode Workspace)

## Typical Setup

Initialize the DB with `restore_db` from the database package.

Start the backend server with `dev` from the server package. This will auto-restart when changing server files.

Start the react frontend server with `dev` from the web package. This will auto-open a browser tab on localhost:3000 and auto-refresh when changing web files.

It's also helpful to monitor both at the same time by putting them [side by side](https://code.visualstudio.com/docs/editor/integrated-terminal#_managing-terminals).

## Known Issues

-   Starting a fresh project may change the line endings based on your editor? (LF vs CRLF) which may appear to show lots of untracked git changes. Safe to discard all changes.
-   If you have a previous build artifacts (ex: ./packages/database/dist), typescript won't recompile them when running code that imports that package.
    -   Therefore make sure to delete those artifacts before developing in that specific package to ensure compilation on every run.

# Building & Running Production

Each package that's used in the final production gets compiled and combined in a root ./dist directory via the root package.json `build` script[\*](https://medium.com/@qjli/simple-intro-to-pre-post-hooks-for-npm-scripts-d6508eeedeb0). The development container should already have a copy in the image. The root `start` script then runs the compiled code.

Additionally, the root `docker-compose.yml` will use the [multi-stage](https://docs.docker.com/develop/develop-images/multistage-build/) Dockerfile to completely build and run the production image.

The database still needs to be manually populated, but if you've already done so in development, the postgres image should use the same docker volume for the data, and therefore already be setup.

If starting fresh, however, you will need to manually import the DB schema and data as this is currently expected to happen in the build pipeline.

# Adding Dependencies

With yarn workspaces, adding nodejs dependencies is done in a similar way to without workspaces.

```
yarn workspace <workspace_name> [add/remove] [-D] package
```

Workspace names are found in each package's package.json name, and should begin with @mttl.

Example:

```
root@host:/mttl#  yarn workspace @mttl/server add express
```

DevDependencies are separated with -D because they are only required to build the application, while normal Dependencies are also required while the application is running.
