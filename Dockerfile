# This final stage of this Dockerfile is setup to result with a minimal production server running on port 5000
# For complete deployment, you must also account for ENV variables and Postgres DB (see docker-compose.yml)

# Stage 1 -> Pipeline
# ALL dependencies to develop, test, compile, and run the server as a developer or CI/CD pipeline
# Because these rarely change, it may be advantageous to pre-build or cache this stage for CI/CD
# TODO: prune unused software
#######################################################
FROM node:17-bullseye AS pipeline
WORKDIR /mttl
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN cargo install mdbook
RUN cargo install mdbook-mermaid
RUN apt-get clean && apt-get -y update \
    && apt-get -y install --no-install-recommends \
    && apt-get -y install sed perl curl git wget \
    && apt-get -y install python3-dev python3-pip \ 
    && apt-get -y install libpq-dev cron \
    && apt-get -y install lsb-release
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' \
    && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && apt-get clean && apt-get update -y \
    && apt-get -y install postgresql-client-14
RUN apt-get install python -y
RUN yarn global add serve ts-node
RUN apt-get install libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb -y
RUN curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash && \
    echo "if [ -f ~/.git-completion.bash ]; then . ~/.git-completion.bash; fi" >> ~/.bashrc
#######################################################


# Stage 2 -> Installer
# This stage is targetted by the .devcontainer
# Installs all required dependencies for ALL packages
# TODO: optimize COPY, only need to copy in package.json files and yarn.lock
#######################################################
FROM pipeline as installer
COPY . .
# COPY yarn.lock package.json packages/common/package.json packages/database/package.json packages/docs/package.json packages/server/package.json packages/tests/package.json packages/utilities/package.json packages/web/package.json ./
RUN pip3 install -r ./packages/utilities/old/requirements.txt
RUN yarn install
#######################################################


# Stage 3 -> Builder
# Compiles all required files in each package and combines into ./dist (see package.json)
# TODO: optimize COPY, only need to copy in packages used during production
#######################################################
FROM installer as builder
COPY . .
RUN yarn build
#######################################################


# Stage 4 -> Production
# Doesn't contain any unused software or dependencies, only what's need to run the compiled server
#######################################################
FROM node:17-alpine
WORKDIR /mttl

# Only install packages used, without devDependencies
COPY ./package.json ./yarn.lock ./
COPY ./packages/server/package.json ./packages/server/package.json
COPY ./packages/database/package.json ./packages/database/package.json
ENV NODE_ENV=production
RUN yarn install

# Copy required compiled files from builder
COPY --from=builder /mttl/dist ./dist
COPY --from=builder /mttl/packages/database/dist ./packages/database/dist

# Start the server
ENV PORT=5000
EXPOSE 5000
USER node
ENTRYPOINT ["yarn", "start"]
#######################################################
